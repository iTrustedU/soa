(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"exchange draggable\">\n    <div class=\"dialog-name\">\n        <span id=\"caravan-name\">E X C H A N G E</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"exchange-content\">\n        <div class=\"exchange-item-1\">\n            <div class=\"exchange-item\">\n                <img src={{sideA.picture}}>\n                <span id=\"sidea-box\" class=\"goods-amount\">{{sideA.minAmount}}</span>\n            </div>\n        </div>\n        <div class=\"exchange-item-2\">\n            <div class=\"exchange-item\">\n                <img src={{sideB.picture}}>\n                <span id=\"sideb-box\" class=\"goods-amount\">{{sideB.amount}}</span>\n            </div>\n        </div>\n        <div class=\"exchange-bar\">\n            <div class=\"exchange-bar-inner-box\">\n                <div class=\"inner-slider ui-slider-handle\"></div>\n            </div>\n        </div>\n        <span class=\"exchange-min\">Min</span>\n        <span class=\"exchange-max\">Max</span>\n        <div class=\"confirm-button\">Confirm</div>\n    </div>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var ExchangeDialog;
    ExchangeDialog = (function(_super) {
      __extends(ExchangeDialog, _super);

      function ExchangeDialog() {
        ExchangeDialog.__super__.constructor.call(this, html);
      }

      return ExchangeDialog;

    })(BaseDialog);
    ExchangeDialog.prototype.onPrologue = function() {
      return this.compileParent(html, {
        sideA: {},
        sideB: {}
      });
    };
    ExchangeDialog.prototype.open = function(sideA, sideB, done) {
      var confirmBtn, exchangeBar, maxToGive, sideaBox, sidebBox, transferedAmount,
        _this = this;
      this.compileHtml({
        sideA: sideA,
        sideB: sideB
      });
      confirmBtn = this.html.find(".confirm-button");
      confirmBtn.unbind("click");
      transferedAmount = sideA.minAmount;
      maxToGive = sideA.minAmount + sideA.amount;
      maxToGive = Math.min(maxToGive, sideB.maxAmount || Number.MAX_VALUE);
      confirmBtn.bind("click", function() {
        done(transferedAmount);
        return _this.hide();
      });
      sideaBox = this.html.find("#sidea-box");
      sidebBox = this.html.find("#sideb-box");
      sideaBox.text(maxToGive);
      this.html.find(".close-button").on("click", function() {
        return _this.hide();
      });
      exchangeBar = this.html.find(".exchange-bar");
      exchangeBar.slider({
        min: sideA.minAmount,
        max: maxToGive,
        orientation: "horizontal",
        slide: function(ev, ui) {
          sideaBox.text(maxToGive - ui.value);
          sidebBox.text(ui.value);
          return transferedAmount = +ui.value;
        }
      });
      this.html.show();
      return this.draggable();
    };
    return ExchangeDialog;
  });

}).call(this);
