"use strict"

define ["PIXI", "TileLayer", "AnimatedLayer", "MapConfig"], 

(PIXI, TileLayer, AnimatedLayer, MapConfig) ->
	_id_ = 0
	class Tile
		constructor: (@row, @col) ->
			@init()
			@layers = {}

	Tile::init = () ->
		@position = HAL.MathUtils.Vec2.from(0, 0)
		@id = _id_++
		
	Tile::addLayer = (layer_container, meta) ->
		spr_layer = new TileLayer(@, meta)
		@layers[meta.layer] = spr_layer
		layer_container.addChild(spr_layer)
		return spr_layer

	Tile::removeLayer = (layerZ) ->
		layer = @layers[layerZ]
		if not layer?
			throw new Error("No layer at #{layerZ}")
		parent = layer.parent
		parent.removeChild(layer)
		delete @layers[layerZ]
		return layer.id

	Tile::addAnimatedLayer = (layer_container, meta) ->
		if not meta?
			throw new Error("AnimatedLayer meta not provided")
		anim_layer = new AnimatedLayer(@, meta)
		@layers[meta.layer] = anim_layer
		layer_container.addChild(anim_layer)
		return anim_layer
	
	Tile::copyLayer = (layer, force) ->
		layer.changeParentTile(@)
		@layers[layer.z] = layer
		parent = layer.parent 
		parent.removeChild(layer)
		parent.addChild(layer)
		return layer

	Tile::destroy = () ->
		HAL.MathUtils.Vec2.release(@position)

	Tile::toString = () ->
		return "row: #{@row}, col: #{@col}"

	return Tile

