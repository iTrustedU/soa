(function() {
  "use strict";
  var $bldslist, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"bottom\">\n<div class=\"left-section\">\n    <div class=\"map-box\">\n    </div>\n    <div class=\"center-map-button\"></div>\n</div>\n<div class=\"right-section\">\n    <img src=\"img/by-gold/premium-dinar.png\" />\n</div>\n<div class=\"middle-section\">\n    <div class=\"bot-bar\">   \n        <div class=\"bot-bar-deco-middle\">\n            <div class=\"bot-bar-deco-name\">\n                <div class=\"friends-tab\" id=\"friends-tab\"></div>\n                <div class=\"alliance-tab\" id=\"aliance-tab\"></div>\n                <div class=\"bot-bar-deco-name-left\"></div>\n                <span class=\"bot-bar-deco-name-middle\"></span>\n                <div class=\"bot-bar-deco-name-right\"></div>\n                <div class=\"inventory-tab\"></div>\n                <div class=\"build-tab\" id=\"build-tab\"></div>\n            </div>\n        </div>\n    </div>\n    <div class=\"friends frame-bg\">\n        <img src=\"img/bottom-middle/friends-frame.png\" class=\"img-frame\" />\n    </div>\n    <div class=\"buildings frame-bg\" id=\"buildings-list\">\n    </div>\n    <div class=\"slider-bar\">\n        <div class=\"slider-bar-l\"></div>\n        <div class=\"slider-bar-r\"></div>\n        <div class=\"slider-bar-middle\">\n            <div class=\"slider\"></div>\n        </div>\n    </div>\n</div>\n</div>";

  $bldslist = "<img id=\"{{type}}\" src=\"{{concat_construction_icon type disabled}}\" class=\"img-frame bottom-menu-building\">";

  define(["jquery-ui", "jquery-perfect-scrollbar", "soa/hud/dialogs/BaseDialog"], function($, _, BaseDialog) {
    var BottomMenu;
    BottomMenu = (function(_super) {
      __extends(BottomMenu, _super);

      function BottomMenu() {
        BottomMenu.__super__.constructor.call(this, html);
      }

      return BottomMenu;

    })(BaseDialog);
    BottomMenu.prototype.init = function() {
      var _this = this;
      this.BuildTab = this.html.find("#build-tab");
      this.FriendsTab = this.html.find("#friends-tab");
      this.BuildingsList = this.html.find("#buildings-list");
      this.CityZoneViewButton = this.html.find(".map-box");
      this.BuildTab.click(function() {
        return _this.listBuildings();
      });
      this.FriendsTab.click(function() {
        _this.html.find(".buildings").hide();
        return _this.html.find(".friends").show();
      });
      return this.CityZoneViewButton.click(function() {
        return SOA.toggleViews();
      });
    };
    BottomMenu.prototype.listBuildings = function() {
      var blds, construction, k, spr_uri, template, _results;
      this.html.find(".friends").hide();
      this.html.find(".buildings").show();
      amjad.empire.constructions.findAvailableConstructions();
      blds = amjad.empire.constructions.available_constructions;
      this.BuildingsList.empty();
      template = Handlebars.compile($bldslist);
      _results = [];
      for (k in blds) {
        construction = blds[k];
        html = $(template({
          type: k,
          disabled: construction.type === "disabled"
        }));
        html.data("construction", construction);
        html.unbind("click");
        if (construction.type !== "disabled") {
          spr_uri = "" + construction.sprite + ".png";
          (function(spr_uri) {
            var grab_left, grab_top, spr;
            spr = PIXI.TextureCache[spr_uri];
            grab_top = spr != null ? spr.frame.height : 0;
            grab_left = spr != null ? spr.frame.width * 0.5 : 0;
            return html.draggable({
              revert: "invalid",
              revertDuration: 0,
              helper: "clone",
              appendTo: "body",
              containment: "window",
              cursorAt: {
                top: grab_top,
                left: grab_left
              },
              drag: function(ev, ui) {
                var pos;
                return pos = [ev.pageX, ev.pageY];
              },
              start: function(ev, ui) {
                ui.helper.css("width", "auto");
                ui.helper.css("height", "auto");
                ui.helper.css("opacity", 0.5);
                return ui.helper.attr("src", spr_uri);
              }
            });
          })(spr_uri);
        }
        _results.push(this.BuildingsList.append(html));
      }
      return _results;
    };
    BottomMenu.prototype.onPrologue = function(hud) {
      $(hud).droppable({
        accept: ".bottom-menu-building",
        drop: function(ev, ui) {
          var constr;
          if (SOA.isInCityView()) {
            constr = ui.draggable.data("construction");
            return amjad.empire.constructions.construct(constr);
          } else {
            return console.error("You can't construct building while in zone view");
          }
        }
      });
      return this.html.find(".center-map-button").click(function() {
        if (SOA.isInZoneView()) {
          return SOA.centerOnZone();
        }
      });
    };
    return BottomMenu;
  });

}).call(this);
