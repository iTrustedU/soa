(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"fountainG-container\">\n	<div id=\"fountainG\">\n		<div id=\"fountainG_1\" class=\"fountainG\">\n		</div>\n		<div id=\"fountainG_2\" class=\"fountainG\">\n		</div>\n		<div id=\"fountainG_3\" class=\"fountainG\">\n		</div>\n		<div id=\"fountainG_4\" class=\"fountainG\">\n		</div>\n		<div id=\"fountainG_5\" class=\"fountainG\">\n		</div>\n		<div id=\"fountainG_6\" class=\"fountainG\">\n		</div>\n		<div id=\"fountainG_7\" class=\"fountainG\">\n		</div>\n		<div id=\"fountainG_8\" class=\"fountainG\">\n		</div>\n	</div>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var SpinnerOverlay;
    SpinnerOverlay = (function(_super) {
      __extends(SpinnerOverlay, _super);

      function SpinnerOverlay() {
        SpinnerOverlay.__super__.constructor.call(this, html);
      }

      return SpinnerOverlay;

    })(BaseDialog);
    SpinnerOverlay.prototype.start = function(dialog, deferred) {
      var _this = this;
      this.html.hide(0);
      this.html.prependTo(dialog);
      this.html.show(200);
      deferred.done(function(msg) {
        return _this.finish(msg);
      }).fail(function(msg) {
        return _this.error(msg);
      });
      return deferred.promise();
    };
    SpinnerOverlay.prototype.finish = function() {
      return this.html.fadeOut(1500);
    };
    SpinnerOverlay.prototype.error = function(msg) {
      return this.html.fadeOut(1500);
    };
    return SpinnerOverlay;
  });

}).call(this);
