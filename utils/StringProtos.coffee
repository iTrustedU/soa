module.exports = () ->
    String.prototype.endsWith = (str) ->
        return @lastIndexOf(str) + str.length is @length

    String.prototype.format = String.prototype.f = () ->
        if arguments[0] instanceof Array
            args = arguments[0]
        else
            args = arguments
        i = args.length
        s = @
        while (i--)
            s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), args[i])
        return s