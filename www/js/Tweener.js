(function() {
  "use strict";
  define([], function() {
    var Tweener, Tweeners;
    Tweeners = {
      Linear: function(t, start, rate, duration) {
        return rate * t / duration + start;
      },
      EaseInQuad: function(t, start, rate, duration) {
        t /= duration;
        return rate * t * t + start;
      },
      EaseInCubic: function(t, start, rate, duration) {
        t /= duration;
        return rate * t * t * t + start;
      }
    };
    Tweener = (function() {
      function Tweener() {
        this.t = performance.now();
        this.delta = 0;
        this.old_t = 0;
        this.num_tweens = 0;
        this.cur_time = 0;
        this.to_wait = 0;
        this.animating = false;
        this.paused = false;
        this.done_clb = null;
        this.frameID = null;
        this.clb_ids = [];
        this.tweenQueue = [];
        this.tween_chain = [];
        return this;
      }

      Tweener.prototype.tween = function(meta, callb) {
        var finnclb,
          _this = this;
        this.num_tweens++;
        meta.callb = callb;
        if (this.to_wait > 0) {
          this.tween_chain.push(meta);
        }
        this.animating = true;
        finnclb = function() {
          var ind;
          ind = _this.clb_ids.indexOf(finnclb);
          _this.clb_ids.splice(ind, 1);
          _this.num_tweens--;
          if (_this.to_wait > 0 && _this.num_tweens !== 0 && !_this.paused) {
            _this.to_wait--;
            _this.tween(_this.tween_chain.pop());
            _this.num_tweens--;
          }
          if (_this.num_tweens <= 0 && (_this.done_clb != null) && !_this.paused) {
            _this.done_clb.call(_this.obj);
          }
          if (_this.num_tweens <= 0 && _this.to_wait === 0) {
            return _this.animating = false;
          }
        };
        this.clb_ids.push(finnclb);
        this.update(meta, finnclb);
        return this;
      };

      Tweener.prototype.update = function(meta, finnclb) {
        meta.finnclb = finnclb;
        if (meta.method == null) {
          meta.method = Tweeners.Linear;
        }
        meta.val = meta.from;
        this.tweenQueue.push(meta);
        this.t = performance.now();
        return this.update_();
      };

      Tweener.prototype.update_ = function() {
        var ind, up, _i, _len, _ref,
          _this = this;
        if (this.paused) {
          return;
        }
        if (this.tweenQueue.length === 0) {
          cancelAnimationFrame(this.frameID);
          this.cur_time = 0;
          return;
        }
        this.old_t = this.t;
        this.t = performance.now();
        this.delta = (this.t - this.old_t) * 0.001;
        this.cur_time += this.delta;
        _ref = this.tweenQueue;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          up = _ref[_i];
          up.val = up.method(this.t, up.val, this.delta, up.duration);
          if (up.val >= up.to) {
            up.repeat--;
            up.callb(up.to);
            if (up.repeat <= 0) {
              ind = this.tweenQueue.indexOf(up);
              this.tweenQueue.splice(ind, 1);
              this.cur_time = 0;
              up.finnclb();
              return;
            } else {
              up.val = up.from;
            }
          } else {
            up.callb(up.val);
          }
        }
        this.frameID = requestAnimFrame(function() {
          return _this.update_();
        });
        return this;
      };

      Tweener.prototype.wait = function(wait_clb, msecs) {
        this.to_wait++;
        return this;
      };

      Tweener.prototype.pause = function() {
        this.paused = true;
        return this;
      };

      Tweener.prototype.resume = function() {
        this.paused = false;
        this.t = performance.now();
        return this;
      };

      Tweener.prototype.stop = function() {
        this.tweenQueue = [];
        this.clb_ids = [];
        this.num_tweens = 0;
        this.to_wait = 0;
        this.animating = false;
        this.wait_clb = null;
        if (this.done_clb != null) {
          this.done_clb(this);
        }
        this.done_clb = null;
        this.tween_chain = [];
        this.cur_time = 0;
        return this;
      };

      Tweener.prototype.done = function(done_clb) {
        this.done_clb = done_clb;
        return this;
      };

      return Tweener;

    })();
    Tweener.Tweeners = Tweeners;
    return Tweener;
  });

}).call(this);
