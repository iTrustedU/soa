(function() {
  define(["jquery"], function($) {
    "use strict";
    var LoadPartials, PARTIALS_LIST;
    PARTIALS_LIST = "hbpartials/partials.list";
    LoadPartials = (function() {
      function LoadPartials() {
        this.loaded = false;
        this.list = [];
        this.load();
      }

      return LoadPartials;

    })();
    return LoadPartials.prototype.load = function() {
      var $deferred,
        _this = this;
      $deferred = new $.Deferred();
      if (this.loaded) {
        $deferred.resolve(this.list);
      } else {
        $.get(PARTIALS_LIST).done(function(data) {
          _this.list = data.split("\n");
          _this.loaded = true;
          return $deferred.resolve();
        });
      }
      return $deferred.promise();
    };
  });

}).call(this);
