"use strict"

define ["soa/StructureLoader"],

(StructureLoader) ->

	class Construction extends StructureLoader
		constructor: (meta) ->
			@loaded                         = false
			@layer                          = null
			@production 					= []
			@CONSTRUCTION_LEVELING_DIVISOR  = 1/5
			super(meta)

	Construction::epilogue = () ->
		if @action_type is "build"
			if @duration > 0
				if @poi_id?
					@addOnEncampQueue()
				else
					@addOnBuildQueue()
			else
				alert("ovo ne bi trebalo nikada da se desi")
				@finishBuild()
				.done (data) =>
					@load(data)
					amjad.trigger "CONSTRUCTIONS_UPDATED"
				.fail (err) ->
					console.error err
		return

	Construction::initListeners = () ->
		super()
		@on "DONE_LOADING", () ->
			console.debug "Loading new construction #{@c_type}"
			@loaded = true
			# if @duration > 0
			# 	type = "Constructing"
			# 	if @action_type is "produce"
			# 		debugger
			# 	else
			# 		SOA.Hud.getDialog("BUILD_ORDERS").addConstructionToBuildQueue(@, "Constructing")

	Construction::upgrade = () ->
		next_level = ((@level+1) * @CONSTRUCTION_LEVELING_DIVISOR)
		console.debug next_level
		okay = true
		if next_level % 1 is 0 # je li ceo broj
			if not @canBeUpgraded(next_level + 1)
				alert("You'll have to relocate the building")
				okay = false
		if okay	
			Rest.post("construction.upgrade_building", {
				cid: @construction_id
				ty: @c_type
				lvl: @level + 1
			})
			.done (data) =>
				@duration = data.duration
				@addOnUpgradeQueue()
			.fail (data) =>
				console.error data

	Construction::changeLevel = () ->
		@level += 1
		normalized = Math.floor(@CONSTRUCTION_LEVELING_DIVISOR*@level)+1
		console.debug "NORMALIZED LEVEL #{normalized}"
		if @canBeUpgraded(normalized)
			tex_name = "assets/sprites/#{amjad.user.CONSTRUCTION_SPRITES_URI}#{@getLevelNormalizedName()}.png"
			@layer.changeTexture(tex_name)
		
	Construction::getNormalizedLevel = () ->
			return Math.floor(@CONSTRUCTION_LEVELING_DIVISOR*@level)+1

	Construction::getLevelNormalizedName = () ->
			return "#{@c_type}#{@getNormalizedLevel()}"

	Construction::getSpan = (to_level = @getNormalizedLevel()) ->
			lvl_meta_name = amjad.splaySpriteURI("#{amjad.user.CONSTRUCTION_SPRITES_URI}#{@c_type}#{to_level}")
			leveled_meta = HAL.TileMetaStorage.findByName(lvl_meta_name)
			return null if not leveled_meta?
			span_size = leveled_meta.size
			span = SOA.City.map.getSpanArea(@layer.parentTile, span_size)
			return span

	Construction::getSpanFromTile = (tile, to_level = @getNormalizedLevel()) ->
			lvl_meta_name = amjad.splaySpriteURI("#{amjad.user.CONSTRUCTION_SPRITES_URI}#{@c_type}#{to_level}")
			leveled_meta = HAL.TileMetaStorage.findByName(lvl_meta_name)
			return null if not leveled_meta?
			span_size = leveled_meta.size
			span = SOA.City.map.getSpanArea(tile, span_size)
			return span

	Construction::drawSpan = (level, color = 0x0292CD) ->
			span = @getSpan(level)
			SOA.City.map.tintSpan(span, 2, color)

	Construction::canBeUpgraded = (to_level) ->
			###
				@todo treba pronaci span za sledeci nivo
				fillovati matricu za sve gradjevionCollisionne 
				zatim proveriti ima li preseka u bitovima
				za gradjevine posmatramo samo 1 i 0 
			###
			span = @getSpan(to_level)
			collisions = amjad.empire.constructions.testSpanArea(span, @construction_id)
			if collisions.length > 0
				@onCollision collisions, () ->
					TweenMax.fromTo(@layer, 1.1, {alpha: 0.1, repeat: 5}, {alpha: 1, repeat: 5})
				return false
			return true

	Construction::addToMap = () ->
		@addToCityMap()

	Construction::finishBuild = () ->
			@layer.alpha = 1
			return Rest.post("construction.finish_build", {cid: @construction_id, spec: "spec1"})

	Construction::assignLayer = (layer) ->
			if not layer?
				throw new Error("Layer couldn't be assigned to construction #{@c_type} at #{@x}, #{@y}")
			@layer = layer
			@layer.onLeftClick = () =>
				SOA.Hud.showDialog(@c_type.toUpperCase(), @)
			#if not @poi_id?
			@level = 1 if @level is 0

			if @action_type is "build"
				@layer.alpha = 0.5

	Construction::addToCityMap = () ->
		console.debug("Added to city map", @)
		layer = amjad.putConstructionInCity(@x, @y, @getLevelNormalizedName())
		@assignLayer(layer)
		if @duration > 0
			@addOnBuildQueue()

	Construction::addOnBuildQueue = () ->
		SOA.Hud.getDialog("BUILD_ORDERS")
		.addConstructionToBuildQueue(@, "Constructing")
		.done () =>
			@finishBuild()
			.done (data) =>
				@load(data)

	Construction::addOnUpgradeQueue = () ->
		SOA.Hud.getDialog("BUILD_ORDERS")
		.addConstructionToUpgradeQueue(@, "Upgrading to level #{@level+1}")
		.done () =>
			@changeLevel()
			@trigger "UPGRADED", @level

	Construction::addOnEncampQueue = () ->
		SOA.Hud.getDialog("BUILD_ORDERS")
		.addPOIToEncampmentQueue(@, "Encamping #{Handlebars.helpers.lang @c_type}")

	Construction::onCollision = (collisions, effect) ->
		all_cns = []
		for collided in collisions
			cnid = SOA.Constructions.getSpanMark(collided.row, collided.col)
			cn = amjad.empire.constructions.getByID(cnid)
			if all_cns.indexOf(cn) is -1
				all_cns.push(cn)
		for cn in all_cns
			effect.call(cn)
	
	return Construction





		# alert(collisionReport(collisions))
		# addToCityMap: () ->
		#     #debugger
		#     console.debug("Added to city map", @)
		#     layer = amjad.putConstructionInCity(@x, @y, @getLevelNormalizedName())
		#     @assignLayer(layer)
		#     ID_MAP[layer.id] = @
		#     if @duration > 0
		#         @enqueBuildRefresh()
		#         return

		# addToZoneMap: () ->
		#     layer = SOA.Zone.map.addLayerSpriteToTile(@x, @y, 5, "assets/sprites/amjad/poi/encampment_established")
		#     @assignLayer(layer)
		#     ID_MAP[layer.id] = @
		#     if @duration > 0
		#         @enqueBuildRefresh()
		#         return


		# HAL.on "CITY_MAP_LOADED", () =>
		#     if not @poi_id?
		#         @addToCityMap()
		#     else
		#         @addToZoneMap()
		#     if @c_type is "training_grounds"
		#         Constructions.loadTrainingGrounds(@)
		#     else if @c_type is "trade_station"
		#         Constructions.loadTradeStation(@)
		#     else if @c_type is "war"
		#         Constructions.loadWar(@)
		#     else if @c_type is "science"
		#         Constructions.loadScience(@)
		#         Constructions.loadTech(@)


		#debugger
		# buildEnds: () ->
		#     if @build_start? and @build_start.length > 0
		#         a = new Date(@build_start)
		#         b = new Date(@build_start)
		#         a.setMilliseconds(a.getMilliseconds() + @buildTime())
		#         c = (b - a)
		#         return [a.toLocaleString(), c]
		#     else
		#         return [0, 0]

		# buildTime: () ->
		#     if not @poi_id?
		#         return 0.3*60*1000
		#         # return +(XMLStore.get("constructions").city[@c_type]["level#{@level+1}"].time*60*1000)
		#     else
		#         return 0.3*60*1000 #+(XMLStore.get("constructions").zone[@c_type].time*60*1000)

		# timeLeft: () ->
		#     a = new Date(@build_start)
		#     b = new Date()
		#     return @buildTime() - (b - a)

		# timeLeftPercentage: () ->
		#     return (1 - (@timeLeft() / @buildTime())).toFixed(2)
