(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"city-info\">\n    <div class=\"dialog-name\">\n        <span id=\"caravan-name\">C I T Y &nbsp; I N F O</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"city-info-content\">\n    	<div class=\"city-info-box\">\n    		<img src=\"assets/sprites/amjad/dialogs/constructions/civil/war.png\" />\n    		<span>\n    			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do \n    			eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim\n    		</span>\n    	</div>\n        <div class=\"city-info-box-2\">\n            <div class=\"text-info\">\n                <span class=\"city-text half\"><img src=\"img/dialog/city-info/city-icon.png\" />Citonija</span>\n                <span class=\"city-text half\"><span class=\"color-text-2\">X</span> 154885 <span class=\"color-text-2\">Y</span> 154885</span>\n                <span class=\"city-text full\"><img src=\"img/dialog/city-info/player-icon.png\" />Player Name</span>\n                <span class=\"city-text full\"><img src=\"img/dialog/city-info/glory-icon.png\" />15425</span>\n                <span class=\"city-text full\"><img src=\"img/dialog/city-info/alliance-icon.png\" />Alianconia</span>\n            </div>\n            <div class=\"city-buttons\">\n                <div class=\"c-button\">Save</div>\n                <div class=\"c-button\">Mail</div>\n            </div>\n        </div>\n        <div class=\"active-gifts-box\">\n            <div class=\"active-gifts\">Active Gifts</div>\n            <img src=\"assets/sprites/amjad/resources/brass.png\" />\n            <img src=\"assets/sprites/amjad/resources/camel.png\" />\n            <img src=\"assets/sprites/amjad/resources/chick.png\" />\n            <img src=\"assets/sprites/amjad/resources/cloth.png\" />\n            <img src=\"assets/sprites/amjad/resources/copper.png\" />\n            <img src=\"assets/sprites/amjad/resources/dates.png\" />\n            <img src=\"assets/sprites/amjad/resources/grain.png\" />\n            <img src=\"assets/sprites/amjad/resources/milk.png\" />\n            <img src=\"assets/sprites/amjad/resources/olives.png\" />\n            <img src=\"assets/sprites/amjad/resources/paper.png\" />\n            <img src=\"assets/sprites/amjad/resources/salt.png\" />\n            <img src=\"assets/sprites/amjad/resources/silk.png\" />\n        </div>\n    </div>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var CityInfoDialog;
    CityInfoDialog = (function(_super) {
      __extends(CityInfoDialog, _super);

      function CityInfoDialog() {
        CityInfoDialog.__super__.constructor.call(this, html);
      }

      return CityInfoDialog;

    })(BaseDialog);
    CityInfoDialog.prototype.onPrologue = function() {
      this.closeable();
      return this.draggable();
    };
    return CityInfoDialog;
  });

}).call(this);
