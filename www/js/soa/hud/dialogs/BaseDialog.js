(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/SOAEventDispatcher", "soa/hud/IntervalQueue"], function(EventDispatcher, IntervalQueue) {
    var BaseDialog, PARSE_TRIGGERS;
    PARSE_TRIGGERS = new RegExp(/(.*)\/(.*)/);
    BaseDialog = (function(_super) {
      __extends(BaseDialog, _super);

      function BaseDialog(html, data) {
        if (data == null) {
          data = {};
        }
        BaseDialog.__super__.constructor.call(this);
        this.data = {};
        this.compiled_data = {};
        this.save_fields = {};
        this.templates = {};
        this.show_check = false;
        this.opened = false;
        this.initListeners();
        if (html != null) {
          this.compileParent(html, data);
        }
        if (this.init != null) {
          this.init();
        }
        return this;
      }

      return BaseDialog;

    })(EventDispatcher);
    BaseDialog.prototype.closeable = function() {
      var clb,
        _this = this;
      clb = this.html.find(".close-button");
      if (clb.length < 0) {
        return;
      }
      clb.unbind("click");
      return clb.click(function() {
        return _this.hide();
      });
    };
    BaseDialog.prototype.draggable = function() {
      this.html.css("position", "absolute");
      return this.html.draggable({
        handle: ".dialog-name"
      });
    };
    BaseDialog.prototype.upgradeable = function() {
      var UpgradeBtn,
        _this = this;
      UpgradeBtn = this.html.find("#upgrade-building");
      UpgradeBtn.unbind("click");
      return UpgradeBtn.on("click", function() {
        _this.hide();
        return _this.ctx.upgrade();
      });
    };
    BaseDialog.prototype.moveable = function() {
      var MoveBTN,
        _this = this;
      MoveBTN = this.html.find("#move-building");
      MoveBTN.unbind("click");
      return MoveBTN.on("click", function() {
        _this.hide();
        return SOA.attachConstructionToMouse("City", _this.ctx);
      });
    };
    BaseDialog.prototype.compileParent = function(html, data) {
      this.parent_template = Handlebars.compile(html);
      return this.html = $(this.parent_template(data));
    };
    BaseDialog.prototype.prologue = function(dom) {
      if (this.onPrologue != null) {
        this.onPrologue.call(this, dom);
      }
      return this;
    };
    BaseDialog.prototype.epilogue = function(dom) {
      if (this.onEpilogue != null) {
        this.onEpilogue.call(this, dom);
      }
      return this;
    };
    BaseDialog.prototype.disable = function() {
      return this.show_check = true;
    };
    BaseDialog.prototype.enable = function() {
      return this.show_check = false;
    };
    BaseDialog.prototype.initListeners = function() {};
    BaseDialog.prototype.scheduleAsyncDataRefresh = function(time, data) {
      var _this = this;
      return IntervalQueue.addToQueue(function() {
        var func, key, prom, _results;
        _results = [];
        for (key in data) {
          func = data[key];
          prom = _this.fetchData(key, func);
          _results.push((function(key) {
            return prom.done(function(data) {
              return _this.data[key] = data;
            });
          })(key));
        }
        return _results;
      }, time);
    };
    BaseDialog.prototype.scheduleDataRefresh = function(time, data) {
      var _this = this;
      return IntervalQueue.addToQueue(function() {
        var key, res, val, _results;
        _results = [];
        for (key in data) {
          val = data[key];
          if (typeof val === "function") {
            res = val.call(_this);
          } else {
            res = val;
          }
          _results.push(_this.data[key] = res);
        }
        return _results;
      }, time);
    };
    BaseDialog.prototype.compile = function(template_key, datakey) {
      var defer, prom,
        _this = this;
      prom = this.fetchData(datakey, this.data[datakey]);
      defer = new $.Deferred();
      prom.done(function(data) {
        var comp_template;
        _this.data[datakey] = data;
        comp_template = _this.compileTemplate(template_key, datakey, data);
        return defer.resolve(comp_template);
      }, function(err) {
        return defer.reject(err);
      });
      return defer.promise();
    };
    BaseDialog.prototype.setName = function(name) {
      this.name = name;
      SOA.Hud.trigger("NAME_CHANGED", this);
      if (this.html != null) {
        return this.html.attr("name", this.name);
      }
    };
    BaseDialog.prototype.compileTemplate = function(tempkey, datakey, data) {
      var compiled_temp;
      compiled_temp = Handlebars.compile(this.templates[tempkey]);
      return (this.compiled_data[datakey] = compiled_temp(data));
    };
    BaseDialog.prototype.fetchData = function(datakey, data) {
      var defer, out;
      defer = new $.Deferred();
      if (typeof data === "function") {
        out = data.call(this);
        if ((out.done != null) && (out.promise != null)) {
          out.done(function(data) {
            return defer.resolve(data);
          }).fail(function(err) {
            return defer.reject(err);
          });
        } else {
          defer.resolve(out);
        }
      } else if (data != null) {
        defer.resolve(data);
      } else {
        console.error("Data not set");
        defer.reject("Data not set");
      }
      return defer.promise();
    };
    BaseDialog.prototype.show = function(ctx, animSpeed) {
      this.ctx = ctx;
      if (animSpeed == null) {
        animSpeed = 200;
      }
      this.enable();
      if (this.ctx == null) {
        return;
      }
      this.trigger("PRE_SHOW", this.ctx);
      this.trigger("SHOW", this.ctx);
      if (!this.show_check) {
        this.html.fadeIn(animSpeed);
        return this.opened = true;
      }
    };
    BaseDialog.prototype.hide = function(animSpeed) {
      var _this = this;
      if (animSpeed == null) {
        animSpeed = 200;
      }
      this.html.fadeOut(animSpeed, function() {
        return _this.html.hide();
      });
      this.trigger("HIDE", this.ctx);
      this.opened = false;
      return this;
    };
    BaseDialog.prototype.setTemplates = function(templates) {
      this.templates = templates;
      return this;
    };
    BaseDialog.prototype.trigger = function(type, msg) {
      BaseDialog.__super__.trigger.call(this, type, msg, this.html);
      return this;
    };
    BaseDialog.prototype.refresh = function(inner_template, compiled_data, duration) {
      var partial, temp;
      if (duration == null) {
        duration = 0;
      }
      temp = {};
      temp[inner_template] = compiled_data;
      partial = this.html.find("#" + inner_template);
      partial.empty();
      partial.hide();
      partial.append(compiled_data);
      partial.show(duration);
      this.clickTriggers(this.clicks);
      return this;
    };
    BaseDialog.prototype.compileHtml = function(loaded_data) {
      var newDialog;
      newDialog = $(this.parent_template(loaded_data));
      newDialog.replaceAll(this.html);
      this.html = newDialog;
      return this;
    };
    BaseDialog.prototype.clickTriggers = function(clicks) {
      var get_from, look_at, save, save_field, split, trigger, _ref, _results,
        _this = this;
      this.clicks = clicks;
      _ref = this.clicks;
      _results = [];
      for (trigger in _ref) {
        save_field = _ref[trigger];
        split = PARSE_TRIGGERS.exec(trigger);
        look_at = split[1];
        get_from = split[2];
        save = this.html.find(look_at);
        if (save.length > 0) {
          save.unbind("click");
        }
        if (get_from.length === 1) {
          _results.push((function(get_from, save_field) {
            return save.click(function(ev) {
              return _this.trigger(save_field, get_from);
            });
          })(get_from, save_field));
        } else if (get_from.indexOf("@") !== -1) {
          _results.push((function(get_from, save_field) {
            return save.click(function(ev) {
              var value;
              value = $(ev.currentTarget).data(get_from.substr(1));
              _this.set(save_field, value);
              return _this.trigger(save_field, value, _this);
            });
          })(get_from, save_field));
        } else if (get_from.indexOf("#") !== -1) {
          _results.push((function(get_from, save_field) {
            return save.click(function(ev) {
              var value;
              value = $(ev.currentTarget).attr(get_from.substr(1));
              _this.set(save_field, value);
              return _this.trigger(save_field, value, _this);
            });
          })(get_from, save_field));
        } else {
          _results.push(console.warn("Unrecognized pattern: " + get_from[0]));
        }
      }
      return _results;
    };
    BaseDialog.prototype.set = function(name, val) {
      return this.save_fields[name] = val;
    };
    BaseDialog.prototype.get = function(name) {
      return this.save_fields[name];
    };
    BaseDialog.prototype.setData = function(key, val) {
      return this.data[key] = val;
    };
    BaseDialog.prototype.getData = function(key) {
      return this.data[key];
    };
    return BaseDialog;
  });

}).call(this);
