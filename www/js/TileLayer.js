(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["PIXI", "MapConfig"], function(PIXI, MapConfig) {
    var TileLayer;
    TileLayer = (function(_super) {
      __extends(TileLayer, _super);

      function TileLayer(parentTile, meta) {
        var texture;
        this.parentTile = parentTile;
        this.id = meta.id;
        this.z = meta.layer;
        texture = PIXI.TextureCache[meta.sprite];
        if (texture == null) {
          throw new Error("Meta " + meta.name + "/" + meta.sprite + " does not exist in the texture cache");
        }
        TileLayer.__super__.constructor.call(this, texture);
        this.init();
      }

      return TileLayer;

    })(PIXI.Sprite);
    TileLayer.prototype.init = function() {
      this.y_off = (this.getBounds().height - MapConfig.TILE_HEIGHT) * 0.5;
      this.position.x = this.parentTile.position.x;
      this.position.y = this.parentTile.position.y - this.y_off;
      this.hitArea = this._bounds.clone();
      this.interactive = true;
      this._pass_me_pixi_ = true;
      this.scale.x = 1.02;
      return this.scale.y = 1.02;
    };
    TileLayer.prototype.onClick = function(btn) {
      console.debug("Kliknuo si me na tajlu: " + (this.parentTile.toString()));
      console.debug("Ja sam na Z-u: " + this.z);
      if (btn === 0) {
        if (this.onLeftClick != null) {
          return this.onLeftClick();
        }
      } else {
        if (this.onRightClick != null) {
          return this.onRightClick();
        }
      }
    };
    TileLayer.prototype.changeTexture = function(tex_name) {
      var texture;
      texture = PIXI.TextureCache[tex_name];
      if (texture == null) {
        throw new Error("No such texture " + tex_name + "!");
      }
      this.texture = texture;
      this.y_off = (this.getBounds().height - MapConfig.TILE_HEIGHT) * 0.5;
      this.position.x = this.parentTile.position.x;
      return this.position.y = this.parentTile.position.y - this.y_off;
    };
    TileLayer.prototype.changeParentTile = function(parentTile) {
      delete this.parentTile.layers[this.z];
      this.parentTile = parentTile;
      this.y_off = (this.getBounds().height - MapConfig.TILE_HEIGHT) * 0.5;
      this.position.x = this.parentTile.position.x;
      this.position.y = this.parentTile.position.y - this.y_off;
      return this.parentTile.layers[this.z] = this;
    };
    return TileLayer;
  });

}).call(this);
