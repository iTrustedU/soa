(function() {
  var MapModel, RegionModel, SectionModel, StatusMessages, TileMeta, async, colors, crypto, fs, mkdirp, mongoose, wrench;

  mongoose = require("mongoose");

  MapModel = require("../../models/editor/Map");

  RegionModel = require("../../models/editor/Region");

  SectionModel = require("../../models/editor/Section");

  StatusMessages = require("../../modules/StatusMessages");

  TileMeta = require("../../models/editor/TileMeta");

  fs = require("graceful-fs");

  mkdirp = require("mkdirp");

  crypto = require("crypto");

  colors = require("colors");

  async = require("async");

  wrench = require("wrench");

  module.exports = function(app) {
    var NUM_LAYERS, NUM_SECTIONS, SECTION_H, SECTION_W, createPNGBuffer, initializeMap;
    NUM_SECTIONS = 9;
    NUM_LAYERS = 4;
    SECTION_W = 50;
    SECTION_H = 50;
    app.get("/map/:name/load", function(req, res) {
      var map_name;
      map_name = req.params.name;
      return MapModel.findOne({
        name: map_name
      }).exec(function(error, map) {
        var stat;
        if (error != null) {
          stat = StatusMessages.ERROR;
          return res.status(stat.code).send(stat.msg);
        } else {
          return res.send(map);
        }
      });
    });
    app.get("/maps", function(req, res) {
      return MapModel.find().select("name -_id").exec(function(error, results) {
        var stat;
        if (error != null) {
          stat = StatusMessages.ERROR;
          res.status(stat.msg).send(stat.msg);
          return;
        }
        return res.send(results);
      });
    });
    app.post("/maps/deletemany", function(req, res) {
      var names;
      names = req.body.names;
      return MapModel.find({
        "name": {
          $in: names
        }
      }).remove().exec(function(error, success) {
        var stat;
        if (error != null) {
          stat = StatusMessages.ERROR;
          return res.status(stat.code).send(stat.msg);
        } else {
          stat = StatusMessages.SUCCESS;
          return res.status(stat.code).send(stat.msg);
        }
      });
    });
    app.post("/maps/delete", function(req, res) {
      return MapModel.find().remove().exec(function(error, success) {
        var stat;
        if (error != null) {
          stat = StatusMessages.ERROR;
          return res.status(stat.code).send(stat.msg);
        } else {
          stat = StatusMessages.SUCCESS;
          return res.status(stat.code).send(stat.msg);
        }
      });
    });
    app.post("/map/:name/delete", function(req, res) {
      var name;
      name = req.params.name;
      return MapModel.findOne({
        name: name
      }).remove().exec(function(error, success) {
        var stat;
        if (error != null) {
          stat = StatusMessages.ERROR;
          return res.status(stat.code).send(stat.msg);
        } else {
          stat = StatusMessages.SUCCESS;
          return res.status(stat.code).send(stat.msg);
        }
      });
    });
    app.post("/maps/new", function(req, res) {
      var data;
      data = req.body;
      return initializeMap(data, function(err, mapData) {
        console.log("Map initialized");
        return MapModel.findOne({
          name: data.name
        }).exec(function(error, map) {
          if ((map != null)) {
            res.status(422).send("Map with the same name already exists!");
          }
          map = new MapModel(mapData);
          return map.save(function(err) {
            var stat;
            if (err != null) {
              stat = StatusMessages.ERROR;
              return res.status(stat.code).send(err.message);
            } else {
              stat = StatusMessages.SUCCESS;
              return res.status(stat.code).send(map);
            }
          });
        });
      });
    });
    app.post("/map/:name/tiles/add", function(req, res) {
      var data;
      data = req.body;
      return MapModel.findOne({
        name: req.params.name
      }).exec(function(error, map) {
        var tiles;
        tiles = map.tiles.filter(function(tile) {
          return tile.seedId === +data.seedId;
        });
        if (tiles.length !== 0) {
          return res.status(422).send("Tile with id:" + data.seedId + " is a duplicate tile");
        } else {
          map.tiles.push(data);
          map.save();
          return res.send(200);
        }
      });
    });
    app.get("/tiles/resetseed", function(req, res) {
      return app.locals.tileSeeder.reset();
    });
    app.post("/tiles/new", function(req, res) {
      var data, tile;
      data = req.body;
      tile = new TileMeta({
        name: data.name,
        layer: data.layer,
        span: data.span,
        sprite: data.sprite,
        seedId: app.locals.tileSeeder.randomize()
      });
      return tile.save(function(err) {
        var stat;
        if (err != null) {
          stat = StatusMessages.ERROR;
          return res.status(stat.code).send(err.message);
        } else {
          return res.send(tile);
        }
      });
    });
    app.post("/tile/:id/delete", function(req, res) {
      var id;
      id = req.params.id;
      return TileMeta.findOne({
        seedId: +id
      }).remove().exec(function(err) {
        if (err != null) {
          return res.send(422);
        }
        return res.send(200);
      });
    });
    app.get("/tiles", function(req, res) {
      return TileMeta.find().select("-_id").exec(function(error, results) {
        var stat;
        if (error != null) {
          stat = StatusMessages.ERROR;
          res.status(stat.msg).send(stat.msg);
          return;
        }
        return res.send(results);
      });
    });
    app.get("/map/:name/region/:row/:col/load", function(req, res) {
      var col, name, row;
      row = req.params.row;
      col = req.params.col;
      name = req.params.name;
      return MapModel.find({
        name: name
      }).exec(function(err, map) {
        if (err != null) {
          res.send(422);
        }
        return res.send(map.regions["" + row + "_" + col]);
      });
    });
    app.get("/map/:name/region/:row/:col/:section/load", function(req, res) {
      var col, name, row, section;
      row = req.params.row;
      col = req.params.col;
      section = req.params.section;
      name = req.params.name;
      return MapModel.find({
        name: name
      }).exec(function(err, map) {
        if (err != null) {
          return res.send(422);
        }
        return res.send(map.regions["" + row + "_" + col].sections[section]);
      });
    });
    app.post("/map/:name/region/:row/:col/:section/save", function(req, res) {
      var col, name, row, section_index;
      row = req.params.row;
      col = req.params.col;
      section_index = req.params.section;
      name = req.params.name;
      return MapModel.findOne({
        name: name
      }).exec(function(err, map) {
        var key, region, section;
        key = "" + row + "_" + col;
        region = map.regions[key];
        if (region == null) {
          return res.send(422, "No region at " + row + ", " + col);
        }
        section = region.sections[section_index];
        section.buffers = req.body.buffers.map(function(buff) {
          return buff.split(",")[1];
        });
        region.section = section;
        map.markModified("regions");
        return map.save(function(err) {
          if (err != null) {
            return res.send(422, err.message);
          }
          return res.send(200);
        });
      });
    });
    createPNGBuffer = function(name, clb) {
      var fdesc,
        _this = this;
      return fdesc = fs.readFile(name, function(err, data) {
        return clb(err, data.toString("base64"));
      });
    };
    return initializeMap = function(map, clb) {
      var empty_buffer, initializeRegion, initializeSection, initializeSectionBuffers, region_keys;
      region_keys = Object.keys(map.regions);
      empty_buffer = null;
      initializeSectionBuffers = function(region) {
        return function(sectionIndex, clb) {
          var i, section;
          section = region.sections[sectionIndex];
          section.buffers = (function() {
            var _i, _results;
            _results = [];
            for (i = _i = 0; 0 <= NUM_LAYERS ? _i < NUM_LAYERS : _i > NUM_LAYERS; i = 0 <= NUM_LAYERS ? ++_i : --_i) {
              _results.push(empty_buffer);
            }
            return _results;
          })();
          section.x = +section.x;
          section.y = +section.y;
          return clb(null, section);
        };
      };
      initializeSection = function(region, clb) {
        var _i, _results,
          _this = this;
        region.x = +region.x;
        region.y = +region.y;
        region.row = +region.row;
        region.col = +region.col;
        return async.map((function() {
          _results = [];
          for (var _i = 0; 0 <= NUM_SECTIONS ? _i < NUM_SECTIONS : _i > NUM_SECTIONS; 0 <= NUM_SECTIONS ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this), initializeSectionBuffers(region), function(err, section) {
          return clb(err, section);
        });
      };
      initializeRegion = function(regkey, clb) {
        var region;
        region = map.regions[regkey];
        return async.map([region], initializeSection, function(err, section) {
          console.log("Section initialized");
          return clb(err, section);
        });
      };
      return createPNGBuffer("temp.png", function(err, data) {
        empty_buffer = data;
        return async.map(region_keys, initializeRegion, function(err) {
          console.log("Regions initialized");
          console.dir(map);
          return clb(err, map);
        });
      });
    };
  };

}).call(this);
