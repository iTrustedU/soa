"use strict"

define [
	"Shims"
	"PIXI"    
	"EventDispatcher"
	"LoggingQueue"
	"TileMetaStorage"
	"DOMManager"
	"ImageUtils"
	"IsometricScene"
	"MathUtils"
	"Ajax"
	"Animations"
	"Tweener"
],

(
	Shims
	PIXI
	EventDispatcher
	LoggingQueue
	TileMetaStorage
	DOMManager
	ImageUtils
	IsometricScene
	MathUtils
	Ajax
	Animations
	Tweener
) ->

	class HAL extends EventDispatcher
		constructor: () ->
			super()
			@__started__ 	= false
			@Ajax 			= Ajax
			@IsometricScene = IsometricScene
			@MathUtils 		= MathUtils
			@Tweener		= Tweener
			
	HAL::start = () ->
		if @__started__
			throw new Error("HAL is already initialized!")
		@__started__ 		= true
		@log 				= new LoggingQueue()
		@TileMetaStorage 	= new TileMetaStorage()
		@Dom 				= new DOMManager()
		@ImageUtils 		= new ImageUtils()
		@Animations 		= new Animations()
		setTimeout((() =>
			@trigger "HAL_LOADED"
		), 100)
		return @

	window.PIXI = PIXI
	window.HAL	= new HAL()

	return (window.HAL)
