"use strict" 

html =
"""
<div class="bottom-top">  
    <div class="heroes">
        <div id="heroes-list">
            {{{heroes_list}}}
        </div>
    </div>
</div>
"""

$heroes = 
"""
{{#each heroes}}
    <div class="hero-frame" type="{{_type_}}" id="{{_type_}}-{{id}}">
        <img src={{concat_hero_avatar _type_ "f"}} class="hero-icon"></img>
        <div class="hero-name-title">
            <span class="hero-name">{{name}}</span>
            <br>
            <span class="hero-title">{{title}}</span>
        </div>
        <div class="expirience">
            <span><img src="img/dialog/training/level.png" class="hero-level-icon" /> {{level}}</span>
            <div class="exp-bar">
                <div class="exp-inner-bar">
                    <div id="bar" {{calc_experience_percentage this}}></div>
                </div>
            </div>
        </div>  
    </div>
{{/each}}
"""

define ["soa/hud/dialogs/BaseDialog", "soa/hud/IntervalQueue"], (BaseDialog, IntervalQueue) ->

    QUEUE_REFRESH_TIME = 1000 * 10 #WHY NOT? 

    class HeroesQueue extends BaseDialog
        constructor: () ->
            super(html)

    HeroesQueue::prologue = () ->
        @setTemplates
            "heroes_list": $heroes
        @setData "queue", "heroes": []

    HeroesQueue::putHero = (hero) ->
        @getData("queue").heroes.unshift hero
        @compile("heroes_list", "queue").then (data) =>
            @refresh("heroes-list", data, 200)
            @show({})
            @html.find(".hero-frame").each (k, v) ->
                h = $(v)
                signature = h.attr("id").split("-")
                type = signature[0]
                id = signature[1]
                h.unbind "click"
                h.dblclick () ->
                    hero = amjad.empire.heroes["#{type}s"][id]
                    dlg = type.toUpperCase()
                    SOA.Hud.showDialog(dlg, hero)
                h.click () ->
                    hero = amjad.empire.heroes["#{type}s"][id]
                    SOA.Zone.centerOn(hero.layer.position)
                    
    return HeroesQueue
