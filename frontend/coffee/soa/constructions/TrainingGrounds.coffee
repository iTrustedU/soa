"use strict"

define ["soa/constructions/Construction"],

(Construction) ->

	class TrainingGrounds extends Construction

	TrainingGrounds::init = () ->
		@units = {}

		for k, unit of XMLStore.get "units"
			hspruri = "assets/sprites/amjad/units/#{amjad.user.architecture}/44x44/#{k}.png"
			spr = PIXI.TextureCache[hspruri]
			if unit.price? and spr?
				 @units[k] = unit

	TrainingGrounds::epilogue = () ->
		if @action_type is "produce"
			alert(@u_type)
			alert()
		#if @build_end?  and @build_end.length > 0

	TrainingGrounds::beginUnitProduction = (unit, res) ->
		amjad.empire.resources.take(res)
		@assignUnitProduction(unit)

	TrainingGrounds::assignUnitProduction = (unit) ->
		@production.push(unit)
		SOA.Hud.getDialog("BUILD_ORDERS")
        .addUnitToTrainingQueue(unit, "Training")
        .done () =>
            amjad.finishProduction(@construction_id)
            .done (data) =>
                console.debug data
                warcnt = (amjad.empire.constructions.getAllOfType("war")[0])
                warcnt.addUnit(unit)
                console.debug warcnt
                war = SOA.Hud.getDialog("WAR")
                if war.opened
                    war.show(warcnt)

	TrainingGrounds::addToMap = () ->
		@addToCityMap()

	TrainingGrounds::getTrainers = () ->
		return amjad.empire.heroes.trainers

	TrainingGrounds::finishUnitProduction = () ->
		amjad.finishProduction(@construction_id).done (data) ->
			console.debug ("Finished unit production")
			console.debug data
		.fail () ->
		    alert "omg. failed to finish unit production"	

	return TrainingGrounds

	# unit.buildTime = () ->
	#     time = XMLStore.units[unit.type].price.time
	#     return 60*(+0.3)*1000 #hardfuckingcoded

	# unit.timeLeft = () ->
	#     a = new Date(@start_production)
	#     b = new Date()
	#     return @buildTime() - (b - a)

	# unit.timeLeftPercentage = () ->
	#     return (1 - (@timeLeft() / @buildTime())).toFixed(2)
	
	# unit.trainingEnds = () ->
	#     a = new Date(@start_production)
	#     b = new Date(@start_production)
	#     a.setMilliseconds(a.getMilliseconds() + @buildTime())
	#     c = (b - a)
	#     return [a.toLocaleString(), c]

	# for unit in cn.production?
	#     cn.buildUnitPrototype(unit)

	# for unit in cn.production?
	#     SOA.Hud.getDialog("BUILD_ORDERS").addToTrainingQueue(unit, "Training")

	# amjad.finishProduction(cn.construction_id).done (data) ->
	#     return
	#     ##console.debug data
	# .fail () ->
	#     alert "omg. failed to finish unit production"