(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader"], function(StructureLoader) {
    var CONSTRUCTIONS_COLLISION_LAYER, CONSTRUCTION_LEVELING_DIVISOR, Construction, Constructions, ID_MAP, ShortMap, collisionReport;
    ShortMap = {
      "*": "*"
    };
    CONSTRUCTION_LEVELING_DIVISOR = 1 / 5;
    CONSTRUCTIONS_COLLISION_LAYER = 6;
    ID_MAP = {};
    collisionReport = function(collided) {
      var cl, outStr, _i, _len;
      outStr = "Collisions at: \n";
      for (_i = 0, _len = collided.length; _i < _len; _i++) {
        cl = collided[_i];
        outStr += "" + cl.row + "," + cl.col + "\n";
      }
      return outStr;
    };
    Construction = (function(_super) {
      __extends(Construction, _super);

      function Construction(meta) {
        this.loaded = false;
        this.layer = null;
        Construction.__super__.constructor.call(this, meta);
        this.assigned_heroes = {};
      }

      Construction.prototype.buildEnds = function() {
        var a, b, c;
        if ((this.build_start != null) && this.build_start.length > 0) {
          a = new Date(this.build_start);
          b = new Date(this.build_start);
          a.setMilliseconds(a.getMilliseconds() + this.buildTime());
          c = b - a;
          return [a.toLocaleString(), c];
        } else {
          return [0, 0];
        }
      };

      Construction.prototype.buildTime = function() {
        if (this.poi_id == null) {
          return 0.3 * 60 * 1000;
        } else {
          return 0.3 * 60 * 1000;
        }
      };

      Construction.prototype.timeLeft = function() {
        var a, b;
        a = new Date(this.build_start);
        b = new Date();
        return this.buildTime() - (b - a);
      };

      Construction.prototype.timeLeftPercentage = function() {
        return (1 - (this.timeLeft() / this.buildTime())).toFixed(2);
      };

      Construction.prototype.initListeners = function() {
        var _this = this;
        Construction.__super__.initListeners.call(this);
        this.on("DONE_LOADING", function() {
          console.trace();
          console.debug("Loading new construction " + this.c_type);
          this.loaded = true;
          if ((this.build_end == null) || !this.build_end) {
            return SOA.Hud.getDialog("BUILD_ORDERS").addToBuildQueue(this, "Constructing");
          }
        });
        return HAL.on("CITY_MAP_LOADED", function() {
          if (_this.poi_id == null) {
            _this.addToCityMap();
          } else {
            _this.addToZoneMap();
          }
          if (_this.c_type === "training_grounds") {
            return Constructions.loadTrainingGrounds(_this);
          } else if (_this.c_type === "trade_station") {
            return Constructions.loadTradeStation(_this);
          } else if (_this.c_type === "war") {
            return Constructions.loadWar(_this);
          } else if (_this.c_type === "science") {
            Constructions.loadScience(_this);
            return Constructions.loadTech(_this);
          }
        });
      };

      Construction.prototype.changeLevel = function() {
        var normalized, tex_name;
        this.level += 1;
        normalized = Math.floor(CONSTRUCTION_LEVELING_DIVISOR * this.level) + 1;
        if (this.canBeUpgraded(normalized)) {
          tex_name = "assets/sprites/" + amjad.user.CONSTRUCTION_SPRITES_URI + (this.getLevelNormalizedName()) + ".png";
          return this.layer.changeTexture(tex_name);
        }
      };

      Construction.prototype.getNormalizedLevel = function() {
        return Math.floor(CONSTRUCTION_LEVELING_DIVISOR * this.level) + 1;
      };

      Construction.prototype.getLevelNormalizedName = function() {
        return "" + this.c_type + (this.getNormalizedLevel());
      };

      Construction.prototype.getSpan = function(to_level) {
        var leveled_meta, lvl_meta_name, span, span_size;
        if (to_level == null) {
          to_level = this.getNormalizedLevel();
        }
        lvl_meta_name = amjad.splaySpriteURI("" + amjad.user.CONSTRUCTION_SPRITES_URI + this.c_type + to_level);
        leveled_meta = HAL.TileMetaStorage.findByName(lvl_meta_name);
        if (leveled_meta == null) {
          return null;
        }
        span_size = leveled_meta.size;
        span = SOA.City.map.getSpanArea(this.layer.parentTile, span_size);
        return span;
      };

      Construction.prototype.getSpanFromTile = function(tile, to_level) {
        var leveled_meta, lvl_meta_name, span, span_size;
        if (to_level == null) {
          to_level = this.getNormalizedLevel();
        }
        lvl_meta_name = amjad.splaySpriteURI("" + amjad.user.CONSTRUCTION_SPRITES_URI + this.c_type + to_level);
        leveled_meta = HAL.TileMetaStorage.findByName(lvl_meta_name);
        if (leveled_meta == null) {
          return null;
        }
        span_size = leveled_meta.size;
        span = SOA.City.map.getSpanArea(tile, span_size);
        return span;
      };

      Construction.prototype.drawSpan = function(level, color) {
        var span;
        if (color == null) {
          color = 0x0292CD;
        }
        span = this.getSpan(level);
        return SOA.City.map.tintSpan(span, 2, color);
      };

      Construction.prototype.canBeUpgraded = function(to_level) {
        /* 
            @todo treba pronaci span za sledeci nivo
            fillovati matricu za sve gradjevine 
            zatim proveriti ima li preseka u bitovima
            za gradjevine posmatramo samo 1 i 0
        */

        var collisions, span;
        span = this.getSpan(to_level);
        collisions = amjad.empire.constructions.fillSpanArea(span, this.layer.id);
        if (collisions.length > 0) {
          this.onCollision(collisions, function() {
            return TweenMax.fromTo(this.layer, 1.1, {
              alpha: 0.1,
              repeat: 5
            }, {
              alpha: 1,
              repeat: 5
            });
          });
          return false;
        }
        return true;
      };

      Construction.prototype.onCollision = function(collisions, effect) {
        var all_cns, c, cn, cnid, _i, _j, _len, _len1, _results;
        all_cns = [];
        for (_i = 0, _len = collisions.length; _i < _len; _i++) {
          c = collisions[_i];
          cnid = SOA.Constructions.getSpanMark(c.row, c.col);
          cn = ID_MAP[cnid];
          if (all_cns.indexOf(cn) === -1) {
            all_cns.push(cn);
          }
        }
        _results = [];
        for (_j = 0, _len1 = all_cns.length; _j < _len1; _j++) {
          cn = all_cns[_j];
          _results.push(effect.call(cn));
        }
        return _results;
      };

      Construction.prototype.addToCityMap = function() {
        var layer;
        console.debug("Added to city map", this);
        layer = amjad.putConstructionInCity(this.x, this.y, this.getLevelNormalizedName());
        this.assignLayer(layer);
        ID_MAP[layer.id] = this;
        if ((this.build_end == null) || !this.build_end) {
          this.enqueBuildRefresh();
        }
      };

      Construction.prototype.addToZoneMap = function() {
        var layer;
        layer = SOA.Zone.map.addLayerSpriteToTile(this.x, this.y, 5, "assets/sprites/amjad/poi/encampment_established");
        this.assignLayer(layer);
        ID_MAP[layer.id] = this;
        if ((this.build_end != null) || !this.build_end) {
          this.enqueBuildRefresh();
        }
      };

      Construction.prototype.enqueBuildRefresh = function(update_time) {
        var id,
          _this = this;
        if (update_time == null) {
          update_time = null;
        }
        if (update_time == null) {
          update_time = this.buildTime();
        }
        if ((this.build_end == null) || !this.build_end) {
          /* @todo spec u buducnosti moze biti svasta*/

          this.updateUponFinish();
          return id = setInterval(function() {
            return _this.updateUponFinish(id);
          }, update_time);
        }
      };

      Construction.prototype.updateUponFinish = function(id) {
        var _this = this;
        return this.finishBuild().done(function(data) {
          if ((data != null) && ((data.build_end == null) || data.build_end === "")) {
            _this.load(data);
            amjad.trigger("CONSTRUCTIONS_UPDATED");
            return clearInterval(id);
          }
        }).fail(function(data) {
          return console.error("Build finish enqueue failed");
        });
      };

      Construction.prototype.finishBuild = function() {
        return Rest.post("construction.finish_build", {
          cid: this.construction_id,
          spec: "spec1"
        });
      };

      Construction.prototype.assignLayer = function(layer) {
        var collisions,
          _this = this;
        if (layer == null) {
          throw new Error("Layer couldn't be assigned to construction " + this.c_type + " at " + this.x + ", " + this.y);
        }
        this.layer = layer;
        this.layer.onLeftClick = function() {
          return SOA.Hud.showDialog(_this.c_type.toUpperCase(), _this);
        };
        if (this.poi_id == null) {
          if (this.level === 0) {
            this.level = 1;
          }
          collisions = amjad.empire.constructions.fillSpanArea(this.getSpan(this.level), this.layer.id);
          if (collisions.length > 0) {
            return console.log(collisionReport(collisions));
          }
        }
      };

      Construction.prototype.load = function(meta) {
        return Construction.__super__.load.call(this, meta);
      };

      return Construction;

    })(StructureLoader);
    /*
        Singleton container for all constructions 
        and their common utility functions
    */

    Constructions = (function(_super) {
      __extends(Constructions, _super);

      function Constructions() {
        this.container = [];
        Constructions.__super__.constructor.call(this, {
          "*": [
            "*", function(meta) {
              var cn;
              cn = new Construction(ShortMap).load(meta);
              console.debug(cn);
              this.container.push(cn);
              return cn;
            }
          ]
        });
      }

      return Constructions;

    })(StructureLoader);
    Constructions.prototype.init = function() {
      return this.available_constructions = {};
    };
    Constructions.prototype.getSpanMark = function(row, col) {
      return this.span_area[row * this.span_area_h + col];
    };
    Constructions.prototype.fillSpanArea = function(spans, id) {
      var index, out, spmark, _i, _len;
      if (spans == null) {
        throw new Error("Span not available!!!");
      }
      out = [];
      for (_i = 0, _len = spans.length; _i < _len; _i++) {
        spmark = spans[_i];
        index = spmark.row * this.span_area_h + spmark.col;
        if (this.span_area[index] === 0) {
          this.span_area[index] = id;
        } else if (this.span_area[index] !== id) {
          out.push({
            row: spmark.row,
            col: spmark.col
          });
        }
      }
      return out;
    };
    Constructions.prototype.testSpanArea = function(spans, id) {
      var index, out, spmark, _i, _len;
      if (spans == null) {
        throw new Error("Span not available!!!");
      }
      out = [];
      for (_i = 0, _len = spans.length; _i < _len; _i++) {
        spmark = spans[_i];
        index = spmark.row * this.span_area_h + spmark.col;
        if (this.span_area[index] !== id && this.span_area[index] !== 0) {
          out.push({
            row: spmark.row,
            col: spmark.col
          });
        }
      }
      return out;
    };
    Constructions.prototype.fromSpanMark = function(row, col) {
      var id;
      id = this.getSpanMark(row, col);
      return ID_MAP[id];
    };
    Constructions.prototype.canBePlacedOn = function(cnA, cnB) {
      return this.testSpanArea(cnA.getSpan(), cnB.getSpan());
    };
    Constructions.prototype.constructionFulfilsRequirements = function(requirements) {
      var hasResources, out;
      out = [];
      hasResources = amjad.empire.resources.hasEnough(requirements.resources);
      out = out.concat(hasResources);
      out = out.concat(amjad.empire.constructions.hasConstructions(requirements.constructions, function(myconstr, xml_level) {
        return (myconstr != null ? myconstr.level : void 0) >= xml_level;
      }));
      return out;
    };
    Constructions.prototype.initSpanArea = function() {
      var i, j, _i, _ref, _results;
      this.span_area_w = SOA.City.map.NROWS;
      this.span_area_h = SOA.City.map.NCOLUMNS;
      this.span_area = [];
      _results = [];
      for (i = _i = 0, _ref = this.span_area_w; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (j = _j = 0, _ref1 = this.span_area_h; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
            _results1.push(this.span_area[i * this.span_area_h + j] = 0);
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };
    Constructions.prototype.initListeners = function() {
      var _this = this;
      Constructions.__super__.initListeners.call(this);
      this.on("DONE_LOADING", function() {});
      HAL.on("SOA_LOADED", function() {
        _this.findAvailableConstructions(XMLStore.get("constructions"));
        return SOA.Hud.getDialog("BOTTOM_MENU").listBuildings();
      });
      HAL.on("CITY_MAP_LOADED", function() {
        return _this.initSpanArea();
      });
      return amjad.on(["CONSTRUCTIONS_UPDATED", "RESOURCES_UPDATED"], function() {
        return _this.updateConstructions();
      });
    };
    Constructions.prototype.updateConstructions = function() {
      var bottom_img, cnt, dis, k, v, _ref, _ref1, _results;
      dis = [];
      _ref = this.available_constructions;
      for (k in _ref) {
        v = _ref[k];
        if (v.type === "disabled") {
          dis.push(k);
        }
      }
      this.findAvailableConstructions(XMLStore.get("constructions"));
      SOA.Hud.getDialog("BOTTOM_MENU").listBuildings();
      _ref1 = this.available_constructions;
      _results = [];
      for (k in _ref1) {
        cnt = _ref1[k];
        if (cnt.type !== "disabled" && dis.indexOf(k) !== -1) {
          bottom_img = $(".img-frame.bottom-menu-building.ui-draggable#" + k);
          bottom_img.addClass("construction-built");
          _results.push(bottom_img.removeClass("construction-built", 5000));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    Constructions.prototype.load = function(meta) {
      return Constructions.__super__.load.call(this, meta);
    };
    Constructions.prototype.getAllOfType = function(type) {
      var cnt, out, _i, _len, _ref;
      out = [];
      _ref = this.container;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        cnt = _ref[_i];
        if (cnt.c_type === type) {
          out.push(cnt);
        }
      }
      return out[0];
    };
    Constructions.prototype.getAll = function(type) {
      return this.container;
    };
    Constructions.prototype.hasConstructions = function(types, filter) {
      var out, type, val;
      if (filter == null) {
        filter = function() {};
      }
      out = [];
      for (type in types) {
        val = types[type];
        if (!this.hasConstruction(type, val) && !filter(this.getAllOfType(type), val)) {
          out.push("Missing required construction: " + type + ", level: " + val);
        }
      }
      return out;
    };
    Constructions.prototype.hasConstruction = function(type, level) {
      var out;
      if (level == null) {
        level = 1;
      }
      if (type === "home") {
        return false;
      }
      out = false;
      this.container.forEach(function(cnt) {
        if (cnt.c_type === type) {
          out = true;
        }
      });
      return out;
    };
    Constructions.prototype.addConstruction = function(type, constr) {
      var cnt;
      if (this.hasConstruction(type)) {
        return console.warn("You already have " + type + " construction");
      } else {
        console.debug(this.getAllOfType(type));
        cnt = new Construction(ShortMap).load(constr);
        console.debug("Constructed " + type);
        SOA.Hud.getDialog("BUILD_ORDERS").addToBuildQueue(cnt, "Constructing");
        return cnt;
      }
    };
    Constructions.prototype.canBeConstructed = function(key, constr_req) {
      var exists, out;
      out = [];
      exists = this.hasConstruction(key);
      if (exists) {
        out.push("Construction exists " + key);
      }
      out = out.concat(this.constructionFulfilsRequirements(constr_req));
      return out.length === 0;
    };
    /* @todo na istu foru prodji kroz ostale, samo ih taguj*/

    Constructions.prototype.findAvailableConstructions = function(xml_constructions) {
      var can_construct, constr, is_supported_by_us, key, norm_name, sprite_uri, _ref, _ref1, _results;
      if (xml_constructions == null) {
        xml_constructions = XMLStore.get("constructions");
      }
      _ref = xml_constructions.city;
      _results = [];
      for (key in _ref) {
        constr = _ref[key];
        norm_name = (_ref1 = this[key]) != null ? _ref1.getLevelNormalizedName() : void 0;
        if (this.hasConstruction(key)) {
          continue;
        }
        sprite_uri = amjad.user.CONSTRUCTION_SPRITES_URI + key;
        can_construct = this.canBeConstructed(key, constr.level1.requirements);
        is_supported_by_us = amjad.layerMetaFromSprite("" + sprite_uri + "1") != null;
        if (can_construct && is_supported_by_us) {
          _results.push(this.available_constructions[key] = {
            sprite: "assets/sprites/" + sprite_uri + "1",
            type: key
          });
        } else if (can_construct && !is_supported_by_us) {
          continue;
        } else if (is_supported_by_us) {
          _results.push(this.available_constructions[key] = {
            sprite: sprite_uri + "_dis",
            type: "disabled"
          });
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    /*
    @action Constructs a building
    @string Building type
    */

    Constructions.prototype.construct = function(building) {
      var col, constr_xml, defer, layermeta, meta_uri, result, row,
        _this = this;
      constr_xml = XMLStore.get("constructions").city[building.type]["level1"];
      if (constr_xml == null) {
        alert("You can't construct this building");
        return;
      }
      meta_uri = amjad.splaySpriteURI("" + amjad.user.CONSTRUCTION_SPRITES_URI);
      layermeta = amjad.layerMetaFromSprite("" + meta_uri + building.type + "1");
      row = SOA.currentMap.tile_under.row;
      col = SOA.currentMap.tile_under.col;
      result = Rest.post("construction.start_build", {
        ty: building.type,
        x: row,
        y: col
      });
      defer = new $.Deferred();
      result.done(function(data) {
        var constr;
        constr = _this.addConstruction(building.type, data);
        if (constr != null) {
          delete _this.available_constructions[building.type];
          amjad.trigger("CONSTRUCTIONS_UPDATED");
          constr.addToCityMap();
          console.debug(constr);
          console.debug("ADDED TO CITY MAP");
          amjad.empire.resources.take(constr_xml.requirements.resources);
          return defer.resolve(constr);
        } else {
          return defer.reject("Construction couldn't be built");
        }
      });
      result.fail(function(err) {
        return defer.reject(err.responseText || err);
      });
      return defer.promise();
    };
    Constructions.loadTrainingGrounds = function(cn) {
      /* @jbg*/

      var hspruri, k, spr, unit, _i, _j, _len, _len1, _ref, _ref1, _ref2;
      cn.units = {};
      _ref = XMLStore.get("units");
      for (k in _ref) {
        unit = _ref[k];
        hspruri = "assets/sprites/amjad/units/" + amjad.user.architecture + "/44x44/" + k + ".png";
        spr = PIXI.TextureCache[hspruri];
        if ((unit.price != null) && (spr != null)) {
          cn.units[k] = unit;
        }
      }
      cn.beginUnitProduction = function(unit, res) {
        amjad.empire.resources.take(res);
        return cn.assignUnitProduction(unit);
      };
      cn.assignUnitProduction = function(unit) {
        cn.production.push(unit);
        cn.buildUnitPrototype(unit);
        return SOA.Hud.getDialog("BUILD_ORDERS").addToTrainingQueue(unit, "Training");
      };
      cn.buildUnitPrototype = function(unit) {
        unit.buildTime = function() {
          var time;
          time = XMLStore.units[unit.type].price.time;
          return 60 * (+0.3) * 1000;
        };
        unit.timeLeft = function() {
          var a, b;
          a = new Date(this.start_production);
          b = new Date();
          return this.buildTime() - (b - a);
        };
        unit.timeLeftPercentage = function() {
          return (1 - (this.timeLeft() / this.buildTime())).toFixed(2);
        };
        return unit.trainingEnds = function() {
          var a, b, c;
          a = new Date(this.start_production);
          b = new Date(this.start_production);
          a.setMilliseconds(a.getMilliseconds() + this.buildTime());
          c = b - a;
          return [a.toLocaleString(), c];
        };
      };
      if (cn.production === !void 0) {
        _ref1 = cn.production;
        for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
          unit = _ref1[_i];
          cn.buildUnitPrototype(unit);
        }
        _ref2 = cn.production;
        for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
          unit = _ref2[_j];
          SOA.Hud.getDialog("BUILD_ORDERS").addToTrainingQueue(unit, "Training");
        }
        return amjad.finishProduction(cn.construction_id).done(function(data) {}).fail(function() {
          return alert("omg. failed to finish unit production");
        });
      }
    };
    Constructions.loadTradeStation = function(station) {
      return station.goods = {};
    };
    Constructions.loadWar = function(war) {
      var un, _i, _len, _ref, _results;
      war.units = [];
      war.addUnit = function(un) {
        var f;
        f = (this.units.filter(function(unit) {
          return un.type === unit.type;
        }))[0];
        if (f != null) {
          f.amount += un.amount;
        } else {
          this.units.push(un);
        }
        return this.trigger("REFRESH_UNITS");
      };
      war.removeUnit = function(type, amnt) {
        var f, ind;
        f = (war.units.filter(function(unit) {
          return unit.type === type;
        }))[0];
        if (f != null) {
          f["amount"] -= amnt;
          if (f["amount"] <= 0) {
            ind = war.units.indexOf(f);
            war.units.splice(ind, 1);
          }
        }
        return this.trigger("REFRESH_UNITS");
      };
      _ref = amjad.empire.units;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        un = _ref[_i];
        _results.push(war.addUnit(un));
      }
      return _results;
    };
    Constructions.loadScience = function(cn) {
      /* @jbg*/

      var hspruri, k, science, spr, _i, _j, _len, _len1, _ref, _ref1, _ref2;
      _ref = XMLStore.get("science");
      for (k in _ref) {
        science = _ref[k];
        hspruri = "assets/sprites/amjad/science/{k}.png".trim();
        spr = PIXI.TextureCache[hspruri];
      }
      cn.beginScienceResearch = function(science, res) {
        amjad.empire.resources.take(res);
        return cn.assignScienceResearch(science);
      };
      cn.assignScienceResearch = function(science) {
        cn.production.push(science);
        cn.researchScience(science);
        return SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(science, "Research", true);
      };
      cn.researchScience = function(science) {
        science.buildTime = function() {
          var time;
          time = XMLStore.science[science.type].resources[science.level].time;
          return 60 * (+time + 0.1) * 1000;
        };
        science.timeLeft = function() {
          var a, b;
          a = new Date(this.start_research);
          b = new Date();
          return this.buildTime() - (b - a);
        };
        science.timeLeftPercentage = function() {
          return (1 - (this.timeLeft() / this.buildTime())).toFixed(2);
        };
        return science.researchEnds = function() {
          var a, b, c;
          a = new Date(this.start_production);
          b = new Date(this.start_production);
          a.setMilliseconds(a.getMilliseconds() + this.buildTime());
          c = b - a;
          return [a.toLocaleString(), c];
        };
      };
      if (cn.production === !void 0) {
        _ref1 = cn.production;
        for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
          science = _ref1[_i];
          cn.researchScience(science);
        }
        _ref2 = cn.production;
        for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
          science = _ref2[_j];
          SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(science, "Research");
        }
        return amjad.finishProduction(cn.construction_id).done(function(data) {}).fail(function() {
          return console.error("omg. failed to finish science research");
        });
      }
    };
    Constructions.loadTech = function(cn) {
      /* @jbg*/

      var hspruri, k, spr, tech, _i, _j, _len, _len1, _ref, _ref1, _ref2;
      _ref = XMLStore.get("technologies");
      for (k in _ref) {
        tech = _ref[k];
        hspruri = "assets/sprites/amjad/technologies/{k}.png".trim();
        spr = PIXI.TextureCache[hspruri];
      }
      cn.beginTechResearch = function(tech, res) {
        amjad.empire.resources.take(res);
        return cn.assignTechResearch(tech);
      };
      cn.assignTechResearch = function(tech) {
        cn.production.push(tech);
        cn.researchTech(tech);
        return SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(tech, "Research");
      };
      cn.researchTech = function(tech) {
        tech.buildTime = function() {
          var time;
          time = XMLStore.technologies[tech.type][tech.level].time;
          return 60 * (+time + 0.1) * 1000;
        };
        tech.timeLeft = function() {
          var a, b;
          a = new Date(this.start_research);
          b = new Date();
          return this.buildTime() - (b - a);
        };
        tech.timeLeftPercentage = function() {
          return (1 - (this.timeLeft() / this.buildTime())).toFixed(2);
        };
        return tech.researchEnds = function() {
          var a, b, c;
          a = new Date(this.start_production);
          b = new Date(this.start_production);
          a.setMilliseconds(a.getMilliseconds() + this.buildTime());
          c = b - a;
          return [a.toLocaleString(), c];
        };
      };
      if (cn.production === !void 0) {
        _ref1 = cn.production;
        for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
          tech = _ref1[_i];
          cn.researchTech(tech);
        }
        _ref2 = cn.production;
        for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
          tech = _ref2[_j];
          SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(tech, "Research");
        }
        return amjad.finishProduction(cn.construction_id).done(function(data) {}).fail(function() {
          return console.error("omg. failed to finish tech research");
        });
      }
    };
    return Constructions;
  });

}).call(this);
