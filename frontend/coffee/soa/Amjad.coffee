"use strict"

define [
    "soa/StructureLoader"
    "soa/XMLStore"
    "soa/User"
    "soa/Empire"
],

(
    StructureLoader, 
    XMLStore, 
    User, 
    Empire
) ->

    ShortMap =
        "us": [
            "user", 
            (meta) -> 
                user = new User()
                user.load(meta)
                return user
        ]
        "e": [
            "empire", 
            (meta) ->
                empire = new Empire()
                empire.load(meta)
                return empire
        ]

    class Amjad extends StructureLoader
        constructor: () ->
            ##console.debug "AMJAD LOADING"
            @loaded = false
            @selected_hero = null
            @current_map = null
            @moved_camera = false
            super(ShortMap)
        
        load: (meta) ->
            super(meta)
            
        initListeners: () ->
            super()
            @on "DONE_LOADING", () ->
                ##console.debug "AMJAD LOADED"
                @trigger "AMJAD_LOADED"
                @loaded = true

        layerMetaFromSprite: (spriteuri) ->
            return HAL.TileMetaStorage.findByName(
                @splaySpriteURI(spriteuri)
            )
        
        splaySpriteURI: (spriteuri) ->
            return spriteuri.replace(/\//g, "_")

        putConstructionInCity: (row, col, type, isPoi) ->
            if isPoi
                name = amjad.user.CONSTRUCTION_SPRITES_URI
            name = amjad.user.CONSTRUCTION_SPRITES_URI + type
            meta = @layerMetaFromSprite(name)
            if not meta?
                console.error "Meta file not found for #{name}"
                return
            if meta.sprite.indexOf("assets/sprites") isnt 0
                meta.sprite = "assets/sprites/#{meta.sprite}"
            res = SOA.City.map.addLayerToTileByID(row, col, meta.id)
            ##console.debug res
            if not res?
                console.debug "Couldn't add layer #{meta.name} on #{row}, #{col}"
            else
                console.debug "Construction #{type} placed on city map at #{row}, #{col}"
            return res

        putCityOnZone: (row, col, type, spriteuri = amjad.user.CONSTRUCTION_SPRITES_URI) ->
            name = spriteuri + type
            meta = @layerMetaFromSprite(name)
            if not meta?
                console.error "Meta file not found for #{name}"
                return
            res = SOA.Zone.map.addLayerSpriteToTile(row, col, meta.layer, "assets/sprites/#{meta.sprite}")
            if not res?
                ##console.debug "Couldn't add layer #{meta.name} on #{row}, #{col}"
            else
                ##console.debug "Construction #{type} placed on city map at #{row}, #{col}"
            return res

    Amjad::initialize = () ->
        return

    Amjad::finishProduction = (id) ->
        return (Rest.post "construction.finish_production", cid: id)

    Amjad::finishTechResearch = (type) ->
        return (Rest.post "settlement.finish_tech_research", ty: type)

    Amjad::finishScienceResearch = (type) ->
        return (Rest.post "settlement.finish_science_research", ty: type)

    return Amjad

