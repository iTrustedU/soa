"use strict"

html = 
"""
<div class="loader-dialog-container">
	<div id="flipping-panel" class="block panel">
		<div class="front loader-dialog gradient-background">
			<i id="new-flip-btn" class="fa fa-windows flip-btn">&nbsp;new map</i>
			<br><br>
			<div class="loader-combobox-container">
				<select class="loader-combobox" id="map-choice">
		            {{#each maplist}}
		            	<option value={{name}}>{{name}}</option>
		            {{/each}}
		        </select> 
		        <i id="map-delete" class="fa fa-times delete-btn"></i>
		        <i id="map-load" class="fa fa-refresh load-btn"></i>
	        </div>
		</div>

		<div class="back loader-dialog gradient-background">
			<i id="load-flip-btn" class="fa fa-windows flip-btn">&nbsp;load map</i>
			<br><br>
			<span>name:</span>
			<input id="map-name" class="stylish-input"></input>
			<br><br>
			<span>rows:</span>
			<input id="num-rows" class="stylish-input"></input>
			<br><br>
			<span>columns:</span>	 
			<input id="num-columns" class="stylish-input"></input>
			<br><br>
			<input id="map-create" class="loader-load-button" type="button" value="create"/>
		</div>
	</div>
</div>
"""

define ["EventDispatcher", "MapConfig"], 

(EventDispatcher, MapConfig) ->

	class LoaderDialog extends EventDispatcher
		constructor: () ->
			super()
			return @

	API =
		SAVE_MAP: "/maps/new"
		MAP_LIST: "/maps"
		DELETE_MAP: "/map/{0}/delete"
		DELETE_ALL: "/maps/delete"
		DELETE_MANY: "/maps/deletemany"
		LOAD_MAP: "/map/{0}/load"

	LoaderDialog::prologue = () ->
		return @loadMapList()

	# Initialize dialog once the map list is retrieved
	LoaderDialog::epilogue = () ->
		$load_btn 		= @html.find("#map-load")
		$create_btn 	= @html.find("#map-create")
		$delete_btn 	= @html.find("#map-delete")
		$map_name 		= @html.find("#map-name")
		$map_rows 		= @html.find("#num-rows")
		$map_columns 	= @html.find("#num-columns")
		$map_cbx 		= @html.find("#map-choice")
		$new_flip_btn 	= @html.find("#new-flip-btn")
		$load_flip_btn  = @html.find("#load-flip-btn")
		$panel 			= @html.find("#flipping-panel")
		@container 		= @html.find(".loader-dialog-container")

		$delete_btn.on "click", () =>
			$selected_map = $map_cbx.find("option:selected")
			$.post(API.DELETE_MAP.f($selected_map.text()))
			.done (map) =>
				$selected_map.remove()
				@trigger "MAP_DELETED", @, map

		$create_btn.on "click", () =>
			data =
				name: $map_name.val()
				rows: $map_rows.val()
				columns: $map_columns.val()
			@createMap(data)

		$load_btn.on "click", () =>
			selected_map = $map_cbx.find("option:selected").text()
			@loadSelectedMap(selected_map)

		$new_flip_btn.on "click", () =>
			$panel.addClass("flip")

		$load_flip_btn.on "click", () =>
			$panel.removeClass("flip")

	LoaderDialog::loadSelectedMap = (selected_map) ->
		$.get(API.LOAD_MAP.f(selected_map))
		.done (map) =>
			@trigger "MAP_LOADED", @, map

	LoaderDialog::showSpinner = () ->
		@spinner = $(Handlebars.partials["spinnerh"])
		@html.hide()
		$(HAL.Dom.viewport).append(@spinner)

	LoaderDialog::hideSpinner = () ->
		@spinner.remove()

	LoaderDialog::createNewBuffer = () ->
		return HAL.ImageUtils.createPixelBuffer(MapConfig.NN_COLUMNS, MapConfig.NN_ROWS, 0x00000000)
		
	# Creates new map and persists it in database
	LoaderDialog::createMap = (data) ->
		cols = data.columns
		rows = data.rows
		width = cols * (MapConfig.TILE_WIDTH * 0.5)
		height = rows * (MapConfig.TILE_HEIGHT)

		regsOnX = width / MapConfig.NN_COLUMNS
		regsOnY = height / MapConfig.NN_ROWS

		reg_width = MapConfig.NN_COLUMNS * (MapConfig.TILE_WIDTH * 0.5)
		reg_height = MapConfig.NN_ROWS * MapConfig.TILE_HEIGHT

		section_width = reg_width / 3
		section_height = reg_height / 3
		num_buffs = Math.ceil(MapConfig.MAX_LAYERS / 2)

		regions = {}
		x = 0
		y = 0
		indY = 0
		indX = 0
		
		buff = (@createNewBuffer() for buff in [0...num_buffs])

		while x < width
			while y < height
				key = indX+"_"+indY
				regions[key] = 
					x: x
					y: y
					row: indY
					col: indX
					sections: [
						{
							x: 0
							y: 0
						},
						{
							x: section_width
							y: 0
						},
						{
							x: 2*section_width
							y: 0
						},
						{
							x: 0
							y: section_height
						},
						{
							x: section_width
							y: section_height
						},
						{
							x: 2*section_width
							y: section_height
						},
						{
							x: 0
							y: 2*section_height
						},
						{
							x: section_width
							y: 2*section_height
						},
						{
							x: 2*section_width
							y: 2*section_height
						}
					]
				y += reg_height
				indY++
			indY = 0
			indX++
			y = 0
			x += reg_width

		data.region_width 	= reg_width
		data.region_height 	= reg_height
		data.width 			= width
		data.height 		= height
		data.regions 		= regions

		return $.post(API.SAVE_MAP, data)
		.done (data) =>
			@trigger "MAP_CREATED", @, data
			console.debug data
			@loadSelectedMap(data.name)

	# Load map list from database
	LoaderDialog::loadMapList = (defer) ->
		return $.get(API.MAP_LIST)
		.done (maplist) =>
			template = Handlebars.compile(html)
			@html = $(template(maplist: maplist))
			@epilogue()

	LoaderDialog::createMapRegions = (rows, cols) ->
		# create many of these

	return LoaderDialog
