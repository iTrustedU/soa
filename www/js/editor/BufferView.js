(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<table class=\"buffer-view\">\n  <tr>\n    <td><div id=\"0\"></div></td>\n    <td><div id=\"1\"></div></td>\n    <td><div id=\"2\"></div></td>\n  </tr>\n  <tr>\n    <td><div id=\"3\"></div></td>\n    <td><div id=\"4\"></div></td>\n    <td><div id=\"5\"></div></td>\n  </tr>\n  <tr>\n    <td><div id=\"6\"></div></td>\n    <td><div id=\"7\"></div></td>\n    <td><div id=\"8\"></div></td>\n  </tr>\n</table>";

  define(["EventDispatcher"], function(EventDispatcher) {
    var BufferView;
    BufferView = (function(_super) {
      __extends(BufferView, _super);

      function BufferView(map) {
        this.map = map;
        BufferView.__super__.constructor.call(this);
        this.init();
      }

      return BufferView;

    })(EventDispatcher);
    BufferView.prototype.init = function() {
      var rmngr,
        _this = this;
      this.html = $(html);
      rmngr = this.map.region_mngr;
      rmngr.on("SECTION_CHANGED", function() {
        console.debug("omg");
        _this.html.find("img").remove();
        return _this.update();
      });
      this.current_region = rmngr.current_region;
      return this.update();
    };
    BufferView.prototype.update = function() {
      var buffer, img, index, section, _i, _len, _ref, _results;
      _ref = this.current_region.sections;
      _results = [];
      for (index = _i = 0, _len = _ref.length; _i < _len; index = ++_i) {
        section = _ref[index];
        _results.push((function() {
          var _j, _len1, _ref1, _results1;
          _ref1 = section.buffers;
          _results1 = [];
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            buffer = _ref1[_j];
            img = HAL.ImageUtils.pixelBufferToImg(buffer);
            _results1.push(this.html.find("#" + index).append(img));
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };
    return BufferView;
  });

}).call(this);
