#!/bin/bash
x-terminal-emulator -d . -e `mongod --dbpath data/db/`
x-terminal-emulator -d . -e `nodemon server`
x-terminal-emulator -d . -e `grunt serve`