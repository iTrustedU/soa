"use strict"

define ["PIXI", "EventDispatcher", "Mat3", "Vec2", "MapManager", "MapConfig"], 

(PIXI, EventDispatcher, Mat3, Vec2, MapManager, MapConfig) ->
	
	class IsometricScene extends EventDispatcher
		constructor: (@name) ->
			super()
			@screen_size 	= Vec2.from(window.innerWidth, window.innerHeight)
			@map 			= new MapManager(@)
			return @

	IsometricScene::init = () ->
		@renderer 				= new PIXI.autoDetectRenderer(@screen_size.x, @screen_size.y, HAL.Dom.game)
		@stage 	  				= new PIXI.Stage(0xffffff)
		@world 					= new PIXI.Graphics()
		@camera 				= new PIXI.DisplayObjectContainer()
		@viewport_bounds 		= @renderer.view.getBoundingClientRect()
		@stage.interactive      = true
		@mpos 					= Vec2.from(0, 0)
		@zoom_bounds 			= Vec2.from(0.85, 1.0)
		@ratio 					= @screen_size.x / @screen_size.y
		@zoom_factor 			= 1.3
		@camera_speed			= 1.0
		@m_button 				= -1
		@drag_started			= false
		@paused					= false
		@world_pos				= Vec2.from(0, 0)
		@screen_center 			= Vec2.from(@screen_size.x*0.5, @screen_size.y*0.5)
		@drag_last_position 	= Vec2.from(0, 0)

		@drag_start_point 		= Vec2.clone(@screen_center)
		@world_inverse 			= Mat3.create()
		
		@camera.pivot.x 		= -@screen_center.x
		@camera.pivot.y 		= -@screen_center.y
		@camera.position.x 		= -@screen_center.x
		@camera.position.y 		= -@screen_center.y
		@camera_last_pos		= Vec2.from(@camera.position.x, @camera.position.y)

		@world_top_left 		= Vec2.from(0, 0)
		@world_bottom_right     = Vec2.clone(@screen_size)
		@mouse_sprites 			= []

		@mouse_listeners		=
			move: []
			left_click: []

		@camera.addChild(@world)
		@stage.addChild(@camera)
		@initEventListeners()
	
	IsometricScene::initEventListeners = () ->
		@stage.hitArea = new PIXI.Rectangle(0, 0, @screen_size.x, @screen_size.y)

		@stage.mousedown = (data) =>
			@m_button = data.originalEvent.button
			console.debug @m_button

		@stage.click = (data) =>
			if not @drag_started
				@trigger "MOUSE_CLICK", @, @m_button, @mpos.x, @mpos.y
			@drag_started = false
			if @m_button is 2
				@trigger "MOUSE_RIGHT_DRAG_ENDED"
			@m_button = -1

		@stage.mousemove = (data) =>
			@mpos.x = data.global.x
			@mpos.y = data.global.y

			if @m_button is 0 and not @drag_started
				@drag_started = true
				@drag_start_point.x = @mpos.x
				@drag_start_point.y = @mpos.y
				@drag_last_position.x = @camera.position.x/@camera_speed
				@drag_last_position.y = @camera.position.y/@camera_speed

			else if @m_button is 2 and not @drag_started
				@drag_started = true 
				@trigger "MOUSE_RIGHT_DRAG_STARTED", @, @mpos

			if @drag_started and @m_button is 0
				@is_dragged = true
				@onDrag()
				return
			else if @drag_started and @m_button is 2
				@is_dragged = true
				@trigger "MOUSE_RIGHT_DRAG", @, @mpos
				return

			translated = @toLocalObject(@mpos.x, @mpos.y)
			Vec2.set(@world_pos, translated.x, translated.y)

			@trigger "MOUSE_WORLD_CHANGED", @, @world_pos.x, @world_pos.y
			@trigger "MOUSE_MOVED", @, @mpos.x, @mpos.y

		window.addEventListener("resize", (evt) =>
			@viewport_bounds = @renderer.view.getBoundingClientRect()
			@screen_size.x = @viewport_bounds.width
			@screen_size.y = @viewport_bounds.height
			@renderer.resize(@viewport_bounds.width, @viewport_bounds.height)
			@stage.hitArea.width = @screen_size.x
			@stage.hitArea.height = @screen_size.y
			@map.updateHitAreaPosition()
			@map.updateHitAreaDimension()
		)

		window.addEventListener("mousewheel", (evt) =>
			return if @paused
			over = HAL.Dom.viewport.querySelectorAll(":hover")
			if over and over.length is 1
				@onZoom(evt.wheelDelta)
				@trigger "MOUSE_ZOOM", @world.scale.x
		)

		window.addEventListener("keyup", (ev) =>
			@trigger "KEY_UP", @, ev
		, false)

		window.addEventListener("keydown", (ev) =>
			@trigger "KEY_DOWN", @, ev
		, false)

	IsometricScene::pause = () ->
		@is_dragged = false
		@paused = true
		@stage.interactive = false

	IsometricScene::resume = () ->
		@paused = false
		@stage.interactive = true
	
	IsometricScene::centerOn = (pos) ->
		temp_vec1 = HAL.MathUtils.Vec2.from((pos.x + @screen_center.x) / @world.scale.x, (pos.y + @screen_center.y) / @world.scale.x)
		HAL.MathUtils.Vec2.transformMat3(temp_vec1, pos, @world.localTransform)
		@map.updateHitAreaPosition()
		@camera.position.x = -temp_vec1.x
		@camera.position.y = -temp_vec1.y
		@map.updateHitAreaDimension()

	IsometricScene::onDrag = (drag_x = @mpos.x, drag_y = @mpos.y) ->
		### @todo ugly clamping ###
		x = (@drag_last_position.x + drag_x - @drag_start_point.x)*@camera_speed
		y = (@drag_last_position.y + drag_y - @drag_start_point.y)*@camera_speed

		temp_vec1 = Vec2.from((x + @screen_center.x) / @world.scale.x, (y + @screen_center.y) / @world.scale.x)
		
		Vec2.transformMat3(@world_top_left, temp_vec1, @world.localTransform)
		
		if @world_top_left.x > 0
			x -= @world_top_left.x
		if @world_top_left.y > 0
			y -= @world_top_left.y

		temp_vec2 = Vec2.from((x - @screen_center.x) / @world.scale.x, (y - @screen_center.y) / @world.scale.x)
		Vec2.transformMat3(@world_top_left, temp_vec2, @world.localTransform)

		if @world_top_left.x < (-@map.world_bounds.x + MapConfig.TILE_WIDTH*0.5) * @world.scale.x
			x -= (@world_top_left.x + (@map.world_bounds.x - MapConfig.TILE_WIDTH*0.5) * @world.scale.x)
		if @world_top_left.y < (-@map.world_bounds.y + MapConfig.TILE_HEIGHT) * @world.scale.x
			y -= (@world_top_left.y + (@map.world_bounds.y - MapConfig.TILE_HEIGHT) * @world.scale.x)

		@camera.position.x = x
		@camera.position.y = y

		@camera.updateTransform()
		@world.updateTransform()
		@map.updateHitAreaPosition()

		Vec2.release(temp_vec1)
		Vec2.release(temp_vec2)

	IsometricScene::toLocalObject = (x, y, obj = @world) ->
		return PIXI.InteractionData.prototype.getLocalPosition.call(
			global:
				x: x,
				y: y
		, obj)

	IsometricScene::onZoom = (direction) ->

		### @todo no need to calculate all of these if scaling is uniform, which it is now ###
		if direction > 0
			z_x = @world.scale.x * @zoom_factor
			z_y = @world.scale.y * @zoom_factor
		else 
			z_x = @world.scale.x / @zoom_factor
			z_y = @world.scale.y / @zoom_factor
		c_x = HAL.MathUtils.clamp(z_x, @zoom_bounds.x, @zoom_bounds.y)
		c_y = HAL.MathUtils.clamp(z_y, @zoom_bounds.x, @zoom_bounds.y)
		
		x = @camera.position.x
		y = @camera.position.y
		where_x = 0
		where_y = 0
		Vec2.transformMat3(@world_top_left, [(x + @screen_center.x) / c_x, (y + @screen_center.y) / c_x], @world.localTransform)
		if @world_top_left.x > 0
			where_x = -@world_top_left.x
		if @world_top_left.y > 0
			where_y = -@world_top_left.y
		loc = @toLocalObject(@screen_center.x, @screen_center.y)

		@world.scale.x = c_x
		@world.scale.y = c_y
		@map.updateHitAreaPosition()
		@map.updateHitAreaDimension()
		@camera.position.x = where_x
		@camera.position.y = where_y
		@world.pivot.x = loc.x
		@world.pivot.y = loc.y

	IsometricScene::update = () ->
		return
		### process the array of update-able objects ###

	IsometricScene::draw = () ->
		@renderer.render(@stage)

	IsometricScene::attachSpriteToMouse = (sprite) ->
		@mouse_sprites.push(sprite)
		@world.addChild(sprite)
		sprite.anchor.y = 1
		list = @on "MOUSE_WORLD_CHANGED", (x, y) ->
			for sprite in @mouse_sprites
				sprite.position.x = x
				sprite.position.y = y
		@mouse_listeners.move.push(list)

	IsometricScene::detachSpriteFromMouse = (sprite) ->
		ind = @mouse_sprites.indexOf(sprite)
		@mouse_sprites = @mouse_sprites.slice(ind, 1)
		@world.removeChild(sprite)

	### Rendering entry point ###
	cur_time            = performance.now()
	delta               = 0
	fps_trigger_time    = 1
	cur_fps_time        = 0
	fps_counter         = 0
	last_frame_id       = null
	prev_time           = 0
	ticks_per_second    = 30
	max_frameskip       = 5
	skip_ticks          = 1000 / ticks_per_second
	loops               = 0
	interpolant         = 0
	next_game_tick      = performance.now()
	fps_acc 			= 0
	frameID 			= 0
	currentScene		= null

	IsometricScene.stopRendering = (scene) ->
		if currentScene isnt scene
			throw new Error("The scene is not being rendered already")
		cancelAnimationFrame(frameID)
		scene.renderer.view.style["display"] = "none"
		
	IsometricScene.startRendering = (scene) ->
		if frameID
			cancelAnimationFrame(frameID)
		scene.renderer.view.style["display"] = ""
		render = () ->
			frameID = requestAnimFrame(render)
			scene.draw()
		render()
		currentScene = scene

	return IsometricScene

# REQUEST ANIMATION FRAME AND INTERPOLATE
# loops = 0
# setTimeout(() ->
# REQUEST FRAME
# prev_time = cur_time
# if Math.random() > 0.9 
# 	scene.update()
# while (performance.now() > next_game_tick and loops < max_frameskip)
#	next_game_tick += skip_ticks 
#	loops++
# interpolant = (performance.now() + skip_ticks - next_game_tick)*0.01/skip_ticks
# ##console.debug(interpolant)
# fps_counter++

# DRAW CALLS
# cur_time = performance.now()
# delta = (cur_time - prev_time) * 0.001
# fps_acc += delta
# if fps_acc > 1
# 	scene.trigger "FPS_UPDATED", fps_counter
# 	fps_counter = 0
# 	fps_acc = 0
# scene.init()