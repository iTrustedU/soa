"use strict"

define [], () ->
	BAD_STATUS_CODES = [422, 400, 500]

	class LoggingQueue
		constructor: () ->
			@errors 	= []
			@warnings 	= []
			@debugs 	= []
			@infos 		= []
			@init()

	LoggingQueue::init = () ->
		# Logs all errors
		# window.onerror = (message, url, line_no, column, errobj) =>
		# 	@error(
		# 		msg: message
		# 		url: url
		# 		line: line_no
		# 		stack: if errobj then errobj.stack else "n/a"
		# 	)
		# 	return true
		# Handles ajax errors
		@setupAjaxHandler() if $?

	LoggingQueue::setupAjaxHandler = () ->
		$(document)
		.ajaxSuccess (event, xhr, settings) =>
			msg = "{0} call to {1}; status: {2}"
			.f(
				settings.type
				settings.url
				xhr.status
			)
			@info(msg)

		$(document)
		.ajaxError (event, xhr, settings) =>
			msg = "{0} to {1}; msg: {2}; status: {3}"
			.f(
				settings.type
				settings.url
				xhr.responseText
				xhr.status
			)
			throw new Error(msg)

	LoggingQueue::debug = (msg) ->
		#@todo if debug mode...
		@debugs.push(msg)

	LoggingQueue::warn = (msg) ->
		@warnings.push(msg)

	LoggingQueue::error = (msg) ->
		@errors.push(msg)

	LoggingQueue::info = (msg) ->
		@infos.push(msg)

	return LoggingQueue