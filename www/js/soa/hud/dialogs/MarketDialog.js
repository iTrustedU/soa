(function() {
  "use strict";
  var $current_trade, $trade_box, html;

  html = "<div class=\"market-tent draggable\">\n	<div class=\"dialog-name\">\n		<span>Market Tent</span>\n		<div class=\"close-button\"></div>\n	</div>\n	{{{trade_box}}}\n	<div class=\"port-building-info\">\n		<img src=\"img/bottom-middle/buildings/trade.png\" class=\"dialog-building-img\" />\n		<div class=\"upgrade-building\">Upgrade</div>\n		<div id=\"move-building\" class=\"upgrade-building move-button\">Move</div>\n	</div>\n	{{{current_trade}}}\n</div>";

  $trade_box = "<div class=\"trade-boxes\">\n	<div class=\"market-tent-name-1\">\n		<span>Server</span>\n	</div>\n	<div class=\"market-tent-name-2\">\n		<span>Player</span>\n	</div>\n	<div class=\"server-items\">\n		<div class=\"item-box ietm-box-1\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-2\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-3\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-4\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-5\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n	</div>\n	<div class=\"player-items\">\n		<div class=\"item-box ietm-box-1\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-2\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-3\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-4\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n		<div class=\"item-box ietm-box-5\">\n			<img src=\"{{concat_sprite building}}\" />\n			<input type=\"text\" value={{price}} />\n		</div>\n	</div>\n</div>";

  $current_trade = "<div class=\"current-trade\">\n	<div class=\"item-box ietm-box-6\">\n		<img src=\"{{concat_sprite building}}\" />\n		<input type=\"text\" value={{price}} />\n	</div>\n	<div class=\"item-box ietm-box-7\">\n		<img src=\"{{concat_sprite building}}\" />\n		<input type=\"text\" value={{price}} />\n	</div>\n	<div class=\"market-tent-slider-box\">\n		<span class=\"min-max\">Min</span>\n		<div class=\"market-tent-slider\">\n			<div class=\"slider-bar-l\"></div>\n			<div class=\"slider-bar-r\"></div>\n			<div class=\"slider-bar-middle\">\n				<div class=\"slider\"></div>\n			</div>\n		</div>\n		<span class=\"min-max\">Max</span>\n	</div>  \n</div>";

  define(["jquery"], function($) {
    var $MarketDialog, ct, mk, template, trade_box;
    trade_box = {
      building: "buildings/war",
      price: 150
    };
    template = Handlebars.compile($trade_box);
    mk = template(trade_box);
    template = Handlebars.compile($current_trade);
    ct = template(trade_box);
    template = Handlebars.compile(html);
    $MarketDialog = $(template({
      trade_box: mk,
      current_trade: ct
    }));
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $(domlayer).append($MarketDialog);
    });
    return Hal.on("OPEN_MARKET_DIALOG", function(market) {
      return $MarketDialog.show();
    });
  });

}).call(this);
