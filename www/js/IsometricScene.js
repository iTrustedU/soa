(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["PIXI", "EventDispatcher", "Mat3", "Vec2", "MapManager", "MapConfig"], function(PIXI, EventDispatcher, Mat3, Vec2, MapManager, MapConfig) {
    var IsometricScene, cur_fps_time, cur_time, currentScene, delta, fps_acc, fps_counter, fps_trigger_time, frameID, interpolant, last_frame_id, loops, max_frameskip, next_game_tick, prev_time, skip_ticks, ticks_per_second;
    IsometricScene = (function(_super) {
      __extends(IsometricScene, _super);

      function IsometricScene(name) {
        this.name = name;
        IsometricScene.__super__.constructor.call(this);
        this.screen_size = Vec2.from(window.innerWidth, window.innerHeight);
        this.map = new MapManager(this);
        return this;
      }

      return IsometricScene;

    })(EventDispatcher);
    IsometricScene.prototype.init = function() {
      this.renderer = new PIXI.autoDetectRenderer(this.screen_size.x, this.screen_size.y, HAL.Dom.game);
      this.stage = new PIXI.Stage(0xffffff);
      this.world = new PIXI.Graphics();
      this.camera = new PIXI.DisplayObjectContainer();
      this.viewport_bounds = this.renderer.view.getBoundingClientRect();
      this.stage.interactive = true;
      this.mpos = Vec2.from(0, 0);
      this.zoom_bounds = Vec2.from(0.85, 1.0);
      this.ratio = this.screen_size.x / this.screen_size.y;
      this.zoom_factor = 1.3;
      this.camera_speed = 1.0;
      this.m_button = -1;
      this.drag_started = false;
      this.paused = false;
      this.world_pos = Vec2.from(0, 0);
      this.screen_center = Vec2.from(this.screen_size.x * 0.5, this.screen_size.y * 0.5);
      this.drag_last_position = Vec2.from(0, 0);
      this.drag_start_point = Vec2.clone(this.screen_center);
      this.world_inverse = Mat3.create();
      this.camera.pivot.x = -this.screen_center.x;
      this.camera.pivot.y = -this.screen_center.y;
      this.camera.position.x = -this.screen_center.x;
      this.camera.position.y = -this.screen_center.y;
      this.camera_last_pos = Vec2.from(this.camera.position.x, this.camera.position.y);
      this.world_top_left = Vec2.from(0, 0);
      this.world_bottom_right = Vec2.clone(this.screen_size);
      this.mouse_sprites = [];
      this.mouse_listeners = {
        move: [],
        left_click: []
      };
      this.camera.addChild(this.world);
      this.stage.addChild(this.camera);
      return this.initEventListeners();
    };
    IsometricScene.prototype.initEventListeners = function() {
      var _this = this;
      this.stage.hitArea = new PIXI.Rectangle(0, 0, this.screen_size.x, this.screen_size.y);
      this.stage.mousedown = function(data) {
        _this.m_button = data.originalEvent.button;
        return console.debug(_this.m_button);
      };
      this.stage.click = function(data) {
        if (!_this.drag_started) {
          _this.trigger("MOUSE_CLICK", _this, _this.m_button, _this.mpos.x, _this.mpos.y);
        }
        _this.drag_started = false;
        if (_this.m_button === 2) {
          _this.trigger("MOUSE_RIGHT_DRAG_ENDED");
        }
        return _this.m_button = -1;
      };
      this.stage.mousemove = function(data) {
        var translated;
        _this.mpos.x = data.global.x;
        _this.mpos.y = data.global.y;
        if (_this.m_button === 0 && !_this.drag_started) {
          _this.drag_started = true;
          _this.drag_start_point.x = _this.mpos.x;
          _this.drag_start_point.y = _this.mpos.y;
          _this.drag_last_position.x = _this.camera.position.x / _this.camera_speed;
          _this.drag_last_position.y = _this.camera.position.y / _this.camera_speed;
        } else if (_this.m_button === 2 && !_this.drag_started) {
          _this.drag_started = true;
          _this.trigger("MOUSE_RIGHT_DRAG_STARTED", _this, _this.mpos);
        }
        if (_this.drag_started && _this.m_button === 0) {
          _this.is_dragged = true;
          _this.onDrag();
          return;
        } else if (_this.drag_started && _this.m_button === 2) {
          _this.is_dragged = true;
          _this.trigger("MOUSE_RIGHT_DRAG", _this, _this.mpos);
          return;
        }
        translated = _this.toLocalObject(_this.mpos.x, _this.mpos.y);
        Vec2.set(_this.world_pos, translated.x, translated.y);
        _this.trigger("MOUSE_WORLD_CHANGED", _this, _this.world_pos.x, _this.world_pos.y);
        return _this.trigger("MOUSE_MOVED", _this, _this.mpos.x, _this.mpos.y);
      };
      window.addEventListener("resize", function(evt) {
        _this.viewport_bounds = _this.renderer.view.getBoundingClientRect();
        _this.screen_size.x = _this.viewport_bounds.width;
        _this.screen_size.y = _this.viewport_bounds.height;
        _this.renderer.resize(_this.viewport_bounds.width, _this.viewport_bounds.height);
        _this.stage.hitArea.width = _this.screen_size.x;
        _this.stage.hitArea.height = _this.screen_size.y;
        _this.map.updateHitAreaPosition();
        return _this.map.updateHitAreaDimension();
      });
      window.addEventListener("mousewheel", function(evt) {
        var over;
        if (_this.paused) {
          return;
        }
        over = HAL.Dom.viewport.querySelectorAll(":hover");
        if (over && over.length === 1) {
          _this.onZoom(evt.wheelDelta);
          return _this.trigger("MOUSE_ZOOM", _this.world.scale.x);
        }
      });
      window.addEventListener("keyup", function(ev) {
        return _this.trigger("KEY_UP", _this, ev);
      }, false);
      return window.addEventListener("keydown", function(ev) {
        return _this.trigger("KEY_DOWN", _this, ev);
      }, false);
    };
    IsometricScene.prototype.pause = function() {
      this.is_dragged = false;
      this.paused = true;
      return this.stage.interactive = false;
    };
    IsometricScene.prototype.resume = function() {
      this.paused = false;
      return this.stage.interactive = true;
    };
    IsometricScene.prototype.centerOn = function(pos) {
      var temp_vec1;
      temp_vec1 = HAL.MathUtils.Vec2.from((pos.x + this.screen_center.x) / this.world.scale.x, (pos.y + this.screen_center.y) / this.world.scale.x);
      HAL.MathUtils.Vec2.transformMat3(temp_vec1, pos, this.world.localTransform);
      this.map.updateHitAreaPosition();
      this.camera.position.x = -temp_vec1.x;
      this.camera.position.y = -temp_vec1.y;
      return this.map.updateHitAreaDimension();
    };
    IsometricScene.prototype.onDrag = function(drag_x, drag_y) {
      var temp_vec1, temp_vec2, x, y;
      if (drag_x == null) {
        drag_x = this.mpos.x;
      }
      if (drag_y == null) {
        drag_y = this.mpos.y;
      }
      /* @todo ugly clamping*/

      x = (this.drag_last_position.x + drag_x - this.drag_start_point.x) * this.camera_speed;
      y = (this.drag_last_position.y + drag_y - this.drag_start_point.y) * this.camera_speed;
      temp_vec1 = Vec2.from((x + this.screen_center.x) / this.world.scale.x, (y + this.screen_center.y) / this.world.scale.x);
      Vec2.transformMat3(this.world_top_left, temp_vec1, this.world.localTransform);
      if (this.world_top_left.x > 0) {
        x -= this.world_top_left.x;
      }
      if (this.world_top_left.y > 0) {
        y -= this.world_top_left.y;
      }
      temp_vec2 = Vec2.from((x - this.screen_center.x) / this.world.scale.x, (y - this.screen_center.y) / this.world.scale.x);
      Vec2.transformMat3(this.world_top_left, temp_vec2, this.world.localTransform);
      if (this.world_top_left.x < (-this.map.world_bounds.x + MapConfig.TILE_WIDTH * 0.5) * this.world.scale.x) {
        x -= this.world_top_left.x + (this.map.world_bounds.x - MapConfig.TILE_WIDTH * 0.5) * this.world.scale.x;
      }
      if (this.world_top_left.y < (-this.map.world_bounds.y + MapConfig.TILE_HEIGHT) * this.world.scale.x) {
        y -= this.world_top_left.y + (this.map.world_bounds.y - MapConfig.TILE_HEIGHT) * this.world.scale.x;
      }
      this.camera.position.x = x;
      this.camera.position.y = y;
      this.camera.updateTransform();
      this.world.updateTransform();
      this.map.updateHitAreaPosition();
      Vec2.release(temp_vec1);
      return Vec2.release(temp_vec2);
    };
    IsometricScene.prototype.toLocalObject = function(x, y, obj) {
      if (obj == null) {
        obj = this.world;
      }
      return PIXI.InteractionData.prototype.getLocalPosition.call({
        global: {
          x: x,
          y: y
        }
      }, obj);
    };
    IsometricScene.prototype.onZoom = function(direction) {
      /* @todo no need to calculate all of these if scaling is uniform, which it is now*/

      var c_x, c_y, loc, where_x, where_y, x, y, z_x, z_y;
      if (direction > 0) {
        z_x = this.world.scale.x * this.zoom_factor;
        z_y = this.world.scale.y * this.zoom_factor;
      } else {
        z_x = this.world.scale.x / this.zoom_factor;
        z_y = this.world.scale.y / this.zoom_factor;
      }
      c_x = HAL.MathUtils.clamp(z_x, this.zoom_bounds.x, this.zoom_bounds.y);
      c_y = HAL.MathUtils.clamp(z_y, this.zoom_bounds.x, this.zoom_bounds.y);
      x = this.camera.position.x;
      y = this.camera.position.y;
      where_x = 0;
      where_y = 0;
      Vec2.transformMat3(this.world_top_left, [(x + this.screen_center.x) / c_x, (y + this.screen_center.y) / c_x], this.world.localTransform);
      if (this.world_top_left.x > 0) {
        where_x = -this.world_top_left.x;
      }
      if (this.world_top_left.y > 0) {
        where_y = -this.world_top_left.y;
      }
      loc = this.toLocalObject(this.screen_center.x, this.screen_center.y);
      this.world.scale.x = c_x;
      this.world.scale.y = c_y;
      this.map.updateHitAreaPosition();
      this.map.updateHitAreaDimension();
      this.camera.position.x = where_x;
      this.camera.position.y = where_y;
      this.world.pivot.x = loc.x;
      return this.world.pivot.y = loc.y;
    };
    IsometricScene.prototype.update = function() {
      /* process the array of update-able objects*/

    };
    IsometricScene.prototype.draw = function() {
      return this.renderer.render(this.stage);
    };
    IsometricScene.prototype.attachSpriteToMouse = function(sprite) {
      var list;
      this.mouse_sprites.push(sprite);
      this.world.addChild(sprite);
      sprite.anchor.y = 1;
      list = this.on("MOUSE_WORLD_CHANGED", function(x, y) {
        var _i, _len, _ref, _results;
        _ref = this.mouse_sprites;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          sprite = _ref[_i];
          sprite.position.x = x;
          _results.push(sprite.position.y = y);
        }
        return _results;
      });
      return this.mouse_listeners.move.push(list);
    };
    IsometricScene.prototype.detachSpriteFromMouse = function(sprite) {
      var ind;
      ind = this.mouse_sprites.indexOf(sprite);
      this.mouse_sprites = this.mouse_sprites.slice(ind, 1);
      return this.world.removeChild(sprite);
    };
    /* Rendering entry point*/

    cur_time = performance.now();
    delta = 0;
    fps_trigger_time = 1;
    cur_fps_time = 0;
    fps_counter = 0;
    last_frame_id = null;
    prev_time = 0;
    ticks_per_second = 30;
    max_frameskip = 5;
    skip_ticks = 1000 / ticks_per_second;
    loops = 0;
    interpolant = 0;
    next_game_tick = performance.now();
    fps_acc = 0;
    frameID = 0;
    currentScene = null;
    IsometricScene.stopRendering = function(scene) {
      if (currentScene !== scene) {
        throw new Error("The scene is not being rendered already");
      }
      cancelAnimationFrame(frameID);
      return scene.renderer.view.style["display"] = "none";
    };
    IsometricScene.startRendering = function(scene) {
      var render;
      if (frameID) {
        cancelAnimationFrame(frameID);
      }
      scene.renderer.view.style["display"] = "";
      render = function() {
        frameID = requestAnimFrame(render);
        return scene.draw();
      };
      render();
      return currentScene = scene;
    };
    return IsometricScene;
  });

}).call(this);
