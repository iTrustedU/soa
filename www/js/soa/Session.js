(function() {
  "use strict";
  define(["soa/Rest", "jquery.cookie"], function(Rest, _) {
    var Session;
    Session = (function() {
      function Session() {
        this.sessKey = $.cookie("sheiksofblabla");
        Rest.setSessionKey(this.sessKey);
      }

      Session.prototype.hasKey = function() {
        var val;
        val = (this.sessKey != null) && this.sessKey !== "undefined";
        return val;
      };

      Session.prototype.setKey = function(sessKey) {
        this.sessKey = sessKey;
        $.cookie("sheiksofblabla", this.sessKey, {
          expires: 1
        });
        return Rest.setSessionKey(this.sessKey);
      };

      Session.prototype.getSessionKey = function() {
        return this.sessKey;
      };

      Session.prototype.trySessionLogin = function() {
        var $deferred,
          _this = this;
        $deferred = new $.Deferred();
        if (this.sessKey == null) {
          $deferred.reject("No login session mate");
        } else {
          Rest.get("user.session_login", {
            session: this.sessKey
          }).done(function(data) {
            return $deferred.resolve(data);
          }).fail(function(data) {
            return $deferred.reject("Login with a session failed");
          });
        }
        return $deferred.promise();
      };

      Session.prototype.tryLogin = function(usn, pass) {
        var $deferred,
          _this = this;
        $deferred = new $.Deferred();
        Rest.get("user.login", {
          "usn": usn,
          "pass": pass
        }).done(function(data) {
          _this.setKey(data.session);
          return $deferred.resolve();
        }).fail(function(data) {
          return $deferred.reject("Login failed mate");
        });
        return $deferred.promise();
      };

      Session.prototype.resetSessionCookie = function() {
        this.sessKey = $.cookie("", null);
        return Rest.setSessionKey(this.sessKey);
      };

      return Session;

    })();
    return Session;
  });

}).call(this);
