"use strict"

define ["soa/constructions/Construction"],

(Construction) ->

	class TradeStation extends Construction


	TradeStation::init = () ->
		@goods = {}
		@traders = []

		# fetches all suported resources (the ones with sprite support)
		for k, good of XMLStore.get "tradegoods"
			guri = "assets/sprites/amjad/resources/#{k}".trim()
			spr = PIXI.TextureCache[guri]
			if spr?
				@goods[k] = good

	TradeStation::epilogue = () ->
		for trader in amjad.empire.heroes.traders
			@addTrader(trader)

	TradeStation::addTrader = (trader) ->
		if trader.isInOwnCity()
			@traders.push trader
	
	return TradeStation