(function() {
  "use strict";
  define([], function() {
    var ImageUtils;
    ImageUtils = (function() {
      function ImageUtils() {
        this.hit_ctx = this.createCanvas(1, 1).getContext("2d");
        /* 
        			 @todo 
        				Ovo treba biti maks velicine
        */

      }

      ImageUtils.prototype.createCanvas = function(w, h) {
        var canvas;
        canvas = document.createElement("canvas");
        canvas.width = w;
        canvas.height = h;
        return canvas;
      };

      ImageUtils.prototype.clipImage = function(img, area) {
        var canvas, ctx;
        canvas = this.createCanvas(area.w, area.h);
        ctx = canvas.getContext("2d");
        ctx.drawImage(img, area.x, area.y, area.w, area.h, 0, 0, area.w, area.h);
        img = new Image();
        img.src = canvas.toDataURL("image/png");
        return img;
      };

      ImageUtils.prototype.isTransparent = function(img, x, y) {
        var data;
        this.hit_ctx.drawImage(img, x, y, 1, 1, 0, 0, 1, 1);
        data = this.hit_ctx.getImageData(0, 0, 1, 1).data;
        this.hit_ctx.clearRect(0, 0, 1, 1);
        return data[3] < 150;
      };

      ImageUtils.prototype.getPixelAtHitImg = function(img, x, y) {
        var data, pos;
        this.hit_ctx.drawImage(img, x, y, 1, 1, 0, 0, 1, 1);
        data = this.hit_ctx.getImageData(0, 0, 1, 1).data;
        pos = (x + y) * 4;
        return [data[pos], data[pos + 1], data[pos + 2], data[pos + 3]];
      };

      ImageUtils.prototype.putPixelToBuffer = function(buffer, x, y, color_pack) {
        var pos;
        pos = (x + buffer.width * y) * 4;
        console.debug(color_pack);
        buffer.data[pos] = color_pack[0];
        buffer.data[pos + 1] = color_pack[1];
        buffer.data[pos + 2] = color_pack[2];
        buffer.data[pos + 3] = 0xFF;
        return buffer;
      };

      ImageUtils.prototype.getPixelAtBuffer = function(buffer, x, y) {
        var pos;
        pos = (x + buffer.width * y) * 4;
        return [buffer.data[pos], buffer.data[pos + 1], buffer.data[pos + 2], buffer.data[pos + 3]];
      };

      ImageUtils.prototype.hexToRGBString = function(hex) {
        var blue, green, red;
        blue = hex >> 16 & 0xFF;
        green = hex >> 8 & 0xFF;
        red = hex & 0xFF;
        return "rgba(" + red + "," + green + "," + blue + ",1)";
      };

      ImageUtils.prototype.hexToRGBAString = function(hex) {
        var alpha, blue, green, red;
        red = hex >> 24 & 0xFF;
        green = hex >> 16 & 0xFF;
        blue = hex >> 8 & 0xFF;
        alpha = hex & 0xFF;
        return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")";
      };

      ImageUtils.prototype.putPixelAtImg = function(img, color) {};

      ImageUtils.prototype.savePixelBufferToImg = function(buffer) {};

      ImageUtils.prototype.readPixelBufferFromImg = function(image) {};

      ImageUtils.prototype.readPixelBufferFromCanvas = function(canvas) {};

      ImageUtils.prototype.createPixelBuffer = function(w, h, fill_color) {
        var canvas, ctx;
        if (fill_color == null) {
          fill_color = 0xFFFFFFFF;
        }
        canvas = this.createCanvas(w, h);
        ctx = canvas.getContext("2d");
        ctx.fillStyle = this.hexToRGBAString(fill_color);
        ctx.fillRect(0, 0, w, h);
        return ctx.getImageData(0, 0, w, h);
      };

      ImageUtils.prototype.pixelBufferToImg = function(buffer) {
        var canvas, ctx, img;
        canvas = this.createCanvas(buffer.width, buffer.height);
        ctx = canvas.getContext("2d");
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.putImageData(buffer, 0, 0);
        img = new Image();
        img.src = canvas.toDataURL("image/png", 1);
        return img;
      };

      ImageUtils.prototype.encode64Buffer = function(buffer) {
        var canvas, ctx;
        canvas = this.createCanvas(buffer.width, buffer.height);
        ctx = canvas.getContext("2d");
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.putImageData(buffer, 0, 0);
        return canvas.toDataURL("image/png", 1);
      };

      ImageUtils.prototype.imgToPixelBuffer = function(img) {
        var canvas, ctx;
        canvas = this.createCanvas(img.width, img.height);
        ctx = canvas.getContext("2d");
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;
        ctx.drawImage(img, 0, 0);
        return ctx.getImageData(0, 0, img.width, img.height);
      };

      ImageUtils.prototype.base64ToPixelBuffer = function(base64data) {
        var img;
        img = new Image();
        img.src = "data:image/png;base64," + base64data;
        return this.imgToPixelBuffer(img);
      };

      ImageUtils.prototype.colorChannelToInt = function(channel) {
        var alpha, blue, green, red;
        red = channel[0];
        green = channel[1];
        blue = channel[2];
        return alpha = channel[3] != null ? channel[3] : 255;
      };

      ImageUtils.prototype.tintImage = function(img, color, opacity) {
        var tint_buff, tint_ctx;
        console.warn("Not implemented");
        return;
        tint_buff = this.createCanvas(img.width, img.height);
        tint_ctx = tint_buff.getContext("2d");
        tint_ctx.globalAlpha = 1.0;
        tint_ctx.drawImage(img, 0, 0);
        tint_ctx.globalAlpha = opacity;
        tint_ctx.globalCompositeOperation = 'source-atop';
        tint_ctx.fillStyle = color;
        tint_ctx.fillRect(0, 0, img.width, img.height);
        return tint_buff;
      };

      return ImageUtils;

    })();
    return ImageUtils;
  });

}).call(this);
