(function() {
  var MapModel, Region, mongoose, schema;

  mongoose = require("mongoose");

  Region = require("./Region");

  schema = mongoose.Schema({
    name: {
      type: String,
      required: true,
      unique: true
    },
    date_created: {
      type: Date,
      "default": Date.now
    },
    rows: {
      type: Number,
      required: true
    },
    columns: {
      type: Number,
      required: true
    },
    regions: {
      type: mongoose.Schema.Types.Mixed
    },
    width: {
      type: Number,
      required: true
    },
    height: {
      type: Number,
      required: true
    },
    region_width: {
      type: Number,
      required: true
    },
    region_height: {
      type: Number,
      required: true
    },
    tiles: [
      {
        name: String,
        layer: Number,
        sprite: String,
        seedId: Number,
        span: String
      }
    ]
  });

  module.exports = MapModel = mongoose.model("Map", schema);

}).call(this);
