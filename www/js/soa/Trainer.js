(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/Hero"], function(Hero) {
    var Trainer, _ref;
    Trainer = (function(_super) {
      __extends(Trainer, _super);

      function Trainer() {
        _ref = Trainer.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      Trainer.prototype.initListeners = function() {};

      return Trainer;

    })(Hero);
    return Trainer;
  });

}).call(this);
