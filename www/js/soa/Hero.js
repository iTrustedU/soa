(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader", "Geometry"], function(StructureLoader, Geometry) {
    var Hero, TILE_SENSE_RADIUS, _ref;
    TILE_SENSE_RADIUS = 3;
    Hero = (function(_super) {
      __extends(Hero, _super);

      function Hero() {
        _ref = Hero.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return Hero;

    })(StructureLoader);
    Hero.prototype.init = function() {
      this.EXPERIENCE_MODIFIER = 1000;
      this.SPEED_PER_TILE = 1;
      this.currentOrientation = "south";
      this.move_type = "";
      this.currentTile = null;
      return Hero.__super__.init.call(this);
    };
    Hero.prototype.initListeners = function() {
      var _this = this;
      Hero.__super__.initListeners.call(this);
      this.on("DONE_LOADING", function() {
        this.loadAnimations();
        if ((SOA.Settlement != null) && (SOA.Settlement.layer != null)) {
          return this.placeMeSomewhere();
        }
      });
      return HAL.on("MY_SETTLEMENT_LOADED", function() {
        return _this.placeMeSomewhere();
      });
    };
    Hero.prototype.placeMeSomewhere = function() {
      return this.putNearSettlement();
    };
    Hero.prototype.calcArrivalTime = function(fromX, fromY) {
      var distance;
      if (fromX == null) {
        fromX = this.o_coord_x;
      }
      if (fromY == null) {
        fromY = this.o_coord_y;
      }
      distance = Math.round(Math.sqrt(Math.pow(this.d_coord_x - fromX, 2) + Math.pow(this.d_coord_y - fromY, 2)));
      distance *= 2;
      return distance;
    };
    Hero.prototype.putNearSettlement = function() {
      var _this = this;
      this.layer = SOA.Settlement.putHeroNearby(this.getCurrentAnimation());
      if (this.move_type === "rest") {
        this.layer.changeParentTile(SOA.Zone.map.getTile(this.o_coord_x, this.o_coord_y));
      } else if (this.move_type === "") {
        this.o_coord_x = this.layer.parentTile.row;
        this.o_coord_y = this.layer.parentTile.col;
      }
      this.layer.onLeftClick = function() {
        var frame;
        SOA.getDialog("HEROES_QUEUE").html.find(".hero-frame").removeClass("hero-frame-a");
        frame = SOA.getDialog("HEROES_QUEUE").html.find("#" + _this._type_ + "-" + _this.id);
        frame.addClass("hero-frame-a");
        return SOA.setSelectedHero(_this);
      };
      if (this.arrival != null) {
        return this.moveTo(this.d_coord_x, this.d_coord_y);
      } else {
        this.moveTo(this.o_coord_x, this.o_coord_y);
        return this.finishArriving();
      }
    };
    Hero.prototype.nextLevelExperience = function() {
      return (this.level + 1) * this.EXPERIENCE_MODIFIER;
    };
    Hero.prototype.checkLeveling = function() {
      if (this.experience >= this.nextLevelExperience()) {
        return this.level++;
      }
    };
    Hero.prototype.changeStat = function(stat_obj) {
      var stat, _i, _len;
      for (_i = 0, _len = stat_obj.length; _i < _len; _i++) {
        stat = stat_obj[_i];
        return;
      }
    };
    Hero.prototype.getType = function() {
      return this._type_;
    };
    Hero.prototype.setType = function(_type_) {
      this._type_ = _type_;
      return this;
    };
    Hero.prototype.getCurrentAnimation = function() {
      return this.animation[this.currentOrientation];
    };
    Hero.prototype.loadAnimations = function() {
      return this.animation = HAL.Animations[this._type_];
    };
    Hero.prototype.findPath = function() {
      var nA, nB, path, trace;
      nA = SOA.Zone.map.getTile(this.o_coord_x, this.o_coord_y);
      nB = SOA.Zone.map.getTile(this.d_coord_x, this.d_coord_y);
      path = SOA.Zone.map.pathfinder.find(nA, nB);
      trace = path.reverse();
      return [
        trace, trace.map(function(t) {
          return SOA.Zone.map.getTile(t.row, t.col).position;
        })
      ];
    };
    Hero.prototype.updatePosition = function() {
      var curr_cell, now, path_len, positions, rate, secs, so_far, trace, _ref1;
      _ref1 = this.findPath(), trace = _ref1[0], positions = _ref1[1];
      path_len = trace.length;
      secs = this.calcArrivalTime();
      console.debug("Total in seconds: " + secs);
      now = Date.now();
      so_far = (now - new Date(this.departure).getTime()) / 1000;
      console.debug("So far: " + so_far);
      rate = secs / trace.length;
      console.debug("Rate is: " + rate);
      curr_cell = Math.floor(so_far / rate);
      console.debug("Currently at: " + curr_cell);
      if (path_len > 1 && (0 < curr_cell && curr_cell <= path_len)) {
        trace = trace.slice(curr_cell);
        positions = positions.slice(curr_cell);
        this.layer.position.x = positions[0].x;
        this.layer.position.y = positions[0].y;
        console.debug("idem normalno");
        console.debug(positions);
        console.debug(trace);
      } else if (curr_cell > path_len) {
        this.layer.changeParentTile(SOA.Zone.map.getTile(this.d_coord_x, this.d_coord_y));
        trace = trace.slice(trace.length - 1);
        positions = positions.slice(positions.length - 1);
        console.debug("kasnim malo");
        this.SPEED_PER_TILE = 0;
      }
      if (trace.length > 0) {
        secs = this.calcArrivalTime(trace[0].row, trace[0].col);
      }
      rate = secs / trace.length;
      if (Number.isFinite(rate)) {
        this.SPEED_PER_TILE = secs + 10;
      }
      return [trace, positions];
    };
    Hero.prototype.resolveArrival = function() {
      throw new Error("Not implemented");
    };
    Hero.prototype.finishArriving = function() {
      throw new Error("Not implemented");
    };
    Hero.prototype.showDialog = function() {
      return SOA.showDialog(this.getType().toUpperCase(), this);
    };
    Hero.prototype.moveTo = function(d_coord_x, d_coord_y) {
      this.d_coord_x = d_coord_x;
      this.d_coord_y = d_coord_y;
      console.debug("Arrival: " + this.arrival);
      console.debug("Departure: " + this.departure);
      console.debug("Moving from " + this.o_coord_x + ", " + this.o_coord_y + " to " + this.d_coord_x + ", " + this.d_coord_y);
      console.debug("Duration: " + this.duration);
      console.debug("Moving from " + this.o_coord_x + ", " + this.o_coord_y + " to " + this.d_coord_x + ", " + this.d_coord_y);
      return this.onMoveTo.call(this);
    };
    Hero.prototype.onMoveTo = function() {
      var first_tile, last_position, pivot, positions, second_tile, startTime, trace, tween, _ref1,
        _this = this;
      _ref1 = this.updatePosition(), trace = _ref1[0], positions = _ref1[1];
      if (trace == null) {
        throw new Error("Path wasn't found!");
      }
      if (trace.length === 0) {
        return;
      }
      console.debug("Arriving in " + this.SPEED_PER_TILE + " seconds");
      console.debug(positions);
      console.debug(trace);
      last_position = this.layer.position.clone();
      if (positions.length === 1) {
        this.arrive();
        return;
      }
      tween = TweenLite.to(this.layer.position, this.SPEED_PER_TILE, {
        bezier: positions.slice(),
        type: "cubic",
        paused: true,
        curviness: 1
      });
      console.debug(this.layer.position);
      pivot = positions.shift();
      first_tile = trace.shift();
      second_tile = trace.shift();
      startTime = performance.now();
      tween.eventCallback("onUpdate", function() {
        var dir;
        if (Geometry.isPointInCircle(_this.layer.position, pivot, TILE_SENSE_RADIUS)) {
          if ((second_tile == null) || (pivot == null)) {
            tween.progress(1.0);
            return;
          }
          dir = _this.strDirection(first_tile, second_tile);
          _this.currentOrientation = dir;
          first_tile = second_tile;
          second_tile = trace.shift();
          pivot = positions.shift();
          if (_this.animation[dir] != null) {
            return _this.layer.textures = _this.animation[dir];
          }
        }
      });
      tween.eventCallback("onComplete", function() {
        var endTime;
        endTime = window.performance.now() - startTime;
        console.debug("It actually took: " + endTime + "ms");
        _this.o_coord_x = _this.d_coord_x;
        _this.o_coord_y = _this.d_coord_y;
        console.debug(_this.d_coord_y, _this.d_coord_x);
        _this.layer.changeParentTile(SOA.Zone.map.getTile(_this.d_coord_x, _this.d_coord_y));
        _this.layer.position.y -= 16;
        tween.kill();
        _this.layer.stop();
        return _this.arrive();
      });
      tween.play();
      return this.layer.play();
    };
    Hero.prototype.arrive = function() {
      return this.trigger("ARRIVAL", this);
    };
    Hero.prototype.showActionBox = function(data) {
      var actBox, x, y;
      x = data.originalEvent.clientX;
      y = data.originalEvent.clientY;
      this.target = data.hitTarget;
      actBox = SOA.getDialog("ACTION_BOX");
      actBox.TradeBtn.hide();
      actBox.CampBtn.hide();
      actBox.MoveBtn.hide();
      actBox.AttackBtn.hide();
      this.onAction(this.target);
      actBox.html.css("left", x - 100);
      actBox.html.css("top", y - 100);
      return actBox.show(this, 200);
    };
    Hero.prototype.strDirection = function(from, to) {
      var diff_col, diff_row, direction, isOdd, str;
      diff_row = to.row - from.row;
      diff_col = to.col - from.col;
      isOdd = this.layer.parentTile.col % 2;
      str = diff_row + "_" + diff_col;
      direction = {
        "-1_0": "north",
        "1_0": "south",
        "0_2": "east",
        "0_-2": "west"
      };
      direction[isOdd + "_" + 1] = "southeast";
      direction[-1 + isOdd + "_" + 1] = "northeast";
      direction[isOdd + ("_" + (-1))] = "southwest";
      direction[-1 + isOdd + ("_" + (-1))] = "northwest";
      return direction[str];
    };
    Hero.prototype.onAction = function(target) {
      this.target = target;
    };
    Hero.prototype.doAction = function(lastAction) {
      this.lastAction = lastAction;
      return console.debug("Action to execute: " + this.lastAction);
    };
    Hero.prototype.isInOwnCity = function() {
      var setCol, setRow;
      setRow = SOA.Settlement.x;
      setCol = SOA.Settlement.y;
      return this.o_coord_x === setRow && this.o_coord_y === setCol;
    };
    return Hero;
  });

  /*
  		# moveOnPath: (@path, @walk_rate) ->
  		#   start = [29, 46]
  		#   end = [33, 85]
  		#   ani = SOA.Zone.map.addAnimatedLayer(start[0], start[1], HAL.Animations.commander.east)
  		#   zmap = SOA.Zone.map
  		#   zmap.tile_graphics.forEach(function(t) { zmap.layers[2].removeChild(t); });
  
  		#path = zmap.pathfinder.find(, zmap.getTile(end[0], end[1]))
  		#leks = path.reverse().map(function(tile) { zmap.tintTileLayer(tile.row, tile.col, 2, 0x00FF00C0); return zmap.getTile(tile.row, tile.col).position.clone(); })
  		#t = TweenLite.to(ani.position, 20, {bezier: leks, type: "cubic", paused: true, curviness: 0});
  		#t.play();
  		#i = 0; 
  		#t.eventCallback("onUpdate", function() {  ++i; if(i > 200) { ##console.debug('wtf'); i = 0; };})
  
  		#@moveLayerTo(next_cell.row, next_cell.col)
  		#SOA.City.map.pathfinder.find(SOA.City.map.getTile(9, 6), SOA.City.map.getTile(10, 24)).forEach(function(coords) { tile = SOA.City.map.getTile(coords.row, coords.col); pos = tile.position; shape = SOA.City.map.createTileShape(pos.x, pos.y); SOA.City.world.addChild(shape); })
  		
  		# animateWalk: (next_cell) ->
  		#   TweenMax.fromTo(
  			
  		#   ) ->
  		#attr: "position[0]"
  		#from: @hero.position[0]
  		#to: amjad.zonemap.getTile(next_cell.row, next_cell.col).position[0]
  		#duration: @walk_rate
  		# return
  		# @hero.tween
  		#   attr: "position[0]"
  		#   from: @hero.position[0]
  		#   to: amjad.zonemap.getTile(next_cell.row, next_cell.col).position[0]
  		#   duration: @walk_rate
  		# .tween
  		#   attr: "position[1]"
  		#   from: @hero.position[1]
  		#   to: amjad.zonemap.getTile(next_cell.row, next_cell.col).position[1]
  		#   duration: @walk_rate
  		# .done () =>
  		#   next = @path[@cur_cell_index + 1]
  		#   if not next?
  		#       Hal.removeTrigger "ENTER_FRAME", @curr_frame                   
  		#       @trigger "ARRIVED", next_cell
  		#       return
  		#   @moveLayerTo(next_cell.row, next_cell.col)
  		#   @cur_cell_index++
  		#   ##console.debug next_cell
  		#   ##console.debug next
  		#   @next_dir = @strDirection(next_cell, next)
  		#   ##console.debug @next_dir
  		#   @animateWalk(next)
  
  		# moveLayerTo: (row, col) ->
  		#   dest_tile = amjad.zonemap.getTile(row, col)
  		#   if not dest_tile?
  		#       return
  		#   @hero.tile.removeLayer(@hero.layer, false)
  		#   dest_tile.addTileLayer(@hero, false)
  		#   @o_coord_x = row
  		#   @o_coord_y = col
  		#   @hero.setPosition(dest_tile.position[0], dest_tile.position[1])
  		#   @hero.quadtree.remove(@hero)
  		#   amjad.zonemap.quadtree.insert(@hero)
  
  		# TweenMax.to(comm.layer.scale, 1, {x: 1.2, y: 1.2, onComplete: function() { comm.layer.scale.x = 1; comm.layer.scale.y = 1; }})
  
  		# @animation["north"] = HAL.Animations[@_type_].north
  		# @animation["south"] = HAL.Animations[@_type_].south
  		# @animation["west"] = HAL.Animations[@_type_].west
  		# @animation["east"] = HAL.Animations[@_type_].east
  		# @animation["northeast"] = HAL.Animations[@_type_].northeast
  		# @animation["southeast"] = HAL.Animations[@_type_].southeast
  		# @animation["southwest"] = HAL.Animations[@_type_].southwest
  		# @animation["northwest"] = HAL.Animations[@_type_].northwest
  		
  		# start = [29, 46]
  		# end = [33, 85]
  		# ani = SOA.Zone.map.addAnimatedLayer(start[0], start[1], HAL.Animations.commander.east)
  		# zmap = SOA.Zone.map
  		# zmap.tile_graphics.forEach(function(t) { zmap.layers[2].removeChild(t); });
  
  		# path = zmap.pathfinder.find(zmap.getTile(start[0], start[1]), zmap.getTile(end[0], end[1]))
  		# leks = path.reverse().map(function(tile) { zmap.tintTileLayer(tile.row, tile.col, 2, 0x00FF00C0); return zmap.getTile(tile.row, tile.col).position.clone(); })
  		# t = TweenLite.to(ani.position, 20, {bezier: leks, type: "cubic", paused: true, curviness: 0});
  		# t.play();
  		# i = 0; 
  		# t.eventCallback("onUpdate", function() {  ++i; if(i > 200) { ##console.debug('wtf'); i = 0; };})
  
  		# putInCitySurrounding: (row, col) ->
  		#   zone = amjad.zonemap
  		#   if not(row is amjad.empire.settlement.y and col is amjad.empire.settlement.x)
  		#       t = zone.getTile(row, col)
  		#       where = zone.findInDirectionOf(t, "east", 5).concat zone.findInDirectionOf(t, "west", 5)
  		#       place = (where.filter (tile) -> 
  		#                   return not tile.layers[5]?)[0]
  		#       row = place.row
  		#       col = place.col
  		#   return [row, col]
  
  		# @loadAnimations()
  		# meta = {}
  		# meta.layer = 6
  		# meta.group = @_type_
  		# meta.name = "#{@_type_}_#{@id}"
  		# meta.sprite = @animation[@current_anim][0].getName()
  		
  		# [row, col] = @putInCitySurrounding(row, col)
  		
  		# @o_coord_x = row
  		# @o_coord_y = col
  
  		@todo find settlement coordinates
  		# main = amjad.empire.settlement
  		# @hero = amjad.zonemap.tm.addTileLayerMeta(row, col, meta)
  		# # @hero.setScale(2.0, 2.0)
  		# @hero.meta = @
  		# @hero.on "SELECTED", () =>
  		#     hero_queue = Hud.getDialog("HEROES_QUEUE")
  		#     frame = hero_queue.html().find("##{@_type_}-#{@id}")
  		#     frame.addClass("hero-frame-a") #css, "5px solid rgb(201,178,10)")
  		#     ##console.debug "Selected #{@hero.name}"
  		#     amjad.selected_hero = @
  
  		# @hero.on "DESELECTED", () =>
  		#     hero_queue = Hud.getDialog("HEROES_QUEUE")
  		#     frame = hero_queue.html().find("##{@_type_}-#{@id}")
  		#     # frame.css("border", "none")
  		#     frame.removeClass("hero-frame-a")
  		#     ##console.debug "Deselected #{@hero.name}"
  		#     # amjad.selected_hero = null
  
  		# amjad.zonemap.on ["CAMERA_PANNING", "CAMERA_LERPING"], (layer) =>
  		#     if not @action_box_hidden
  		#         @hideActionBox()
  		#         ##console.debug "Deselected #{@hero.name}"
  		#         amjad.selected_hero = null
  		#     hero_queue = Hud.getDialog("HEROES_QUEUE")
  		#     frame = hero_queue.html().find("##{@getType()}-#{@id}")
  		#     frame.css("border", "none")
  
  		# @hero.on "SELECTED_DBL_CLICK", () =>
  		#     show hero window
  		#     console.error "Yay it works"
  		#     @dbl_clicked = true
  		#     dlg = Hud.getDialog(@getType().toUpperCase())
  		#     dlg.show(@)
  */


}).call(this);
