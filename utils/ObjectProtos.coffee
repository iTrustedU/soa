# It is a very bad practice to extend Object prototype
# It adds an extra function overhead to all JS instances, which impacts performance 
# by a lot
# I use this utility set for dev solely!!

module.exports = () ->	
	Object.defineProperty(Object.prototype, "stringify",
		get: () -> 
			return JSON.stringify(@)
		enumerable: false
	)