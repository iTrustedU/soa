"use strict"
# "jquery.cookie"

define ["soa/Rest", "jquery.cookie"], 

(Rest, _) ->
    
    class Session
        constructor: () -> 
            @sessKey = $.cookie("sheiksofblabla")
            Rest.setSessionKey @sessKey

        hasKey: () ->
            val = (@sessKey? and @sessKey isnt "undefined")
            return val

        setKey: (@sessKey) ->
            $.cookie("sheiksofblabla", @sessKey, expires : 1)
            Rest.setSessionKey @sessKey

        getSessionKey: () ->
            return @sessKey

        trySessionLogin: () ->
            $deferred = new $.Deferred()
            if not @sessKey?
                $deferred.reject("No login session mate")
            else
                Rest.get("user.session_login", session: @sessKey)
                .done (data) =>
                    $deferred.resolve(data)
                .fail (data) =>
                    $deferred.reject("Login with a session failed")
            return $deferred.promise()

        tryLogin: (usn, pass) ->
            $deferred = new $.Deferred()
            Rest.get "user.login",
                "usn": usn
                "pass": pass
            .done (data) =>
                @setKey(data.session)
                $deferred.resolve()
            .fail (data) =>
                $deferred.reject("Login failed mate")
            return $deferred.promise()

        resetSessionCookie: () ->
            @sessKey = $.cookie("", null)
            Rest.setSessionKey @sessKey

    return Session