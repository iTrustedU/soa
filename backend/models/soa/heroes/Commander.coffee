mongoose = require "mongoose"
Statistics = require("./Statistics").COMMANDER
Coordinates = require("./Coordinates")

schema = mongoose.Schema
	name:
		type: String
		required: true

	coords:
		type: Object
		ref: Coordinates

	level:
		type: Number
		default: 1

	stat:
		type: Object
		ref: Statistics

	experience:
		type: Number
		default: 0

	unassignedPoints:
		type: Number
		default: 0

	# troops:
	# 	type: Object
	# 	default: []
	# 	ref: Troop

module.exports = Commander = mongoose.model("Commander", schema)