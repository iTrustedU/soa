(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["jquery", "jquery-ui", "EventDispatcher", "soa/hud/HBHelpers", "soa/hud/dialogs/DialogsLoader"], function($, $$, EventDispatcher, HBHelpers, DialogsLoader) {
    var HUDManager;
    HUDManager = (function(_super) {
      __extends(HUDManager, _super);

      function HUDManager() {
        HUDManager.__super__.constructor.call(this);
        this.dialogs = {};
        this.domElement = null;
        this.dm = null;
        this.queue = {};
      }

      return HUDManager;

    })(EventDispatcher);
    HUDManager.prototype.prologue = function() {
      var $deferred,
        _this = this;
      $deferred = new $.Deferred();
      $(document).ready(function() {
        _this.onPrologue();
        return $deferred.resolve();
      });
      return $deferred.promise();
    };
    HUDManager.prototype.hideAllDialogs = function() {
      var dlg, nm, _ref, _results;
      _ref = this.dialogs;
      _results = [];
      for (nm in _ref) {
        dlg = _ref[nm];
        _results.push(dlg.hide());
      }
      return _results;
    };
    HUDManager.prototype.getDialog = function(name) {
      return this.dialogs[name];
    };
    HUDManager.prototype.onPrologue = function() {
      var dialogCls, name, _ref, _results;
      this.start_time = window.performance.now();
      this.domElement = $("#viewport");
      this.dialogs_loader = new DialogsLoader();
      _ref = this.queue;
      _results = [];
      for (name in _ref) {
        dialogCls = _ref[name];
        _results.push(this.addDialog(name, new dialogCls()));
      }
      return _results;
    };
    HUDManager.prototype.epilogue = function() {
      var dlg, name, _ref;
      _ref = this.dialogs;
      for (name in _ref) {
        dlg = _ref[name];
        /* @todo ovo nije potrebno, al eto kad vlada zuri*/

        if (dlg.epilogue != null) {
          dlg.epilogue(this.domElement);
        }
      }
      return this.end_time = window.performance.now() - this.start_time;
    };
    HUDManager.prototype.showDialog = function(name, ctx, animationSpeed) {
      var dlg;
      if (animationSpeed == null) {
        animationSpeed = 300;
      }
      dlg = this.dialogs[name];
      if (dlg == null) {
        throw new Error("No dialog with name: " + name);
      }
      if (ctx == null) {
        ctx = {};
        console.warn("Context not passed to " + name);
      }
      dlg.show(ctx, animationSpeed);
      return dlg;
    };
    HUDManager.prototype.hideDialog = function(name, animationSpeed) {
      var dlg;
      if (animationSpeed == null) {
        animationSpeed = 300;
      }
      dlg = this.dialogs[name];
      if (!(typeof dlg === "function" ? dlg((function() {
        throw new Error("No dialog with name: " + name);
      })()) : void 0)) {
        dlg.hide(animationSpeed);
      }
      return dlg;
    };
    HUDManager.prototype.insert = function(name, Class) {
      return this.queue[name] = Class;
    };
    HUDManager.prototype.fadeInViewport = function() {
      this.domElement.css("opacity", 0);
      return this.domElement.animate({
        "opacity": 1
      }, 1000).promise();
    };
    HUDManager.prototype.addDialog = function(name, dialog) {
      var _this = this;
      if (this.dialogs[name] != null) {
        throw new Error("Dialog with name:" + name + " is already loaded");
      } else {
        this.dialogs[name] = dialog;
        dialog.setName(name);
        this.on("SHOW_" + name + "_DIALOG", function(ctx) {
          return wrapper.show(ctx);
        });
        this.on("HIDE_" + name + "_DIALOG", function(ctx) {
          return wrapper.hide(ctx);
        });
        dialog.prologue(this.domElement);
        dialog.hide(0);
        this.domElement.append(dialog.html);
      }
      return dialog;
    };
    return HUDManager;
  });

}).call(this);
