"use strict"

define ["EventDispatcher", "editor/LoaderDialog", "editor/SpritesDialog", "editor/TilesDialog", "editor/MainMenu", "editor/BufferView"], 

(EventDispatcher, LoaderDialog, SpritesDialog, TilesDialog, MainMenu, BufferView) ->

	SPRITES_URI = "assets/sprites.list"
	API = 
		SAVE_SECTION: "/map/{0}/region/{1}/{2}/{3}/save"
		SAVE_MAP_TILE: "/map/{0}/tiles/add"

	class IsometricEditor extends EventDispatcher
		constructor: () ->
			super()
			@init()
			@initListeners()

	IsometricEditor::init = () ->
		@container = $("<div/>", {"class": "editor-dialog-container"})
		$(HAL.Dom.viewport).append(@container)
		@initLoaderDialog()
		
		@mouse_sprite 	= null
		@shift_pressed 	= false
		@selected_tile 	= null

	IsometricEditor::loadMap = (@map_data) ->
		if @scene?
			HAL.IsometricScene.stopRendering(@scene)
		@scene = new HAL.IsometricScene(@map_data.name)
		@scene.init()
		@regmngr = @scene.map.region_mngr
		@container.empty()
		@downloadSprites()
		@loader_dialog.showSpinner()

	IsometricEditor::initLoaderDialog = () ->
		@loader_dialog = new LoaderDialog()
		@loader_dialog.prologue().done () =>
			$(HAL.Dom.viewport).append(@loader_dialog.html)

	IsometricEditor::initSpritesDialog = () ->
		@sprites_dialog = new SpritesDialog(PIXI.TextureCache)
		@container.append(@sprites_dialog.html)

	IsometricEditor::initBufferView = () -> return
		# @buffer_view = new BufferView(@scene.map)
		# @container.append(@buffer_view.html)

	IsometricEditor::initMainMenu = () ->
		@main_menu = new MainMenu()
		@container.append(@main_menu.html)

	IsometricEditor::initTilesDialog = () ->
		@tiles_dialog = new TilesDialog()
		@container.append(@tiles_dialog.html)
		@container.append(@tiles_dialog.edit.html)

		@tiles_dialog.on "TILE_SELECTED", (@selected_tile) =>
			texture = PIXI.TextureCache[@selected_tile.sprite]
			if not @mouse_sprite?
				@mouse_sprite = new PIXI.Sprite(texture)
				@mouse_sprite.visible = false
				Editor.scene.attachSpriteToMouse(@mouse_sprite)
			else
				@mouse_sprite.setTexture(texture)

		@scene.on "MOUSE_CLICK", (button, x, y) =>
			if button is 0
				@onLeftClick(@scene.world_pos.x, @scene.world_pos.y)

		@scene.on "KEY_UP", (ev) =>
			if @shift_pressed and @mouse_sprite?
				@mouse_sprite.visible = false
				@shift_pressed = false

		@scene.on "KEY_DOWN", (ev) =>
			if ev.shiftKey and @mouse_sprite?
				@shift_pressed = true
				@mouse_sprite.visible = true

	IsometricEditor::onLeftClick = (x, y) ->
		if @mouse_sprite? and @mouse_sprite.visible
			tilemeta = @tiles_dialog.storage.findByID(@selected_tile.seedId)
			@scene.map.addTileLayerAtPosition(x, y, tilemeta)
			console.debug "MY seedId is #{@selected_tile.seedId}"
			[region, section] = 
				@regmngr.writeToSectionBufferAtPosition(x, y, tilemeta.layer, @selected_tile.seedId)
			@regmngr.redrawCurrentSections()
			@scene.map.sortLayers()
			@saveSection(region, section)
			.done () =>
				@saveTileMeta(tilemeta)
	
	IsometricEditor::saveSection = (region, section) ->
		console.debug region, section
		key = @regmngr.getRegionKey(region).split("_")
		sectionIndex = @regmngr.getSectionIndex(region, section)
		url = API.SAVE_SECTION.f(@scene.map.map_meta.name, key[0], key[1], sectionIndex)
		return $.post(url, buffers: @regmngr.encodeSectionBuffers(section))

	IsometricEditor::saveTileMeta = (tilemeta) ->
		$.post(API.SAVE_MAP_TILE.f(@scene.map.map_meta.name), tilemeta)
		.done () =>
			@scene.map.tile_storage.addTile(tilemeta)

	IsometricEditor::initListeners = () ->
		@loader_dialog.on "MAP_LOADED", (data) =>
			@loadMap(data)

		@on "SPRITES_PROGRESS", () ->
			return

		@on "SPRITES_COMPLETE", () ->
			HAL.IsometricScene.startRendering(@scene)
			HAL.Dom.clearInfoBox()
			HAL.Dom.updateTextOn(@scene, "MOUSE_MOVED", "Mouse position: {0}, {1}")
			HAL.Dom.updateTextOn(@scene, "MOUSE_WORLD_CHANGED", "Mouse world position: {0}, {1}")
			HAL.Dom.updateTextOn(@scene.map, "MOUSE_OVER_TILE", "Tile position in world: {0}, {1}")
			HAL.Dom.updateTextOn(@regmngr, "REGION_CHANGED", "In region no. {0}, {1}")
			HAL.Dom.updateTextOn(@regmngr, "MOUSE_OVER_REGION", "Mouse over region: {0},{1}, section: {2},{3}")
			HAL.Dom.updateTextOn(@regmngr, "REGION_TILE_CHANGED", "Tile position in section: {0}, {1}")

			@regmngr.on "LOADED", () =>
				@initTilesDialog()
				@initSpritesDialog()
				@initMainMenu()
				@initBufferView()
				@loader_dialog.html.hide()
				@loader_dialog.hideSpinner()
			
			@scene.map.load(@map_data)

	IsometricEditor::downloadSprites = () ->
		$request = $.get(SPRITES_URI)
		$deferred = new $.Deferred()
		$request.done (data) =>
			sheetLoader = new PIXI.AssetLoader(data.split("\n"))
			sheetLoader.onProgress = (item) =>
				@trigger "SPRITES_PROGRESS", @, item
				$deferred.progress(item)
			sheetLoader.onComplete = () =>
				@trigger "SPRITES_COMPLETE"
				$deferred.resolve()
			sheetLoader.load()
		.fail () =>
			$deferred.reject("Download of #{SPRITES_URI} failed")
		return $deferred.promise()

	return IsometricEditor
