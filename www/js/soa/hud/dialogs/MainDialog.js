(function() {
  "use strict";
  var $buildings_content, $overview_content, $pois_content, $resources_content, $upgrade_price, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"main-tent draggable\">\n    <div class=\"dialog-name\">\n        <span>Main Tent</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"science-tabs\">\n        <div id=\"overview-tab\" class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Overview</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div id=\"resources-tab\" class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Resources</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div id=\"pois-tab\" class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">POI's</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div id=\"buildings-tab\" class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Buildings</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n    </div>\n    <div id=\"tab_content_overview_template\" class=\"main-tent-content overview\">\n        {{{overview_content}}}\n    </div>\n    <div id=\"tab_content_resources_template\" class=\"main-tent-content resources default-skin\">\n       {{{resources_content}}}\n    </div>\n    <div id=\"tab_content_pois_template\" class=\"main-tent-content pois default-skin\">\n       {{{pois_content}}} \n    </div>\n    <div id=\"tab_content_buildings_template\"  class=\"main-tent-content main-buildings default-skin\">\n        {{{buildings_content}}} \n    </div>\n    <div class=\"war-building-info\">\n        <img class=\"dialog-building-img\" />\n        <div id=\"upgrade-price\" class=\"upgrade-price\">\n           {{{upgrade_price}}}\n        </div>\n        <div id=\"upgrade-building\" class=\"upgrade-building\">Upgrade</div>\n    </div>\n</div>";

  $upgrade_price = "{{#each data.resources}}\n    <img src=\"img/top-resources/{{@key}}.png\">\n    <span>{{this}}</span>\n{{/each}}\n";

  $overview_content = "<div id=\"overview-tab\" class=\"main-tent-content main-overview\">\n    <span class=\"par\">City Name:</span><span class=\"main-tent-content-value nepar\">{{data.city_name}}</span><br />\n    <span class=\"par\">Player Name:</span><span class=\"main-tent-content-value nepar\">{{data.player_name}}</span><br />\n    <span class=\"par\">Ruling Title:</span><span class=\"main-tent-content-value nepar\">{{data.ruling_title}}</span><br />\n    <span class=\"par\">Achieved Title:</span><span class=\"main-tent-content-value nepar\">{{data.achived_title}}</span><br />\n    <span class=\"par\">Poi's:</span><span class=\"main-tent-content-value nepar\">{{data.pois}}</span><br />\n    <span class=\"par\">Glory:</span><span class=\"main-tent-content-value nepar\">{{data.glory}}</span><br />\n    <span class=\"par\">Rank:</span><span class=\"main-tent-content-value nepar\">{{data.rank}}</span><br />\n    <span class=\"par\">Heroes:</span><span class=\"main-tent-content-value nepar\">{{data.heroes}}</span><br />\n    <span class=\"par\">Army:</span><span class=\"main-tent-content-value nepar\">{{data.army}}</span><br />\n    <span class=\"par\">Camels:</span><span class=\"main-tent-content-value nepar\">{{data.camels}}</span><br />\n    <span class=\"par\">Horses:</span><span class=\"main-tent-content-value nepar\">{{data.horses}}</span><br />\n    <span class=\"par\">Cows:</span><span class=\"main-tent-content-value nepar\">{{data.cows}}</span><br />\n    <span class=\"par\">Sheeps:</span><span class=\"main-tent-content-value nepar\">{{data.sheeps}}</span><br />\n    <span class=\"par\">Goats:</span><span class=\"main-tent-content-value nepar\">{{data.goats}}</span><br />\n</div>";

  $resources_content = "<div id=\"resources-tab\" class=\"main-tent-resources-box default-skin\">\n    {{#each data}}\n    <div class=\"resources-inner-box\">\n        <img src=\"img/dialog/commander/timber.png\" class=\"resources-image\">\n        <div class=\"resources-shadow-box\">\n            <div class=\"main-resources-stats\">\n                <span class=\"color-text\">{{lang @key}}</span>\n                <img src=\"img/dialog/commander/income.png\">\n                <span>12</span>\n            </div>\n        </div>\n    </div>\n    {{/each}}\n</div>";

  $pois_content = "{{#each data}}\n<div id=\"pois-tab\" class=\"pois-box\">\n    <img src=\"{{concat_poi c_type}}\" class=\"poi-image\">\n    <div class=\"poi-shadow-box\">\n        <div class=\"main-poi-stats\">\n            <span class=\"color-text\">{{lang c_type}}</span>\n            <span class=\"poi-coordinates\"><span class=\"color-text\">X</span><span>{{x}}</span>\n            <span class=\"color-text\">Y</span><span>{{y}}</span></span><br />\n            <!-- <img src=\"{{concat_poi c_type}}\" class=\"poi-resource-img\"> -->\n            <img src=\"img/dialog/commander/income.png\">\n            <span class=\"level-value-2\">152</span>\n        </div>\n    </div>\n</div>\n{{/each}}";

  $buildings_content = "{{#each data}}\n<div class=\"main-buildings-box\">\n    <img src=\"{{concat_construction_icon c_type}}\" />\n    <div class=\"buildings-shadow-box\">\n        <div class=\"main-buildings-stats\">\n            <span class=\"color-text\">{{lang c_type}}</span>\n            <div class=\"main-building-level-icon\"></div>\n            <span class=\"level-value-2\">{{level}}</span><br />\n            <span class=\"building-status\">Idle</span>\n        </div>\n        <div class=\"main-buildings-stats-2\">\n            {{#each resources.resources}}\n                <img src=\"img/top-resources/{{@key}}.png\">\n                <span>{{this}}</span><br />\n            {{/each}}\n        </div>\n        <div id=\"{{construction_id}}\" class=\"building-upgrade-button\">Upgrade</div>\n    </div>\n</div>\n{{/each}}";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var MainDialog;
    MainDialog = (function(_super) {
      __extends(MainDialog, _super);

      function MainDialog() {
        MainDialog.__super__.constructor.call(this, html);
      }

      return MainDialog;

    })(BaseDialog);
    MainDialog.prototype.onPrologue = function() {
      this.setTemplates({
        "overview_template": $overview_content,
        "resources_template": $resources_content,
        "pois_template": $pois_content,
        "buildings_template": $buildings_content,
        "upgrade_template": $upgrade_price
      });
      this.upgradeable();
      this.closeable();
      return this.draggable();
    };
    MainDialog.prototype.onEpilogue = function(hud) {
      var _me_;
      this.on("PRE_SHOW", _me_ = function() {
        this.html.find(".dialog-building-img").attr("src", Handlebars.helpers.concat_dialog_construction("main"));
        return this.remove("PRE_SHOW", _me_);
      });
      this.on("SELECTED_TAB", function(val) {
        var datakey, template,
          _this = this;
        template = "overview_template";
        if (val === "overview-tab") {
          datakey = "overview_content";
        } else if (val === "resources-tab") {
          template = "resources_template";
          datakey = "resources_content";
        } else if (val === "pois-tab") {
          template = "pois_template";
          datakey = "pois_content";
        } else if (val === "buildings-tab") {
          template = "buildings_template";
          datakey = "buildings_content";
        }
        $("div[id^=tab_content_").css('display', 'none');
        $('.science-tabs #tab_content_' + datakey).css('display', 'block');
        this.compile(template, datakey).then(function(data) {
          _this.refresh("tab_content_" + template, data);
          return _this.html.find("#tab_content_" + template).customScrollbar();
        });
        this.UpgradeBtn = this.html.find(".building-upgrade-button");
        this.UpgradeBtn.unbind("click");
        return this.UpgradeBtn.on("click", function() {
          var _this = this;
          this.buildingId = $(this).attr('id');
          this.building = amjad.empire.constructions.getByID(this.buildingId);
          this.building.on("UPGRADED", function() {
            return SOA.getDialog("MAIN_BUILDING").trigger("SELECTED_TAB", "buildings-tab");
          });
          return this.building.upgrade();
        });
      });
      this.on("SHOW", function() {
        var buildings, buildingsData, empireResources, key, mainBuildingUpgradeResources, overviewData, poisData, upgradeResources, value, _i, _len,
          _this = this;
        this.ctx.on("UPGRADED", function() {
          return _this.trigger("SELECTED_TAB", "overview-tab");
        });
        empireResources = amjad.empire.resources.getEmpireResources();
        overviewData = {
          city_name: "Citonija",
          player_name: amjad.user.getFullName(),
          ruling_title: "Ruler",
          achived_title: "Achiver",
          glory: 12,
          rank: 600,
          army: 56,
          camels: empireResources['majaheem'].amount,
          heroes: amjad.empire.heroes.getCountAll(),
          horses: empireResources['majaheem'].amount,
          cows: empireResources['cow'].amount,
          sheeps: empireResources['sheep'].amount,
          goats: empireResources['goat'].amount,
          pois: amjad.empire.constructions.pois.length
        };
        mainBuildingUpgradeResources = amjad.empire.resources.getConstructionUpgradeResources('main_building', this.ctx.level + 1);
        poisData = amjad.empire.constructions.pois;
        buildings = amjad.empire.constructions.getAll();
        buildingsData = {};
        for (key = _i = 0, _len = buildings.length; _i < _len; key = ++_i) {
          value = buildings[key];
          upgradeResources = amjad.empire.resources.getConstructionUpgradeResources(value.c_type, value.level + 1);
          buildingsData[value.construction_id] = value;
          buildingsData[value.construction_id]['resources'] = upgradeResources;
        }
        this.setData("overview_content", {
          data: overviewData
        });
        this.setData("resources_content", {
          data: empireResources
        });
        this.setData("pois_content", {
          data: poisData
        });
        this.setData("buildings_content", {
          data: buildingsData
        });
        this.setData("upgrade_price", {
          data: mainBuildingUpgradeResources
        });
        this.html.find(".main-tent-content").customScrollbar();
        this.compile("upgrade_template", "upgrade_price").then(function(data) {
          return _this.refresh("upgrade-price", data);
        });
        this.set("SELECTED_TAB", "overview-tab");
        return this.trigger("SELECTED_TAB", "overview-tab");
      });
      return this.clickTriggers({
        ".chat-tab/#id": "SELECTED_TAB"
      });
    };
    return MainDialog;
  });

}).call(this);
