(function() {
  "use strict";
  var back_btn_html, folder_html, sprite_html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  back_btn_html = "<i id=\"back-btn\" class=\"fa fa-arrow-circle-left\"></i>";

  folder_html = "<li id={{id}} class=\"selectable-box\" title={{tooltip}}>\n    <i class=\"fa fa-folder-open\"></i>\n    <span class=\"selectable-title\">\n        {{title}}\n    </span>\n</li>";

  sprite_html = "<li id={{id}} class=\"selectable-box draggable-sprite\" title={{tooltip}}>\n    <img src={{img_url}}>\n    <span class=\"selectable-title\">\n        {{title}}\n    </span>\n</li>";

  define(["editor/BaseDialog"], function(BaseDialog) {
    var FOLDER_TOOLTIP, SHEETS_FOLDER, SPRITE_FOLDER, SPRITE_TEST, SPRITE_TOOLTIP, SpritesDialog;
    SPRITE_FOLDER = "assets/sprites/soa/";
    SHEETS_FOLDER = "assets/sprites/sheets/";
    FOLDER_TOOLTIP = "{0},sprites:{1},folders:{2}";
    SPRITE_TOOLTIP = "{0},{1}x{2}";
    SPRITE_TEST = function(url) {
      return /.+\.png$/.test(url);
    };
    SpritesDialog = (function(_super) {
      __extends(SpritesDialog, _super);

      function SpritesDialog(sprites) {
        this.sprites = sprites != null ? sprites : {};
        SpritesDialog.__super__.constructor.call(this, "Sprites", "sprites-dialog");
      }

      return SpritesDialog;

    })(BaseDialog);
    SpritesDialog.prototype.init = function() {
      this.addToToolbox(back_btn_html);
      this.initButtons();
      return this.createFolderStructures();
    };
    SpritesDialog.prototype.initListeners = function() {
      var _this = this;
      this.on("CONTENT_REFRESH", function(url) {
        _this.back_url = url;
        if (_this.back_url === "") {
          return _this.back_btn.hide();
        } else {
          return _this.back_btn.show();
        }
      });
      return this.on("MOVE_BACK", function() {
        var folder, lindex;
        folder = _this.getFolderByPath(_this.back_url);
        lindex = _this.back_url.lastIndexOf("/");
        if (lindex === 0 && _this.back_url.length > 1) {
          lindex = 1;
        }
        _this.back_url = _this.back_url.substring(0, lindex);
        lindex = _this.back_url.lastIndexOf("/");
        if (lindex === 0) {
          lindex = 1;
        }
        return _this.showFolderContents(folder, _this.back_url.substring(0, lindex));
      });
    };
    SpritesDialog.prototype.initButtons = function() {
      var _this = this;
      this.back_btn = this.html.find("#back-btn");
      this.back_btn.hide();
      return this.back_btn.on("click", function() {
        return _this.trigger("MOVE_BACK");
      });
    };
    SpritesDialog.prototype.createFolderStructures = function() {
      var direct_folder, folders, spr_name, sprite, url, _ref;
      this.root_folder = {
        url: "/",
        folders: {},
        sprites: {}
      };
      this.back_url = "";
      _ref = this.sprites;
      for (url in _ref) {
        sprite = _ref[url];
        if (!(SPRITE_TEST(url) && url.startsWith(SPRITE_FOLDER))) {
          continue;
        }
        folders = url.replace(SPRITE_FOLDER, "").split("/");
        spr_name = folders.pop();
        direct_folder = this.findOrCreateFolder(folders);
        direct_folder.sprites[spr_name] = sprite;
      }
      return this.showFolderContents(this.root_folder, this.back_url);
    };
    SpritesDialog.prototype.showFolderContents = function(root_folder, back_url) {
      this.trigger("CONTENT_REFRESH", this, back_url);
      this.content.empty();
      this.showSubfolders(root_folder);
      return this.showSprites(root_folder);
    };
    SpritesDialog.prototype.getFolderByPath = function(path) {
      var folders;
      if (path.indexOf("/") === 0 && path.length !== 1) {
        path = path.substring(1);
      }
      folders = path.split("/");
      return this.findOrCreateFolder(folders);
    };
    SpritesDialog.prototype.showSprites = function(root_folder) {
      var $sprite_box, spr_name, sprite, sprite_tpl, tooltip, _ref, _results;
      sprite_tpl = Handlebars.compile(sprite_html);
      _ref = root_folder.sprites;
      _results = [];
      for (spr_name in _ref) {
        sprite = _ref[spr_name];
        tooltip = SPRITE_TOOLTIP.f(spr_name, sprite.width, sprite.height);
        $sprite_box = $(sprite_tpl({
          id: spr_name,
          title: spr_name,
          tooltip: tooltip,
          img_url: sprite.baseTexture.imageUrl
        }));
        $sprite_box.draggable({
          revert: "invalid",
          helper: "clone",
          zIndex: 10000
        });
        _results.push(this.content.append($sprite_box));
      }
      return _results;
    };
    SpritesDialog.prototype.showSubfolders = function(root_folder) {
      var $folder_box, folder, folder_name, folder_tpl, tooltip, _fn, _ref, _results,
        _this = this;
      folder_tpl = Handlebars.compile(folder_html);
      _ref = root_folder.folders;
      _fn = function(folder) {
        $folder_box.unbind("click");
        return $folder_box.on("click", function() {
          return _this.showFolderContents(folder, root_folder.url);
        });
      };
      _results = [];
      for (folder_name in _ref) {
        folder = _ref[folder_name];
        tooltip = FOLDER_TOOLTIP.f(folder_name, Object.keys(folder.sprites).length, Object.keys(folder.folders).length);
        $folder_box = $(folder_tpl({
          id: folder_name,
          title: folder_name,
          tooltip: tooltip
        }));
        _fn(folder);
        _results.push(this.content.append($folder_box));
      }
      return _results;
    };
    SpritesDialog.prototype.findOrCreateFolder = function(folders_path) {
      var folder, path, topmost, _i, _len;
      topmost = this.root_folder;
      path = "/";
      for (_i = 0, _len = folders_path.length; _i < _len; _i++) {
        folder = folders_path[_i];
        if (folder === "") {
          break;
        }
        path = "{0}{1}/".f(path, folder);
        if (topmost.folders[folder] != null) {
          topmost = topmost.folders[folder];
        } else {
          topmost = topmost.folders[folder] = {
            url: path,
            folders: {},
            sprites: {}
          };
          break;
        }
      }
      return topmost;
    };
    return SpritesDialog;
  });

}).call(this);
