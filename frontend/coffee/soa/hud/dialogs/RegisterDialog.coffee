"use strict"

html = 
"""
<div class="index-body">
<div class="login-content">
    <div id="register-dialog" class="register-box">
        <div class="dialog-name">
            <span>Sign Up</span>
        </div>
        <form method="post" action="register" id="regform"> 
            <span class="register-name">Username:</span>
            <input name="username" id="username" class="register-input" type="text" minlength="5" maxlength="30" value="" required>
            <span class="register-name">First Name:</span>
            <input  name="firstname" id="firstname" class="register-input" type="text" minlength="2" maxlength="50" value="" required>
            <span class="register-name">Family Name:</span>
            <input name="familyname" id="familyname" class="register-input" type="text" minlength="2" maxlength="50" value="" required>
            <span class="register-name">Email:</span>
            <input id="email" name="email" class="register-input" type="email" value="" required>
            <span class="register-name">Verify Email:</span>
            <input id="email-verify" name="email-verify" class="register-input" type="email" value="" required>
            <span class="register-name">Password:</span>
            <input name="password" id="password" class="register-input" type="password" value="" required>
            <span class="register-name">Verify Password:</span>
            <input name="password-verify" id="password-verify" class="register-input" type="password" value="" required>
            <span class="register-name">Civil:</span>
            <input id="faction" class="radio-button" name="faction" type="radio" value="civil">
            <div class="sub-choices disable" id="sub-choices">
                <span class="register-name">Arabian:</span>
                <input id="architecture" checked="checked" class="radio-button" name="architecture" type="radio" value="arabian">
                <span class="register-name">Southern:</span>
                <input id="architecture" class="radio-button" name="architecture" type="radio" value="southern">
            </div>
            <span class="register-name">Bedouin:</span>
            <input id="faction" class="radio-button" name="faction" checked="checked" type="radio" value="bedouin">
            <label for="" class="error validation-message"></label>
            <input type="submit" class="register-button" value="REGISTER"/>
            <br />
            <div class="back-button">BACK</div>
        </form>
    </div>
</div>
</div>
"""

media_preview_html = 
"""
<div class="media-preview-box">
    <div class="dialog-name">
        <span>Preview</span>
    </div>
</div>
"""

registration_confirmation_html = 
"""
<div class="register-box">
    <div class="dialog-name">
        <span>Confirmation</span>
    </div>
    <div>
        <span class="validation-message">
            <p>Thank You for registering for Sheiks of Arabia!</p>
            <p>A confirmation mail has been sent to the e-mail you provided.</p>
            <p>Open your e-mail and click on the confirmation link to complete the registration.</p>
        </span>
    </div>
</div>
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->

    class RegisterDialog extends BaseDialog
        constructor: () ->
            super(html)

    RegisterDialog::init = () ->
        @RegisterDialogContent =
            @html.find(".login-content")

        @RegisterDialogBox = @html.find("#register-dialog")
        @MediaPreviewBox =
            $(media_preview_html)

        @ConfirmationDialog =
            $(registration_confirmation_html)

        @CivilChoice =
            @html.find("#civil")

        @BedouinChoice =
            @html.find("#bedouin")

        @CivilChoiceSubmenu =
            @html.find(".sub-choices")

        @RegisterButton =
            @html.find(".register-button")

        @BackButton =
            @html.find(".back-button")

        @ValidationMessage =
            @html.find(".validation-message")
        
    RegisterDialog::onPrologue = () ->
        @RegisterDialogContent.append(@MediaPreviewBox)
        
        @BackButton.click () =>
            @hide()
            SOA.Hud.showDialog("LOGIN")

        # @RegisterButton.click () =>
        #     username = 
        #         @html.find("#username").val()

        #     email = 
        #         @html.find("#email").val()

        #     password = 
        #         @html.find("#password").val()

        #     firstname = 
        #         @html.find("#firstname").val()

        #     familyname = 
        #         @html.find("#familyname").val()

        #     faction = "bedouin"
        #     if @html.find("#civil").is(":checked")
        #         faction = "civil"

        #     architecture = $("#architecture").find(":checked").val()
        #     # if not architecture or architecture is "arabian"
                 
        #     architecture = "civil"

        #     if faction is "bedouin"
        #         architecture = "bedouin"

        #     data =
        #         usn: username
        #         pass: password
        #         em: email
        #         n: firstname
        #         fn: familyname
        #         fr: faction
        #         arch: architecture

        #     @RegisterButton.hide()
            
        #     Rest.post("user.register", data)                
        #     .done (data) =>
        #         @RegisterDialogContent.empty()
        #         @RegisterDialogContent.append(@ConfirmationDialog)
        #         @RegisterDialogContent.append(@MediaPreviewBox)
        #     .fail (data) =>
        #         @RegisterButton.show()

    return RegisterDialog