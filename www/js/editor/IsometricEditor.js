(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher", "editor/LoaderDialog", "editor/SpritesDialog", "editor/TilesDialog", "editor/MainMenu", "editor/BufferView"], function(EventDispatcher, LoaderDialog, SpritesDialog, TilesDialog, MainMenu, BufferView) {
    var API, IsometricEditor, SPRITES_URI;
    SPRITES_URI = "assets/sprites.list";
    API = {
      SAVE_SECTION: "/map/{0}/region/{1}/{2}/{3}/save",
      SAVE_MAP_TILE: "/map/{0}/tiles/add"
    };
    IsometricEditor = (function(_super) {
      __extends(IsometricEditor, _super);

      function IsometricEditor() {
        IsometricEditor.__super__.constructor.call(this);
        this.init();
        this.initListeners();
      }

      return IsometricEditor;

    })(EventDispatcher);
    IsometricEditor.prototype.init = function() {
      this.container = $("<div/>", {
        "class": "editor-dialog-container"
      });
      $(HAL.Dom.viewport).append(this.container);
      this.initLoaderDialog();
      this.mouse_sprite = null;
      this.shift_pressed = false;
      return this.selected_tile = null;
    };
    IsometricEditor.prototype.loadMap = function(map_data) {
      this.map_data = map_data;
      if (this.scene != null) {
        HAL.IsometricScene.stopRendering(this.scene);
      }
      this.scene = new HAL.IsometricScene(this.map_data.name);
      this.scene.init();
      this.regmngr = this.scene.map.region_mngr;
      this.container.empty();
      this.downloadSprites();
      return this.loader_dialog.showSpinner();
    };
    IsometricEditor.prototype.initLoaderDialog = function() {
      var _this = this;
      this.loader_dialog = new LoaderDialog();
      return this.loader_dialog.prologue().done(function() {
        return $(HAL.Dom.viewport).append(_this.loader_dialog.html);
      });
    };
    IsometricEditor.prototype.initSpritesDialog = function() {
      this.sprites_dialog = new SpritesDialog(PIXI.TextureCache);
      return this.container.append(this.sprites_dialog.html);
    };
    IsometricEditor.prototype.initBufferView = function() {};
    IsometricEditor.prototype.initMainMenu = function() {
      this.main_menu = new MainMenu();
      return this.container.append(this.main_menu.html);
    };
    IsometricEditor.prototype.initTilesDialog = function() {
      var _this = this;
      this.tiles_dialog = new TilesDialog();
      this.container.append(this.tiles_dialog.html);
      this.container.append(this.tiles_dialog.edit.html);
      this.tiles_dialog.on("TILE_SELECTED", function(selected_tile) {
        var texture;
        _this.selected_tile = selected_tile;
        texture = PIXI.TextureCache[_this.selected_tile.sprite];
        if (_this.mouse_sprite == null) {
          _this.mouse_sprite = new PIXI.Sprite(texture);
          _this.mouse_sprite.visible = false;
          return Editor.scene.attachSpriteToMouse(_this.mouse_sprite);
        } else {
          return _this.mouse_sprite.setTexture(texture);
        }
      });
      this.scene.on("MOUSE_CLICK", function(button, x, y) {
        if (button === 0) {
          return _this.onLeftClick(_this.scene.world_pos.x, _this.scene.world_pos.y);
        }
      });
      this.scene.on("KEY_UP", function(ev) {
        if (_this.shift_pressed && (_this.mouse_sprite != null)) {
          _this.mouse_sprite.visible = false;
          return _this.shift_pressed = false;
        }
      });
      return this.scene.on("KEY_DOWN", function(ev) {
        if (ev.shiftKey && (_this.mouse_sprite != null)) {
          _this.shift_pressed = true;
          return _this.mouse_sprite.visible = true;
        }
      });
    };
    IsometricEditor.prototype.onLeftClick = function(x, y) {
      var region, section, tilemeta, _ref,
        _this = this;
      if ((this.mouse_sprite != null) && this.mouse_sprite.visible) {
        tilemeta = this.tiles_dialog.storage.findByID(this.selected_tile.seedId);
        this.scene.map.addTileLayerAtPosition(x, y, tilemeta);
        console.debug("MY seedId is " + this.selected_tile.seedId);
        _ref = this.regmngr.writeToSectionBufferAtPosition(x, y, tilemeta.layer, this.selected_tile.seedId), region = _ref[0], section = _ref[1];
        this.regmngr.redrawCurrentSections();
        this.scene.map.sortLayers();
        return this.saveSection(region, section).done(function() {
          return _this.saveTileMeta(tilemeta);
        });
      }
    };
    IsometricEditor.prototype.saveSection = function(region, section) {
      var key, sectionIndex, url;
      console.debug(region, section);
      key = this.regmngr.getRegionKey(region).split("_");
      sectionIndex = this.regmngr.getSectionIndex(region, section);
      url = API.SAVE_SECTION.f(this.scene.map.map_meta.name, key[0], key[1], sectionIndex);
      return $.post(url, {
        buffers: this.regmngr.encodeSectionBuffers(section)
      });
    };
    IsometricEditor.prototype.saveTileMeta = function(tilemeta) {
      var _this = this;
      return $.post(API.SAVE_MAP_TILE.f(this.scene.map.map_meta.name), tilemeta).done(function() {
        return _this.scene.map.tile_storage.addTile(tilemeta);
      });
    };
    IsometricEditor.prototype.initListeners = function() {
      var _this = this;
      this.loader_dialog.on("MAP_LOADED", function(data) {
        return _this.loadMap(data);
      });
      this.on("SPRITES_PROGRESS", function() {});
      return this.on("SPRITES_COMPLETE", function() {
        var _this = this;
        HAL.IsometricScene.startRendering(this.scene);
        HAL.Dom.clearInfoBox();
        HAL.Dom.updateTextOn(this.scene, "MOUSE_MOVED", "Mouse position: {0}, {1}");
        HAL.Dom.updateTextOn(this.scene, "MOUSE_WORLD_CHANGED", "Mouse world position: {0}, {1}");
        HAL.Dom.updateTextOn(this.scene.map, "MOUSE_OVER_TILE", "Tile position in world: {0}, {1}");
        HAL.Dom.updateTextOn(this.regmngr, "REGION_CHANGED", "In region no. {0}, {1}");
        HAL.Dom.updateTextOn(this.regmngr, "MOUSE_OVER_REGION", "Mouse over region: {0},{1}, section: {2},{3}");
        HAL.Dom.updateTextOn(this.regmngr, "REGION_TILE_CHANGED", "Tile position in section: {0}, {1}");
        this.regmngr.on("LOADED", function() {
          _this.initTilesDialog();
          _this.initSpritesDialog();
          _this.initMainMenu();
          _this.initBufferView();
          _this.loader_dialog.html.hide();
          return _this.loader_dialog.hideSpinner();
        });
        return this.scene.map.load(this.map_data);
      });
    };
    IsometricEditor.prototype.downloadSprites = function() {
      var $deferred, $request,
        _this = this;
      $request = $.get(SPRITES_URI);
      $deferred = new $.Deferred();
      $request.done(function(data) {
        var sheetLoader;
        sheetLoader = new PIXI.AssetLoader(data.split("\n"));
        sheetLoader.onProgress = function(item) {
          _this.trigger("SPRITES_PROGRESS", _this, item);
          return $deferred.progress(item);
        };
        sheetLoader.onComplete = function() {
          _this.trigger("SPRITES_COMPLETE");
          return $deferred.resolve();
        };
        return sheetLoader.load();
      }).fail(function() {
        return $deferred.reject("Download of " + SPRITES_URI + " failed");
      });
      return $deferred.promise();
    };
    return IsometricEditor;
  });

}).call(this);
