CommanderModel = require "../models/soa/heroes/Commander"

module.exports = (app) ->

	app.get "/", (req, res) ->
		res.render("./layouts/main")

	app.get "/editor", (req, res) ->
		seeder = require("../models/editor/TileSeed")
		seeder(app)
		res.render("main", {editor: true})

	app.get "/empire/new", (req, res) ->
		return
		
		# comm = new CommanderModel
		# 	name: "Tralala"
		# 	coords:
		# 		ox: 1
		# 		oy: 2
		# 		dx: 5
		# 		dy: 10
		# 	level: 5
		# 	stat:
		# 		speed: 5
		# 		leadership: 10
		# 		readiness: 15

		# 	experience: 15

		# 	unassignedPoints: 100

		# comm.save()

		# console.dir(comm)
		# res.send(comm)
