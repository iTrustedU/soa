(function() {
  var TileMetaModel, mongoose, schema;

  mongoose = require("mongoose");

  schema = mongoose.Schema({
    name: {
      type: String,
      required: true,
      unique: true
    },
    layer: {
      type: Number,
      required: true
    },
    span: {
      type: String,
      required: true
    },
    sprite: {
      type: String,
      required: true
    },
    seedId: {
      type: Number,
      required: true
    }
  });

  module.exports = TileMetaModel = mongoose.model("TileMeta", schema);

  schema.pre("save", function(next) {
    return TileMetaModel.findOne({
      seedId: this.seedId
    }).exec(function(err, result) {
      if (result != null) {
        return next(new Error("Tile SeedID is a duplicate!!!"));
      } else {
        return next();
      }
    });
  });

}).call(this);
