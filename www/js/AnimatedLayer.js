(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["PIXI"], function(PIXI) {
    var AnimatedLayer, TILE_HEIGHT, TILE_WIDTH;
    TILE_HEIGHT = 64;
    TILE_WIDTH = 128;
    AnimatedLayer = (function(_super) {
      __extends(AnimatedLayer, _super);

      function AnimatedLayer(parentTile, meta) {
        this.parentTile = parentTile;
        /* @todo 
        				Meta textures could become just an array of strings
        */

        AnimatedLayer.__super__.constructor.call(this, meta.textures);
        this.z = meta.layer;
        this.id = meta.id;
        this.animationSpeed = 0.3;
        this.init();
      }

      return AnimatedLayer;

    })(PIXI.MovieClip);
    AnimatedLayer.prototype.init = function() {
      this.y_off = (this.getBounds().height - TILE_HEIGHT) * 0.5;
      this.position.x = this.parentTile.position.x;
      this.position.y = this.parentTile.position.y;
      this.interactive = true;
      this._pass_me_pixi_ = true;
      return this.hitArea = this._bounds.clone();
    };
    AnimatedLayer.prototype.mouseover = function() {
      if (this.onMouseOver != null) {
        return this.onMouseOver();
      }
    };
    AnimatedLayer.prototype.mouseout = function() {
      if (this.onMouseOut != null) {
        return this.onMouseOut();
      }
    };
    AnimatedLayer.prototype.onClick = function(btn) {
      if (btn === 0) {
        if (this.onLeftClick != null) {
          return this.onLeftClick();
        }
      } else {
        if (this.onRightClick != null) {
          return this.onRightClick();
        }
      }
    };
    AnimatedLayer.prototype.changeTexture = function(tex_name) {
      var texture;
      texture = PIXI.TextureCache[tex_name];
      if (texture == null) {
        throw new Error("No such texture " + tex_name + "!");
      }
      this.texture = texture;
      this.y_off = (this.getBounds().height - TILE_HEIGHT) * 0.5;
      this.position.x = this.parentTile.position.x;
      return this.position.y = this.parentTile.position.y - this.y_off;
    };
    AnimatedLayer.prototype.changeParentTile = function(parentTile, isPlaying) {
      if (this.playing) {
        this.stop();
      }
      delete this.parentTile.layers[this.z];
      this.parentTile = parentTile;
      this.parentTile.layers[this.z] = this;
      this.y_off = (this.getBounds().height - TILE_HEIGHT) * 0.5;
      this.position.x = this.parentTile.position.x;
      this.position.y = this.parentTile.position.y - this.y_off;
      return this.play();
    };
    return AnimatedLayer;
  });

}).call(this);
