"use strict"

define [], () ->

	class IntervalQueue
		constructor: () ->
			@intervals = {}
			@_ids_ = {}

		createInterval: (interval) ->
			@intervals[interval] = []
			id = @_ids_[interval] = 
			setInterval((id) => 
				clb(id) for clb in @intervals[interval].slice()
			, interval)
			return id

		clear: (interval, callb) ->
			id = @_ids_[interval]
			calls = @intervals[interval]
			ind = calls.indexOf(callb)
			if ind isnt -1
				calls.splice(ind, 1)
			else if calls.length is 0
				clearInterval(id)
		
		addToQueue: (callb, interval, name = "") ->
			if not @intervals[interval]?
				@createInterval(interval)
			queue = @intervals[interval]
			if queue.indexOf(callb) isnt -1
				throw new Error("Same callback is already on interval #{interval} queue")
			queue.push(callb)

	return (window.interval_queue = new IntervalQueue())