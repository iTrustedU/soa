
"use strict"

html = 
"""
<div class="action-box" style="z-index: 10000;">
    <img class="action-box-img" />
    <img src="assets/sprites/amjad/abox/move.png" class="ac-move-to" />
    <img src="assets/sprites/amjad/abox/trade.png" class="ac-spy" />
    <img src="assets/sprites/amjad/abox/camp.png" class="ac-camp" />
    <img src="assets/sprites/amjad/abox/attack.png" class="ac-attack" />
</div>
"""

# <img src="img/action-box/move-to-icon.png" class="ac-move-to" />
# <img src="img/action-box/attack-icon.png" class="ac-attack" />
# <img src="img/action-box/spy-icon.png" class="ac-spy" />
# <img src="img/action-box/incampment-icon.png" class="ac-camp" />


define ["soa/hud/dialogs/BaseDialog"], 

(BaseDialog) ->

    class ActionBox extends BaseDialog
        constructor: () ->
            super(html)

    ActionBox::onPrologue = () ->
        @hide()

        @MoveBtn = @html.find(".ac-move-to")
        @AttackBtn = @html.find(".ac-attack")
        @CampBtn = @html.find(".ac-camp")
        @TradeBtn = @html.find(".ac-spy")

        # @clickTriggers(
        #     ".ac-move-to/#": "HERO_MOVE_TO"
        #     ".ac-attack/#": "HERO_ATTACK"
        #     ".ac-camp/#": "HERO_CAMP"
        #     ".ac-trade/#": "HERO_TRADE"
        #     ".action-box-img/#": "HIDE_BOX"
        # )

        @MoveBtn.click () =>
            @ctx.doAction("move")
            @hide()

        @AttackBtn.click () =>
            @ctx.doAction("attack")
            @hide()
        
        @TradeBtn.click () =>
            @ctx.doAction("trade")
            @hide()

        @CampBtn.click () =>
            @ctx.doAction("camp")
            @hide()
        
        @html.click () =>
            @hide()
            
    ActionBox::setMoveAsToCity = () ->
        @MoveBtn.attr("src", "assets/sprites/amjad/abox/city.png")

    ActionBox::setMoveAsDefault = () ->
        @MoveBtn.attr("src", "assets/sprites/amjad/abox/move.png")

        # ".ac-move-to-city/#": "HERO_MOVE_TO_CITY"
        # ActionBox.on "HIDE_BOX", () ->
        #     @hide()

        # ActionBox.on "SHOW", () ->
        #     uri = Handlebars.helpers.concat_hero_avatar(@ctx.getType(), "s")
        #     middle = @html().find(".action-box-img")
        #     middle.attr("src", uri)

        # ActionBox.move = (type, callb) ->
        #     where = @get "ACT_UPON"
        #     @set "HERO", @ctx
        #     @get("HERO").doAction(type, where)
        #     @hide()

        # ActionBox.on "HERO_ATTACK", () ->
        #    ActionBox.move("ATTACK", @ctx)

        # @on "HERO_MOVE_TO_CITY", () ->
        #     @ctx.doAction("move-city")



    return ActionBox
