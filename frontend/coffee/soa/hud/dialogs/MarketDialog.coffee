"use strict"

html =
"""
<div class="market-tent draggable">
	<div class="dialog-name">
		<span>Market Tent</span>
		<div class="close-button"></div>
	</div>
	{{{trade_box}}}
	<div class="port-building-info">
		<img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img" />
		<div class="upgrade-building">Upgrade</div>
		<div id="move-building" class="upgrade-building move-button">Move</div>
	</div>
	{{{current_trade}}}
</div>
"""

$trade_box = 
"""
<div class="trade-boxes">
	<div class="market-tent-name-1">
		<span>Server</span>
	</div>
	<div class="market-tent-name-2">
		<span>Player</span>
	</div>
	<div class="server-items">
		<div class="item-box ietm-box-1">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-2">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-3">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-4">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-5">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
	</div>
	<div class="player-items">
		<div class="item-box ietm-box-1">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-2">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-3">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-4">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
		<div class="item-box ietm-box-5">
			<img src="{{concat_sprite building}}" />
			<input type="text" value={{price}} />
		</div>
	</div>
</div>
"""

$current_trade = 
"""
<div class="current-trade">
	<div class="item-box ietm-box-6">
		<img src="{{concat_sprite building}}" />
		<input type="text" value={{price}} />
	</div>
	<div class="item-box ietm-box-7">
		<img src="{{concat_sprite building}}" />
		<input type="text" value={{price}} />
	</div>
	<div class="market-tent-slider-box">
		<span class="min-max">Min</span>
		<div class="market-tent-slider">
			<div class="slider-bar-l"></div>
			<div class="slider-bar-r"></div>
			<div class="slider-bar-middle">
				<div class="slider"></div>
			</div>
		</div>
		<span class="min-max">Max</span>
	</div>  
</div>
"""

define ["jquery"],
($) ->

	trade_box =
		building: "buildings/war"
		price: 150

	template = Handlebars.compile($trade_box);
	mk = template(trade_box)

	template = Handlebars.compile($current_trade);
	ct = template(trade_box)

	template = Handlebars.compile(html)
	$MarketDialog = $(template(
		trade_box: mk
		current_trade: ct
	))

	Hal.trigger "DOM_ADD", (domlayer) ->
		$(domlayer).append($MarketDialog)

	Hal.on "OPEN_MARKET_DIALOG", (market) ->
		$MarketDialog.show()