(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Shims", "PIXI", "EventDispatcher", "LoggingQueue", "TileMetaStorage", "DOMManager", "ImageUtils", "IsometricScene", "MathUtils", "Ajax", "Animations", "Tweener"], function(Shims, PIXI, EventDispatcher, LoggingQueue, TileMetaStorage, DOMManager, ImageUtils, IsometricScene, MathUtils, Ajax, Animations, Tweener) {
    var HAL;
    HAL = (function(_super) {
      __extends(HAL, _super);

      function HAL() {
        HAL.__super__.constructor.call(this);
        this.__started__ = false;
        this.Ajax = Ajax;
        this.IsometricScene = IsometricScene;
        this.MathUtils = MathUtils;
        this.Tweener = Tweener;
      }

      return HAL;

    })(EventDispatcher);
    HAL.prototype.start = function() {
      var _this = this;
      if (this.__started__) {
        throw new Error("HAL is already initialized!");
      }
      this.__started__ = true;
      this.log = new LoggingQueue();
      this.TileMetaStorage = new TileMetaStorage();
      this.Dom = new DOMManager();
      this.ImageUtils = new ImageUtils();
      this.Animations = new Animations();
      setTimeout((function() {
        return _this.trigger("HAL_LOADED");
      }), 100);
      return this;
    };
    window.PIXI = PIXI;
    window.HAL = new HAL();
    return window.HAL;
  });

}).call(this);
