"use strict"

html =
"""
<div class="build-order">
    <div id="build">{{{build_template}}}</div>
    <div id="research">{{{research_template}}}</div>
    <div id="training">{{{training_template}}}</div>
</div>
"""

$build_template =
"""
<div class="build-order-content">
    <div class="build-order-box build-box">
        <div class="order-info">
            <span class="name">{{lang name}}</span>
            <span class="status">{{title}}</span><br/>
            <span id="time-left" class="time">{{time}}</span>
            <div class="order-progress-bar">
                <div class="order-inner-progress-bar">
                    {{{time_left}}}
                </div>
            </div>
        </div>
        <div class="order-content">
            <img width="89px" src={{sprite}}>
            <div class="order-button">
                <span>Finish</span>
            </div>
            <div class="order-button">
                <span>Cancel</span>
            </div>
        </div>
    </div>
    <div class="build-order-tab tab-build"></div>
</div>
"""

$research_template =
"""
<div class="build-order-content">
    <div class="build-order-box research-box">
        <div class="order-info">
            <span class="name">{{lang name}}</span>
            <span class="status">{{title}}</span><br/>
            <span id="time-left" class="time">{{time}}</span>
            <div class="order-progress-bar">
                <div class="order-inner-progress-bar">
                    {{{time_left}}}
                </div>
            </div>
        </div>
        <div class="order-content">
            <img width="89px" src={{sprite}}>
            <div class="order-button">
                <span>Finish</span>
            </div>
            <div class="order-button">
                <span>Cancel</span>
            </div>
        </div>
    </div>
    <div class="build-order-tab tab-research"></div>
</div>
"""

$training_template = 
"""
<!-- training -->
<div class="build-order-content">
    <div class="build-order-box training-box">
        <div class="order-info">
            <span class="name">{{lang name}}</span>
            <span class="status">{{title}}</span><br/>
            <span id="time-left" class="time">{{time}}</span>
            <div class="order-progress-bar">
                <div class="order-inner-progress-bar">
                    {{{time_left}}}
                </div>
            </div>
        </div>
        <div class="order-content">
            <img width="89px" src={{sprite}} />
            <div class="order-button">
                <span>Finish</span>
            </div>
            <div class="order-button">
                <span>Cancel</span>
            </div>
        </div>
    </div>
    <div class="build-order-tab tab-training"></div>
</div>
"""

$progress_template = 
"""
<div id="progress-bar" style="width:{{time_left}}%"></div>
"""

define ["jquery-ui", "soa/hud/IntervalQueue", "soa/hud/dialogs/BaseDialog"], 

($, IntervalQueue, BaseDialog) ->
    
    REFRESH_TIME = 1000
    class BuildOrders extends BaseDialog
        constructor: () ->
            super(html)

    BuildOrders::init = () ->
        @setTemplates
            "training_template": $training_template
            "build_template": $build_template
            "research_template": $research_template
            "progress_template": $progress_template

    BuildOrders::attachListeners = () ->
        dlg = @html
        build_tab = dlg.find(".tab-build")
        build_tab.unbind("click")
        build_tab.click () ->
            dlg.find(".build-box").toggle()
            dlg.find(".tab-build").toggleClass("tab-build-click")
            dlg.find(".research-box").hide()
            dlg.find(".tab-research").removeClass("tab-research-click")
            dlg.find(".workshop-box").hide()
            dlg.find(".tab-workshop").removeClass("tab-workshop-click")
            dlg.find(".training-box").hide()
            dlg.find(".tab-training").removeClass("tab-training-click")
            dlg.find(".tab-training").removeClass("tab-training2")
            dlg.find(".tab-workshop").removeClass("tab-workshop2")
        
        training_tab = dlg.find(".tab-training")
        training_tab.unbind("click")
        training_tab.click () ->
            dlg.find(".training-box").toggle()
            dlg.find(".tab-training").toggleClass("tab-training-click")
            dlg.find(".build-box").hide()
            dlg.find(".tab-build").removeClass("tab-build-click")
            dlg.find(".workshop-box").hide()
            dlg.find(".tab-workshop").removeClass("tab-workshop-click")
            dlg.find(".research-box").hide()
            dlg.find(".tab-research").removeClass("tab-research-click")
            dlg.find(".tab-training").removeClass("tab-training2")
            dlg.find(".tab-workshop").toggleClass("tab-workshop2")
            dlg.find(".tab-workshop").removeClass("tab-workshop3")
        
        research_tab = dlg.find(".tab-research")
        research_tab.unbind("click")
        research_tab.click () ->
            dlg.find(".research-box").toggle()
            dlg.find(".tab-research").toggleClass("tab-research-click")
            dlg.find(".build-box").hide()
            dlg.find(".tab-build").removeClass("tab-build-click")
            dlg.find(".workshop-box").hide()
            dlg.find(".tab-workshop").removeClass("tab-workshop-click")
            dlg.find(".training-box").hide()
            dlg.find(".tab-training").removeClass("tab-training-click")
            dlg.find(".tab-training").toggleClass("tab-training2")
            dlg.find(".tab-workshop").toggleClass("tab-workshop3")
        
    BuildOrders::formatMillSeconds = (msecs) ->
        date = new Date(0)
        date.setHours(0)
        date.setMilliseconds(msecs)
        minutes = "#{date.getMinutes()}"
        minutes = "0#{minutes}" if minutes.length is 1
        hours = "#{date.getHours()}"
        hours = "0#{hours}" if hours.length is 1
        seconds = "#{date.getSeconds()}"
        seconds= "0#{seconds}" if seconds.length is 1
        return "#{hours}:#{minutes}:#{seconds}"

    BuildOrders::formatSeconds = (secs) ->
        res = @formatMillSeconds(secs*1000)
        return res

    BuildOrders::attachTimerFunctors = (obj, duration, buildTime, callb) ->
        obj.decreaseDuration = 
            decDur = () ->
                duration--
                if duration <= 0
                    callb.call(obj)
                    IntervalQueue.clear(1000, decDur)

        obj.timeLeft = () ->
            return duration

        obj.timeLeftPercentage = () ->
            out = ((1 - (@timeLeft() / buildTime) * 1000)).toFixed(2)
            # console.debug out
            #console.debug ((@timeLeft() / buildTime) * 1000).toFixed(2)
            return out

        return obj

    BuildOrders::addToQueue = (obj, spriteURI, name, duration, buildTime, tempName, title) ->
        $deferred = new $.Deferred()
        
        obj = @attachTimerFunctors(
            obj,
            duration,
            buildTime,
            () =>
                $deferred.resolve()
        )

        IntervalQueue.addToQueue(obj.decreaseDuration, 1000)

        tleft_data = @compileTemplate(
            "progress_template", 
            "time_left", 
            obj.timeLeftPercentage()
        )

        ref = @get("refresh_#{tempName}")
        if ref?
            IntervalQueue.clear(REFRESH_TIME, ref)

        dataN = "#{tempName}_queue"
        @setData dataN,
            name: name
            title: title
            time: @formatSeconds(obj.timeLeft())
            sprite: spriteURI
            time_left: tleft_data

        @compile("#{tempName}_template", dataN).then (data) =>
            @refresh(tempName, data)
            @attachListeners()
            @show()
            $tleft = $(@html.find("#time-left"))
            $prbar = $(@html.find("##{tempName} #progress-bar"))
            refresh =
                (id) =>
                    tleft = obj.timeLeftPercentage() * 100
                    if tleft >= 100
                        IntervalQueue.clear(REFRESH_TIME, refresh)
                        @html.find("##{tempName}").empty()
                    $prbar.css("width", tleft+"%")
                    $tleft.text(@formatSeconds(obj.timeLeft()))
            @set "refresh_#{tempName}", refresh
            IntervalQueue.addToQueue refresh, REFRESH_TIME

        return $deferred.promise()

    #where type is one of ["constructing", "upgrading"]
    BuildOrders::addConstructionToBuildQueue = (constr, title) ->
        buildTime = 0.2*60*1000 #+(XMLStore.get("constructions").zone[@c_type].time*60*1000)
        
        return @addToQueue(
            constr
            Handlebars.helpers.concat_construction constr.c_type
            constr.c_type
            constr.duration
            buildTime
            "build"
            title
        )

    #where type is one of ["constructing", "upgrading"]
    BuildOrders::addConstructionToUpgradeQueue = (constr, title) ->
        buildTime = 0.2*60*1000

        return @addToQueue(
            constr
            Handlebars.helpers.concat_construction constr.c_type
            constr.c_type
            constr.duration
            buildTime
            "build"
            title
        )

    BuildOrders::addPOIToEncampmentQueue = (poi, title) ->
        buildTime = 0.2*60*1000

        return @addToQueue(
            poi
            Handlebars.helpers.concat_poi poi.c_type
            poi.c_type
            poi.duration
            buildTime
            "build"
            title
        )

    #where type is one of ["constructing", "upgrading"]
    BuildOrders::addUnitToTrainingQueue = (unit, title) ->
        buildTime = 0.2*60*1000

        return @addToQueue(
            unit
            Handlebars.helpers.concat_unit_avatar(unit.type, "44x44")
            unit.type
            unit.duration
            buildTime
            "training"
            title
        )

    BuildOrders::addScienceToResearchQueue = (science, bldtime, title) ->
        return @addToQueue(
            unit
            Handlebars.helpers.concat_unit_avatar(unit.type, "44x44")
            science.type
            science.duration
            science.buildTime
            "training"
            title
        )

    #if not constr.poi_id?
    #buildTime = 0.2*60*1000
        # return +(XMLStore.get("constructions").city[@c_type]["level#{@level+1}"].time*60*1000)
    #else
    # buildTime=  0.2*60*1000 #+(XMLStore.get("constructions").zone[@c_type].time*60*1000)
    
    # return @addToQueue(
    #     unit
    #     Handlebars.helpers.concat_construction constr.c_type
    #     constr.c_type
    #     constr.duration
    #     buildTime
    #     "build"
    #     type
    # )

    # BuildOrders::addToTrainingQueue = (unit, act) ->
    #     tleft_data = @compileTemplate(
    #         "progress_template", 
    #         "time_left",
    #         (unit.timeLeftPercentage() * 100)
    #     )

    #     ref = @get("refresh_training")
    #     if ref?
    #         IntervalQueue.clear(REFRESH_TIME, ref)

    #     @setData "train_queue",
    #         name: unit.type
    #         action: act
    #         time: @formatMillSeconds(unit.timeLeft())
    #         sprite: Handlebars.helpers.concat_unit_avatar unit.type, "44x44"
    #         train_time_left: tleft_data

    #     @compile("training_template", "train_queue").then (data) =>
    #         @refresh("train", data)
    #         @attachListeners()
    #         @show()
    #         $tleft = $(@html.find("#time-left"))
    #         $prbar = $(@html.find("#train #progress-bar"))
    #         refresh = 
    #             (id) =>
    #                 tleft = unit.timeLeftPercentage() * 100
    #                 if tleft >= 100
    #                     IntervalQueue.clear(REFRESH_TIME, refresh)
    #                     @html.find("#train").empty()
    #                     res = amjad.finishProduction(amjad.empire.constructions.getAllOfType("training_grounds").construction_id)
    #                     res.done (data) ->
    #                         amjad.empire.constructions.getAllOfType("war").addUnit(unit)
    #                         war = SOA.Hud.getDialog("WAR")
    #                         if war.opened
    #                             war.show(amjad.empire.constructions.getAllOfType("war"))
    #                 $prbar.css("width", tleft+"%")
    #                 $tleft.text(@formatMillSeconds(unit.timeLeft()))
    #         @set "refresh_training", refresh 
    #         IntervalQueue.addToQueue(refresh, REFRESH_TIME)
    
    #typeOfResearch we are passing science or tech as parameter 
    #as different sprite should be passed for tech and science
    
    BuildOrders::addToResearchQueue = (research, title, typeOfResearch=null) ->
        tleft_data = @compileTemplate(
            "progress_template", 
            "time_left",
            (research.timeLeftPercentage() * 100)
        )

        ref = @get("refresh_research")
        if ref?
            IntervalQueue.clear(REFRESH_TIME, ref)

        @setData "research_queue",
            name: research.name
            title: title
            time: @formatMillSeconds(research.timeLeft())
            sprite: Handlebars.helpers.concat_research(research.name, typeOfResearch)
            time_left: tleft_data

        @compile("research_template", "research_queue").then (data) =>
            @refresh("research", data)
            @attachListeners()
            @show()
            $tleft  = $(@html.find("#time-left"))
            $prbar  = $(@html.find("#research #progress-bar"))
            refresh = 
                (id) =>
                    tleft = research.timeLeftPercentage() * 100
                    if tleft >= 100
                        IntervalQueue.clear(REFRESH_TIME, refresh)
                        @html.find("#research").empty()
                        
                        if typeOfResearch #case that we are researching science
                            res = amjad.finishScienceResearch(research.name)
                            res.done (data) ->
                                science = SOA.Hud.getDialog("SCIENCE")
                                if science.opened
                                    science.show(amjad.empire.constructions.science)
                        else    #case when we are researching technology
                            res = amjad.finishTechResearch(research.name)
                            res.done (data) ->
                                science = SOA.Hud.getDialog("SCIENCE")
                                if science.opened
                                    science.show(amjad.empire.constructions.science)

                    $prbar.css("width", tleft+"%")
                    $tleft.text(@formatMillSeconds(research.timeLeft()))
            @set "refresh_research", refresh 
            IntervalQueue.addToQueue(refresh, REFRESH_TIME)

    return BuildOrders