"use strict"


define [], () ->
    
    ##console.debug "Importing shims"
    ###
        A shim to support timer. 
        performance.now is an ultra-precise timer and is preferred over Date.now
    ###
    if not window.performance?
        window.performance = Date

    String.prototype.format = String.prototype.f = () ->
        if arguments[0] instanceof Array
            args = arguments[0]
        else
            args = arguments
        i = args.length
        s = @
        while (i--)
            s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), args[i])
        return s
        
    String.prototype.endsWith = (str) ->
        return @lastIndexOf(str) + str.length is @length

    String.prototype.startsWith = (str) ->
        return @indexOf(str) is 0

    Object.defineProperty(Array.prototype, "chunk", {
    value: Array.prototype.chunk = (chunkSize) ->
    return (@slice(i, i + chunkSize) for i in [0...@length] by chunkSize)
    })
        

    # creates a global "addWheelListener" method
    # example: addWheelListener( elem, function( e ) { console.log( e.deltaY ); e.preventDefault(); } );

    # ((window, document) ->

    #     prefix = "", _addEventListener, onwheel, support

    #     # detect event model
    #     if ( window.addEventListener )
    #         _addEventListener = "addEventListener"
    #     else
    #         _addEventListener = "attachEvent"
    #         prefix = "on"

    #     # detect available wheel event
    #     support = "onwheel" in document.createElement("div") ? "wheel" : # Modern browsers support "wheel"
    #               document.onmousewheel isnt undefined ? "mousewheel" : # Webkit and IE support at least "mousewheel"
    #               "DOMMouseScroll" # let's assume that remaining browsers are older Firefox

    #     window.addWheelListener = ( elem, callback, useCapture ) ->
    #         _addWheelListener( elem, support, callback, useCapture )

    #         # handle MozMousePixelScroll in older Firefox
    #         if support is "DOMMouseScroll" 
    #             _addWheelListener( elem, "MozMousePixelScroll", callback, useCapture )
        
     
    #     _addWheelListener = (( elem, eventName, callback, useCapture ) ->
    #         elem[ _addEventListener ](prefix + eventName, support is "wheel" ? callback : ( originalEvent ) ->
    #             !originalEvent && ( originalEvent = window.event )

    #             #create a normalized event object
    #             event =
    #                 #keep a ref to the original event object
    #                 originalEvent: originalEvent
    #                 target: originalEvent.target or originalEvent.srcElement
    #                 type: "wheel"
    #                 deltaMode: originalEvent.type is "MozMousePixelScroll" ? 0 : 1
    #                 deltaX: 0
    #                 delatZ: 0
    #                 preventDefault: () ->
    #                     originalEvent.preventDefault ?
    #                         originalEvent.preventDefault() :
    #                         originalEvent.returnValue = false
                
    #             #calculate deltaY (and deltaX) according to the event
    #             if ( support is "mousewheel" )
    #                 event.deltaY = - 1/40 * originalEvent.wheelDelta
    #                 # Webkit also support wheelDeltaX
    #                 originalEvent.wheelDeltaX and ( event.deltaX = - 1/40 * originalEvent.wheelDeltaX )
    #             else
    #                 event.deltaY = originalEvent.detail

    #             #it's time to fire the callback
    #             return callback(event)

    #         ),useCapture or false)
    # )(window,document)