"use strict"

define [
    "Tile", 
    "EventDispatcher", 
    "Vec2", 
    "Ajax", 
    "MapConfig", 
    "TileMaskTable", 
    "PathFinder",
    "Geometry",
    "RegionManager",
    "MiniMapView",
    "TileMetaStorage"
],

(
    Tile, 
    EventDispatcher, 
    Vec2, 
    Ajax, 
    MapConfig, 
    TileMaskTable, 
    PathFinder,
    Geometry,
    RegionManager,
    MiniMapView,
    TileMetaStorage
) ->

    TILE_WIDTH              = MapConfig.TILE_WIDTH
    TILE_HEIGHT             = MapConfig.TILE_HEIGHT
    MAX_LAYERS              = MapConfig.MAX_LAYERS
    DEFAULT_ANIMATED_LAYER  = MapConfig.DEFAULT_ANIMATED_LAYER
    OVER_NEW_TILE_LAYER     = MapConfig.OVER_NEW_TILE_LAYER
    TILEW2_PROP             = 2 / TILE_WIDTH
    TILEH2_PROP             = 2 / TILE_HEIGHT
    TILEW2                  = TILE_WIDTH * 0.5
    TILEH2                  = TILE_HEIGHT * 0.5
    TEXTURE_CHUNK_WIDTH     = 2048
    TEXTURE_CHUNK_HEIGHT    = 2048

    DIRECTIONS = [
        {
            north: [-1, 0],
            south: [1, 0],
            east: [0, 2],
            west: [0, -2],
            southeast: [0, 1],
            northeast: [-1, 1],
            southwest: [0, -1],
            northwest: [-1, -1]
        },
        {
            north: [-1, 0],
            south: [1, 0],
            east: [0, 2],
            west: [0, -2],
            southeast: [1, 1],
            northeast: [0, 1],
            southwest: [1, -1],
            northwest: [0, -1]
        }
    ]
    DIRECTIONS_KEYS = Object.keys(DIRECTIONS[0])

    class MapManager extends EventDispatcher
        constructor: (@scene) ->
            super()
            @init()

    MapManager::init = () ->
        @NROWS          = 0
        @NCOLUMNS       = 0
        @ortho_vector   = Vec2.from(0, 0)
        @world_bounds   = Vec2.from(0, 0)
        @mmap_view_size = Vec2.clone(@scene.screen_size)
        @layers         = []
        @tiles          = []
        @map_buffers    = []
        @map_viewports  = []
        @prerendered    = {}
        @tile_graphics  = []
        @tile_under     = null
        @last_clicked   = null
        @floor_layer    = null
        @top_layer      = null
        @pathfinder     = null
        @tile_storage   = new TileMetaStorage()

        # this thing gets rendered for each tile under a mouse
        @tile_shape     = @createFillTileShape(0, 0, MapConfig.TILE_OVER_COLOR)

        ### Hit area that gets updated with every camera transform ###
        @hit_area       = new PIXI.Rectangle(0, 0, @scene.screen_size.x, @scene.screen_size.y)

        @selection = 
            rect: new PIXI.Rectangle(0, 0, 0, 0)
            graphics: new PIXI.Graphics()
            tiles: []
        
        @region_mngr = new RegionManager(@)
        @minimap = new MiniMapView(@)

        @initListeners()
        
    MapManager::initListeners = () ->
        @scene.on "MOUSE_RIGHT_DRAG_STARTED", (mpos) =>
            @selection.rect.x = mpos.x
            @selection.rect.y = mpos.y

        @scene.on "MOUSE_RIGHT_DRAG", (mpos) =>
            @selection.rect.width = @selection.rect.x - mpos.x
            @selection.rect.height = @selection.rect.y - mpos.y
            @selection.graphics.clear()
            @selection.graphics.lineStyle(2, MapConfig.SELECTION_RECT_COLOR, 1)
            g = @selection.graphics
            g.moveTo(@selection.rect.x, @selection.rect.y)
            g.lineTo(@selection.rect.x, mpos.y)
            g.lineTo(mpos.x, mpos.y)
            g.lineTo(mpos.x, @selection.rect.y)
            g.lineTo(@selection.rect.x, @selection.rect.y)

            top_left_pivot = @getTileAtPosition(@selection.rect.x, @selection.rect.y)
            bottom_right_pivot = @getTileAtPosition(mpos.x, mpos.y)

            ncols = (bottom_right_pivot.col - top_left_pivot.col + 1)
            nrows = (bottom_right_pivot.row - top_left_pivot.row + 1)
            start_row = top_left_pivot.row
            start_col = top_left_pivot.col
            @selection.tiles = []
            graphics.clear() for graphics in @tile_graphics
            i = 0
            for col in [0...ncols]
                for row in [0...nrows]
                    tile = @getTileAt(start_row + row , start_col + col)
                    @selection.tiles.push(tile)
                    # tgraphics = @fillTileShape(tile)
                    # @scene.world.addChild(tgraphics)
                    # @selection.tile_graphics.push(@tile_graphics[i])
                    @fillTileShape(tile, @tile_graphics[i])
                    ++i

        @scene.on "MOUSE_WORLD_CHANGED", (worldX, worldY) =>
            new_tile = @getTileAtPosition(worldX, worldY)
            if new_tile? and not @areTilesEqual(new_tile, @tile_under)
                @tile_under = new_tile
                @tile_shape.position.x = @tile_under.position.x
                @tile_shape.position.y = @tile_under.position.y
                @trigger "MOUSE_OVER_TILE", @, new_tile.row, new_tile.col

        @scene.on "MOUSE_RIGHT_DRAG_ENDED", (mpos) =>
            @selection.graphics.clear()
            graphics.clear() for graphics in @tile_graphics

    MapManager::allocateTileGraphics = () ->
        total = @calculateMaxTilesOnScreen()
        for i in [0...total]
            graphics = new PIXI.Graphics()
            graphics.visible = false
            @scene.world.addChild(graphics)
            @tile_graphics.push(graphics)

    MapManager::fillTileShape = (tile, graphics) ->
        pos = @getTilePosition(tile.row, tile.col)
        graphics.visible = true
        graphics.beginFill(0xC32F01, 1)
        graphics.moveTo(-64+pos.x, 0+pos.y)
        graphics.lineTo(0+pos.x, -32+pos.y)
        graphics.lineTo(64+pos.x, 0+pos.y)
        graphics.lineTo(0+pos.x, 32+pos.y)
        graphics.lineTo(-64+pos.x, 0+pos.y)
        graphics.endFill()
        return graphics

    #removes layers from the world if they exist
    MapManager::removeLayers = () ->
        console.warn "Removing #{@layers.length} layers"
        @layers.forEach (layer) =>
            @scene.world.removeChild layer
        @layers = []

    MapManager::createFillTileShape = (x, y, color) ->
        tile_shape  = new PIXI.Graphics()
        tile_shape.beginFill(color, 0.7)
        tile_shape.moveTo(-TILEW2, 0)
        tile_shape.lineTo(0, -TILEH2)
        tile_shape.lineTo(TILEW2, 0)
        tile_shape.lineTo(0, TILEH2)
        tile_shape.lineTo(-TILEW2, 0)
        tile_shape.endFill()
        tile_shape.position.x = x
        tile_shape.position.y = y
        return tile_shape

    MapManager::tintTileLayer = (row, col, layer = 2, tint_color = 0x0292CD) ->
        pos = @getTileAt(row, col).position
        shape = @createFillTileShape(pos.x, pos.y, tint_color)
        @tile_graphics.push(shape)
        @layers[layer].addChild(shape)

    MapManager::tintSpan = (span_area, layer = 2, tint_color = 0x0292CD) ->
        for t in span_area
            @tintTileLayer(t.row, t.col, layer, tint_color)

    #Creates MAX_LAYERS, call initializeLayers
    #to set them interactive
    MapManager::createLayers = () ->
        @scene.world.addChild(@tile_shape)
        @removeLayers() if @layers.length > 0
        for i in [0...MAX_LAYERS]
            layer = @layers[i] = new PIXI.DisplayObjectContainer()
            #Communicates to PIXI's interaction manager to 
            #let me through its hit-testing
            #in the case I'm invisible
            #(such is the case when the floor layer is prerendered)
            layer._pass_me_pixi_ = true
            @scene.world.addChild(layer)
        
        @top_layer      = @layers[MAX_LAYERS-1]
        @floor_layer    = @layers[0]
        console.debug "#{MAX_LAYERS} map layers initialized"
        @initializeLayers()

    #Initializes top layer to interactive state, so that it receives click events
    #and puts the tile shape graphic on the OVER_NEW_TILE_LAYER layer
    MapManager::initializeLayers = () ->
        @top_layer.interactive  = true
        @top_layer.hitArea      = @hit_area
        @floor_layer            = @layers[0]
        @top_layer.click = (data) =>
            #we all hate PIXI for a reason!
            #this hack here stops it from calling
            #unecessary and very expensive getBoudingRect call
            #and really fucking unnecessary calculations on local matrices
            #and puts me in charge of calculating transforms!
            @floor_layer._i_hate_you_pixi_  = false
            @floor_layer._pass_me_pixi_     = false
            @floor_layer.updateTransform()

            # @scene.stage.interactionManager.hitted = []
            @scene.stage.interactionManager.update()

            #another hack I did in PIXI, hitted array holds all the 
            #displayobjects that got hit by a mouse ray
            data.hitTarget = 
                @resolveClickedLayers(@scene.stage.interactionManager.hitted.slice(), data)
            @trigger "TOP_LAYER_CLICK", @, data
            
            #now we lock it
            @floor_layer._i_hate_you_pixi_ = false #true
            @floor_layer._pass_me_pixi_ = false

    #prerenders layer_index onto a huge texture
    #that gets buffered on the GPU
    #...which isnt really a good optimization, it heavily depends on the
    #amount of VGA memory available - having huge textures is never good
    #especially when they're not PoT
    #this needs to be taken care of
    ### @todo dimensions not being used ###
    MapManager::prerenderLayer = (layer_index, frame, chunkW, chunkH) ->
        #Yes, crazy I know, instead of world_bounds dimensions
        #I should've split the texture in screen dimensions and further
        #split it in PoT chunks
        renderer    = new PIXI.RenderTexture(chunkW, chunkH)
        renderable  = new PIXI.Sprite(renderer)
        layer       = @layers[layer_index]

        #fucking anchor point defaults to 0.5
        renderable.anchor.x = 0
        renderable.anchor.y = 0
        
        layer._i_hate_you_pixi_ = false
        layer._pass_me_pixi_ = false

        #weird glitch in pixi's renderer causes it to flip the world view matrix
        layer.position.x = -frame.x
        layer.position.y = -frame.y
        layer.updateTransform()
        layer._i_hate_you_pixi_ = true
        renderer.render(layer, layer.position)

        renderable.position.x = frame.x
        renderable.position.y = frame.y

        renderer.destroy()

        return renderable

    MapManager::prerenderLayerChunks = (layer) ->
        rect = new PIXI.Rectangle()
        layert = new PIXI.DisplayObjectContainer()
        layert.visible = false
        @scene.world.addChild(layert)

        ### 
            @todo
            screen size or perhaps the next (upper, lower) power of two?
        ###
        cw  = TEXTURE_CHUNK_WIDTH
        ch  = TEXTURE_CHUNK_HEIGHT
        rect.width  = cw
        rect.height = ch
        w = 0
        h = 0

        while w < @world_bounds.x
            while h < @world_bounds.y
                rect.x = w
                rect.y = h
                try
                    chunk = @prerenderLayer(0, rect, cw, ch)
                    layert.addChild(chunk)
                catch e
                    window.alert(e)
                    break
                h+=ch
            w+=cw
            h = 0
        
        layers = @layers[layer]
        layert.visible = true
        layers.position.x = 0
        layers.position.y = 0
        @scene.world.swapChildren(layers, layert)
        layers._pass_me_pixi_ = true
        layers._i_hate_you_pixi_ = false
        layers.updateTransform()
        layers._pass_me_pixi_ = false
        layers._i_hate_you_pixi_ = true
        layers.visible = false

    MapManager::load = (@map_meta) ->
        @NROWS      = @map_meta.rows
        @NCOLUMNS   = @map_meta.columns
        @loadTileStorage(@map_meta.tiles)
        @initTiles()
        @createLayers()
        @calculateWorldBounds()
        @trigger "TILES_INITIALIZED", @, @map_meta
        @drawGrid()
        @allocateTileGraphics()
        @scene.stage.addChild(@selection.graphics)
        @scene.stage.interactive = true
        @trigger "MAP_LOADED", @

    MapManager::loadTileStorage = (tiles) ->
        @tile_storage.addTile(t) for t in tiles

    MapManager::resetMapTiles = (rows = @NROWS, cols = @NCOLUMNS) ->
        @scene.world.clear()
        @tiles.forEach (t) ->
            t.destroy()
        @tiles = []
        for layer in @layers
            for tile in layer.children.slice()
                continue if tile? and (tile instanceof PIXI.Graphics)
                layer.removeChild(tile)

    MapManager::initTiles = () ->
        for row in [0...MapConfig.NN_ROWS]
            for col in [0...MapConfig.NN_COLUMNS]
                tile = new Tile(row, col)
                @tiles.push(tile)
                x = tile.position.x = (col / 2) * TILE_WIDTH
                y = tile.position.y = (row + ((col % 2) / 2)) * TILE_HEIGHT

        @total = @NROWS * @NCOLUMNS
        @pathfinder = new PathFinder(@)

    MapManager::drawGrid = () ->
        @scene.world.lineStyle(
            MapConfig.GRID_LINE_THICKNESS,
            MapConfig.GRID_LINE_COLOR,
            MapConfig.GRID_LINE_ALPHA
        )

        y1 = 32

        pVA = Vec2.from(@world_bounds.x - 64, 0)
        pVB = Vec2.from(@world_bounds.x - 64, @world_bounds.y)

        pHA = Vec2.from(0, 0)
        pHB = Vec2.from(@world_bounds.x, 0)

        out = Vec2.from(0, 0)

        pA1 = Vec2.from(0, 0)
        pB1 = Vec2.from(0, 0)               

        for i in [0...@NROWS*2]
            y1 = TILE_HEIGHT * (i+0.5)
            x1 = y1 * 2
            Vec2.set(pA1, 0, y1)
            Vec2.set(pB1, x1, 0)
            if i > (@NCOLUMNS * 0.5 - 1)
                Geometry.findLinesIntersection(out, pA1, pB1, pVA, pVB)
            else
                Geometry.findLinesIntersection(out, pA1, pB1, pHA, pHB)
            @scene.world.moveTo(0, y1)
            @scene.world.lineTo(out.x, out.y)


        pVA = Vec2.from(0, 0)
        pVB = Vec2.from(0, @world_bounds.y)

        for i in [0...@NROWS*2]
            y1 = TILE_HEIGHT * (i + 0.5)
            x1 = @world_bounds.x - y1 * 2
            Vec2.set(pA1, @world_bounds.x, y1)
            Vec2.set(pB1, x1, 0)
            if i > (@NCOLUMNS * 0.5 - 1)
                Geometry.findLinesIntersection(out, pA1, pB1, pVA, pVB)
            else
                Geometry.findLinesIntersection(out, pA1, pB1, pHA, pHB)
            @scene.world.moveTo(@world_bounds.x, y1)
            @scene.world.lineTo(out.x, out.y)

        Vec2.release(pA1)
        Vec2.release(pB1)
        Vec2.release(pVA)
        Vec2.release(pVB)
        Vec2.release(pHA)
        Vec2.release(pHB)
        Vec2.release(out)

    MapManager::addTileLayerAtPosition = (x, y, tilemeta) ->
        t = @region_mngr.getTileAtRegionPosition(x, y)
        @addTileLayer(t.row, t.col, tilemeta)

    MapManager::addTileLayer = (row, col, tilemeta, layer) ->
        tile = @getTileAt(row, col)
        throw new Error("No tile at #{row},#{col}") if not tile?
        tile.addLayer(@layers[layer or tilemeta.layer], tilemeta)
    
    # @trigger "TILE_PLACED", @, row, col, tilemeta
    ### @TODO All of these need a rehaul ###
    # @todo Once the map is normalized to suit the current format
    # this method is out of here
    MapManager::addTileLayerByID = (row, col, id) ->
        meta = @tile_storage.findByID(id)
        return if not meta?
        return @addTileLayer(row, col, meta)

    MapManager::drawPoint = (p, size, color = 0x000000) ->
        @scene.world.beginFill(color, size)
        @scene.world.drawCircle(p.x, p.y, size)
        @scene.world.endFill()

    MapManager::addTileShape = (x, y) ->
        @scene.world.lineStyle(
            MapConfig.GRID_LINE_COLOR
            MapConfig.GRID_LINE_COLOR,
            MapConfig.GRID_LINE_ALPHA
        )
        @scene.world.moveTo(-64+x, 0+y)
        @scene.world.lineTo(0+x, -32+y)
        @scene.world.lineTo(64+x, 0+y)
        @scene.world.lineTo(0+x, 32+y)
        @scene.world.lineTo(-64+x, 0+y)

    MapManager::getTilePosition = (row, col) ->
        x = (col / 2) * TILE_WIDTH
        y = (row + ((col % 2) / 2)) * TILE_HEIGHT
        return new PIXI.Point(x, y)

    MapManager::worldCenter = () ->
        return new PIXI.Point(@world_bounds.x*0.5, @world_bounds.y*0.5)

    MapManager::getTileAtPosition = (x, y) ->
        @fillOrthoVector(x, y)
        # ret = @tiles[@ortho_vector.x + @ortho_vector.y * MapConfig.NN_COLUMNS]
        # if not ret?
        ret =
            position: @getTilePosition(@ortho_vector.y, @ortho_vector.x)
            row: @ortho_vector.y
            col: @ortho_vector.x
        return ret

    MapManager::areTilesEqual = (a, b) ->
        return (a? and b?) and (a isnt b) and (a.row is b.row and a.col is b.col)

    MapManager::fillOrthoVector = (x, y) ->
        coldiv  = ((x + TILEW2) * TILEW2_PROP)
        rowdiv  = ((y + TILEH2) * TILEH2_PROP)
        off_x   = ~~((x + TILEW2) - ~~(coldiv * 0.5) * TILE_WIDTH)
        off_y   = ~~((y + TILEH2) - ~~(rowdiv * 0.5) * TILE_HEIGHT)
        transp  = TileMaskTable[(off_x + TILE_WIDTH * off_y)]
        @ortho_vector.x = ~~(coldiv - (transp ^ !(coldiv & 1)))
        @ortho_vector.y = ~~((rowdiv - (transp ^ !(rowdiv & 1)))*0.5)

    MapManager::getTileAt = (row, col, dirX=0, dirY=0) ->
        return @tiles[(col+dirY) + (row+dirX) * MapConfig.NN_COLUMNS]

    MapManager::addLayerSpriteToTile = (row, col, layer, sprite) ->
        tile = @getTileAt(row, col)
        if not tile?
            throw new Error("No tile at #{row}, #{col}")
        meta =
            sprite: sprite
            layer: layer
            name: sprite
            id: +(Math.random() * 0xFFFFFF)
        return tile.addLayer(@layers[layer], meta)

    MapManager::loadBitmapFromFile = (file_uri) ->
        console.warn("This function is now deprecated in favor of loadPNGMap")
        Ajax.get(file_uri).success (data) =>
            @createMapFromBitmap((data.split(",").map (t) -> return +t))
    
    MapManager::getNeighbours = (tile) ->
        out = []
        parity = (tile.col % 2)
        direction = DIRECTIONS[parity]
        return out if not tile?
        for key in DIRECTIONS_KEYS
            dir = direction[key]
            n = @getTileAt(tile.row, tile.col, dir[0], dir[1])
            if n?
                out.push(n)
        return out

    MapManager::saveAsPNG = () ->
        num_buffers = Math.ceil(MAX_LAYERS / 2)
        buffers = []
        ### prepare buffers ###
        for i in [0...num_buffers]
            buffers[i] = HAL.ImageUtils.createPixelBuffer(@NCOLUMNS, @NROWS, 0xFFFFFF)
        
        for col in [0...@NCOLUMNS]
            for row in [0...@NROWS]
                tile = @getTileAt(row, col)
                color_channel = []
                color_channel[k] = 0 for k in [0...MAX_LAYERS]
                layers = tile.layers
                for l in [0...MAX_LAYERS]
                    layer = layers[l]
                    if not layer?
                        color_channel[l] = 0
                        continue
                    if not layer.id?
                        ##console.debug "Layer at #{row},#{col} has no id assigned to it"
                        continue
                    color_channel[l] = layer.id
                ### save color channels to appropriate buffers ###
                for m in [0...num_buffers]
                    buffer = buffers[m]
                    hi = color_channel[m*2]
                    lo = color_channel[(m*2)+1]
                    color = ((hi << 16) | (lo))
                    HAL.ImageUtils.putPixelToBuffer(buffer, col, row, color)
        
        return buffers
    
    MapManager::createMapFromBitmap = (mapData) ->
        @loadBitmapMap(mapData)
        HAL.trigger "MAP_LOADED", @scene

    MapManager::loadBitmapMap = (bitmap) ->
        bitmap      = bitmap.slice()
        mask        = 0xFFFF
        qword       = bitmap.pop()
        map_rows    = (qword >> 32) & mask
        map_cols    = (qword >> 16) & mask
        @createEmptyMap(map_rows, map_cols)
        @scene.world.clear()
        while (tile_qword = bitmap.pop())?
            tile_row = (tile_qword >> 32) & mask
            tile_col = (tile_qword >> 16) & mask
            for layer in [0...7]
                layer_qword  = bitmap.pop()
                continue if layer_qword is -1
                layer_id     = (layer_qword >> 32) & mask
                layer_height = (layer_qword >> 16) & mask
                @addLayerToTileByID(tile_row, tile_col, layer_id)
        @prerenderLayerChunks(0)

    MapManager::hideLayers = () ->
        layer.visible = false for layer in @layers[1...]

    MapManager::calculateMaxTilesOnScreen = () ->
        BOUNDS_TRESHOLD = 3
        width       = @scene.screen_size.x
        height      = @scene.screen_size.y
        max_rows    = Math.round(height / TILE_HEIGHT / @scene.zoom_bounds.x)
        max_cols    = Math.round(width / TILEW2 / @scene.zoom_bounds.x)
        rows        = max_rows + 2*BOUNDS_TRESHOLD
        cols        = max_cols + 2*BOUNDS_TRESHOLD
        @total      = rows * cols

    MapManager::calculateWorldBounds = () ->
        @world_bounds.x = @NCOLUMNS * TILEW2
        @world_bounds.y = @NROWS * TILE_HEIGHT
        return @world_bounds

    MapManager::createMiniMapView = () ->
        throw new Error("Not implemented")
        @map_buffers = @saveAsPNG()
        for buffer, index in @map_buffers
            map_img_div = document.createElement("div")
            map_img_div.setAttribute("class", "map-img")
            map_img_viewport = document.createElement("div")
            map_img_viewport.setAttribute("class", "map-img-viewport")
            img = HAL.ImageUtils.pixelBufferToImg(buffer)
            map_img_div.appendChild(img)
            map_img_div.appendChild(map_img_viewport)
            @dom_minimap.appendChild(map_img_div)
            @map_viewports[index] = map_img_viewport

    MapManager::updateHitAreaPosition = (x, y) ->
        top_left = @scene.toLocalObject(0, 0)
        @hit_area.x = top_left.x
        @hit_area.y = top_left.y
        @trigger "HIT_AREA_CHANGED", @, @hit_area

    MapManager::updateHitAreaDimension = (zoom) ->
        @hit_area.width = @scene.screen_size.x / @scene.world.scale.x
        @hit_area.height = @scene.screen_size.y / @scene.world.scale.x
        @trigger "HIT_AREA_CHANGED", @, @hit_area

    MapManager::loadPNGMap = (map_uris) ->
        loader = new PIXI.AssetLoader(map_uris)
        loader.onComplete = (data) =>
            return

    MapManager::resolveClickedLayers = (hitted, data) ->
        return if !hitted.length
        hitted = hitted.sort (a, b) ->
            z = a.parentTile.row - b.parentTile.row
            if z is 0
                z = (a.z - b.z)
            return z
        clicked = null
        for hit in hitted
            pt = @scene.toLocalObject(data.global.x, data.global.y, hit)
            transp = HAL.ImageUtils.isTransparent(
                hit.texture.baseTexture.source, 
                pt.x + hit.texture.frame.width*0.5 + hit.texture.frame.x, 
                pt.y + hit.texture.frame.height*0.5 + hit.texture.frame.y
            )
            continue if transp
            if not clicked?
                clicked = hit
            else
                if @areTilesEqual(clicked.parentTile, hit.parentTile) and hit.z > clicked.z
                    clicked = hit
                else if (hit.parentTile.row isnt clicked.parentTile.row) and (hit.parentTile.col isnt clicked.parentTile.col)
                    if (hit.y_off + hit.position.y > clicked.y_off + clicked.position.y)
                        clicked = hit
                else if (hit.parentTile.row is clicked.parentTile.row)
                    if ((hit.position.x + pt.x) > clicked.position.x)
                        clicked = hit
                else if (hit.parentTile.col is clicked.parentTile.col)
                    if (hit.y_off + hit.position.y > clicked.y_off + clicked.position.y)
                        clicked = hit
                else
                    clicked = hit

        return if not clicked?
        button_type = data.originalEvent.button

        # if it's a left click
        if button_type is 0 and not @scene.is_dragged
            clicked.onClick(button_type)
        return clicked

    MapManager::addAnimatedLayer = (row, col, texture_array) ->
        tile = @getTileAt(row, col)
        if not tile?
            throw new Error("No tile at #{row}, #{col}")
        meta = 
            textures: texture_array
            layer: DEFAULT_ANIMATED_LAYER
            id: 0
        return tile.addAnimatedLayer(@layers[meta.layer], meta)
    
    MapManager::clearLayers = () ->
        for layer in @layers
            layer.children.slice().forEach (child) -> layer.removeChild(child)

    MapManager::findInDirectionOf = (tile, dirstr, len) ->
        if not tile?
            return []
        out = []
        out.push(tile)
        fromr = tile.row
        fromc = tile.col
        direction = DIRECTIONS[fromc % 2]
        dir = direction[dirstr]
        while len > 0
            t = @getTileAt(fromr, fromc, dir[0], dir[1])
            if t?
                out.push(t)
                fromr = t.row
                fromc = t.col
                direction = DIRECTIONS[fromc % 2]
                dir = direction[dirstr]
            else
                break
            len--
        return out
    
    MapManager::getSpanArea = (tile, size) ->
        root    = ~~Math.sqrt(size.length)
        sizelen = size.length - 1
        nwests  = @findInDirectionOf(tile, "northwest", root - 1)
        out     = []
        for w in nwests
            neasts = @findInDirectionOf(w, "northeast", root - 1)
            out = out.concat(neasts)
        out = out.filter (_, i) ->
            return +size[sizelen-((i%root)*root)-~~(i/root)]
        return out

    MapManager::canBePlacedOn = (span, layer) ->
        out = []
        return out if not span?
        for k in span
            if k.layers[layer]?
                out.push k
        return out

    MapManager::recreateLayerFrom = (tileA, tileB, z_index, force = false) ->
        meta_id = tileA.removeLayer(z_index)
        if tileB.layers[z_index]?
            if force
                metab_id = tileB.removeLayer(z_index)
                @addLayerToTileByID(tileB.row, tileB.col, metab_id)
            else
                throw new Error("You can't move layer at Z: #{z_index} from [#{tileA.row},#{tileA.col}] to [#{tileB.row},#{tileB.col}]")
        return @addLayerToTileByID(tileB.row, tileB.col, meta_id)
      
    MapManager::sortLayer = (layer) ->
        @layers[layer].children.sort (a, b) ->
            return a.position.y - b.position.y

    MapManager::sortLayers = () ->
        @sortLayer(i) for i in [0...MAX_LAYERS]
    
    return MapManager