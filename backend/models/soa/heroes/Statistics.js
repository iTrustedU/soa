(function() {
  var commanderSchema, mongoose;

  mongoose = require("mongoose");

  commanderSchema = mongoose.Schema({
    speed: {
      type: Number,
      "default": 0
    },
    leadership: {
      type: Number,
      "default": 0
    },
    readiness: {
      type: Number,
      "default": 0
    }
  });

  module.exports.COMMANDER = mongoose.model("CommanderStat", commanderSchema);

}).call(this);
