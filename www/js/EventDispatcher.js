(function() {
  "use strict";
  var __slice = [].slice;

  define([], function() {
    var EventDispatcher;
    EventDispatcher = (function() {
      function EventDispatcher() {
        this.listeners = {};
      }

      return EventDispatcher;

    })();
    EventDispatcher.prototype.on = function(type, clb) {
      var t, _i, _len;
      if (type instanceof Array) {
        for (_i = 0, _len = type.length; _i < _len; _i++) {
          t = type[_i];
          if (this.listeners[t] == null) {
            this.listeners[t] = [];
          }
          this.listeners[t].push(clb);
        }
      } else {
        if (this.listeners[type] == null) {
          this.listeners[type] = [];
        }
        this.listeners[type].push(clb);
      }
      return clb;
    };
    EventDispatcher.prototype.removeTrigger = function(type, clb) {
      var ind;
      if (this.listeners[type] != null) {
        ind = this.listeners[type].indexOf(clb);
        if (ind !== -1) {
          if (ind !== -1) {
            this.listeners[type].splice(ind, 1);
          }
        }
        return clb = null;
      }
    };
    EventDispatcher.prototype.removeAllTriggers = function(type) {
      var key, keys, list, _i, _j, _len, _len1, _ref;
      if (type) {
        delete this.listeners[type];
      } else {
        keys = Object.keys(this.listeners);
        for (_i = 0, _len = keys.length; _i < _len; _i++) {
          key = keys[_i];
          _ref = this.listeners[key];
          for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
            list = _ref[_j];
            this.removeTrigger(key, list);
          }
        }
      }
      return delete this.listeners;
    };
    EventDispatcher.prototype.trigger = function() {
      var args, clb, type, _i, _len, _ref, _results;
      type = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (!this.listeners[type]) {
        return;
      }
      _ref = this.listeners[type];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        clb = _ref[_i];
        if (clb != null) {
          _results.push(clb.apply(args[0] || this, args.slice(1)));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    return EventDispatcher;
  });

}).call(this);
