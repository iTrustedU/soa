(function() {
  var fs;

  fs = require("fs");

  module.exports = function(generating_seed) {
    var numc;
    numc = Math.sin(generating_seed++) * 10000;
    return numc - Math.floor(numc);
  };

}).call(this);
