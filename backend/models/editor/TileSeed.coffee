mongoose = require "mongoose"
randomizer = require "../../utils/NaiveRandomizer"

# Random seed
RANDOM_SEED = 0xABCDEF

#used to clamp random numbers up to 2^16 - 2 range
SEED_CLAMPER = 0x1000-1

schema = mongoose.Schema
	last_state: 
		type: Number
		required: true

	starting_state:
		type: Number
		required: true

schema.methods.randomize = (token) ->
	next_seed = randomizer(@last_state) * SEED_CLAMPER
	@last_state = next_seed
	@save()
	return Math.floor(next_seed)

schema.methods.reset = () ->
	@last_state = @starting_state = RANDOM_SEED
	@save()

TileSeed = mongoose.model("TileSeed", schema)

module.exports = (app) ->
	TileSeed
	.find()
	.exec (err, results) ->
		randseeder = null
		if results.length is 0
			randseeder = saveOnce()
		else
			randseeder = results[0]
		if not randseeder?
			throw Error("Seeder is null!!!")
		
		app.locals.tileSeeder = randseeder

	# called only once per server deploy
	saveOnce = () ->
		seed = new TileSeed()
		seed.last_state = RANDOM_SEED
		seed.starting_state = RANDOM_SEED
		seed.save (err) ->
			if err?
				throw new Error("Error initializing seeder")
		return seed

	return app.locals.tileSeeder
