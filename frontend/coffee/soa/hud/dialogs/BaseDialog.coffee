"use strict"

define ["soa/SOAEventDispatcher", "soa/hud/IntervalQueue"],

(EventDispatcher, IntervalQueue) ->

    PARSE_TRIGGERS = new RegExp(/(.*)\/(.*)/)

    class BaseDialog extends EventDispatcher
        constructor: (html, data = {}) ->
            super()
            @data           = {}
            @compiled_data  = {}
            @save_fields    = {}
            @templates      = {}
            @show_check     = false
            @opened         = false
            @initListeners()
            if html? 
                @compileParent(html, data)
            if @init? 
                @init()
            return @

    BaseDialog::closeable = () ->
        clb = @html.find(".close-button")
        return if clb.length < 0
        clb.unbind "click"
        clb.click () =>
            @hide()

    BaseDialog::draggable = () ->
        @html.css("position", "absolute")
        @html.draggable({
            handle: ".dialog-name"
        })

    BaseDialog::upgradeable = () ->
        UpgradeBtn = @html.find("#upgrade-building")
        UpgradeBtn.unbind "click"
        UpgradeBtn.on "click", () =>
            @hide()
            @ctx.upgrade()

    BaseDialog::moveable = () ->
        MoveBTN = @html.find("#move-building")
        MoveBTN.unbind "click"
        MoveBTN.on "click", () =>
            @hide()
            SOA.attachConstructionToMouse("City", @ctx)

    BaseDialog::compileParent = (html, data) ->
        @parent_template = Handlebars.compile(html)
        @html = $(@parent_template(data))

    BaseDialog::prologue = (dom) ->
        @onPrologue.call(@, dom) if @onPrologue?
        return @

    BaseDialog::epilogue = (dom) ->
        @onEpilogue.call(@, dom) if @onEpilogue?
        return @

    BaseDialog::disable = () ->
        @show_check = true

    BaseDialog::enable = () ->
        @show_check = false

    BaseDialog::initListeners = () ->
        return
    
    BaseDialog::scheduleAsyncDataRefresh = (time, data) ->
        IntervalQueue.addToQueue(() => 
            for key, func of data
                prom = @fetchData(key, func)
                do (key) =>
                    prom.done (data) =>
                        @data[key] = data
        , time)

    BaseDialog::scheduleDataRefresh = (time, data) ->
        IntervalQueue.addToQueue(() => 
            for key, val of data
                if typeof val is "function"
                    res = val.call(@)
                else
                    res = val
                @data[key] = res
        , time)

    BaseDialog::compile = (template_key, datakey) ->
        prom = (@fetchData datakey, @data[datakey])
        defer = new $.Deferred()
        prom.done (data) =>
            @data[datakey] = data
            comp_template = @compileTemplate(template_key, datakey, data)
            defer.resolve(comp_template)
        , (err) ->
            defer.reject(err)
        return defer.promise()

    BaseDialog::setName = (@name) ->
        SOA.Hud.trigger "NAME_CHANGED", @
        @html.attr("name", @name) if @html?

    BaseDialog::compileTemplate = (tempkey, datakey, data) ->
        compiled_temp = Handlebars.compile(@templates[tempkey])
        return (@compiled_data[datakey] = compiled_temp(data))

    BaseDialog::fetchData = (datakey, data) ->
        defer = new $.Deferred()
        if typeof data is "function"
            out = data.call(@)
            ##console.debug out
            if out.done? and out.promise?
                ##console.debug "AJAX CALLING"
                out.done (data) ->
                    defer.resolve(data)
                .fail (err) ->
                    defer.reject(err)
            else
                ##console.debug "PLAIN FUNCTION CALLING"
                defer.resolve(out)
        else if data?
            ##console.debug "OBJECT CALLING"
            defer.resolve(data)
        else
            console.error "Data not set"
            defer.reject("Data not set")
        return defer.promise()

    BaseDialog::show = (@ctx, animSpeed = 200) ->
        @enable() 
        if not @ctx?
            return
        @trigger "PRE_SHOW", @ctx
        @trigger "SHOW", @ctx
        if not @show_check
            @html.fadeIn(animSpeed)
            @opened = true

    #reset ctx-a je bio ovde? @ctx
    BaseDialog::hide = (animSpeed = 200) ->
        @html.fadeOut(animSpeed, () => @html.hide())
        @trigger "HIDE", @ctx
        @opened = false
        return @

    BaseDialog::setTemplates = (@templates) -> return @

    BaseDialog::trigger = (type, msg) ->
        super(type, msg, @html)
        return @

    BaseDialog::refresh = (inner_template, compiled_data, duration = 0) ->
        temp = {}
        temp[inner_template] = compiled_data
        partial = @html.find("##{inner_template}")
        partial.empty()
        partial.hide()
        partial.append(compiled_data)
        partial.show(duration)
        @clickTriggers(@clicks)
        return @

    BaseDialog::compileHtml = (loaded_data) ->
        newDialog = $(@parent_template(loaded_data))
        newDialog.replaceAll(@html)
        @html = newDialog
        return @

    BaseDialog::clickTriggers = (@clicks) ->
        for trigger, save_field of @clicks
            split = PARSE_TRIGGERS.exec(trigger)
            look_at = split[1]
            get_from = split[2]
            save = @html.find(look_at)
            if save.length > 0
                save.unbind "click"
            if get_from.length is 1
                do (get_from, save_field) =>
                    save.click (ev) =>
                        @trigger save_field, get_from

            else if get_from.indexOf("@") isnt -1
                do (get_from, save_field) =>
                    save.click (ev) =>
                        value = $(ev.currentTarget).data(get_from.substr(1))
                        @set(save_field, value)
                        @trigger save_field, value, @
            else if get_from.indexOf("#") isnt -1
                do (get_from, save_field) =>
                    save.click (ev) =>
                        value = $(ev.currentTarget).attr(get_from.substr(1))
                        @set(save_field, value)
                        @trigger save_field, value, @
            else
                console.warn "Unrecognized pattern: #{get_from[0]}"
    
    BaseDialog::set = (name, val) ->
        @save_fields[name] = val

    BaseDialog::get = (name) ->
        return @save_fields[name]

    BaseDialog::setData = (key, val) ->
        @data[key] = val

    BaseDialog::getData = (key) ->
        return @data[key]

    return BaseDialog
