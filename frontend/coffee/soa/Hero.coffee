"use strict"

define ["soa/StructureLoader", "Geometry"],

(StructureLoader, Geometry) ->

	TILE_SENSE_RADIUS   = 3 #pixels
	#NUM_FRAMES          = 9 #frames per walking animation

	class Hero extends StructureLoader
	
	Hero::init = () ->
		@EXPERIENCE_MODIFIER 	= 1000
		@SPEED_PER_TILE     	= 1 #seconds
		@currentOrientation 	= "south"
		@move_type 				= ""
		@currentTile 			= null
		super()

	Hero::initListeners = () ->
		super()
		
		@on "DONE_LOADING", () ->
			@loadAnimations()
			if SOA.Settlement? and SOA.Settlement.layer?
				@placeMeSomewhere()

		HAL.on "MY_SETTLEMENT_LOADED", () =>
			@placeMeSomewhere()

	Hero::placeMeSomewhere = () ->
		@putNearSettlement()
		# if SOA.Settlement? and SOA.Settlement.layer? and @move_type is ""

	Hero::calcArrivalTime = (fromX = @o_coord_x, fromY = @o_coord_y) ->
		distance = Math.round(Math.sqrt(Math.pow(@d_coord_x - fromX, 2) + Math.pow(@d_coord_y - fromY, 2)))
		distance *= 2
		return distance

	Hero::putNearSettlement = () ->
		@layer = SOA.Settlement.putHeroNearby(@getCurrentAnimation())

		if @move_type is "rest"
			@layer.changeParentTile(SOA.Zone.map.getTile(@o_coord_x, @o_coord_y))
		else if @move_type is ""
			@o_coord_x = @layer.parentTile.row
			@o_coord_y = @layer.parentTile.col

		@layer.onLeftClick = () =>
			SOA.getDialog("HEROES_QUEUE").html.find(".hero-frame").removeClass("hero-frame-a")
			frame = SOA.getDialog("HEROES_QUEUE").html.find("##{@_type_}-#{@id}")
			frame.addClass("hero-frame-a")
			SOA.setSelectedHero(@)

		if @arrival?
			@moveTo(@d_coord_x, @d_coord_y)
		else
			@moveTo(@o_coord_x, @o_coord_y)
			@finishArriving()

	Hero::nextLevelExperience = () ->
		return ((@level+1) * @EXPERIENCE_MODIFIER)

	Hero::checkLeveling = () ->
		if @experience >= @nextLevelExperience()
			@level++

	Hero::changeStat = (stat_obj) ->
		for stat in stat_obj
			#do something
			return

	Hero::getType = () ->
		return @_type_

	Hero::setType = (@_type_) -> return @

	Hero::getCurrentAnimation = () ->
		return @animation[@currentOrientation]

	Hero::loadAnimations = () ->
		@animation = HAL.Animations[@_type_]

	# Hero::updateLastAction = () ->
	# 	target = SOA.Zone.map.getTile(@d_coord_x, @d_coord_y)
	# 	if target?
	# 		for layer in target.layers
	# 			continue if layer.type?
	# 			if layer.type is "city"
	# 				@lastAction = "attack"
	# 				#console.debug "Moving to city"
	# 			if layer.type is "poi"
	# 				@lastAction = "camp"
	# 				#console.debug "Moving to POI"
	# 			else
	# 				@lastAction = "move"

	Hero::findPath = () ->
		nA = SOA.Zone.map.getTile(@o_coord_x, @o_coord_y)
		nB = SOA.Zone.map.getTile(@d_coord_x, @d_coord_y)
		path = SOA.Zone.map.pathfinder.find(nA, nB)
		trace = path.reverse()
		return [
			trace, 
			trace.map (t) -> return SOA.Zone.map.getTile(t.row, t.col).position
		]

	Hero::updatePosition = () ->
		[trace, positions] = @findPath()
		path_len = trace.length
					
		#console.debug "Path is #{path_len}km long"
		# a = new Date(@departure).getTime()
		# b = new Date(@arrival).getTime()
		# secs = ((b - a) / 1000)

		secs = @calcArrivalTime()
		console.debug("Total in seconds: #{secs}")
		now = Date.now()
		so_far = (now - new Date(@departure).getTime()) / 1000
		console.debug("So far: #{so_far}")
		
		rate = (secs / trace.length)
		console.debug("Rate is: #{rate}")
		
		curr_cell = Math.floor(so_far / rate)
		console.debug("Currently at: #{curr_cell}")
		
		if path_len > 1 and 0 < curr_cell <= path_len
			trace = trace.slice(curr_cell)
			positions = positions.slice(curr_cell)
			@layer.position.x = positions[0].x
			@layer.position.y = positions[0].y
			console.debug "idem normalno"
			console.debug positions
			console.debug trace
		else if curr_cell > path_len
			@layer.changeParentTile(SOA.Zone.map.getTile(@d_coord_x, @d_coord_y))
			trace = trace.slice(trace.length - 1)
			positions = positions.slice(positions.length - 1)
			console.debug "kasnim malo"
			@SPEED_PER_TILE = 0

		if trace.length > 0
			secs = @calcArrivalTime(trace[0].row, trace[0].col)
		
		rate = (secs / trace.length) #+ 10

		if Number.isFinite(rate)
			@SPEED_PER_TILE = secs + 10

		#console.debug("new rate is: #{@SPEED_PER_TILE}")
		#console.debug "My guesstimate: #{new_rate}"
		return [trace, positions]

	Hero::resolveArrival = () ->
		throw new Error("Not implemented")

	Hero::finishArriving = () ->
		throw new Error("Not implemented")
		return

	Hero::showDialog = () ->
		SOA.showDialog(@getType().toUpperCase(), @)

	Hero::moveTo = (@d_coord_x, @d_coord_y) ->
		console.debug "Arrival: #{@arrival}"
		console.debug "Departure: #{@departure}"
		console.debug "Moving from #{@o_coord_x}, #{@o_coord_y} to #{@d_coord_x}, #{@d_coord_y}"
		console.debug "Duration: #{@duration}"

		console.debug "Moving from #{@o_coord_x}, #{@o_coord_y} to #{@d_coord_x}, #{@d_coord_y}"
		@onMoveTo.call(@)

	Hero::onMoveTo = () ->
		[trace, positions] = @updatePosition()
		if not trace?
			throw new Error("Path wasn't found!")		
		return if trace.length is 0
		console.debug "Arriving in #{@SPEED_PER_TILE} seconds"
		console.debug positions
		console.debug trace


		last_position = @layer.position.clone()

		if positions.length is 1
			@arrive()
			return

		tween = TweenLite.to(@layer.position, @SPEED_PER_TILE, {bezier: positions.slice(), type: "cubic", paused: true, curviness: 1})

		console.debug @layer.position
		pivot = positions.shift()
		first_tile = trace.shift()
		second_tile = trace.shift()
		startTime = performance.now()

		tween.eventCallback("onUpdate", () =>
			if Geometry.isPointInCircle(@layer.position, pivot, TILE_SENSE_RADIUS)
				if not second_tile? or not pivot?
					tween.progress(1.0)
					return
				dir = @strDirection(first_tile, second_tile)
				@currentOrientation = dir
				first_tile = second_tile
				second_tile = trace.shift()
				pivot = positions.shift()
				if @animation[dir]?
					@layer.textures = @animation[dir]
		)
		
		#check if the point is in some treshold range 
		tween.eventCallback("onComplete", 
			() =>
				endTime = window.performance.now() - startTime
				console.debug "It actually took: #{endTime}ms"
				@o_coord_x = @d_coord_x
				@o_coord_y = @d_coord_y
				console.debug @d_coord_y, @d_coord_x
				@layer.changeParentTile(SOA.Zone.map.getTile(@d_coord_x, @d_coord_y))
				@layer.position.y -= 16
				tween.kill()
				@layer.stop()
				@arrive()
		)

		tween.play()
		@layer.play()

	Hero::arrive = () ->
		@trigger "ARRIVAL", @

	Hero::showActionBox = (data) ->
		x = data.originalEvent.clientX
		y = data.originalEvent.clientY
		
		@target = data.hitTarget

		actBox = SOA.getDialog("ACTION_BOX")

		actBox.TradeBtn.hide()
		actBox.CampBtn.hide()
		actBox.MoveBtn.hide()
		actBox.AttackBtn.hide()
		
		@onAction(@target)

		actBox.html.css("left", x - 100)
		actBox.html.css("top", y - 100)

		actBox.show(@, 200)

	Hero::strDirection = (from, to) ->
		diff_row = to.row - from.row
		diff_col = to.col - from.col
		isOdd = @layer.parentTile.col % 2
		str = diff_row+"_"+diff_col
		direction = 
		  "-1_0": "north"
		  "1_0": "south"
		  "0_2": "east"
		  "0_-2": "west"
		direction[isOdd+"_"+1] = "southeast"
		direction[-1+isOdd+"_"+1] = "northeast"
		direction[isOdd+"_#{-1}"] = "southwest"
		direction[-1+isOdd+"_#{-1}"] = "northwest"
		return direction[str]

	Hero::onAction = (@target) ->
		return

	Hero::doAction = (@lastAction) ->
		console.debug "Action to execute: #{@lastAction}"

	Hero::isInOwnCity = () ->
		setRow = SOA.Settlement.x
		setCol = SOA.Settlement.y
		return @o_coord_x is setRow and @o_coord_y is setCol
		
	return Hero

###
		# moveOnPath: (@path, @walk_rate) ->
		#   start = [29, 46]
		#   end = [33, 85]
		#   ani = SOA.Zone.map.addAnimatedLayer(start[0], start[1], HAL.Animations.commander.east)
		#   zmap = SOA.Zone.map
		#   zmap.tile_graphics.forEach(function(t) { zmap.layers[2].removeChild(t); });

		#path = zmap.pathfinder.find(, zmap.getTile(end[0], end[1]))
		#leks = path.reverse().map(function(tile) { zmap.tintTileLayer(tile.row, tile.col, 2, 0x00FF00C0); return zmap.getTile(tile.row, tile.col).position.clone(); })
		#t = TweenLite.to(ani.position, 20, {bezier: leks, type: "cubic", paused: true, curviness: 0});
		#t.play();
		#i = 0; 
		#t.eventCallback("onUpdate", function() {  ++i; if(i > 200) { ##console.debug('wtf'); i = 0; };})

		#@moveLayerTo(next_cell.row, next_cell.col)
		#SOA.City.map.pathfinder.find(SOA.City.map.getTile(9, 6), SOA.City.map.getTile(10, 24)).forEach(function(coords) { tile = SOA.City.map.getTile(coords.row, coords.col); pos = tile.position; shape = SOA.City.map.createTileShape(pos.x, pos.y); SOA.City.world.addChild(shape); })
		
		# animateWalk: (next_cell) ->
		#   TweenMax.fromTo(
			
		#   ) ->
		#attr: "position[0]"
		#from: @hero.position[0]
		#to: amjad.zonemap.getTile(next_cell.row, next_cell.col).position[0]
		#duration: @walk_rate
		# return
		# @hero.tween
		#   attr: "position[0]"
		#   from: @hero.position[0]
		#   to: amjad.zonemap.getTile(next_cell.row, next_cell.col).position[0]
		#   duration: @walk_rate
		# .tween
		#   attr: "position[1]"
		#   from: @hero.position[1]
		#   to: amjad.zonemap.getTile(next_cell.row, next_cell.col).position[1]
		#   duration: @walk_rate
		# .done () =>
		#   next = @path[@cur_cell_index + 1]
		#   if not next?
		#       Hal.removeTrigger "ENTER_FRAME", @curr_frame                   
		#       @trigger "ARRIVED", next_cell
		#       return
		#   @moveLayerTo(next_cell.row, next_cell.col)
		#   @cur_cell_index++
		#   ##console.debug next_cell
		#   ##console.debug next
		#   @next_dir = @strDirection(next_cell, next)
		#   ##console.debug @next_dir
		#   @animateWalk(next)

		# moveLayerTo: (row, col) ->
		#   dest_tile = amjad.zonemap.getTile(row, col)
		#   if not dest_tile?
		#       return
		#   @hero.tile.removeLayer(@hero.layer, false)
		#   dest_tile.addTileLayer(@hero, false)
		#   @o_coord_x = row
		#   @o_coord_y = col
		#   @hero.setPosition(dest_tile.position[0], dest_tile.position[1])
		#   @hero.quadtree.remove(@hero)
		#   amjad.zonemap.quadtree.insert(@hero)

		# TweenMax.to(comm.layer.scale, 1, {x: 1.2, y: 1.2, onComplete: function() { comm.layer.scale.x = 1; comm.layer.scale.y = 1; }})

		# @animation["north"] = HAL.Animations[@_type_].north
		# @animation["south"] = HAL.Animations[@_type_].south
		# @animation["west"] = HAL.Animations[@_type_].west
		# @animation["east"] = HAL.Animations[@_type_].east
		# @animation["northeast"] = HAL.Animations[@_type_].northeast
		# @animation["southeast"] = HAL.Animations[@_type_].southeast
		# @animation["southwest"] = HAL.Animations[@_type_].southwest
		# @animation["northwest"] = HAL.Animations[@_type_].northwest
		
		# start = [29, 46]
		# end = [33, 85]
		# ani = SOA.Zone.map.addAnimatedLayer(start[0], start[1], HAL.Animations.commander.east)
		# zmap = SOA.Zone.map
		# zmap.tile_graphics.forEach(function(t) { zmap.layers[2].removeChild(t); });

		# path = zmap.pathfinder.find(zmap.getTile(start[0], start[1]), zmap.getTile(end[0], end[1]))
		# leks = path.reverse().map(function(tile) { zmap.tintTileLayer(tile.row, tile.col, 2, 0x00FF00C0); return zmap.getTile(tile.row, tile.col).position.clone(); })
		# t = TweenLite.to(ani.position, 20, {bezier: leks, type: "cubic", paused: true, curviness: 0});
		# t.play();
		# i = 0; 
		# t.eventCallback("onUpdate", function() {  ++i; if(i > 200) { ##console.debug('wtf'); i = 0; };})

		# putInCitySurrounding: (row, col) ->
		#   zone = amjad.zonemap
		#   if not(row is amjad.empire.settlement.y and col is amjad.empire.settlement.x)
		#       t = zone.getTile(row, col)
		#       where = zone.findInDirectionOf(t, "east", 5).concat zone.findInDirectionOf(t, "west", 5)
		#       place = (where.filter (tile) -> 
		#                   return not tile.layers[5]?)[0]
		#       row = place.row
		#       col = place.col
		#   return [row, col]

		# @loadAnimations()
		# meta = {}
		# meta.layer = 6
		# meta.group = @_type_
		# meta.name = "#{@_type_}_#{@id}"
		# meta.sprite = @animation[@current_anim][0].getName()
		
		# [row, col] = @putInCitySurrounding(row, col)
		
		# @o_coord_x = row
		# @o_coord_y = col

		@todo find settlement coordinates
		# main = amjad.empire.settlement
		# @hero = amjad.zonemap.tm.addTileLayerMeta(row, col, meta)
		# # @hero.setScale(2.0, 2.0)
		# @hero.meta = @
		# @hero.on "SELECTED", () =>
		#     hero_queue = Hud.getDialog("HEROES_QUEUE")
		#     frame = hero_queue.html().find("##{@_type_}-#{@id}")
		#     frame.addClass("hero-frame-a") #css, "5px solid rgb(201,178,10)")
		#     ##console.debug "Selected #{@hero.name}"
		#     amjad.selected_hero = @

		# @hero.on "DESELECTED", () =>
		#     hero_queue = Hud.getDialog("HEROES_QUEUE")
		#     frame = hero_queue.html().find("##{@_type_}-#{@id}")
		#     # frame.css("border", "none")
		#     frame.removeClass("hero-frame-a")
		#     ##console.debug "Deselected #{@hero.name}"
		#     # amjad.selected_hero = null

		# amjad.zonemap.on ["CAMERA_PANNING", "CAMERA_LERPING"], (layer) =>
		#     if not @action_box_hidden
		#         @hideActionBox()
		#         ##console.debug "Deselected #{@hero.name}"
		#         amjad.selected_hero = null
		#     hero_queue = Hud.getDialog("HEROES_QUEUE")
		#     frame = hero_queue.html().find("##{@getType()}-#{@id}")
		#     frame.css("border", "none")

		# @hero.on "SELECTED_DBL_CLICK", () =>
		#     show hero window
		#     console.error "Yay it works"
		#     @dbl_clicked = true
		#     dlg = Hud.getDialog(@getType().toUpperCase())
		#     dlg.show(@)
###