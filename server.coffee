PORT = 3000

express = require "express"
mongoose = require "mongoose"
passport = require "passport"
dbconfig = require "./config/Database"
handlebars = require "express3-handlebars"
helpers = require "./lib/helpers"
flash = require "connect-flash"
wrench = require "wrench"
require("./config/Passport")(passport)

app = express()
log = console.log

#connect to database
mongoose.connect(dbconfig.url)
db = mongoose.connection
db.on "error", console.error.bind(console, "Connection error: ")
db.once "open", () ->
	console.log "Connected to DB"

#create and configure handlebars
hbs = handlebars.create
	defaultLayout: "main"
	helpers: helpers
	layoutsDir: "views/layouts"
	partialsDir: [
		 "views/partials/"
	]

#load utils and prototypes
wrench.readdirSyncRecursive("utils/")
.filter (file) -> 
	return file.lastIndexOf(".js") isnt -1 
.forEach (util) ->
	require("./utils/#{util}")()

#configure app
app.configure () ->
	app.use express.logger("dev")
	app.use express.cookieParser()
	app.use express.bodyParser(limit: "50mb")
	app.engine("handlebars", hbs.engine)
	app.set("view engine", "handlebars")
	app.use(express.session(secret: "#soa-square-monthly-cube-disorder#"))
	app.use((req, res, next) ->
		if req.method is "POST" and req.url is "/login"
			if req.body.rememberme?
				req.session.cookie.maxAge = 30*24*60*60*1000 #30 dana
			else
				req.session.cookie.expires = false
		next()
	)
	app.use(passport.initialize())
	app.use(passport.session())

	router = require("./backend/Router")
	router(app, passport)

app.use(express.static("www/"))
app.listen(PORT)

console.log("Running on: #{PORT}");