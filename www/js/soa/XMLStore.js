(function() {
  "use strict";
  /*
  @todo neka opcija da se sacuvaju lokalno, sa version brojem
  */

  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader"], function(StructureLoader) {
    var ShortMap, URIMap, XMLStore, _ref;
    URIMap = {
      "constructions": "construction.load_constr_xml",
      "units": "army.load_units_xml",
      "tradegoods": "caravan.load_tradegoods_xml",
      "equipment": "army.load_items_xml",
      "science": "settlement.load_sciences_xml",
      "technologies": "settlement.load_technologies_xml"
    };
    ShortMap = {
      "*": [
        "*", function(data) {
          return data;
        }
      ]
    };
    XMLStore = (function(_super) {
      __extends(XMLStore, _super);

      function XMLStore() {
        _ref = XMLStore.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      XMLStore.prototype.fetch = function(type) {
        var _this = this;
        if (URIMap[type] == null) {
          console.error("Request not allowed");
          return;
        }
        if (this[type] == null) {
          return Rest.get(URIMap[type]).done(function(data) {
            var temp;
            temp = {};
            temp[type] = data;
            _this.load(temp);
            return _this.trigger("LOADED_" + (type.toUpperCase()) + "_XML", data);
          }).fail(function(data) {
            return console.error(data);
          });
        }
      };

      XMLStore.prototype.get = function(type) {
        if (this[type] == null) {
          return this.fetch(type);
        } else {
          return this[type];
        }
      };

      return XMLStore;

    })(StructureLoader);
    return (window.XMLStore = new XMLStore(ShortMap));
  });

}).call(this);
