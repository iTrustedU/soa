(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader", "soa/constructions/Construction", "soa/constructions/TrainingGrounds", "soa/constructions/WarTent", "soa/constructions/Science", "soa/constructions/TradeStation", "soa/constructions/EncampedPOI"], function(StructureLoader, Construction, TrainingGrounds, WarTent, Science, TradeStation, EncampedPOI) {
    var CONSTRUCTIONS_COLLISION_LAYER, Constructions, ID_MAP, ShortMap, TYPE_MAP, collisionReport;
    ShortMap = {
      "*": "*"
    };
    CONSTRUCTIONS_COLLISION_LAYER = 6;
    ID_MAP = {};
    TYPE_MAP = {};
    collisionReport = function(collided) {
      var cl, outStr, _i, _len;
      outStr = "Collisions at: \n";
      for (_i = 0, _len = collided.length; _i < _len; _i++) {
        cl = collided[_i];
        outStr += "" + cl.row + "," + cl.col + "\n";
      }
      return outStr;
    };
    Constructions = (function(_super) {
      __extends(Constructions, _super);

      function Constructions() {
        this.buildings = [];
        this.pois = [];
        this.available_constructions = {};
        Constructions.__super__.constructor.call(this, {
          "*": [
            "*", function(meta) {
              var e;
              try {
                this.loadNewConstruction(meta);
              } catch (_error) {
                e = _error;
                console.error(e);
              }
              return null;
            }
          ]
        });
      }

      return Constructions;

    })(StructureLoader);
    Constructions.prototype.loadNewConstruction = function(meta) {
      var cn;
      cn = null;
      switch (meta.c_type) {
        case "training_grounds":
          cn = new TrainingGrounds(ShortMap).load(meta);
          break;
        case "war":
          cn = new WarTent(ShortMap).load(meta);
          break;
        case "science":
          cn = new Science(ShortMap).load(meta);
          break;
        case "trade_station":
          cn = new TradeStation(ShortMap).load(meta);
          break;
        default:
          if (meta.poi_id != null) {
            cn = new EncampedPOI(ShortMap).load(meta);
            this.pois.push(cn);
          } else {
            cn = new Construction(ShortMap).load(meta);
          }
      }
      if (cn == null) {
        throw new Error("No construction implementation for " + meta.c_type);
      }
      if (meta.poi_id == null) {
        this.buildings.push(cn);
      }
      if (TYPE_MAP[cn.c_type] == null) {
        TYPE_MAP[cn.c_type] = [];
      }
      TYPE_MAP[cn.c_type].push(cn);
      return cn;
    };
    Constructions.prototype.getIDMap = function() {
      return ID_MAP;
    };
    Constructions.prototype.getSpanMark = function(row, col) {
      return this.span_area[row * this.span_area_h + col];
    };
    Constructions.prototype.fillSpanArea = function(spans, id) {
      var index, out, spmark, _i, _len;
      out = [];
      if (spans == null) {
        return out;
      }
      for (_i = 0, _len = spans.length; _i < _len; _i++) {
        spmark = spans[_i];
        index = spmark.row * this.span_area_h + spmark.col;
        if (this.span_area[index] === 0) {
          this.span_area[index] = id;
        } else if (this.span_area[index] !== id) {
          out.push({
            row: spmark.row,
            col: spmark.col
          });
        }
      }
      return out;
    };
    Constructions.prototype.testSpanArea = function(spans, id) {
      var index, out, spmark, _i, _len;
      out = [];
      if (spans == null) {
        return out;
      }
      for (_i = 0, _len = spans.length; _i < _len; _i++) {
        spmark = spans[_i];
        index = spmark.row * this.span_area_h + spmark.col;
        if (this.span_area[index] !== id && this.span_area[index] !== 0) {
          out.push({
            row: spmark.row,
            col: spmark.col
          });
        }
      }
      return out;
    };
    Constructions.prototype.fromSpanMark = function(row, col) {
      var id;
      id = this.getSpanMark(row, col);
      return ID_MAP[id];
    };
    Constructions.prototype.canBePlacedOn = function(cnA, cnB) {
      return this.testSpanArea(cnA.getSpan(), cnB.getSpan());
    };
    Constructions.prototype.initSpanArea = function() {
      var i, j, _i, _ref, _results;
      this.span_area_w = SOA.City.map.NROWS;
      this.span_area_h = SOA.City.map.NCOLUMNS;
      this.span_area = [];
      _results = [];
      for (i = _i = 0, _ref = this.span_area_w; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (j = _j = 0, _ref1 = this.span_area_h; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
            _results1.push(this.span_area[i * this.span_area_h + j] = 0);
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };
    Constructions.prototype.constructionFulfilsRequirements = function(requirements) {
      var hasResources, out;
      out = [];
      hasResources = amjad.empire.resources.hasEnough(requirements.resources);
      out = out.concat(hasResources);
      out = out.concat(amjad.empire.constructions.hasConstructions(requirements.constructions, function(myconstr, xml_level) {
        return (myconstr != null ? myconstr.level : void 0) >= xml_level;
      }));
      return out;
    };
    Constructions.prototype.initListeners = function() {
      var _this = this;
      Constructions.__super__.initListeners.call(this);
      this.on("DONE_LOADING", function() {
        return console.debug("CONSTRUCTIONS LOADED");
      });
      HAL.on("SOA_LOADED", function() {
        var cn, _i, _len, _ref;
        _ref = _this.buildings;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          cn = _ref[_i];
          cn.epilogue();
        }
        _this.findAvailableConstructions(XMLStore.get("constructions"));
        return SOA.Hud.getDialog("BOTTOM_MENU").listBuildings();
      });
      HAL.on("CITY_MAP_LOADED", function() {
        _this.initSpanArea();
        _this.placeConstructions();
        return _this.placePOIEncampments();
      });
      return amjad.on(["CONSTRUCTIONS_UPDATED", "RESOURCES_UPDATED"], function() {
        console.debug("Constructions/resources updated");
        return _this.updateConstructions();
      });
    };
    Constructions.prototype.updateConstructions = function() {
      var bottom_img, cnt, dis, k, v, _ref, _ref1, _results;
      dis = [];
      _ref = this.available_constructions;
      for (k in _ref) {
        v = _ref[k];
        if (v.type === "disabled") {
          dis.push(k);
        }
      }
      this.findAvailableConstructions(XMLStore.get("constructions"));
      SOA.Hud.getDialog("BOTTOM_MENU").listBuildings();
      _ref1 = this.available_constructions;
      _results = [];
      for (k in _ref1) {
        cnt = _ref1[k];
        if (cnt.type !== "disabled" && dis.indexOf(k) !== -1) {
          bottom_img = $(".img-frame.bottom-menu-building.ui-draggable#" + k);
          bottom_img.addClass("construction-built");
          _results.push(bottom_img.removeClass("construction-built", 5000));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    Constructions.prototype.getAllOfType = function(type) {
      return TYPE_MAP[type];
    };
    Constructions.prototype.getAll = function(type) {
      return this.buildings;
    };
    Constructions.prototype.getByID = function(id) {
      return ID_MAP[id];
    };
    Constructions.prototype.hasConstructions = function(types, filter) {
      var out, type, val, _ref;
      if (filter == null) {
        filter = function() {};
      }
      out = [];
      for (type in types) {
        val = types[type];
        if (!this.hasConstruction(type, val) && !filter((_ref = this.getAllOfType(type)) != null ? _ref[0] : void 0, val)) {
          out.push("Missing required construction: " + type + ", level: " + val);
        }
      }
      return out;
    };
    Constructions.prototype.hasConstruction = function(type, level) {
      if (level == null) {
        level = 1;
      }
      if (type === "home") {
        return false;
      }
      return (TYPE_MAP[type] != null) && TYPE_MAP[type].length > 0;
    };
    Constructions.prototype.buildConstruction = function(type, constr) {
      if (this.hasConstruction(type)) {
        throw new Error("You already have " + type + " construction");
      }
      return this.loadNewConstruction(constr);
    };
    Constructions.prototype.canBeConstructed = function(key, constr_req) {
      var exists, out;
      out = [];
      exists = this.hasConstruction(key);
      if (exists) {
        out.push("Construction exists " + key);
      }
      out = out.concat(this.constructionFulfilsRequirements(constr_req));
      return out.length === 0;
    };
    /* @todo na istu foru prodji kroz ostale, samo ih taguj*/

    Constructions.prototype.findAvailableConstructions = function(xml_constructions) {
      var can_construct, constr, is_supported_by_us, key, sprite_uri, _ref, _results;
      if (xml_constructions == null) {
        xml_constructions = XMLStore.get("constructions");
      }
      _ref = xml_constructions.city;
      _results = [];
      for (key in _ref) {
        constr = _ref[key];
        if (this.hasConstruction(key)) {
          continue;
        }
        sprite_uri = amjad.user.CONSTRUCTION_SPRITES_URI + key;
        can_construct = this.canBeConstructed(key, constr.level1.requirements);
        is_supported_by_us = amjad.layerMetaFromSprite("" + sprite_uri + "1") != null;
        if (can_construct && is_supported_by_us) {
          _results.push(this.available_constructions[key] = {
            sprite: "assets/sprites/" + sprite_uri + "1",
            type: key
          });
        } else if (can_construct && !is_supported_by_us) {
          continue;
        } else if (is_supported_by_us) {
          _results.push(this.available_constructions[key] = {
            sprite: sprite_uri + "_dis",
            type: "disabled"
          });
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    /*
    	@action Constructs a building
    	@string Building type
    */

    Constructions.prototype.construct = function(building) {
      var col, constr_xml, defer, layermeta, meta_uri, result, row,
        _this = this;
      constr_xml = XMLStore.get("constructions").city[building.type]["level1"];
      if (constr_xml == null) {
        throw new Error("You can't construct this building");
      }
      meta_uri = amjad.splaySpriteURI("" + amjad.user.CONSTRUCTION_SPRITES_URI);
      layermeta = amjad.layerMetaFromSprite("" + meta_uri + building.type + "1");
      row = SOA.currentMap.tile_under.row;
      col = SOA.currentMap.tile_under.col;
      result = Rest.post("construction.start_build", {
        ty: building.type,
        x: row,
        y: col
      });
      defer = new $.Deferred();
      result.done(function(data) {
        var constr;
        constr = _this.buildConstruction(building.type, data);
        if (constr != null) {
          delete _this.available_constructions[building.type];
          _this.placeConstruction(constr);
          amjad.empire.resources.take(constr_xml.requirements.resources);
          amjad.trigger("CONSTRUCTIONS_UPDATED");
          return defer.resolve(constr);
        } else {
          return defer.reject("Construction couldn't be built");
        }
      });
      result.fail(function(err) {
        return defer.reject(err.responseText || err);
      });
      return defer.promise();
    };
    Constructions.prototype.placePOIEncampment = function(poienc) {
      try {
        return poienc.addToMap();
      } catch (_error) {
        return console.error(e);
      }
    };
    Constructions.prototype.placePOIEncampments = function() {
      var poi, _i, _len, _ref, _results;
      _ref = this.pois;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        poi = _ref[_i];
        _results.push(this.placePOIEncampment(poi));
      }
      return _results;
    };
    Constructions.prototype.placeConstructions = function() {
      var cn, _i, _len, _ref, _results;
      _ref = this.buildings;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        cn = _ref[_i];
        _results.push(this.placeConstruction(cn));
      }
      return _results;
    };
    Constructions.prototype.placeConstruction = function(cn) {
      var collisions, e;
      try {
        cn.addToMap();
        ID_MAP[cn.construction_id] = cn;
        collisions = this.fillSpanArea(cn.getSpan(cn.level), cn.construction_id);
      } catch (_error) {
        e = _error;
        console.error(e);
      }
      if ((collisions != null ? collisions.length : void 0) > 0) {
        return console.error(collisionReport(collisions));
      }
    };
    return Constructions;
  });

}).call(this);
