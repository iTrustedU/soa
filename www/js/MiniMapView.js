(function() {
  "use strict";
  define(["Geometry", "MapConfig"], function(Geometry, MapConfig) {
    var MiniMapView;
    MiniMapView = (function() {
      /* @todo Ovo sranjce ce da leti odavde*/

      function MiniMapView(map) {
        var x;
        this.map = map;
        this.width = 600;
        this.height = 600;
        this.viewport_size = new PIXI.Point();
        this.sections = (function() {
          var _i, _results;
          _results = [];
          for (x = _i = 0; _i < 9; x = ++_i) {
            _results.push(new PIXI.Graphics());
          }
          return _results;
        })();
        this.section_container = new PIXI.DisplayObjectContainer();
        this.sections_pivot = new PIXI.Point();
        this.envelope_bbox_g = new PIXI.Graphics();
        this.envelope_bbox = new PIXI.Rectangle();
        this.margin = {
          left: 5,
          top: 5
        };
        this.section_size = new PIXI.Point();
        this.init();
      }

      return MiniMapView;

    })();
    MiniMapView.prototype.pointToLocalSpace = function(out, x, y) {
      out.x = (x / this.map.world_bounds.x) * this.width;
      return out.y = (y / this.map.world_bounds.y) * this.height;
    };
    MiniMapView.prototype.rectToLocalSpace = function(rectOut, rectIn) {
      rectOut.x = (rectIn.x / this.map.world_bounds.x) * this.width;
      rectOut.y = (rectIn.y / this.map.world_bounds.y) * this.height;
      rectOut.width = (rectIn.width / this.map.world_bounds.x) * this.width;
      return rectOut.height = (rectIn.height / this.map.world_bounds.y) * this.height;
    };
    MiniMapView.prototype.initSections = function() {
      var col, ind, row, section, _i, _j;
      ind = 0;
      for (row = _i = 0; _i < 3; row = ++_i) {
        for (col = _j = 0; _j < 3; col = ++_j) {
          section = this.sections[ind++];
          section.beginFill(0x2005DF, 0.2);
          section.lineStyle(1, 0x2F05CC, 1);
          section.drawRect(row * this.section_size.x, col * this.section_size.y, this.section_size.x, this.section_size.y);
          section.endFill();
          this.world.addChild(section);
        }
      }
      this.rectToLocalSpace(this.envelope_bbox, this.regmn.envelope.bbox);
      this.envelope_bbox_g.clear();
      this.envelope_bbox_g.beginFill(0xFF0c10, 0.4);
      this.envelope_bbox_g.lineStyle(1, 0x1F0c10, 1);
      this.envelope_bbox_g.drawRect(this.envelope_bbox.x, this.envelope_bbox.y, this.envelope_bbox.width, this.envelope_bbox.height);
      this.envelope_bbox_g.endFill();
      return this.world.addChild(this.envelope_bbox_g);
    };
    MiniMapView.prototype.updateSections = function() {
      this.rectToLocalSpace(this.envelope_bbox, this.regmn.envelope.bbox);
      console.debug(this.envelope_bbox);
      this.envelope_bbox_g.clear();
      this.envelope_bbox_g.beginFill(0xFF0c10, 0.4);
      this.envelope_bbox_g.lineStyle(1, 0x1F0c10, 1);
      this.envelope_bbox_g.drawRect(this.envelope_bbox.x, this.envelope_bbox.y, this.envelope_bbox.width, this.envelope_bbox.height);
      return this.envelope_bbox_g.endFill();
    };
    MiniMapView.prototype.init = function() {
      var _this = this;
      this.viewport = new PIXI.Graphics();
      this.world = new PIXI.Graphics();
      this.regmn = this.map.region_mngr;
      this.map.scene.on("KEY_UP", function(ev) {
        if (ev.keyCode === 32) {
          return _this.world.visible = !_this.world.visible;
        }
      });
      this.map.on("HIT_AREA_CHANGED", function(hit_area) {
        _this.pointToLocalSpace(_this.viewport.position, hit_area.x, hit_area.y);
        _this.pointToLocalSpace(_this.viewport_size, hit_area.width, hit_area.height);
        _this.viewport.clear();
        _this.viewport.lineStyle(1, 0xFF0000, 1);
        return _this.viewport.drawRect(0, 0, _this.viewport_size.x, _this.viewport_size.y);
      });
      this.regmn.on("REGION_CHANGED", function() {
        return _this.updateSections();
      });
      this.regmn.on("TILE_PLACED", function(region, section, row, col, value) {
        var pos;
        pos = _this.map.getTilePosition(row, col);
        _this.pointToLocalSpace(pos, pos.x, pos.y);
        _this.world.beginFill(value, 1);
        _this.world.lineStyle(1, 0xFFFFFF ^ value << 8, 1);
        _this.world.drawRect(pos.x - 1.5, pos.y - 1.5, 3, 3);
        return _this.world.endFill();
      });
      return this.regmn.on("LOADED", function() {
        var col, rh, row, rw, scaledrh, scaledrw, x, y, _i, _j, _ref, _ref1;
        _this.world.position.x = _this.map.scene.screen_size.x - _this.height - _this.margin.left;
        _this.world.position.y = _this.margin.top;
        _this.viewport_size.x = (_this.map.scene.screen_size.x / _this.map.world_bounds.x) * _this.width;
        _this.viewport_size.y = (_this.map.scene.screen_size.y / _this.map.world_bounds.y) * _this.height;
        _this.viewport.position.x = 0;
        _this.viewport.position.y = 0;
        _this.viewport.lineStyle(1, 0xFF0000, 1);
        _this.viewport.drawRect(0, 0, _this.viewport_size.x, _this.viewport_size.y);
        _this.world.beginFill(0x0C0C0C, 0.3);
        _this.world.drawRect(0, 0, _this.width, _this.height);
        _this.world.endFill();
        _this.world.lineStyle(1, 0xFFFFFF, 0.8);
        rw = _this.regmn.rwidth;
        rh = _this.regmn.rheight;
        scaledrw = _this.width / _this.regmn.columns;
        scaledrh = _this.height / _this.regmn.rows;
        _this.section_size.x = scaledrw / 3;
        _this.section_size.y = scaledrh / 3;
        console.debug(_this.regmn.rows, _this.regmn.columns);
        for (row = _i = 0, _ref = ~~_this.regmn.rows; 0 <= _ref ? _i < _ref : _i > _ref; row = 0 <= _ref ? ++_i : --_i) {
          y = row * scaledrh;
          for (col = _j = 0, _ref1 = ~~_this.regmn.columns; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; col = 0 <= _ref1 ? ++_j : --_j) {
            x = col * scaledrw;
            _this.world.drawRect(x, y, scaledrw, scaledrh);
          }
        }
        _this.initSections();
        _this.world.addChild(_this.viewport);
        return _this.map.scene.stage.addChild(_this.world);
      });
    };
    return MiniMapView;
  });

}).call(this);
