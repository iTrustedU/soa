mongoose        = require "mongoose"
MapModel        = require "../../models/editor/Map"
RegionModel     = require "../../models/editor/Region"
SectionModel    = require "../../models/editor/Section"
StatusMessages  = require "../../modules/StatusMessages"
TileMeta        = require "../../models/editor/TileMeta"
fs              = require "graceful-fs"
mkdirp          = require "mkdirp"
crypto          = require "crypto"
colors          = require "colors"
async           = require "async"
wrench          = require "wrench"

module.exports = (app) ->
    NUM_SECTIONS = 9
    NUM_LAYERS = 4
    SECTION_W = 50
    SECTION_H = 50

    app.get "/map/:name/load", (req, res) ->
        map_name = req.params.name
        MapModel
        .findOne(name: map_name)
        .exec (error, map) ->
            if error?
                stat = StatusMessages.ERROR
                res.status(stat.code).send(stat.msg)
            else
                res.send(map)

    app.get "/maps", (req, res) ->
        MapModel
        .find()
        .select("name -_id")
        .exec (error, results) ->
            if error?
                stat = StatusMessages.ERROR
                res.status(stat.msg).send(stat.msg)
                return
            res.send results

    app.post "/maps/deletemany", (req, res) ->
        names = req.body.names
        MapModel
        .find("name" : {$in : names})
        .remove()
        .exec (error, success) ->
            if error?
                stat = StatusMessages.ERROR
                res.status(stat.code).send(stat.msg)
            else
                stat = StatusMessages.SUCCESS
                res.status(stat.code).send(stat.msg)

    app.post "/maps/delete", (req, res) ->
        MapModel
        .find()
        .remove()
        .exec (error, success) ->
            if error?
                stat = StatusMessages.ERROR
                res.status(stat.code).send(stat.msg)
            else
                stat = StatusMessages.SUCCESS
                res.status(stat.code).send(stat.msg)

    app.post "/map/:name/delete", (req, res) ->
        name = req.params.name
        MapModel
        .findOne(name: name)
        .remove()
        .exec (error, success) ->
            if error?
                stat = StatusMessages.ERROR
                res.status(stat.code).send(stat.msg)
            else
                stat = StatusMessages.SUCCESS
                res.status(stat.code).send(stat.msg)            

    app.post "/maps/new", (req, res) ->
        data = req.body

        initializeMap data, (err, mapData) ->
            console.log "Map initialized"
            MapModel
            .findOne(name: data.name)
            .exec (error, map) ->
                if (map?)
                    res.status(422).send("Map with the same name already exists!")
                map = new MapModel mapData
                map.save (err) ->
                    if err?
                        stat = StatusMessages.ERROR
                        res.status(stat.code).send(err.message)
                    else
                        stat = StatusMessages.SUCCESS
                        res.status(stat.code).send(map)

    app.post "/map/:name/tiles/add", (req, res) ->
        data = req.body
        MapModel
        .findOne(name: req.params.name)
        .exec (error, map) ->
            tiles = map.tiles.filter (tile) -> tile.seedId is +data.seedId
            if tiles.length isnt 0
                res.status(422).send("Tile with id:#{data.seedId} is a duplicate tile")
            else
                map.tiles.push(data)
                map.save()
                res.send(200)

    app.get "/tiles/resetseed", (req, res) ->
        app.locals.tileSeeder.reset()

    app.post "/tiles/new", (req, res) ->
        data = req.body
        tile = new TileMeta
            name: data.name
            layer: data.layer
            span: data.span
            sprite: data.sprite
            seedId: app.locals.tileSeeder.randomize()

        tile.save (err) ->
            if err?
                stat = StatusMessages.ERROR
                res.status(stat.code).send(err.message)
            else
                res.send(tile)

    app.post "/tile/:id/delete", (req, res) ->
        id = req.params.id
        TileMeta
        .findOne(seedId: +id)
        .remove()
        .exec (err) ->
            return res.send(422) if err?
            res.send(200)
    
    app.get "/tiles", (req, res) ->
        TileMeta
        .find()
        .select("-_id")
        .exec (error, results) ->
            if error?
                stat = StatusMessages.ERROR
                res.status(stat.msg).send(stat.msg)
                return
            res.send results

    app.get "/map/:name/region/:row/:col/load", (req, res) ->
        row     = req.params.row
        col     = req.params.col
        name    = req.params.name

        MapModel
        .find(name: name)
        .exec (err, map) ->
            res.send(422) if err?
            res.send(map.regions["#{row}_#{col}"])

    app.get "/map/:name/region/:row/:col/:section/load", (req, res) ->
        row     = req.params.row
        col     = req.params.col
        section = req.params.section
        name    = req.params.name

        MapModel
        .find(name: name)
        .exec (err, map) ->
            return res.send(422) if err?
            res.send(map.regions["#{row}_#{col}"].sections[section])

    app.post "/map/:name/region/:row/:col/:section/save", (req, res) ->
        row             = req.params.row
        col             = req.params.col
        section_index   = req.params.section
        name            = req.params.name

        MapModel
        .findOne(name: name)
        .exec (err, map) ->
            key = "#{row}_#{col}"
            region = map.regions[key]
            return res.send(422, "No region at #{row}, #{col}") if not region?
            section = region.sections[section_index]
            section.buffers = req.body.buffers.map (buff) -> return buff.split(",")[1] # Trim excess MIME bytes
            region.section = section
            map.markModified("regions")
            map.save (err) ->
                return res.send(422, err.message) if err?
                res.send(200)

    createPNGBuffer = (name, clb) ->
        fdesc = fs.readFile name, (err, data) =>
            clb(err, data.toString("base64"))

    initializeMap = (map, clb) ->
        region_keys = Object.keys(map.regions)
        empty_buffer = null

        initializeSectionBuffers = (region) ->
            return (sectionIndex, clb) ->
                section = region.sections[sectionIndex]
                section.buffers = (empty_buffer for i in [0...NUM_LAYERS])
                section.x = +section.x
                section.y = +section.y
                clb(null, section)

        initializeSection = (region, clb) ->
            region.x    = +region.x
            region.y    = +region.y
            region.row  = +region.row
            region.col  = +region.col
            
            async.map([0...NUM_SECTIONS], initializeSectionBuffers(region), 
                (err, section) =>
                    clb(err, section)
            )

        initializeRegion = (regkey, clb) ->
            region = map.regions[regkey]
            async.map([region], initializeSection, 
                (err, section) ->
                    console.log "Section initialized"
                    clb(err, section)
            )

        createPNGBuffer "temp.png", (err, data) ->
            empty_buffer = data
            async.map(region_keys, initializeRegion, (err) ->
                console.log "Regions initialized"
                console.dir map
                clb(err, map)
            )
