(function() {
  "use strict";
  var $infrastructure_tab, $science_tab, $upgrade_price, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"science-tent draggable\">\n    <div class=\"dialog-name\">\n        <span>Science-tent</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"science-tabs\">\n        <div id=\"science-tab\" class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Science</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div id=\"infrastructure-tab\"  class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Infrastructure</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n    </div>\n    <div id=\"tab_content_infrastructure_template\" class=\"science-tent-content infrastructure-tab\">\n        {{{infrastructure_tab}}}\n    </div>\n    <div id=\"tab_content_science_template\">{{{science_tab}}}</div>\n    <div class=\"war-building-info\">\n        <img src=\"assets/sprites/amjad/dialogs/constructions/civil/science.png\" class=\"dialog-building-img\" />\n        <div id=\"upgrade-price\" class=\"upgrade-price\">\n            {{{upgrade_price}}}\n        </div>\n        <div id=\"upgrade-building\" class=\"upgrade-building\">Upgrade</div>\n        <div id=\"move-building\" class=\"upgrade-building move-button\">Move</div>\n    </div>\n</div>";

  $upgrade_price = "\n{{#each data.resources}}\n    <img src=\"img/top-resources/{{@key}}.png\">\n    <span>{{this}}</span>\n{{/each}}\n";

  $infrastructure_tab = "{{#each data}}\n<div class=\"science-content-tab\">\n     <span class=\"science-middle-deco\">{{@key}}</span>\n</div>\n<div class=\"science-tent-content-box  resources-content-tab-sub-content\">\n    {{#each this.techs}}\n        <div class=\"science-tab-box\" id=\"{{@key}}\">\n            <div class=\"img-name\">\n                <img src=\"{{concat_technology @key}}\" class=\"war-research-img\" />\n                <span class=\"science-name\">{{lang @key}}</span>\n                <div id={{@key}} class=\"science-research-button\">Research</div>\n            </div>\n            <div class=\"st-info-box\">\n                <div class=\"st-level\">\n                    <div class=\"science-info-icons science-level-icon\"></div>\n                    <span class=\"science-level\">{{lvl}}</span>\n                </div>\n                <div class=\"st-time\">\n                    <div class=\"science-info-icons science-time-icon\"></div>\n                    <span>{{time}}</span>\n                </div>\n                <div class=\"st-price\">\n                    {{#each res}}\n                    <img src=\"img/top-resources/{{@key}}.png\" />\n                    <span>{{this}}</span>\n                    {{/each}}\n                </div>\n                <div class=\"st-info\">\n                    <span>{{info}}</span>\n                </div>\n            </div>\n        </div>\n    {{/each}}\n    </div>\n{{/each}}";

  $science_tab = "<div id=\"science-tab-container\" class=\"science-tent-content science-tab default-skin\">\n    {{#each data}}\n    <div class=\"science-tab-box\" id=\"{{@key}}\">\n        <div class=\"img-name\">\n            <img src=\"{{concat_science @key}}\" class=\"war-research-img\" />\n            <span class=\"science-name\">{{lang @key}}</span>\n            <div id={{@key}} class=\"science-research-button\">Research</div>\n        </div>\n        <div class=\"st-info-box\">\n            <div class=\"st-level\">\n                <div class=\"science-info-icons science-level-icon\"></div>\n                <span class=\"science-level\">{{lvl}}</span>\n            </div>\n            <div class=\"st-time\">\n                <div class=\"science-info-icons science-time-icon\"></div>\n                <span>{{time}}</span>\n            </div>\n            <div class=\"st-price\">\n                {{#each res}}\n                <img src=\"img/top-resources/{{@key}}.png\">\n                <span>{{this}}</span>\n                {{/each}}\n            </div>\n            <div class=\"st-info\">\n                <span>{{info}}</span>\n            </div>\n        </div>\n    </div>\n    {{/each}}\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var ScienceDialog;
    ScienceDialog = (function(_super) {
      __extends(ScienceDialog, _super);

      function ScienceDialog() {
        ScienceDialog.__super__.constructor.call(this, html);
      }

      return ScienceDialog;

    })(BaseDialog);
    ScienceDialog.prototype.onPrologue = function() {
      this.setTemplates({
        "science_template": $science_tab,
        "infrastructure_template": $infrastructure_tab,
        "upgrade_template": $upgrade_price
      });
      this.upgradeable();
      this.moveable();
      this.closeable();
      return this.draggable();
    };
    ScienceDialog.prototype.onEpilogue = function(hud) {
      var SELF;
      SELF = this.html;
      this.on("SELECTED_TAB", function(val) {
        var datakey, template,
          _this = this;
        template = "science_template";
        if (val === "science-tab") {
          datakey = "science_tab";
        } else if (val === "infrastructure-tab") {
          template = "infrastructure_template";
          datakey = "infrastructure_tab";
        } else {
          alert("not supported");
          return;
        }
        $("div[id^=tab_content_").css('display', 'none');
        $('.science-tabs #tab_content_' + datakey).css('display', 'block');
        return this.compile(template, datakey).then(function(data) {
          _this.refresh("tab_content_" + template, data);
          _this.html.find("#science-tab-container").customScrollbar();
          return _this.html.find('.science-content-tab').click(function() {
            SELF.find('.science-tent-content-box').hide();
            return $(this).next().toggle();
          });
        });
      });
      this.on("START_RESEARCH", function() {
        var defer, id, level, row, tab,
          _this = this;
        id = this.get("START_RESEARCH");
        tab = this.get("SELECTED_TAB");
        row = this.html.find(".science-tab-box#" + id);
        level = this.ctx.level;
        defer = new $.Deferred();
        if ((tab == null) || (id == null)) {
          defer.reject("TAB: " + tab + ", ID: " + id);
        } else if (tab === "science-tab") {
          amjad.empire.startScienceResearch(id, level).done(function(data) {
            var res;
            res = amjad.empire.resources.getScienceResources(id, level);
            data["name"] = _this.get("SELECTED_SCIENCE");
            data["type"] = _this.get("SELECTED_SCIENCE");
            data["amount"] = 1;
            data["construction_id"] = _this.ctx.construction_id;
            data["level"] = 'level' + level;
            return _this.ctx.beginScienceResearch(data, res);
          }).fail(function(err) {
            return defer.reject(err);
          });
        } else if (tab === "infrastructure-tab") {
          amjad.empire.startTechResearch(id, level).done(function(data) {
            var res;
            res = amjad.empire.resources.getTechResources(id, level);
            data["name"] = _this.get("SELECTED_SCIENCE");
            data["type"] = _this.get("SELECTED_SCIENCE");
            data["amount"] = 1;
            data["construction_id"] = _this.ctx.construction_id;
            data["level"] = 'level' + level;
            return _this.ctx.beginTechResearch(data, res);
          }).fail(function(err) {
            return defer.reject(err);
          });
        }
        return defer.promise().done(function(research) {}).fail(function(err) {
          return alert(err.statusText);
        });
      });
      this.on("SHOW", function() {
        var datakey, scienceData, techData, upgradeResources,
          _this = this;
        scienceData = this.getScienceData();
        this.setData("science_tab", {
          data: scienceData
        });
        techData = this.getTechData();
        this.setData("infrastructure_tab", {
          data: techData
        });
        upgradeResources = amjad.empire.resources.getConstructionUpgradeResources('science', this.ctx.level + 1);
        this.setData("upgrade_price", {
          data: upgradeResources
        });
        datakey = "upgrade_price";
        this.compile("upgrade_template", datakey).then(function(data) {
          return _this.refresh("upgrade-price", data);
        });
        this.set("SELECTED_TAB", "science-tab");
        this.trigger("SELECTED_TAB", "science-tab");
        return this.html.find("#science-tab-container").customScrollbar();
      });
      this.clickTriggers({
        ".chat-tab/#id": "SELECTED_TAB",
        ".science-research-button/#id": "START_RESEARCH",
        ".science-tab-box/#id": "SELECTED_SCIENCE"
      });
      ScienceDialog.prototype.getScienceData = function() {
        var currentLevel, key, resources, scienceData, value, xmlSciences, _ref, _ref1;
        xmlSciences = XMLStore.get("science");
        scienceData = {};
        for (key in xmlSciences) {
          value = xmlSciences[key];
          currentLevel = ((_ref = amjad.empire) != null ? (_ref1 = _ref.sciences) != null ? _ref1[key] : void 0 : void 0) ? amjad.empire.sciences[key].level : 1;
          resources = amjad.empire.resources.getScienceResources(key, currentLevel);
          scienceData[key] = {
            lvl: currentLevel,
            res: resources
          };
        }
        return scienceData;
      };
      return ScienceDialog.prototype.getTechData = function() {
        var currentLevel, key, resources, techData, technologies, value, xmlTechnologies, _ref, _ref1;
        xmlTechnologies = XMLStore.get("technologies");
        technologies = {};
        for (key in xmlTechnologies) {
          value = xmlTechnologies[key];
          currentLevel = ((_ref = amjad.empire) != null ? (_ref1 = _ref.technologies) != null ? _ref1[key] : void 0 : void 0) ? amjad.empire.technologies[key].level : 1;
          resources = amjad.empire.resources.getTechResources(key, currentLevel);
          technologies[key] = {
            lvl: currentLevel,
            res: resources
          };
        }
        techData = {
          trade: {
            techs: {
              caravan_discipline: technologies.caravan_discipline,
              load_management: technologies.load_management
            }
          },
          resource: {
            techs: {
              wood_hauling: technologies.wood_hauling
            }
          },
          construction: {
            techs: {
              worker_education: technologies.worker_education
            }
          }
        };
        return techData;
      };
    };
    return ScienceDialog;
  });

}).call(this);
