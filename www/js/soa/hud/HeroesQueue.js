(function() {
  "use strict";
  var $heroes, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"bottom-top\">  \n    <div class=\"heroes\">\n        <div id=\"heroes-list\">\n            {{{heroes_list}}}\n        </div>\n    </div>\n</div>";

  $heroes = "{{#each heroes}}\n    <div class=\"hero-frame\" type=\"{{_type_}}\" id=\"{{_type_}}-{{id}}\">\n        <img src={{concat_hero_avatar _type_ \"f\"}} class=\"hero-icon\"></img>\n        <div class=\"hero-name-title\">\n            <span class=\"hero-name\">{{name}}</span>\n            <br>\n            <span class=\"hero-title\">{{title}}</span>\n        </div>\n        <div class=\"expirience\">\n            <span><img src=\"img/dialog/training/level.png\" class=\"hero-level-icon\" /> {{level}}</span>\n            <div class=\"exp-bar\">\n                <div class=\"exp-inner-bar\">\n                    <div id=\"bar\" {{calc_experience_percentage this}}></div>\n                </div>\n            </div>\n        </div>  \n    </div>\n{{/each}}";

  define(["soa/hud/dialogs/BaseDialog", "soa/hud/IntervalQueue"], function(BaseDialog, IntervalQueue) {
    var HeroesQueue, QUEUE_REFRESH_TIME;
    QUEUE_REFRESH_TIME = 1000 * 10;
    HeroesQueue = (function(_super) {
      __extends(HeroesQueue, _super);

      function HeroesQueue() {
        HeroesQueue.__super__.constructor.call(this, html);
      }

      return HeroesQueue;

    })(BaseDialog);
    HeroesQueue.prototype.prologue = function() {
      this.setTemplates({
        "heroes_list": $heroes
      });
      return this.setData("queue", {
        "heroes": []
      });
    };
    HeroesQueue.prototype.putHero = function(hero) {
      var _this = this;
      this.getData("queue").heroes.unshift(hero);
      return this.compile("heroes_list", "queue").then(function(data) {
        _this.refresh("heroes-list", data, 200);
        _this.show({});
        return _this.html.find(".hero-frame").each(function(k, v) {
          var h, id, signature, type;
          h = $(v);
          signature = h.attr("id").split("-");
          type = signature[0];
          id = signature[1];
          h.unbind("click");
          h.dblclick(function() {
            var dlg;
            hero = amjad.empire.heroes["" + type + "s"][id];
            dlg = type.toUpperCase();
            return SOA.Hud.showDialog(dlg, hero);
          });
          return h.click(function() {
            hero = amjad.empire.heroes["" + type + "s"][id];
            return SOA.Zone.centerOn(hero.layer.position);
          });
        });
      });
    };
    return HeroesQueue;
  });

}).call(this);
