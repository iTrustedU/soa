(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Tile", "EventDispatcher", "Vec2", "Ajax", "MapConfig", "TileMaskTable", "PathFinder", "Geometry", "RegionManager", "MiniMapView", "TileMetaStorage"], function(Tile, EventDispatcher, Vec2, Ajax, MapConfig, TileMaskTable, PathFinder, Geometry, RegionManager, MiniMapView, TileMetaStorage) {
    var DEFAULT_ANIMATED_LAYER, DIRECTIONS, DIRECTIONS_KEYS, MAX_LAYERS, MapManager, OVER_NEW_TILE_LAYER, TEXTURE_CHUNK_HEIGHT, TEXTURE_CHUNK_WIDTH, TILEH2, TILEH2_PROP, TILEW2, TILEW2_PROP, TILE_HEIGHT, TILE_WIDTH;
    TILE_WIDTH = MapConfig.TILE_WIDTH;
    TILE_HEIGHT = MapConfig.TILE_HEIGHT;
    MAX_LAYERS = MapConfig.MAX_LAYERS;
    DEFAULT_ANIMATED_LAYER = MapConfig.DEFAULT_ANIMATED_LAYER;
    OVER_NEW_TILE_LAYER = MapConfig.OVER_NEW_TILE_LAYER;
    TILEW2_PROP = 2 / TILE_WIDTH;
    TILEH2_PROP = 2 / TILE_HEIGHT;
    TILEW2 = TILE_WIDTH * 0.5;
    TILEH2 = TILE_HEIGHT * 0.5;
    TEXTURE_CHUNK_WIDTH = 2048;
    TEXTURE_CHUNK_HEIGHT = 2048;
    DIRECTIONS = [
      {
        north: [-1, 0],
        south: [1, 0],
        east: [0, 2],
        west: [0, -2],
        southeast: [0, 1],
        northeast: [-1, 1],
        southwest: [0, -1],
        northwest: [-1, -1]
      }, {
        north: [-1, 0],
        south: [1, 0],
        east: [0, 2],
        west: [0, -2],
        southeast: [1, 1],
        northeast: [0, 1],
        southwest: [1, -1],
        northwest: [0, -1]
      }
    ];
    DIRECTIONS_KEYS = Object.keys(DIRECTIONS[0]);
    MapManager = (function(_super) {
      __extends(MapManager, _super);

      function MapManager(scene) {
        this.scene = scene;
        MapManager.__super__.constructor.call(this);
        this.init();
      }

      return MapManager;

    })(EventDispatcher);
    MapManager.prototype.init = function() {
      this.NROWS = 0;
      this.NCOLUMNS = 0;
      this.ortho_vector = Vec2.from(0, 0);
      this.world_bounds = Vec2.from(0, 0);
      this.mmap_view_size = Vec2.clone(this.scene.screen_size);
      this.layers = [];
      this.tiles = [];
      this.map_buffers = [];
      this.map_viewports = [];
      this.prerendered = {};
      this.tile_graphics = [];
      this.tile_under = null;
      this.last_clicked = null;
      this.floor_layer = null;
      this.top_layer = null;
      this.pathfinder = null;
      this.tile_storage = new TileMetaStorage();
      this.tile_shape = this.createFillTileShape(0, 0, MapConfig.TILE_OVER_COLOR);
      /* Hit area that gets updated with every camera transform*/

      this.hit_area = new PIXI.Rectangle(0, 0, this.scene.screen_size.x, this.scene.screen_size.y);
      this.selection = {
        rect: new PIXI.Rectangle(0, 0, 0, 0),
        graphics: new PIXI.Graphics(),
        tiles: []
      };
      this.region_mngr = new RegionManager(this);
      this.minimap = new MiniMapView(this);
      return this.initListeners();
    };
    MapManager.prototype.initListeners = function() {
      var _this = this;
      this.scene.on("MOUSE_RIGHT_DRAG_STARTED", function(mpos) {
        _this.selection.rect.x = mpos.x;
        return _this.selection.rect.y = mpos.y;
      });
      this.scene.on("MOUSE_RIGHT_DRAG", function(mpos) {
        var bottom_right_pivot, col, g, graphics, i, ncols, nrows, row, start_col, start_row, tile, top_left_pivot, _i, _j, _len, _ref, _results;
        _this.selection.rect.width = _this.selection.rect.x - mpos.x;
        _this.selection.rect.height = _this.selection.rect.y - mpos.y;
        _this.selection.graphics.clear();
        _this.selection.graphics.lineStyle(2, MapConfig.SELECTION_RECT_COLOR, 1);
        g = _this.selection.graphics;
        g.moveTo(_this.selection.rect.x, _this.selection.rect.y);
        g.lineTo(_this.selection.rect.x, mpos.y);
        g.lineTo(mpos.x, mpos.y);
        g.lineTo(mpos.x, _this.selection.rect.y);
        g.lineTo(_this.selection.rect.x, _this.selection.rect.y);
        top_left_pivot = _this.getTileAtPosition(_this.selection.rect.x, _this.selection.rect.y);
        bottom_right_pivot = _this.getTileAtPosition(mpos.x, mpos.y);
        ncols = bottom_right_pivot.col - top_left_pivot.col + 1;
        nrows = bottom_right_pivot.row - top_left_pivot.row + 1;
        start_row = top_left_pivot.row;
        start_col = top_left_pivot.col;
        _this.selection.tiles = [];
        _ref = _this.tile_graphics;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          graphics = _ref[_i];
          graphics.clear();
        }
        i = 0;
        _results = [];
        for (col = _j = 0; 0 <= ncols ? _j < ncols : _j > ncols; col = 0 <= ncols ? ++_j : --_j) {
          _results.push((function() {
            var _k, _results1;
            _results1 = [];
            for (row = _k = 0; 0 <= nrows ? _k < nrows : _k > nrows; row = 0 <= nrows ? ++_k : --_k) {
              tile = this.getTileAt(start_row + row, start_col + col);
              this.selection.tiles.push(tile);
              this.fillTileShape(tile, this.tile_graphics[i]);
              _results1.push(++i);
            }
            return _results1;
          }).call(_this));
        }
        return _results;
      });
      this.scene.on("MOUSE_WORLD_CHANGED", function(worldX, worldY) {
        var new_tile;
        new_tile = _this.getTileAtPosition(worldX, worldY);
        if ((new_tile != null) && !_this.areTilesEqual(new_tile, _this.tile_under)) {
          _this.tile_under = new_tile;
          _this.tile_shape.position.x = _this.tile_under.position.x;
          _this.tile_shape.position.y = _this.tile_under.position.y;
          return _this.trigger("MOUSE_OVER_TILE", _this, new_tile.row, new_tile.col);
        }
      });
      return this.scene.on("MOUSE_RIGHT_DRAG_ENDED", function(mpos) {
        var graphics, _i, _len, _ref, _results;
        _this.selection.graphics.clear();
        _ref = _this.tile_graphics;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          graphics = _ref[_i];
          _results.push(graphics.clear());
        }
        return _results;
      });
    };
    MapManager.prototype.allocateTileGraphics = function() {
      var graphics, i, total, _i, _results;
      total = this.calculateMaxTilesOnScreen();
      _results = [];
      for (i = _i = 0; 0 <= total ? _i < total : _i > total; i = 0 <= total ? ++_i : --_i) {
        graphics = new PIXI.Graphics();
        graphics.visible = false;
        this.scene.world.addChild(graphics);
        _results.push(this.tile_graphics.push(graphics));
      }
      return _results;
    };
    MapManager.prototype.fillTileShape = function(tile, graphics) {
      var pos;
      pos = this.getTilePosition(tile.row, tile.col);
      graphics.visible = true;
      graphics.beginFill(0xC32F01, 1);
      graphics.moveTo(-64 + pos.x, 0 + pos.y);
      graphics.lineTo(0 + pos.x, -32 + pos.y);
      graphics.lineTo(64 + pos.x, 0 + pos.y);
      graphics.lineTo(0 + pos.x, 32 + pos.y);
      graphics.lineTo(-64 + pos.x, 0 + pos.y);
      graphics.endFill();
      return graphics;
    };
    MapManager.prototype.removeLayers = function() {
      var _this = this;
      console.warn("Removing " + this.layers.length + " layers");
      this.layers.forEach(function(layer) {
        return _this.scene.world.removeChild(layer);
      });
      return this.layers = [];
    };
    MapManager.prototype.createFillTileShape = function(x, y, color) {
      var tile_shape;
      tile_shape = new PIXI.Graphics();
      tile_shape.beginFill(color, 0.7);
      tile_shape.moveTo(-TILEW2, 0);
      tile_shape.lineTo(0, -TILEH2);
      tile_shape.lineTo(TILEW2, 0);
      tile_shape.lineTo(0, TILEH2);
      tile_shape.lineTo(-TILEW2, 0);
      tile_shape.endFill();
      tile_shape.position.x = x;
      tile_shape.position.y = y;
      return tile_shape;
    };
    MapManager.prototype.tintTileLayer = function(row, col, layer, tint_color) {
      var pos, shape;
      if (layer == null) {
        layer = 2;
      }
      if (tint_color == null) {
        tint_color = 0x0292CD;
      }
      pos = this.getTileAt(row, col).position;
      shape = this.createFillTileShape(pos.x, pos.y, tint_color);
      this.tile_graphics.push(shape);
      return this.layers[layer].addChild(shape);
    };
    MapManager.prototype.tintSpan = function(span_area, layer, tint_color) {
      var t, _i, _len, _results;
      if (layer == null) {
        layer = 2;
      }
      if (tint_color == null) {
        tint_color = 0x0292CD;
      }
      _results = [];
      for (_i = 0, _len = span_area.length; _i < _len; _i++) {
        t = span_area[_i];
        _results.push(this.tintTileLayer(t.row, t.col, layer, tint_color));
      }
      return _results;
    };
    MapManager.prototype.createLayers = function() {
      var i, layer, _i;
      this.scene.world.addChild(this.tile_shape);
      if (this.layers.length > 0) {
        this.removeLayers();
      }
      for (i = _i = 0; 0 <= MAX_LAYERS ? _i < MAX_LAYERS : _i > MAX_LAYERS; i = 0 <= MAX_LAYERS ? ++_i : --_i) {
        layer = this.layers[i] = new PIXI.DisplayObjectContainer();
        layer._pass_me_pixi_ = true;
        this.scene.world.addChild(layer);
      }
      this.top_layer = this.layers[MAX_LAYERS - 1];
      this.floor_layer = this.layers[0];
      console.debug("" + MAX_LAYERS + " map layers initialized");
      return this.initializeLayers();
    };
    MapManager.prototype.initializeLayers = function() {
      var _this = this;
      this.top_layer.interactive = true;
      this.top_layer.hitArea = this.hit_area;
      this.floor_layer = this.layers[0];
      return this.top_layer.click = function(data) {
        _this.floor_layer._i_hate_you_pixi_ = false;
        _this.floor_layer._pass_me_pixi_ = false;
        _this.floor_layer.updateTransform();
        _this.scene.stage.interactionManager.update();
        data.hitTarget = _this.resolveClickedLayers(_this.scene.stage.interactionManager.hitted.slice(), data);
        _this.trigger("TOP_LAYER_CLICK", _this, data);
        _this.floor_layer._i_hate_you_pixi_ = false;
        return _this.floor_layer._pass_me_pixi_ = false;
      };
    };
    /* @todo dimensions not being used*/

    MapManager.prototype.prerenderLayer = function(layer_index, frame, chunkW, chunkH) {
      var layer, renderable, renderer;
      renderer = new PIXI.RenderTexture(chunkW, chunkH);
      renderable = new PIXI.Sprite(renderer);
      layer = this.layers[layer_index];
      renderable.anchor.x = 0;
      renderable.anchor.y = 0;
      layer._i_hate_you_pixi_ = false;
      layer._pass_me_pixi_ = false;
      layer.position.x = -frame.x;
      layer.position.y = -frame.y;
      layer.updateTransform();
      layer._i_hate_you_pixi_ = true;
      renderer.render(layer, layer.position);
      renderable.position.x = frame.x;
      renderable.position.y = frame.y;
      renderer.destroy();
      return renderable;
    };
    MapManager.prototype.prerenderLayerChunks = function(layer) {
      var ch, chunk, cw, e, h, layers, layert, rect, w;
      rect = new PIXI.Rectangle();
      layert = new PIXI.DisplayObjectContainer();
      layert.visible = false;
      this.scene.world.addChild(layert);
      /* 
          @todo
          screen size or perhaps the next (upper, lower) power of two?
      */

      cw = TEXTURE_CHUNK_WIDTH;
      ch = TEXTURE_CHUNK_HEIGHT;
      rect.width = cw;
      rect.height = ch;
      w = 0;
      h = 0;
      while (w < this.world_bounds.x) {
        while (h < this.world_bounds.y) {
          rect.x = w;
          rect.y = h;
          try {
            chunk = this.prerenderLayer(0, rect, cw, ch);
            layert.addChild(chunk);
          } catch (_error) {
            e = _error;
            window.alert(e);
            break;
          }
          h += ch;
        }
        w += cw;
        h = 0;
      }
      layers = this.layers[layer];
      layert.visible = true;
      layers.position.x = 0;
      layers.position.y = 0;
      this.scene.world.swapChildren(layers, layert);
      layers._pass_me_pixi_ = true;
      layers._i_hate_you_pixi_ = false;
      layers.updateTransform();
      layers._pass_me_pixi_ = false;
      layers._i_hate_you_pixi_ = true;
      return layers.visible = false;
    };
    MapManager.prototype.load = function(map_meta) {
      this.map_meta = map_meta;
      this.NROWS = this.map_meta.rows;
      this.NCOLUMNS = this.map_meta.columns;
      this.loadTileStorage(this.map_meta.tiles);
      this.initTiles();
      this.createLayers();
      this.calculateWorldBounds();
      this.trigger("TILES_INITIALIZED", this, this.map_meta);
      this.drawGrid();
      this.allocateTileGraphics();
      this.scene.stage.addChild(this.selection.graphics);
      this.scene.stage.interactive = true;
      return this.trigger("MAP_LOADED", this);
    };
    MapManager.prototype.loadTileStorage = function(tiles) {
      var t, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = tiles.length; _i < _len; _i++) {
        t = tiles[_i];
        _results.push(this.tile_storage.addTile(t));
      }
      return _results;
    };
    MapManager.prototype.resetMapTiles = function(rows, cols) {
      var layer, tile, _i, _len, _ref, _results;
      if (rows == null) {
        rows = this.NROWS;
      }
      if (cols == null) {
        cols = this.NCOLUMNS;
      }
      this.scene.world.clear();
      this.tiles.forEach(function(t) {
        return t.destroy();
      });
      this.tiles = [];
      _ref = this.layers;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        layer = _ref[_i];
        _results.push((function() {
          var _j, _len1, _ref1, _results1;
          _ref1 = layer.children.slice();
          _results1 = [];
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            tile = _ref1[_j];
            if ((tile != null) && (tile instanceof PIXI.Graphics)) {
              continue;
            }
            _results1.push(layer.removeChild(tile));
          }
          return _results1;
        })());
      }
      return _results;
    };
    MapManager.prototype.initTiles = function() {
      var col, row, tile, x, y, _i, _j, _ref, _ref1;
      for (row = _i = 0, _ref = MapConfig.NN_ROWS; 0 <= _ref ? _i < _ref : _i > _ref; row = 0 <= _ref ? ++_i : --_i) {
        for (col = _j = 0, _ref1 = MapConfig.NN_COLUMNS; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; col = 0 <= _ref1 ? ++_j : --_j) {
          tile = new Tile(row, col);
          this.tiles.push(tile);
          x = tile.position.x = (col / 2) * TILE_WIDTH;
          y = tile.position.y = (row + ((col % 2) / 2)) * TILE_HEIGHT;
        }
      }
      this.total = this.NROWS * this.NCOLUMNS;
      return this.pathfinder = new PathFinder(this);
    };
    MapManager.prototype.drawGrid = function() {
      var i, out, pA1, pB1, pHA, pHB, pVA, pVB, x1, y1, _i, _j, _ref, _ref1;
      this.scene.world.lineStyle(MapConfig.GRID_LINE_THICKNESS, MapConfig.GRID_LINE_COLOR, MapConfig.GRID_LINE_ALPHA);
      y1 = 32;
      pVA = Vec2.from(this.world_bounds.x - 64, 0);
      pVB = Vec2.from(this.world_bounds.x - 64, this.world_bounds.y);
      pHA = Vec2.from(0, 0);
      pHB = Vec2.from(this.world_bounds.x, 0);
      out = Vec2.from(0, 0);
      pA1 = Vec2.from(0, 0);
      pB1 = Vec2.from(0, 0);
      for (i = _i = 0, _ref = this.NROWS * 2; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
        y1 = TILE_HEIGHT * (i + 0.5);
        x1 = y1 * 2;
        Vec2.set(pA1, 0, y1);
        Vec2.set(pB1, x1, 0);
        if (i > (this.NCOLUMNS * 0.5 - 1)) {
          Geometry.findLinesIntersection(out, pA1, pB1, pVA, pVB);
        } else {
          Geometry.findLinesIntersection(out, pA1, pB1, pHA, pHB);
        }
        this.scene.world.moveTo(0, y1);
        this.scene.world.lineTo(out.x, out.y);
      }
      pVA = Vec2.from(0, 0);
      pVB = Vec2.from(0, this.world_bounds.y);
      for (i = _j = 0, _ref1 = this.NROWS * 2; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; i = 0 <= _ref1 ? ++_j : --_j) {
        y1 = TILE_HEIGHT * (i + 0.5);
        x1 = this.world_bounds.x - y1 * 2;
        Vec2.set(pA1, this.world_bounds.x, y1);
        Vec2.set(pB1, x1, 0);
        if (i > (this.NCOLUMNS * 0.5 - 1)) {
          Geometry.findLinesIntersection(out, pA1, pB1, pVA, pVB);
        } else {
          Geometry.findLinesIntersection(out, pA1, pB1, pHA, pHB);
        }
        this.scene.world.moveTo(this.world_bounds.x, y1);
        this.scene.world.lineTo(out.x, out.y);
      }
      Vec2.release(pA1);
      Vec2.release(pB1);
      Vec2.release(pVA);
      Vec2.release(pVB);
      Vec2.release(pHA);
      Vec2.release(pHB);
      return Vec2.release(out);
    };
    MapManager.prototype.addTileLayerAtPosition = function(x, y, tilemeta) {
      var t;
      t = this.region_mngr.getTileAtRegionPosition(x, y);
      return this.addTileLayer(t.row, t.col, tilemeta);
    };
    MapManager.prototype.addTileLayer = function(row, col, tilemeta, layer) {
      var tile;
      tile = this.getTileAt(row, col);
      if (tile == null) {
        throw new Error("No tile at " + row + "," + col);
      }
      return tile.addLayer(this.layers[layer || tilemeta.layer], tilemeta);
    };
    /* @TODO All of these need a rehaul*/

    MapManager.prototype.addTileLayerByID = function(row, col, id) {
      var meta;
      meta = this.tile_storage.findByID(id);
      if (meta == null) {
        return;
      }
      return this.addTileLayer(row, col, meta);
    };
    MapManager.prototype.drawPoint = function(p, size, color) {
      if (color == null) {
        color = 0x000000;
      }
      this.scene.world.beginFill(color, size);
      this.scene.world.drawCircle(p.x, p.y, size);
      return this.scene.world.endFill();
    };
    MapManager.prototype.addTileShape = function(x, y) {
      this.scene.world.lineStyle(MapConfig.GRID_LINE_COLOR, MapConfig.GRID_LINE_COLOR, MapConfig.GRID_LINE_ALPHA);
      this.scene.world.moveTo(-64 + x, 0 + y);
      this.scene.world.lineTo(0 + x, -32 + y);
      this.scene.world.lineTo(64 + x, 0 + y);
      this.scene.world.lineTo(0 + x, 32 + y);
      return this.scene.world.lineTo(-64 + x, 0 + y);
    };
    MapManager.prototype.getTilePosition = function(row, col) {
      var x, y;
      x = (col / 2) * TILE_WIDTH;
      y = (row + ((col % 2) / 2)) * TILE_HEIGHT;
      return new PIXI.Point(x, y);
    };
    MapManager.prototype.worldCenter = function() {
      return new PIXI.Point(this.world_bounds.x * 0.5, this.world_bounds.y * 0.5);
    };
    MapManager.prototype.getTileAtPosition = function(x, y) {
      var ret;
      this.fillOrthoVector(x, y);
      ret = {
        position: this.getTilePosition(this.ortho_vector.y, this.ortho_vector.x),
        row: this.ortho_vector.y,
        col: this.ortho_vector.x
      };
      return ret;
    };
    MapManager.prototype.areTilesEqual = function(a, b) {
      return ((a != null) && (b != null)) && (a !== b) && (a.row === b.row && a.col === b.col);
    };
    MapManager.prototype.fillOrthoVector = function(x, y) {
      var coldiv, off_x, off_y, rowdiv, transp;
      coldiv = (x + TILEW2) * TILEW2_PROP;
      rowdiv = (y + TILEH2) * TILEH2_PROP;
      off_x = ~~((x + TILEW2) - ~~(coldiv * 0.5) * TILE_WIDTH);
      off_y = ~~((y + TILEH2) - ~~(rowdiv * 0.5) * TILE_HEIGHT);
      transp = TileMaskTable[off_x + TILE_WIDTH * off_y];
      this.ortho_vector.x = ~~(coldiv - (transp ^ !(coldiv & 1)));
      return this.ortho_vector.y = ~~((rowdiv - (transp ^ !(rowdiv & 1))) * 0.5);
    };
    MapManager.prototype.getTileAt = function(row, col, dirX, dirY) {
      if (dirX == null) {
        dirX = 0;
      }
      if (dirY == null) {
        dirY = 0;
      }
      return this.tiles[(col + dirY) + (row + dirX) * MapConfig.NN_COLUMNS];
    };
    MapManager.prototype.addLayerSpriteToTile = function(row, col, layer, sprite) {
      var meta, tile;
      tile = this.getTileAt(row, col);
      if (tile == null) {
        throw new Error("No tile at " + row + ", " + col);
      }
      meta = {
        sprite: sprite,
        layer: layer,
        name: sprite,
        id: +(Math.random() * 0xFFFFFF)
      };
      return tile.addLayer(this.layers[layer], meta);
    };
    MapManager.prototype.loadBitmapFromFile = function(file_uri) {
      var _this = this;
      console.warn("This function is now deprecated in favor of loadPNGMap");
      return Ajax.get(file_uri).success(function(data) {
        return _this.createMapFromBitmap(data.split(",").map(function(t) {
          return +t;
        }));
      });
    };
    MapManager.prototype.getNeighbours = function(tile) {
      var dir, direction, key, n, out, parity, _i, _len;
      out = [];
      parity = tile.col % 2;
      direction = DIRECTIONS[parity];
      if (tile == null) {
        return out;
      }
      for (_i = 0, _len = DIRECTIONS_KEYS.length; _i < _len; _i++) {
        key = DIRECTIONS_KEYS[_i];
        dir = direction[key];
        n = this.getTileAt(tile.row, tile.col, dir[0], dir[1]);
        if (n != null) {
          out.push(n);
        }
      }
      return out;
    };
    MapManager.prototype.saveAsPNG = function() {
      var buffer, buffers, col, color, color_channel, hi, i, k, l, layer, layers, lo, m, num_buffers, row, tile, _i, _j, _k, _l, _m, _n, _ref, _ref1;
      num_buffers = Math.ceil(MAX_LAYERS / 2);
      buffers = [];
      /* prepare buffers*/

      for (i = _i = 0; 0 <= num_buffers ? _i < num_buffers : _i > num_buffers; i = 0 <= num_buffers ? ++_i : --_i) {
        buffers[i] = HAL.ImageUtils.createPixelBuffer(this.NCOLUMNS, this.NROWS, 0xFFFFFF);
      }
      for (col = _j = 0, _ref = this.NCOLUMNS; 0 <= _ref ? _j < _ref : _j > _ref; col = 0 <= _ref ? ++_j : --_j) {
        for (row = _k = 0, _ref1 = this.NROWS; 0 <= _ref1 ? _k < _ref1 : _k > _ref1; row = 0 <= _ref1 ? ++_k : --_k) {
          tile = this.getTileAt(row, col);
          color_channel = [];
          for (k = _l = 0; 0 <= MAX_LAYERS ? _l < MAX_LAYERS : _l > MAX_LAYERS; k = 0 <= MAX_LAYERS ? ++_l : --_l) {
            color_channel[k] = 0;
          }
          layers = tile.layers;
          for (l = _m = 0; 0 <= MAX_LAYERS ? _m < MAX_LAYERS : _m > MAX_LAYERS; l = 0 <= MAX_LAYERS ? ++_m : --_m) {
            layer = layers[l];
            if (layer == null) {
              color_channel[l] = 0;
              continue;
            }
            if (layer.id == null) {
              continue;
            }
            color_channel[l] = layer.id;
          }
          /* save color channels to appropriate buffers*/

          for (m = _n = 0; 0 <= num_buffers ? _n < num_buffers : _n > num_buffers; m = 0 <= num_buffers ? ++_n : --_n) {
            buffer = buffers[m];
            hi = color_channel[m * 2];
            lo = color_channel[(m * 2) + 1];
            color = (hi << 16) | lo;
            HAL.ImageUtils.putPixelToBuffer(buffer, col, row, color);
          }
        }
      }
      return buffers;
    };
    MapManager.prototype.createMapFromBitmap = function(mapData) {
      this.loadBitmapMap(mapData);
      return HAL.trigger("MAP_LOADED", this.scene);
    };
    MapManager.prototype.loadBitmapMap = function(bitmap) {
      var layer, layer_height, layer_id, layer_qword, map_cols, map_rows, mask, qword, tile_col, tile_qword, tile_row, _i;
      bitmap = bitmap.slice();
      mask = 0xFFFF;
      qword = bitmap.pop();
      map_rows = (qword >> 32) & mask;
      map_cols = (qword >> 16) & mask;
      this.createEmptyMap(map_rows, map_cols);
      this.scene.world.clear();
      while ((tile_qword = bitmap.pop()) != null) {
        tile_row = (tile_qword >> 32) & mask;
        tile_col = (tile_qword >> 16) & mask;
        for (layer = _i = 0; _i < 7; layer = ++_i) {
          layer_qword = bitmap.pop();
          if (layer_qword === -1) {
            continue;
          }
          layer_id = (layer_qword >> 32) & mask;
          layer_height = (layer_qword >> 16) & mask;
          this.addLayerToTileByID(tile_row, tile_col, layer_id);
        }
      }
      return this.prerenderLayerChunks(0);
    };
    MapManager.prototype.hideLayers = function() {
      var layer, _i, _len, _ref, _results;
      _ref = this.layers.slice(1);
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        layer = _ref[_i];
        _results.push(layer.visible = false);
      }
      return _results;
    };
    MapManager.prototype.calculateMaxTilesOnScreen = function() {
      var BOUNDS_TRESHOLD, cols, height, max_cols, max_rows, rows, width;
      BOUNDS_TRESHOLD = 3;
      width = this.scene.screen_size.x;
      height = this.scene.screen_size.y;
      max_rows = Math.round(height / TILE_HEIGHT / this.scene.zoom_bounds.x);
      max_cols = Math.round(width / TILEW2 / this.scene.zoom_bounds.x);
      rows = max_rows + 2 * BOUNDS_TRESHOLD;
      cols = max_cols + 2 * BOUNDS_TRESHOLD;
      return this.total = rows * cols;
    };
    MapManager.prototype.calculateWorldBounds = function() {
      this.world_bounds.x = this.NCOLUMNS * TILEW2;
      this.world_bounds.y = this.NROWS * TILE_HEIGHT;
      return this.world_bounds;
    };
    MapManager.prototype.createMiniMapView = function() {
      var buffer, img, index, map_img_div, map_img_viewport, _i, _len, _ref, _results;
      throw new Error("Not implemented");
      this.map_buffers = this.saveAsPNG();
      _ref = this.map_buffers;
      _results = [];
      for (index = _i = 0, _len = _ref.length; _i < _len; index = ++_i) {
        buffer = _ref[index];
        map_img_div = document.createElement("div");
        map_img_div.setAttribute("class", "map-img");
        map_img_viewport = document.createElement("div");
        map_img_viewport.setAttribute("class", "map-img-viewport");
        img = HAL.ImageUtils.pixelBufferToImg(buffer);
        map_img_div.appendChild(img);
        map_img_div.appendChild(map_img_viewport);
        this.dom_minimap.appendChild(map_img_div);
        _results.push(this.map_viewports[index] = map_img_viewport);
      }
      return _results;
    };
    MapManager.prototype.updateHitAreaPosition = function(x, y) {
      var top_left;
      top_left = this.scene.toLocalObject(0, 0);
      this.hit_area.x = top_left.x;
      this.hit_area.y = top_left.y;
      return this.trigger("HIT_AREA_CHANGED", this, this.hit_area);
    };
    MapManager.prototype.updateHitAreaDimension = function(zoom) {
      this.hit_area.width = this.scene.screen_size.x / this.scene.world.scale.x;
      this.hit_area.height = this.scene.screen_size.y / this.scene.world.scale.x;
      return this.trigger("HIT_AREA_CHANGED", this, this.hit_area);
    };
    MapManager.prototype.loadPNGMap = function(map_uris) {
      var loader,
        _this = this;
      loader = new PIXI.AssetLoader(map_uris);
      return loader.onComplete = function(data) {};
    };
    MapManager.prototype.resolveClickedLayers = function(hitted, data) {
      var button_type, clicked, hit, pt, transp, _i, _len;
      if (!hitted.length) {
        return;
      }
      hitted = hitted.sort(function(a, b) {
        var z;
        z = a.parentTile.row - b.parentTile.row;
        if (z === 0) {
          z = a.z - b.z;
        }
        return z;
      });
      clicked = null;
      for (_i = 0, _len = hitted.length; _i < _len; _i++) {
        hit = hitted[_i];
        pt = this.scene.toLocalObject(data.global.x, data.global.y, hit);
        transp = HAL.ImageUtils.isTransparent(hit.texture.baseTexture.source, pt.x + hit.texture.frame.width * 0.5 + hit.texture.frame.x, pt.y + hit.texture.frame.height * 0.5 + hit.texture.frame.y);
        if (transp) {
          continue;
        }
        if (clicked == null) {
          clicked = hit;
        } else {
          if (this.areTilesEqual(clicked.parentTile, hit.parentTile) && hit.z > clicked.z) {
            clicked = hit;
          } else if ((hit.parentTile.row !== clicked.parentTile.row) && (hit.parentTile.col !== clicked.parentTile.col)) {
            if (hit.y_off + hit.position.y > clicked.y_off + clicked.position.y) {
              clicked = hit;
            }
          } else if (hit.parentTile.row === clicked.parentTile.row) {
            if ((hit.position.x + pt.x) > clicked.position.x) {
              clicked = hit;
            }
          } else if (hit.parentTile.col === clicked.parentTile.col) {
            if (hit.y_off + hit.position.y > clicked.y_off + clicked.position.y) {
              clicked = hit;
            }
          } else {
            clicked = hit;
          }
        }
      }
      if (clicked == null) {
        return;
      }
      button_type = data.originalEvent.button;
      if (button_type === 0 && !this.scene.is_dragged) {
        clicked.onClick(button_type);
      }
      return clicked;
    };
    MapManager.prototype.addAnimatedLayer = function(row, col, texture_array) {
      var meta, tile;
      tile = this.getTileAt(row, col);
      if (tile == null) {
        throw new Error("No tile at " + row + ", " + col);
      }
      meta = {
        textures: texture_array,
        layer: DEFAULT_ANIMATED_LAYER,
        id: 0
      };
      return tile.addAnimatedLayer(this.layers[meta.layer], meta);
    };
    MapManager.prototype.clearLayers = function() {
      var layer, _i, _len, _ref, _results;
      _ref = this.layers;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        layer = _ref[_i];
        _results.push(layer.children.slice().forEach(function(child) {
          return layer.removeChild(child);
        }));
      }
      return _results;
    };
    MapManager.prototype.findInDirectionOf = function(tile, dirstr, len) {
      var dir, direction, fromc, fromr, out, t;
      if (tile == null) {
        return [];
      }
      out = [];
      out.push(tile);
      fromr = tile.row;
      fromc = tile.col;
      direction = DIRECTIONS[fromc % 2];
      dir = direction[dirstr];
      while (len > 0) {
        t = this.getTileAt(fromr, fromc, dir[0], dir[1]);
        if (t != null) {
          out.push(t);
          fromr = t.row;
          fromc = t.col;
          direction = DIRECTIONS[fromc % 2];
          dir = direction[dirstr];
        } else {
          break;
        }
        len--;
      }
      return out;
    };
    MapManager.prototype.getSpanArea = function(tile, size) {
      var neasts, nwests, out, root, sizelen, w, _i, _len;
      root = ~~Math.sqrt(size.length);
      sizelen = size.length - 1;
      nwests = this.findInDirectionOf(tile, "northwest", root - 1);
      out = [];
      for (_i = 0, _len = nwests.length; _i < _len; _i++) {
        w = nwests[_i];
        neasts = this.findInDirectionOf(w, "northeast", root - 1);
        out = out.concat(neasts);
      }
      out = out.filter(function(_, i) {
        return +size[sizelen - ((i % root) * root) - ~~(i / root)];
      });
      return out;
    };
    MapManager.prototype.canBePlacedOn = function(span, layer) {
      var k, out, _i, _len;
      out = [];
      if (span == null) {
        return out;
      }
      for (_i = 0, _len = span.length; _i < _len; _i++) {
        k = span[_i];
        if (k.layers[layer] != null) {
          out.push(k);
        }
      }
      return out;
    };
    MapManager.prototype.recreateLayerFrom = function(tileA, tileB, z_index, force) {
      var meta_id, metab_id;
      if (force == null) {
        force = false;
      }
      meta_id = tileA.removeLayer(z_index);
      if (tileB.layers[z_index] != null) {
        if (force) {
          metab_id = tileB.removeLayer(z_index);
          this.addLayerToTileByID(tileB.row, tileB.col, metab_id);
        } else {
          throw new Error("You can't move layer at Z: " + z_index + " from [" + tileA.row + "," + tileA.col + "] to [" + tileB.row + "," + tileB.col + "]");
        }
      }
      return this.addLayerToTileByID(tileB.row, tileB.col, meta_id);
    };
    MapManager.prototype.sortLayer = function(layer) {
      return this.layers[layer].children.sort(function(a, b) {
        return a.position.y - b.position.y;
      });
    };
    MapManager.prototype.sortLayers = function() {
      var i, _i, _results;
      _results = [];
      for (i = _i = 0; 0 <= MAX_LAYERS ? _i < MAX_LAYERS : _i > MAX_LAYERS; i = 0 <= MAX_LAYERS ? ++_i : --_i) {
        _results.push(this.sortLayer(i));
      }
      return _results;
    };
    return MapManager;
  });

}).call(this);
