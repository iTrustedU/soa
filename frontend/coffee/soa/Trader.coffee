"use strict"

define ["soa/Hero"],

(Hero) ->

    TRADER_EXPERIENCE_MODIFIER = 1000
    SPEED_PER_TILE = 1.5

    class Trader extends Hero

    Trader::initListeners = () ->
        super()
        ##console.debug "COMMANDER LOADED"
        @EXPERIENCE_MODIFIER = TRADER_EXPERIENCE_MODIFIER

        @on "ARRIVAL", () ->
            # if @lastAction is "trade"
            #     alert("I'll trade here")
            # else if @lastAction is "move"
            #     alert("I just moved")
            @finishArriving()

    Trader::doAction = (action) ->
        super(action)
        row = @target.parentTile.row
        col = @target.parentTile.col
        Rest.post("caravan.move_caravan", {
            cid: @caravan_id
            x: row
            y: col
            mt: action
        })
        .done (data) =>
            @arrival = data.arrival
            @departure = data.departure
            @move_type = action
            @moveTo(row, col, data)
        .fail (data) =>
            alert(data)

    Trader::moveTo = (@d_coord_x, @d_coord_y, data) ->
        @onMoveTo()

    Trader::onAction = (target) ->
        actBox = SOA.getDialog("ACTION_BOX")
        actBox.setMoveAsDefault()
        actBox.MoveBtn.show()
        if target.type?
            if target.type is "trade" and 
            not @isMyPOI(@target.parentTile.row, @target.parentTile.col)
                actBox.TradeBtn.show()

    Trader::isMyPOI = (row, col) ->
        poi = SOA.getPOI(row, col)
        return poi? and poi.user_id is amjad.user.user_id

    Trader::finishArriving = () ->
        Rest.post "caravan.arrive", 
            usid: amjad.user.user_id,
            cid: @caravan_id
        .done (data) =>
            @o_coord_x = @d_coord_x
            @o_coord_y = @d_coord_y
            @resolveArrival(data)
        .fail (data) =>
            console.error data
        @o_coord_x = @d_coord_x
        @o_coord_y = @d_coord_y
        @layer.stop()

    Trader::resolveArrival = (data) ->
        if @move_type is "trade" 
            @doTrade(data)
        else
            @doMove(data)
        @departure = null

    Trader::doTrade = (data) ->
        console.debug data
        # alert("I'll trade now")
        trpoi = SOA.getDialog("TRADE_POI")
        trpoi.show(@)

        # # sprite = PIXI.TextureCache["amjad/poi/encampment_established.png"]
        # poi_id = @target.poid
        # subtype = @target.subtype
        # ##console.debug @target
        # poi = SOA.pois[subtype][poi_id]

        # Rest.post("construction.create_poi", {poid: poi.poi_id, x: poi.x_coord, y: poi.y_coord})
        # .done (data) =>
        #     SOA.Zone.map.addLayerSpriteToTile(poi.x_coord, poi.y_coord, 5, "assets/sprites/amjad/poi/encampment_established")
        # .fail (data) =>
        #     console.error("PLease no!@!")

        # HAL.trigger "CAMP_ESTABLISHED", data

    Trader::doMove = (data) ->
        HAL.trigger "TRADER_MOVED", data

    Trader::getCapacity = () ->
        return {
            capacity: @capacity
            load: @getLoad()
        }

    Trader::getLoad = () ->
        trload = 0
        for k, amount of @bag
            continue if k is "balance"
            weight = (+XMLStore.tradegoods[k].weight)
            console.debug k, amount, weight, amount*weight
            trload += amount*weight
            console.debug trload
        return trload

    Trader::getGoods = () ->
        out = {}
        for k, v of @bag
            continue if k is "balance" or v is 0
            out[k] = v
        return out
    
    Trader::getBalance = () ->
        return @bag.balance

    Trader::putInBag = (good, amnt, price)  ->
        if not @bag[good]?
            @bag[good] = 0
        @bag[good] += amnt
        @bag["balance"] -= (amnt * price)
        ##console.debug "Bought: good: #{good}, amount: #{amnt}, price: #{price}"

    Trader::takeFromBag = (good, amnt, price) ->
        if not @bag[good]?
            console.error ("You don't have #{good} good")
            return
        @bag[good] -= amnt
        if @bag[good] is 0
            delete @bag[good]
        @bag["balance"] += (amnt * price)

    Trader::emptyBag = () ->
        total = 0
        for k, v of @bag
            continue if k is "balance"
            price = +XMLStore.tradegoods[k].value
            @takeFromBag(k, v, price)
            total+=v*price
        @bag["balance"] += total

    return Trader

##console.debug "You sold all your shit"

# Trader::getUnits = () ->
#     out = {}
#     for k, v of @troops
#         continue if not v?
#         out[k] = v
#     return out

# Trader::addUnit = (k, amnt) ->
#     if @troops[k]?
#         @troops[k] += amnt
#     else
#         @troops[k] = amnt

# Trader::removeUnit = (k, amnt) ->
#     if @troops[k]?
#         @troops[k] -= amnt
#         delete @troops[k] if @troops[k] <= 0

# "use strict"

# define ["soa/Hero"],

# (Hero) ->

#     TRADER_EXPERIENCE_MODIFIER = 1000
#     class Trader extends Hero
#         initListeners: () ->
#             super()
#             ##console.debug "TRADER LOADED"
#             @EXPERIENCE_MODIFIER = TRADER_EXPERIENCE_MODIFIER

#             @on "ARRIVAL", () ->
#                 if @lastAction is "trade"
#                     alert("I'll trade at this poi, eventually")
#                 else if @lastAction is "move"
#                     alert("Holly molly, I just moved")
    
#         Trader::doAction = (action) ->
#             super(action)
#             @moveTo(
#                 SOA.Zone.tile_under.row,
#                 SOA.Zone.tile_under.col
#             )

#         Trader::onAction = (target) ->
#             actBox = SOA.getDialog("ACTION_BOX")
#             actBox.setMoveAsDefault()
#             actBox.MoveBtn.show()
#             if target.type?
#                 if target.type is "city" or target.type is "trade"
#                     actBox.TradeBtn.show()
                
#         # @on "ARRIVED", (destination) =>
#         #     @finishArriving()

#         # if amjad.zonemap?
#         #     ### @todo ###
#         #     amjad.zonemap.on "LAYER_SELECTED", (layer) =>
#         #         if layer.layer is 0 and amjad.selected_hero is @
#         #             @showActionBox(layer)

#         # amjad.on "ZONE_MAP_LOADED", () =>
#         #     @putOnMap()
#         #     @hero.attr("group", "trader")
#         #     @on "ARRIVED", (destination) =>
#         #         @finishArriving(destination.row, destination.col)

#         #     @move("MOVE", @d_coord_x, @d_coord_y)
#         #     amjad.zonemap.on "LAYER_SELECTED", (layer) =>
#         #         # alert "lol"
#         #         if layer.layer is 0 and amjad.selected_hero is @
#         #             @showActionBox(layer)
#         # @on "DONE_LOADING", () ->
#         #     out = {}
#         #     for k, v of @bag
#         #         continue if v is 0
#         #         out[k] = v
#         #     @bag = out

#         # finishArriving: (row, col) ->
#         #     ##console.debug "Trader Arrived at #{row}, #{col}"
#         #     Rest.post "caravan.arrive", 
#         #         usid: amjad.user.user_id,
#         #         cid: @caravan_id
#         #     .then (data) =>
#         #         @d_coord_x = row #or @o_coord_x
#         #         @d_coord_y = col #or @o_coord_y
#         #         @resolveArrival(data, row, col)
#         #         if @hero.quadtree?
#         #             @moveLayerTo(@d_coord_x, @d_coord_y)
#         #         else
#         #             @hero.on "ON_MAP", () =>
#         #                 @moveLayerTo(@d_coord_x, @d_coord_y)
#         #     .fail (data) =>
#         #         console.error data

#         # showActionBox: (layer) ->
#         #     dlg = Hud.getDialog("ACTION_BOX")
#         #     dlg.set "ACT_UPON", layer
#         #     dlg.show(@)

#         #     h = dlg.html()
#         #     h.find("img[class^='ac-']").hide()
            
#         #     if layer.type is "trade" or layer.layer is 0
#         #         #     h.find(".ac-attack").show()
#         #         # else 
#         #         #    
#         #         h.find(".ac-move-to").show()

#         #     #dlg.html().find(".ac-spy,.ac-camp,.ac-attack").hide()

#         #     dlg.html().css("left", amjad.zonemap.mpos[0] - 100)
#         #     dlg.html().css("top", amjad.zonemap.mpos[1] - 100)
#         #     dlg.html().show(300)
#         #     @action_box_hidden = false

#         # hideActionBox: (layer) ->
#         #     dlg = Hud.getDialog("ACTION_BOX")
#         #     dlg.html().hide(300)
#         #     @action_box_hidden = true

#         # resolveArrival: (data, row, col) ->
#         #     ##console.debug "Battle resolved"
#         #     ##console.debug data
#         #     ### @todo ###
#         #     t = amjad.zonemap.getTile(row, col)
#         #     layer = (t.getLayers().filter (l) -> return l.group is "trade")[0]
#         #     if layer? 
#         #         ##console.debug "arrived at poi"
#         #         @moveLayerTo(t.row, t.col)
#         #         dlg = Hud.getDialog("TRADE")
#         #         layer.parked_hero = @
#         #         ##console.debug layer
#         #         dlg.show(layer)

#         # findPath: (from_row, from_col, dest_row, dest_col) ->
#         #     p = amjad.zonemap.pathfinder
#         #     x = amjad.zonemap.getTile(from_row, from_col)
#         #     y = amjad.zonemap.getTile(dest_row, dest_col)
#         #     path = p.find(x, y).reverse()
#         #     return path

#         # move: (type, x, y) ->
#         #     if not x?
#         #         ##console.debug "Trader is stationary"
#         #         return
#         #     a = new Date(@departure).getTime()
#         #     b = new Date(@arrival).getTime()
#         #     secs = ((b - a) / 1000)
#         #     ##console.debug("seconds: #{secs}")
#         #     now = Date.now()
#         #     so_far = (now - a) / 1000
#         #     ##console.debug("so far: #{so_far}")
#         #     ##console.debug @o_coord_x
#         #     ##console.debug @o_coord_y

#         #     path = @findPath(@o_coord_x, @o_coord_y, x, y)
            
#         #     rate = (secs / path.length)
#         #     ##console.debug("rate is: #{rate}")
            
#         #     curr_cell = Math.floor(so_far / rate)
#         #     ##console.debug("currently at: #{curr_cell}")
#         #     path = path.slice(curr_cell)
#         #     rate = (secs / path.length)
#         #     ##console.debug("new rate is: #{rate}")

#         #     if path.length is 0
#         #         #path.push({row: x, col: y})
#         #         # debugger
#         #         # @moveLayerTo(x, y)
#         #         @finishArriving(x, y)
#         #     else
#         #         # debugger
#         #         ##console.debug path
#         #         if curr_cell isnt 0
#         #             @moveLayerTo(path[0].row, path[0].col)
#         #         # ##console.debug path
#         #         @moveOnPath(path, rate*2000)

#             #doAction: (type, @where) ->
#             # if @where.city?
#             #     dest_x = @where.coord_x
#             #     dest_y = @where.coord_y
#             # else
#             # dest_x = @where.tile.row
#             # dest_y = @where.tile.col
#             # Rest.post "caravan.move_caravan", 
#             #     cid: @caravan_id
#             #     x: dest_x
#             #     y: dest_y
#             # .then (data) =>
#             #     ##console.debug "success"
#             #     ##console.debug data
#             #     # hero = @get "HERO"
#             #     @d_coord_x = dest_x
#             #     @d_coord_y = dest_y
#             #     @arrival = data.arrival
#             #     @departure = data.departure
#             #     @move(type, dest_x, dest_y)
#             # .fail (data) =>
#             #     console.error data
#             # amjad.selected_hero = null    
#     return Trader