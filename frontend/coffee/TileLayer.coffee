"use strict"

define ["PIXI", "MapConfig"], 

(PIXI, MapConfig) ->

	class TileLayer extends PIXI.Sprite
		constructor: (@parentTile, meta) ->
			@id 	= meta.id
			@z 		= meta.layer
			texture = PIXI.TextureCache[meta.sprite]
			if not texture?
				throw new Error("Meta #{meta.name}/#{meta.sprite} does not exist in the texture cache")
			super(texture)
			@init()

	TileLayer::init = () ->
		@y_off 		 	= (@getBounds().height - MapConfig.TILE_HEIGHT) * 0.5
		@position.x  	= @parentTile.position.x
		@position.y  	= @parentTile.position.y - @y_off
		@hitArea 	 	= @_bounds.clone()
		@interactive 	= true
		@_pass_me_pixi_ = true
		@scale.x = 1.02
		@scale.y = 1.02

	TileLayer::onClick = (btn) ->
		console.debug "Kliknuo si me na tajlu: #{@parentTile.toString()}"
		console.debug "Ja sam na Z-u: #{@z}"
		#console.debug @
		#console.debug btn
		if btn is 0
			@onLeftClick() if @onLeftClick?
		else 
			@onRightClick() if @onRightClick?

	TileLayer::changeTexture = (tex_name) ->
		texture = PIXI.TextureCache[tex_name]
		if not texture?
			throw new Error("No such texture #{tex_name}!")
		@texture 		= texture
		@y_off          = (@getBounds().height - MapConfig.TILE_HEIGHT) * 0.5
		@position.x     = @parentTile.position.x
		@position.y     = @parentTile.position.y - @y_off

	TileLayer::changeParentTile = (parentTile) ->
		delete @parentTile.layers[@z]
		@parentTile 	= parentTile
		@y_off 		 	= (@getBounds().height - MapConfig.TILE_HEIGHT) * 0.5
		@position.x  	= @parentTile.position.x
		@position.y  	= @parentTile.position.y - @y_off
		@parentTile.layers[@z] = @

	return TileLayer