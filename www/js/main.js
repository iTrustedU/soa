(function() {
  "use strict";
  require.config({
    urlArgs: Math.random(),
    baseUrl: "js",
    paths: {
      "jquery": "../vendor/jquery/jquery.min",
      "jquery-ui": "../vendor/jquery-ui/ui/minified/jquery-ui.min",
      "handlebars": "../vendor/handlebars/handlebars.min",
      "jquery.cookie": "../vendor/jquery-cookie/jquery.cookie",
      "jquery-perfect-scrollbar": "../vendor/jquery-custom-scrollbar/jquery.custom-scrollbar",
      "requireLib": "../vendor/requirejs/require",
      "PIXI": "../vendor/pixi/bin/pixi.dev",
      "tweener": "../vendor/tween.min"
    },
    shim: {
      "jquery": {
        exports: "$"
      },
      "jquery.cookie": {
        exports: "$",
        deps: ["jquery"]
      },
      "jquery-ui": {
        deps: ["jquery"],
        exports: "$"
      },
      "jquery-perfect-scrollbar": {
        deps: ["jquery"],
        exports: "$"
      },
      "PIXI": {
        exports: "PIXI"
      },
      "handlebars": {
        exports: "Handlebars"
      }
    }
  });

  require(["handlebars", "jquery", "tweener", "soa/SOAManager"], function(_, $, tweener, SOAManager) {
    var _this = this;
    return HAL.start().on("HAL_LOADED", function() {
      var SOA;
      SOA = new SOAManager();
      window.SOA = SOA;
      return SOA.start();
    });
  });

}).call(this);
