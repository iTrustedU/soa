"use strict"

define ["soa/Hero"],

(Hero) ->

    COMMANDER_EXPERIENCE_MODIFIER = 1000
    SPEED_PER_TILE = 1.5

    class Commander extends Hero

    Commander::initListeners = () ->
        super()
        @EXPERIENCE_MODIFIER = COMMANDER_EXPERIENCE_MODIFIER

        @on "ARRIVAL", () ->
            @finishArriving()
    
    Commander::doAction = (action) ->
        super(action)
        row = @target.parentTile.row
        col = @target.parentTile.col

        Rest.post "army.move_commander", 
            comid: @commander_id
            x: row
            y: col
            mt: action
        .done (data) =>
            @arrival = data.arrival
            @departure = data.departure
            @duration = data.duration
            @move_type = action
            @moveTo(row, col, data)
        .fail (data) =>
            alert(data)

    # Commander::moveTo = (@d_coord_x, @d_coord_y, data) ->
    # @onMoveTo()

    Commander::onAction = (target) ->
        actBox = SOA.getDialog("ACTION_BOX")
        actBox.setMoveAsDefault()
        actBox.MoveBtn.show()
        console.debug "Target: ", target
        if target.type?
            if target.type is "city"
                actBox.setMoveAsToCity()
                actBox.AttackBtn.show()
            if target.type is "poi" and not @isMyPOI(@target.parentTile.row, @target.parentTile.col)
                actBox.CampBtn.show()

    Commander::isMyPOI = (row, col) ->
        poi = SOA.getPOI(row, col)
        return poi? and poi.user_id is amjad.user.user_id

    Commander::finishArriving = () ->
        Rest.post "army.arrive", 
            usid: amjad.user.user_id, 
            arid: @army_id
        .done (data) =>
            @o_coord_x = @d_coord_x
            @o_coord_y = @d_coord_y
            @resolveArrival(data)
        .fail (data) =>
            alert data
        
        @o_coord_x = @d_coord_x
        @o_coord_y = @d_coord_y

        @layer.stop()

    Commander::resolveArrival = (data) ->
        console.debug "resolving arrival", data
        if @move_type is "attack" 
            @doFight(data)
        else if @move_type is "camp"  
            @doEncampment(data)
        else
            @doMove(data)
        @departure = null

    Commander::doEncampment = (data) ->
        if not @target?
            tile = SOA.Zone.map.getTile(@d_coord_x, @d_coord_y)
            for k, l of tile.layers
                if l.poid?
                    @target = l
                    break
        poi_id = @target.poid
        subtype = @target.subtype
        poi = SOA.pois[subtype][poi_id]
        Rest.post("construction.create_poi", {poid: poi.poi_id, x: poi.x_coord, y: poi.y_coord})
        .done (data) =>
            poi.user_id = amjad.user.user_id
            poicn = SOA.Constructions.loadNewConstruction(data)
            SOA.Constructions.placePOIEncampment(poicn)
            poicn.epilogue()
        .fail (data) =>
            console.error("PLease no!@!")
        HAL.trigger "CAMP_ESTABLISHED", data
        console.debug "I just camped"

    Commander::doFight = (data) ->
        HAL.trigger "FIGHT_FINISHED", @, data
        console.debug "I just fought"

    Commander::doMove = (data) ->
        HAL.trigger "COMMANDER_MOVED", data
        console.debug "I just moved"

    Commander::getUnits = () ->
        out = {}
        for k, v of @troops
            continue if not v?
            out[k] = v
        return out

    Commander::addUnit = (k, amnt) ->
        if @troops[k]?
            @troops[k] += amnt
        else
            @troops[k] = amnt

    Commander::removeUnit = (k, amnt) ->
        if @troops[k]?
            @troops[k] -= amnt
            delete @troops[k] if @troops[k] <= 0

    return Commander