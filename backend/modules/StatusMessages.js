(function() {
  module.exports = {
    ERROR: {
      code: 422,
      msg: "API call error"
    },
    SUCCESS: {
      code: 200,
      msg: "API call success"
    },
    /* Map messages*/

    MAP_EXISTS: {
      code: 422,
      msg: "Map with the same name already exists"
    },
    MAP_SAVE_ERROR: {
      code: 422,
      msg: "Map couldn't be saved"
    },
    TILE_SAVE_ERROR: {
      code: 422,
      msg: "Tile couldn't be saved"
    }
  };

}).call(this);
