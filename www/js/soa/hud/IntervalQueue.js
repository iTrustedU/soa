(function() {
  "use strict";
  define([], function() {
    var IntervalQueue;
    IntervalQueue = (function() {
      function IntervalQueue() {
        this.intervals = {};
        this._ids_ = {};
      }

      IntervalQueue.prototype.createInterval = function(interval) {
        var id,
          _this = this;
        this.intervals[interval] = [];
        id = this._ids_[interval] = setInterval(function(id) {
          var clb, _i, _len, _ref, _results;
          _ref = _this.intervals[interval].slice();
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            clb = _ref[_i];
            _results.push(clb(id));
          }
          return _results;
        }, interval);
        return id;
      };

      IntervalQueue.prototype.clear = function(interval, callb) {
        var calls, id, ind;
        id = this._ids_[interval];
        calls = this.intervals[interval];
        ind = calls.indexOf(callb);
        if (ind !== -1) {
          return calls.splice(ind, 1);
        } else if (calls.length === 0) {
          return clearInterval(id);
        }
      };

      IntervalQueue.prototype.addToQueue = function(callb, interval, name) {
        var queue;
        if (name == null) {
          name = "";
        }
        if (this.intervals[interval] == null) {
          this.createInterval(interval);
        }
        queue = this.intervals[interval];
        if (queue.indexOf(callb) !== -1) {
          throw new Error("Same callback is already on interval " + interval + " queue");
        }
        return queue.push(callb);
      };

      return IntervalQueue;

    })();
    return (window.interval_queue = new IntervalQueue());
  });

}).call(this);
