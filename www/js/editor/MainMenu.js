(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"editing-bar\">\n    <i id=\"mode-place\" class=\"fa fa-edit\"></i>\n    <i id=\"mode-erase\" class=\"fa fa-times\"></i>\n    <i id=\"mode-default\" class=\"fa fa-ban\"></i>\n    <i id=\"map-save\" class=\"fa fa-save\"></i>\n    <i id=\"map-load\" class=\"fa fa-refresh\"></i>\n    <i id=\"map-list\" class=\"fa fa-list-alt\"></i>\n</div>";

  define(["EventDispatcher", "MapConfig"], function(EventDispatcher, MapConfig) {
    var MainMenu;
    MainMenu = (function(_super) {
      __extends(MainMenu, _super);

      function MainMenu() {
        MainMenu.__super__.constructor.call(this);
        this.init();
      }

      return MainMenu;

    })(EventDispatcher);
    MainMenu.prototype.init = function() {
      this.html = $(html);
      return console.debug("ohdsadsa");
    };
    return MainMenu;
  });

}).call(this);
