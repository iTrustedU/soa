"use strict"

html =
"""
<div class="exchange draggable">
    <div class="dialog-name">
        <span id="caravan-name">E X C H A N G E</span>
        <div class="close-button"></div>
    </div>
    <div class="exchange-content">
        <div class="exchange-item-1">
            <div class="exchange-item">
                <img src={{sideA.picture}}>
                <span id="sidea-box" class="goods-amount">{{sideA.minAmount}}</span>
            </div>
        </div>
        <div class="exchange-item-2">
            <div class="exchange-item">
                <img src={{sideB.picture}}>
                <span id="sideb-box" class="goods-amount">{{sideB.amount}}</span>
            </div>
        </div>
        <div class="exchange-bar">
            <div class="exchange-bar-inner-box">
                <div class="inner-slider ui-slider-handle"></div>
            </div>
        </div>
        <span class="exchange-min">Min</span>
        <span class="exchange-max">Max</span>
        <div class="confirm-button">Confirm</div>
    </div>
</div>
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->  

    class ExchangeDialog extends BaseDialog
        constructor: () ->
            super(html)
    
    ExchangeDialog::onPrologue = () ->
        @compileParent(html, {
            sideA: {}
            sideB: {}
        })

    ExchangeDialog::open = (sideA, sideB, done) ->
        @compileHtml(
            sideA: sideA,
            sideB: sideB
        )

        confirmBtn = @html.find(".confirm-button")
        confirmBtn.unbind("click")

        transferedAmount = sideA.minAmount
        maxToGive = sideA.minAmount + sideA.amount

        maxToGive = Math.min(maxToGive, sideB.maxAmount || Number.MAX_VALUE)

        confirmBtn.bind "click", () =>
            done(transferedAmount)
            @hide()

        sideaBox = @html.find("#sidea-box")
        sidebBox = @html.find("#sideb-box")

        sideaBox.text(maxToGive)

        @html.find(".close-button").on "click", () =>
            @hide()

        exchangeBar = @html.find(".exchange-bar")
        exchangeBar.slider(
            min: sideA.minAmount
            max: maxToGive
            orientation: "horizontal"
            slide: (ev, ui) =>
                sideaBox.text(maxToGive - ui.value)
                sidebBox.text(ui.value)
                transferedAmount = +(ui.value)
        )

        @html.show()
        @draggable()

    return ExchangeDialog