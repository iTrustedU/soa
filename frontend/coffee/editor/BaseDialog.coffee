"use strict"

define ["EventDispatcher"],

(EventDispatcher) ->

    class BaseDialog extends EventDispatcher
        constructor: (title, id="") ->
            super()
            template = Handlebars.compile(Handlebars.partials.editor_dialog)
            @html = $(template(id: id, title: title))
            @content = @html.find(".content")
            @toolbox = @html.find(".toolbox")
            @show_btn = @html.find("#toggle-show")
            @show_btn.on "click", () => 
                @trigger "TOGGLE_VISIBILITY", @
                $holder = @html.find(".holder")
                $holder.toggle("slide", direction: "down")
                @show_btn.toggleClass("fa-minus-circle fa-plus-circle")

            @init()
            @initListeners()

    BaseDialog::addToToolbox = (elem) -> 
        @toolbox.append(elem)

    BaseDialog::init = -> 
        return

    BaseDialog::initListeners = ->
        return

    return BaseDialog