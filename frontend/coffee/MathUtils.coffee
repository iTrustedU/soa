"use strict"

define [], 

() ->

    MathUtils = {
        ARRAY_TYPE: if (typeof Float32Array isnt 'undefined') then Float32Array else Array;
        TAU: (Math.PI * 2)
        EPSILON: 0.000001
        DEGREE: Math.PI / 180
        RADIAN: 180 / Math.PI
    }
    
    MathUtils.nextPOT = (num) ->
        throw new Error("Not implemented")

    MathUtils.clamp = (val, min, max) ->
        if val < min
            val = min
        else if val > max
            val = max
        return val

    MathUtils.encode16 = (vals) ->
        return (vals[0] << 16 | vals[1])
    
    MathUtils.decode16 = (val) ->
        return [val >> 16 & 0xFFFF, val & 0xFFFF]

    MathUtils.encode8 = (r, g, b) ->
        return ((r << 8 | g) << 8) | b

    MathUtils.decode8 = (val) ->
        return [val >> 16 & 0xFF, val >> 8 & 0xFF, val & 0xFF]

    MathUtils.encode12 = (a, b) ->
        return (a << 12 | b)

    MathUtils.decode12 = (val) ->
        return [val >> 12 & 0xFFF, val & 0xFFF]

    # pads to 24
    # MathUtils.encode8__ = (vals) ->
    #     out = 0
    #     out = (out | vals[0]) << 8
    #     out = (out | vals[1]) << 8
    #     return out | vals[2]

    # MathUtils.decode8__ = (val) ->
    #     out = []
    #     out.unshift val & 0xFF
    #     val = val >> 8
    #     out.unshift val & 0xFF
    #     out.unshift (val >> 8) & 0xFF
    #     return out

    # # pads to 24 bits
    # MathUtils.encode12  = (vals) ->
    #     out = 0
    #     out = (out | vals[0]) << 0xFFF
    #     return out | (vals[1] << 0xF)

    # MathUtils.decode12 = (val) ->
    #     # out = []
    #     # out.unshift val & 0xF
    #     # val = val >> 8
    #     # out.unshift (val & 0xFF)
    #     return [val >> 12 & 0xFFF, val & 0xFFF]

    # # pads to 32
    # MathUtils.encode8___ = (vals) ->
    #     out = 0
    #     out = (out | vals[0]) << 8
    #     out = (out | vals[1]) << 8
    #     out = (out | vals[2]) << 8
    #     return out | vals[3]

    # MathUtils.decode8___ = (val) ->
    #     out = []
    #     out.unshift val & 0xFF
    #     val = val >> 8
    #     out.unshift val & 0xFF
    #     val = val >> 8
    #     out.unshift val & 0xFF
    #     out.unshift (val >> 8) & 0xFF
    #     return out
        
    # MathUtils.decode8_ = (val) ->
    #     return [(val >> 8) & 0xFF, (val & 0xFF)]

    # # pads to 16
    # MathUtils.encode8_ = (vals) ->
    #     return (vals[0] << 8 | vals[1])

    return MathUtils