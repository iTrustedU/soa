(function() {
  "use strict";
  var $gold_amount_template, $load_text_template, $other_traders_template, $trade_station_inventory_template, $trader_info_template, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"trade-station draggable\">\n    <div class=\"dialog-name\">\n        <span>T R A D E &nbsp; S T A T I O N</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"trader-info\">\n        <div class=\"trade-station-name\">\n            <span>Selected trader</span>\n        </div>\n        <div id=\"trader-info\">{{{trader_info}}}</div>\n        <div class=\"load-unload\">\n            <div class=\"unload\"></div>\n            <div class=\"load\"></div>\n        </div>\n        <div id=\"gold-amount\">{{{gold_amount}}}</div>\n        <div id=\"trade-station-inventory\">{{{trade_station_inventory}}}</div>\n        <div id=\"load-text\">{{{load_text}}}</div>\n    </div>      \n    <div class=\"available-traders\">\n        <span class=\"available-traders-name\">Available Traders</span>\n        <div id=\"available-traders\">{{{available_traders}}}</div>\n    </div>\n    <div class=\"war-building-info\">\n        <img src=\"img/bottom-middle/buildings/trade.png\" class=\"dialog-building-img\" />\n        <div class=\"upgrade-price\">\n            <img src=\"img/top-resources/wood.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/iron.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/gold.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/food.png\">\n            <span>200000</span>\n        </div>\n        <div id=\"upgrade-building\" class=\"upgrade-building\">Upgrade</div>\n        <div id=\"move-building\" class=\"upgrade-building move-button\">Move</div>\n    </div>\n</div>";

  $trader_info_template = "<div class=\"trader\">\n    <img src=\"{{concat_hero_avatar \"trader\" \"l\"}}\" />\n    <span class=\"trader-name\">{{name}}</span><br />\n    <div class=\"slider-bar-middle\">\n        <div class=\"inner-slider-box2\">\n            <div style=\"top:0px!important\" class=\"inner-slider2 ui-slider-handle\"></div>\n        </div>\n    </div>\n</div>\n<div class=\"trade-goods-own\">\n    {{#each getGoods}}\n        <div id=\"{{@key}}\" class=\"poi-good\">\n            <img class=\"trader-good\" id=\"{{@key}}\" src=\"{{concat_resource @key}}\"/>\n            <span class=\"goods-amount5\">{{this}}</span>\n        </div>\n    {{/each}}\n</div>";

  $trade_station_inventory_template = "<div class=\"trade-goods-inventory\">\n    {{#each station_inventory}}\n        <div id=\"{{@key}}\" class=\"goods-box\">\n            <img src={{concat_resource @key}}>\n            <span class=\"goods-amount\">{{amount}}</span>\n        </div>\n    {{/each}}\n</div>";

  $gold_amount_template = "<div class=\"gold-amount\">\n   <input id=\"gold-amount-value\" value=\"{{gold}}\"/>\n   <img src=\"img/top-resources/gold.png\" class=\"top-resources\">\n</div>";

  $load_text_template = "<div class=\"load-text\">\n    <img src=\"img/dialog/trade-station/load.png\" class=\"load-icon\">\n    <span>{{load}}</span>\n    <span>/ {{capacity}}</span>\n</div>";

  $other_traders_template = "{{#each other_traders}}\n<div class=\"available-traders-box\" id=\"{{caravan_id}}\">\n    <img src={{concat_hero_avatar \"trader\" \"m\"}} />\n    <div class=\"available-trader-info\">\n        <span class=\"trader-name-b\">{{name}}</span><br />\n        <span class=\"level-name-b\">Level:</span>\n        <span class=\"level-value-b\">{{level}}</span>\n    </div>\n</div>\n{{/each}}";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var TradeStation;
    TradeStation = (function(_super) {
      __extends(TradeStation, _super);

      function TradeStation() {
        TradeStation.__super__.constructor.call(this, html);
      }

      return TradeStation;

    })(BaseDialog);
    TradeStation.prototype.onPrologue = function() {
      var _me_,
        _this = this;
      this.setTemplates({
        "trader_info_template": $trader_info_template,
        "other_traders_template": $other_traders_template,
        "load_text_template": $load_text_template,
        "gold_amount_template": $gold_amount_template,
        "trade_station_inventory_template": $trade_station_inventory_template
      });
      this.upgradeable();
      this.moveable();
      this.closeable();
      this.draggable();
      this.activeTrader = null;
      this.stationGood = null;
      this.traderGood = null;
      this.on("PRE_SHOW", _me_ = function() {
        _this.html.find(".dialog-building-img").attr("src", Handlebars.helpers.concat_dialog_construction("trade_station"));
        return _this.remove("PRE_SHOW", _me_);
      });
      this.clickTriggers({
        ".available-traders-box/#id": "TRADER_CLICKED",
        ".unload/#": "UNLOAD",
        ".load/#": "LOAD",
        ".goods-box/#id": "STATION_GOOD_CLICKED",
        ".poi-good/#id": "TRADER_GOOD_CLICKED"
      });
      this.on("STATION_GOOD_CLICKED", function(id) {
        var elem;
        _this.stationGood = id;
        _this.clearSelected();
        elem = _this.html.find(".goods-box#" + id);
        return elem.addClass("goods-box-clicked");
      });
      this.on("TRADER_GOOD_CLICKED", function(id) {
        var elem;
        _this.traderGood = id;
        _this.clearSelected();
        elem = _this.html.find(".poi-good#" + id);
        return elem.addClass("goods-box-clicked");
      });
      this.on("LOAD", function() {
        var amnt, caps, dlg, maxCaps;
        if (_this.stationGood == null) {
          alert("You didn't select the resource to unload");
        }
        dlg = SOA.Hud.getDialog("EXCHANGE_DIALOG");
        caps = _this.activeTraderLoad();
        maxCaps = caps.capacity - caps.load;
        amnt = 0;
        if ((_this.activeTrader.getGoods()[_this.stationGood]) != null) {
          amnt = (_this.activeTrader.getGoods()[_this.stationGood]);
        }
        return dlg.open({
          minAmount: 0,
          amount: (amjad.empire.resources.getTradeResources())[_this.stationGood].amount,
          picture: Handlebars.helpers.concat_resource(_this.stationGood)
        }, {
          amount: amnt,
          maxAmount: maxCaps,
          picture: Handlebars.helpers.concat_resource(_this.stationGood)
        }, function(amount) {
          var resources;
          resources = {};
          resources[_this.stationGood] = amount;
          return Rest.post("caravan.load_caravan", {
            cid: _this.activeTrader.caravan_id,
            res: resources,
            mod: "plus"
          }).done(function(data) {
            amjad.empire.resources.take(resources);
            _this.activeTrader.putInBag(_this.stationGood, amount, 0);
            return _this.recompile();
          }).fail(function(data) {
            return alert("Loading failed");
          });
        });
      });
      this.on("UNLOAD", function() {
        var caps, dlg, maxCaps;
        if (_this.traderGood == null) {
          alert("You didn't select the resource to unload");
        }
        dlg = SOA.Hud.getDialog("EXCHANGE_DIALOG");
        caps = _this.activeTraderLoad();
        maxCaps = caps.capacity - caps.load;
        dlg.open({
          minAmount: 0,
          amount: (_this.activeTrader.getGoods()[_this.traderGood]),
          picture: Handlebars.helpers.concat_resource(_this.traderGood)
        }, {
          amount: 0,
          picture: Handlebars.helpers.concat_resource(_this.traderGood),
          maxAmount: Number.MAX_VALUE
        }, function(amount) {
          var resources;
          resources = {};
          resources[_this.traderGood] = amount;
          return Rest.post("caravan.load_caravan", {
            cid: _this.activeTrader.caravan_id,
            res: resources,
            mod: "minus"
          }).done(function(data) {
            amjad.empire.resources.add(resources);
            _this.activeTrader.takeFromBag(_this.traderGood, amount, 0);
            return _this.recompile();
          }).fail(function(data) {
            return alert("Unloading failed");
          });
        });
      });
      this.on("TRADER_CLICKED", function(id) {
        var new_trader;
        new_trader = _this.getTraders()[id];
        if (new_trader !== _this.activeTrader) {
          _this.loadTraderBag("balance", _this.activeTrader.bag.balance);
          _this.activeTrader = new_trader;
          return _this.recompile();
        }
      });
      this.on("SHOW", function() {
        var trids;
        trids = Object.keys(_this.getTraders());
        if (trids.length === 0) {
          alert("You don't own any traders at the moment");
          _this.disable();
          return;
        }
        _this.activeTrader = _this.getTraders()[trids[0]];
        return _this.recompile();
      });
      return this.on("HIDE", function() {
        if (_this.activeTrader != null) {
          _this.loadTraderBag("balance", _this.activeTrader.bag.balance);
        }
      });
    };
    TradeStation.prototype.clearSelected = function() {
      var station, trader;
      trader = this.html.find(".poi-good");
      station = this.html.find(".goods-box");
      trader.removeClass("goods-box-clicked");
      return station.removeClass("goods-box-clicked");
    };
    TradeStation.prototype.stationInventory = function() {
      return {
        "station_inventory": amjad.empire.resources.getTradeResources()
      };
    };
    TradeStation.prototype.recompile = function() {
      var _this = this;
      this.setData("active_trader", this.activeTrader);
      this.setData("other_traders", this.otherTraders(this.activeTrader.id));
      this.setData("trader_load", this.activeTraderLoad);
      this.setData("empire_gold", this.empireGoldAmount);
      this.setData("trade_station_inventory", this.stationInventory);
      this.setData("trader_balance", this.activeTraderBalance);
      this.compile("trader_info_template", "active_trader").then(function(data) {
        _this.refresh("trader-info", data);
        _this.initSlider();
        _this.compile("load_text_template", "trader_load").then(function(data) {
          return _this.refresh("load-text", data);
        });
        return _this.compile("gold_amount_template", "trader_balance").then(function(data) {
          _this.refresh("gold-amount", data);
          return _this.set("GOLD_AMOUNT_ELEMENT", _this.html.find("#gold-amount-value"));
        });
      });
      this.compile("other_traders_template", "other_traders").then(function(data) {
        return _this.refresh("available-traders", data);
      });
      return this.compile("trade_station_inventory_template", "trade_station_inventory").then(function(data) {
        return _this.refresh("trade-station-inventory", data);
      });
    };
    TradeStation.prototype.otherTraders = function(id) {
      var k, out, v, _ref;
      out = {};
      _ref = this.getTraders();
      for (k in _ref) {
        v = _ref[k];
        if (v.id !== id) {
          out[k] = v;
        }
      }
      return {
        "other_traders": out
      };
    };
    TradeStation.prototype.initSlider = function() {
      var _this = this;
      return this.html.find(".slider-bar-middle").slider({
        min: 0,
        max: amjad.empire.resources.balance.amount,
        value: this.activeTrader.bag.balance,
        orientation: "horizontal",
        slide: function(ev, ui) {
          var gold_am;
          _this.activeTrader.bag.balance = ui.value;
          gold_am = _this.html.find("#gold-amount-value");
          return gold_am.val("" + _this.activeTrader.bag.balance);
        }
      });
    };
    TradeStation.prototype.loadTraderBag = function(res, amount) {
      var id, resource;
      id = this.activeTrader.caravan_id;
      resource = {};
      resource[res] = amount;
      return Rest.post("caravan.load_caravan", {
        cid: id,
        res: resource
      }).done(function(data) {}).fail(function(data) {
        return console.error(data);
      });
    };
    TradeStation.prototype.activeTraderLoad = function() {
      return this.activeTrader.getCapacity();
    };
    TradeStation.prototype.activeTraderBalance = function() {
      return {
        gold: this.activeTrader.getBalance()
      };
    };
    TradeStation.prototype.empireGoldAmount = function() {
      return {
        gold: amjad.empire.resources.balance.amount
      };
    };
    TradeStation.prototype.getTraders = function() {
      return amjad.empire.heroes.traders;
    };
    return TradeStation;
  });

}).call(this);
