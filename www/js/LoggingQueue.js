(function() {
  "use strict";
  define([], function() {
    var BAD_STATUS_CODES, LoggingQueue;
    BAD_STATUS_CODES = [422, 400, 500];
    LoggingQueue = (function() {
      function LoggingQueue() {
        this.errors = [];
        this.warnings = [];
        this.debugs = [];
        this.infos = [];
        this.init();
      }

      return LoggingQueue;

    })();
    LoggingQueue.prototype.init = function() {
      if (typeof $ !== "undefined" && $ !== null) {
        return this.setupAjaxHandler();
      }
    };
    LoggingQueue.prototype.setupAjaxHandler = function() {
      var _this = this;
      $(document).ajaxSuccess(function(event, xhr, settings) {
        var msg;
        msg = "{0} call to {1}; status: {2}".f(settings.type, settings.url, xhr.status);
        return _this.info(msg);
      });
      return $(document).ajaxError(function(event, xhr, settings) {
        var msg;
        msg = "{0} to {1}; msg: {2}; status: {3}".f(settings.type, settings.url, xhr.responseText, xhr.status);
        throw new Error(msg);
      });
    };
    LoggingQueue.prototype.debug = function(msg) {
      return this.debugs.push(msg);
    };
    LoggingQueue.prototype.warn = function(msg) {
      return this.warnings.push(msg);
    };
    LoggingQueue.prototype.error = function(msg) {
      return this.errors.push(msg);
    };
    LoggingQueue.prototype.info = function(msg) {
      return this.infos.push(msg);
    };
    return LoggingQueue;
  });

}).call(this);
