"use strict"

html =
"""
<div class="training draggable">
    <div class="dialog-name">
        <span>T R A I N I N G &nbsp; G R O U N D S</span>
        <div class="close-button"></div>
    </div>
    <div id="active-trainer">{{{active_trainer}}}</div>
    <div id="other-trainers">{{{other_trainers}}}</div>
    <div class="training-troop">
        <div id="active-troop">{{{select_troops}}}</div>
        <div id="available-troops">{{{available_troops_info}}}</div>
        <div class="slider-amount">
            <input type="text" class="troop-amount" />
            <span class="min-max">Min</span>
            <div class="troop-slider">
                <div class="inner-slider-box">
                    <div class="inner-slider ui-slider-handle"></div>
                </div> 
            </div>
            <span class="min-max">Max</span>
        </div>
    </div>
    <div class="train-button-box">Train</div>
    <div class="war-building-info">
        <img src="assets/sprites/amjad/dialogs/constructions/civil//training_grounds.png" class="dialog-building-img">
        <div class="upgrade-price">
            <img src="img/top-resources/wood.png">
            <span>200000</span>
            <img src="img/top-resources/iron.png">
            <span>200000</span>
            <img src="img/top-resources/gold.png">
            <span>200000</span>
            <img src="img/top-resources/food.png">
            <span>200000</span>
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
        <div id="move-building" class="upgrade-building move-button">Move</div>
    </div>
</div>
"""

$active_trainer_template = 
"""
<div class="trainer-statistic">
    <img src={{concat_hero_avatar "trainer" "m"}} id={{id}} class="trainer-avatar">
    <div class="trainers-name-level">
        <span class="traniers-name">{{name}}</span>
        <img src="img/dialog/training/level.png" class="level-icon">
        <span class="traniers-level-value">{{level}}</span>
        <div class="tranier-stat">  
            <div class="tranier-stat-name"> 
                <span>Brawn:</span>
                <span>Reflexes:</span>
                <span>Endurance:</span>
                <span>Experience:</span>
            </div>
            <div class="tranier-stat-value">    
                <span>{{brawn}}</span>
                <span>{{reflexes}}</span>
                <span>{{endurance}}</span>
                <span>{{experience}}</span>
            </div>
        </div>
    </div>
</div>    
"""

$other_trainers_template = 
"""
{{#each other_trainers}}
<img src={{concat_hero_avatar "trainer" "s"}} id={{id}} class="training-reserve disable" />
{{/each}}
"""

$available_troops_template = 
"""
<div class="select-troops">
    <div class="select-troops-box">
        {{#each troops}}
            <img id={{@key}} src={{concat_unit_avatar @key "44x44"}} class="list-troop"/>
        {{/each}}
    </div>
    <div id="selected-troop-stat">{{{selected_troop_stat}}}</div>
</div>
"""

$selected_troop_stat_template =
"""
<div class="available-unit-info">
    <div class="unit-info-name">
        <div class="unit-info-box">
            <img src="img/dialog/training/health.png" class="training-stats-icon">
            <span>{{stats.health}}</span>
        </div>
        <div class="unit-info-box">
            <img src="img/dialog/training/attack.png" class="training-stats-icon">
            <span>{{stats.attack}}</span>
        </div>
        <div class="unit-info-box">
            <img src="img/dialog/training/defense.png" class="training-stats-icon">
            <span>{{stats.defense}}</span>
        </div>
        <div class="unit-info-box">
            <img src="img/dialog/training/upkeep.png" class="training-stats-icon">
            <span>{{stats.upkeep}}</span>
        </div>
    </div>
    <div class="unit-info-name2">
        <div class="unit-info-box">
            <img src="img/dialog/training/pillage.png" class="training-stats-icon">
            <span>{{stats.pillage}}</span>
        </div>
        <div class="unit-info-box">
            <img src="img/dialog/training/raze.png" class="training-stats-icon">
            <span>{{stats.raze}}</span>
        </div>
        <div class="unit-info-box">
            <img src="img/dialog/training/steal.png" class="training-stats-icon">
            <span>{{stats.steal}}</span>
        </div>
    </div>
</div>
"""

$selected_troop_template = 
"""
<div class="selected-troop">
    <img src={{concat_unit_avatar type "portrait"}} class="selected-troop-img" />
    <div class="troop-price">
        <img src="img/top-resources/gold.png" class="top-resources">
        <span id="selected-troop-balance" class="value">{{price.balance}}</span>
        <img src="img/top-resources/food.png" class="top-resources">
        <span id="selected-troop-food" class="value">{{price.food}}</span>
        <img src="img/top-resources/iron.png" class="top-resources">
        <span id="selected-troop-iron" class="value">{{price.iron}}</span>
    </div>
</div>
"""

define ["soa/hud/dialogs/BaseDialog"],
(BaseDialog) ->

    class TrainingGroundsDialog extends BaseDialog
        constructor: () ->
            super(html)

    TrainingGroundsDialog::onPrologue = () ->
        @setTemplates
            "active_trainer_template": $active_trainer_template
            "selected_troop_template": $selected_troop_template
            "available_troops_template": $available_troops_template
            "other_trainers_template": $other_trainers_template
            "selected_troop_stat_template": $selected_troop_stat_template
        
        @upgradeable()
        @moveable()

        @closeable()
        @draggable()
        
        @on "PRE_SHOW", _me_ = () ->
            @html
            .find(".dialog-building-img")
            .attr("src", Handlebars.helpers.concat_dialog_construction("training_grounds"))
            @remove "PRE_SHOW", _me_

        @clickTriggers
            ".trainer-avatar/#id": "SELECTED_TRAINER"
            ".training-reserve/#id": "SELECTED_TRAINER"
            ".list-troop/#id": "SELECTED_TROOP"
            ".train-button-box/#": "TRAIN_CLICKED"

        @on "SELECTED_TRAINER", (id) ->
            @set "ACTIVE_TRAINER", @ctx.getTrainers()[id]
            @set "OTHER_TRAINERS", @otherTrainers(id)
            @recompile()

        @on "SELECTED_TROOP", (id) ->
            @set "ACTIVE_TROOP", @ctx.units[id]
            @recompile()
                
        @on "TRAIN_CLICKED", () ->
            trainer = @get "ACTIVE_TRAINER"
            troop   = @get "SELECTED_TROOP"
            amount  = @get "AMOUNT"

            xhr = Rest.post("construction.start_production", 
                cid: @ctx.construction_id,
                ty: troop
                amt: amount
            ).done (data) =>
                res = 
                    "balance": @get("ACTIVE_BALANCE").text()
                    "food": @get("ACTIVE_FOOD").text()
                    "iron": @get("ACTIVE_IRON").text()

                data["type"] = troop
                data["amount"] = amount
                data["construction_id"] = @ctx.construction_id
                #data["time"] = XMLStore.units[troop].time
                ##console.debug "Ordering production of #{amount} of #{troop} units"
                ##console.debug data
                @ctx.beginUnitProduction(data, res)
                @hide()
            .fail (data) =>
                console.error "Production ordering of #{amount} of #{troop} units failed"
                console.error data
                
        @on "SHOW", () ->      
            trainers = @ctx.getTrainers()
            trids = Object.keys(trainers)
            if trids.length is 0
                alert("You don't own any trainers at the moment")
                @disable()
                return
            id = trids[0]
            uid = Object.keys(@ctx.units)[0]
            @set "SELECTED_TROOP", uid
            @set "AVAILABLE_TROOPS", "troops": @ctx.units
            @set "ACTIVE_TROOP", @ctx.units[uid]
            @set "ACTIVE_TRAINER", trainers[id]
            @set "OTHER_TRAINERS", @otherTrainers(id)
            @recompile()

    TrainingGroundsDialog::initSliderAndInput = () ->
        @set "ACTIVE_MAX_AMOUNT", @findMaxToTrain(@get("ACTIVE_TROOP").price)
        @set "AMOUNT", 1
        @slider = @html.find(".troop-slider")
        .slider(
            min: 1
            max: @get("ACTIVE_MAX_AMOUNT") - 1
            orientation: "horizontal"
            slide: (ev, ui) =>
                @validateInput(+ui.value)
                @get("INPUT_AMOUNT").val(ui.value)
        )
        @slider.slider('value', 1)
        @set "INPUT_AMOUNT", @html.find(".troop-amount")    
        @get("INPUT_AMOUNT").val(1)
        @get("INPUT_AMOUNT").on "keyup", (ev) =>
            amount = @get("INPUT_AMOUNT").val()
            if not @validateInput(amount)
                @get("INPUT_AMOUNT").val(@get("ACTIVE_MAX_AMOUNT"))

    TrainingGroundsDialog::recompile = () ->
        @setData "active_trainer", @get "ACTIVE_TRAINER" #@ctx.trainers
        @setData "troop_info", (price: (@get "ACTIVE_TROOP").price, type: @get ("SELECTED_TROOP"))
        @setData "available_troops", @get "AVAILABLE_TROOPS"
        @setData "other_trainers", @get "OTHER_TRAINERS"
        @setData "active_troop", @get "ACTIVE_TROOP"

        @compile("selected_troop_template", "troop_info").then (data) =>
            @refresh("active-troop", data)

        @compile("available_troops_template", "available_troops").then (data) =>
            @refresh("available-troops", data)

        @compile("other_trainers_template", "other_trainers").then (data) =>
            @refresh("other-trainers", data)

        @compile("selected_troop_stat_template", "active_troop").then (data) =>
            @refresh("selected-troop-stat", data)
            @set "ACTIVE_BALANCE", @html.find("#selected-troop-balance")
            @set "ACTIVE_FOOD", @html.find("#selected-troop-food")
            @set "ACTIVE_IRON", @html.find("#selected-troop-iron")
            @set "ACTIVE_MAX_AMOUNT", @findMaxToTrain(@get("ACTIVE_TROOP").price)
            @initSliderAndInput()

        @compile("active_trainer_template", "active_trainer").then (data) =>
            @refresh("active-trainer", data)   
            @initSliderAndInput()

    TrainingGroundsDialog::otherTrainers = (id) ->
        out = {}
        for k, v of @ctx.getTrainers()
            if k isnt id
                out[k] = v
        return "other_trainers": out

    TrainingGroundsDialog::calculateTotalMax = (price, amount) ->
        mul = {}
        for key, val of price
            continue if key is "time"
            mul[key] = val*amount
        return mul

    TrainingGroundsDialog::findMaxToTrain = (price) ->
        max = Number.MAX_VALUE
        amt = 0
        for k, v of price
            continue if k is "time"
            my_res = amjad.empire.resources[k].amount
            max_of_this = my_res/(+v)
            if max_of_this < max
                amt = max_of_this
                max = max_of_this
        return Math.round(amt)

    TrainingGroundsDialog::validateInput = (amount) ->
        max = @calculateTotalMax(
            @get("ACTIVE_TROOP").price,
            amount
        )
        res_check = amjad.empire.resources.hasEnough(max)
        if res_check.length isnt 0
            alert("You're missing some resources")
            # Hud.trigger "SHOW_TILE_NOTIFICATION",
            #   x: Hal.evm.pos[0]
            #   y: Hal.evm.pos[1]
            #   text: res_check.join("\n\n")
            #   velocity: 1
            #   speed: 3000
            return
    
        @get("ACTIVE_BALANCE").text(max['balance'])
        @get("ACTIVE_FOOD").text(max['food'])
        @get("ACTIVE_IRON").text(max['iron'])

        @set "AMOUNT", amount
        return amount <= @get("ACTIVE_MAX_AMOUNT")

    return TrainingGroundsDialog