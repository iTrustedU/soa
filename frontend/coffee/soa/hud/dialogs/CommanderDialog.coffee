"use strict"

html =
"""
<div class="commander draggable">
    <div class="dialog-name">
        <span>C O M M A N D E R</span>
        <div class="close-button"></div>
    </div>
    <div class="commander-info">
        <div class="commander-statistics"> 
            <span class="commander-level-name"><img src="img/dialog/training/level.png" /></span>
            <span class="commander-level-value">{{level}}</span>
            <span class="commander-expirience-name color-text">Experience:</span>
            <span class="commander-current-expirience">{{experience}} / {{nextLevelExperience}}</span>
            <span class="commander-level-name color-text">Readiness:</span>
            <span class="commander-level-value">{{readiness}}</span>
            <span class="commander-level-name color-text">Speed:</span>
            <span class="commander-level-value">{{speed}}</span>
            <span class="commander-level-name color-text">Leadership:</span>
            <span class="commander-level-value">{{leadership}}</span>
        </div>
    </div>
    <div id="commander-name" class="commander-name">
        <span>{{name}}</span>
    </div>
    <div class="commander-inventory">
    </div>
    <div class="commander-equipment">
        <img src="img/dialog/commander/equipslika.png" class="equip-img" />
        <img src="img/dialog/commander/equipment-holder.png" class="equipment-holder" />
        <div class="equipment-box-b head"></div>
        <div class="equipment-box-b back"></div>
        <div class="equipment-box-b torso"></div>
        <div class="equipment-box-b r-hand"></div>
        <div class="equipment-box-b l-hand"></div>
        <div class="equipment-box-b legs"></div>
        <div class="equipment-box-b boots"></div>
        <div class="equipment-box-s neck"></div>
        <div class="equipment-box-s l-ring"></div>
        <div class="equipment-box-s r-ring"></div>
    </div>
    <div class="commander-exp-bar-box">
        <div class="commander-bar">
            <div class="commander-bar-full" {{calc_experience_percentage this}}></div>
        </div>
    </div>
    <div id="commander-troops" class="commander-troops">
        {{{commander_troops}}}
    </div>
    <div class="attach-agent">
    </div>
</div>
"""

$commander_name =
"""
<span>{{name}}</span>
"""

$commander_inventory = 
"""
<img src="{{concat_sprite sprite}}" id="{{name}}" type="{{type}}">
"""

$commander_troops = 
"""
{{#each getUnits}}
    <div class="unit-box3">
        <img id="{{key}}" src={{concat_unit_avatar @key "borderless"}}>
        <span class="units-amount2">{{this}}</span>
    </div>
{{/each}}
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->

    class CommanderDialog extends BaseDialog
        constructor: () -> 
            # CommanderDialog = Hud.dm.compileAndAddDialog("COMMANDER", html)
            super(html)

    CommanderDialog::onPrologue = () ->
        @setTemplates
            "commander_troops": $commander_troops
            "commander_window": html

        @on "SHOW", () ->
            @setData "commander", @ctx
            @compileHtml(@ctx)
            @compile("commander_troops", "commander").then (data) =>
                @refresh("commander-troops", data)

            @closeable()
            @draggable()

    return CommanderDialog

        # AvailableEquipment =
        #     "Saber":
        #         sprite: "equipment/saber"
        #         readiness: 3
        #         type: "r-hand"
            
        #     "Armor":
        #         sprite: "equipment/armor"
        #         leadership: 3
        #         type: "torso"
            
        #     "Shield":
        #         sprite: "equipment/shield"
        #         readiness: 2
        #         leadership: 1
        #         type: "l-hand"

        #     "Silk":
        #         sprite: "equipment/silk"
        #         readiness: 2
        #         leadership: 1
        #         type: "back"    

        # $CommanderDialog = $(html)

        # Hal.trigger "DOM_ADD", (domlayer) ->
        #     $(domlayer).append($CommanderDialog)

        # Hal.on "OPEN_COMMANDER_DIALOG", (commander) ->
        #     commander =
        #         name: "Nikola Soro"
        #         experience: 600
        #         next_level_in: 900
        #         readiness: 9
        #         speed: 12
        #         leadership: 15
        #         units: {
        #             "NikolaA Swordsman":
        #                 sprite: "units-icons/swordsman"
        #             "NikolaB Swordsman":
        #                 sprite: "units-icons/skirmisher"
        #             "NikolaC Swordsman":
        #                 sprite: "units-icons/heavyswordsman"
        #             "NikolaD Swordsman":
        #                 sprite: "units-icons/swordsman"
        #         }

        #     $CommanderDialog.data("commander", commander)

        #     #popunjavanje imena
        #     $name = $CommanderDialog.find(".commander-name")
        #     $name.empty()
        #     template = Handlebars.compile($commander_name);
        #     html = template(commander)
        #     $name.append(html)

        #     #popunjavanje stata
        #     $stat = $CommanderDialog.find(".commander-statistics")
        #     $stat.empty()

        #     template = Handlebars.compile($commander_stat);

        #     html = template(commander)
        #     $stat.append(html)


        #     #popunjavanje inventara
        #     $inventory = $CommanderDialog.find(".commander-inventory")
        #     $inventory.empty()

        #     template = Handlebars.compile($commander_inventory)

        #     for k,v of AvailableEquipment
        #         $html = $(template(
        #             name: k 
        #             sprite: v.sprite
        #             type: v.type
        #         ))
        #         $html.draggable {revert: "invalid"}
        #         $inventory.append($html)
       
        #     $commander_troops = $CommanderDialog.find(".commander-troops")
        #     $commander_troops.empty()
        #     template = Handlebars.compile($commander_units)

        #     for k, v of commander.units
                
        #         html = template(v)
        #         $commander_troops.append(html)

        #     $CommanderDialog.find(".l-hand").droppable {
        #         activeClass: "l-hand-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "l-hand"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.find(".torso").droppable {
        #         activeClass: "torso-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "torso"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             # commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             # amjad.empire.AvailableEquipment[name] = null
        #             # delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.find(".r-hand").droppable {
        #         activeClass: "r-hand-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "r-hand"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.find(".head").droppable {
        #         activeClass: "head-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "head"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     #hahah
        #     $CommanderDialog.find(".back").droppable {
        #         activeClass: "back-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "back"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.find(".legs").droppable {
        #         activeClass: "legs-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "legs"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #     }

        #     $CommanderDialog.find(".boots").droppable {
        #         activeClass: "boots-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "boots"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.find(".neck").droppable {
        #         activeClass: "neck-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "neck"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.find(".l-ring").droppable {
        #         activeClass: "l-ring-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "l-ring"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.find(".r-ring").droppable {
        #         activeClass: "r-ring-active"
        #         accept: (draggable) ->
        #             return draggable.attr("type") is "r-ring"

        #         drop: (ev, ui) ->
                    
        #             name = ui.draggable.attr("id")
        #             ui.draggable.attr("style", "")
        #             $(@).append(ui.draggable)
        #             commander = $CommanderDialog.data("commander")
        #             #commander.addInventory(name, amjad.empire.AvailableEquipment[name])
        #             #amjad.empire.AvailableEquipment[name] = null
        #             #delete amjad.empire.AvailableEquipment[name]
        #     }

        #     $CommanderDialog.fadeIn(200)