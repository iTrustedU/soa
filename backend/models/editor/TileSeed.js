(function() {
  var RANDOM_SEED, SEED_CLAMPER, TileSeed, mongoose, randomizer, schema;

  mongoose = require("mongoose");

  randomizer = require("../../utils/NaiveRandomizer");

  RANDOM_SEED = 0xABCDEF;

  SEED_CLAMPER = 0x1000 - 1;

  schema = mongoose.Schema({
    last_state: {
      type: Number,
      required: true
    },
    starting_state: {
      type: Number,
      required: true
    }
  });

  schema.methods.randomize = function(token) {
    var next_seed;
    next_seed = randomizer(this.last_state) * SEED_CLAMPER;
    this.last_state = next_seed;
    this.save();
    return Math.floor(next_seed);
  };

  schema.methods.reset = function() {
    this.last_state = this.starting_state = RANDOM_SEED;
    return this.save();
  };

  TileSeed = mongoose.model("TileSeed", schema);

  module.exports = function(app) {
    var saveOnce;
    TileSeed.find().exec(function(err, results) {
      var randseeder;
      randseeder = null;
      if (results.length === 0) {
        randseeder = saveOnce();
      } else {
        randseeder = results[0];
      }
      if (randseeder == null) {
        throw Error("Seeder is null!!!");
      }
      return app.locals.tileSeeder = randseeder;
    });
    saveOnce = function() {
      var seed;
      seed = new TileSeed();
      seed.last_state = RANDOM_SEED;
      seed.starting_state = RANDOM_SEED;
      seed.save(function(err) {
        if (err != null) {
          throw new Error("Error initializing seeder");
        }
      });
      return seed;
    };
    return app.locals.tileSeeder;
  };

}).call(this);
