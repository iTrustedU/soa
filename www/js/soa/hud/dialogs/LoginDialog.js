(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"index-body\">\n<div class=\"login-content\">\n    <div class=\"login-box\">\n    <div class=\"dialog-name\">\n        <span>Login</span>\n    </div>\n    <span class=\"register-name\">Username:</span>\n    <input class=\"register-input\" type=\"text\" id=\"username\"/>\n    <span class=\"register-name\">Password:</span>\n    <input class=\"register-input\" id=\"password\" type=\"password\" />\n    <input class=\"check-button\" name=\"choice\" type=\"checkbox\" />\n    <span class=\"remember-me\">Keep me logged in</span>\n    <a href=\"#\" class=\"forgot-user-id\">Forgot your User ID or Password</a>\n    <div id=\"loginbtn\" class=\"register-button\">LOGIN</div>\n    <br />\n    <span class=\"register-name\">No, I'm new here!</span>\n    <div id=\"regbtn\" class=\"register-button\">REGISTER</div>\n</div>\n</div>\n</div>";

  define(["soa/hud/dialogs/BaseDialog", "soa/Rest"], function(BaseDialog, Rest) {
    var LoginDialog;
    LoginDialog = (function(_super) {
      __extends(LoginDialog, _super);

      function LoginDialog() {
        LoginDialog.__super__.constructor.call(this, html);
      }

      return LoginDialog;

    })(BaseDialog);
    LoginDialog.prototype.init = function() {
      this.LoginButton = this.html.find("#loginbtn");
      return this.SignupButton = this.html.find("#regbtn");
    };
    LoginDialog.prototype.onPrologue = function() {
      var _this = this;
      this.SignupButton.click(function() {
        SOA.Hud.showDialog("REGISTER");
        return _this.hide();
      });
      return this.LoginButton.click(function() {
        var pass, usn;
        usn = _this.html.find("#username").val();
        pass = _this.html.find("#password").val();
        return SOA.Session.tryLogin(usn, pass).done(function(data) {
          return SOA.logMeIn();
        }).fail(function(err) {
          return console.debug(err);
        });
      });
    };
    return LoginDialog;
  });

}).call(this);
