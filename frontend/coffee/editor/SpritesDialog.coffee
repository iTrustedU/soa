"use strict"

back_btn_html = 
"""
<i id="back-btn" class="fa fa-arrow-circle-left"></i>
"""

folder_html =
"""
<li id={{id}} class="selectable-box" title={{tooltip}}>
    <i class="fa fa-folder-open"></i>
    <span class="selectable-title">
        {{title}}
    </span>
</li>
"""

sprite_html =
"""
<li id={{id}} class="selectable-box draggable-sprite" title={{tooltip}}>
    <img src={{img_url}}>
    <span class="selectable-title">
        {{title}}
    </span>
</li>
"""

define ["editor/BaseDialog"], 

(BaseDialog) ->

    SPRITE_FOLDER = "assets/sprites/soa/"
    SHEETS_FOLDER = "assets/sprites/sheets/"
    FOLDER_TOOLTIP = "{0},sprites:{1},folders:{2}"
    SPRITE_TOOLTIP = "{0},{1}x{2}"
    SPRITE_TEST = (url) -> return /.+\.png$/.test(url)

    class SpritesDialog extends BaseDialog
        constructor: (@sprites = {}) ->
            super("Sprites", "sprites-dialog")

    SpritesDialog::init = () ->
        @addToToolbox(back_btn_html)
        @initButtons()
        @createFolderStructures()

    SpritesDialog::initListeners = () ->
        @on "CONTENT_REFRESH", (url) =>
            @back_url = url
            # Is root url?
            if @back_url is ""
                @back_btn.hide()
            else
                @back_btn.show()

        @on "MOVE_BACK", () =>
            folder = @getFolderByPath(@back_url)
            # Fixes quirks on some weird paths
            # Don't touch this
            lindex = @back_url.lastIndexOf("/")
            lindex = 1 if lindex is 0 and @back_url.length > 1
            @back_url = @back_url.substring(0, lindex)
            lindex = @back_url.lastIndexOf("/")
            lindex = 1 if lindex is 0
            @showFolderContents(folder, @back_url.substring(0, lindex))

    SpritesDialog::initButtons= () ->
        @back_btn = @html.find("#back-btn")
        @back_btn.hide()
        @back_btn.on "click", () =>
            @trigger "MOVE_BACK"

    SpritesDialog::createFolderStructures = () ->
        @root_folder = {
            url: "/"
            folders: {}
            sprites: {}
        }
        @back_url = ""

        for url, sprite of @sprites
            # Is it a sprite?
            continue if not (SPRITE_TEST(url) and url.startsWith(SPRITE_FOLDER))
            folders = url.replace(SPRITE_FOLDER, "").split("/")
            # Pop the last element which is a filename
            spr_name = folders.pop()
            direct_folder = @findOrCreateFolder(folders)
            direct_folder.sprites[spr_name] = sprite

        @showFolderContents(@root_folder, @back_url)

    SpritesDialog::showFolderContents = (root_folder, back_url) ->
        @trigger "CONTENT_REFRESH", @, back_url
        @content.empty()
        @showSubfolders(root_folder)
        @showSprites(root_folder)

    SpritesDialog::getFolderByPath = (path) ->
        # If it's not a root folder
        if path.indexOf("/") is 0 and path.length isnt 1
            path = path.substring(1)
        folders = path.split("/")
        return @findOrCreateFolder(folders)

    SpritesDialog::showSprites = (root_folder) ->
        sprite_tpl = Handlebars.compile(sprite_html)
        for spr_name, sprite of root_folder.sprites
            tooltip = SPRITE_TOOLTIP.f(
                spr_name
                sprite.width
                sprite.height
            )
            $sprite_box = $(sprite_tpl(
                id: spr_name
                title: spr_name
                tooltip: tooltip
                img_url: sprite.baseTexture.imageUrl
            ))
            $sprite_box.draggable(
                revert: "invalid"
                helper: "clone"
                zIndex: 10000
            )
            @content.append($sprite_box)

    SpritesDialog::showSubfolders = (root_folder) ->
        folder_tpl = Handlebars.compile(folder_html)
        for folder_name, folder of root_folder.folders
            tooltip = FOLDER_TOOLTIP.f(
                folder_name
                Object.keys(folder.sprites).length
                Object.keys(folder.folders).length
            )
            $folder_box = $(folder_tpl(
                id: folder_name
                title: folder_name
                tooltip: tooltip
            ))
            do (folder) =>
                $folder_box.unbind "click"
                $folder_box.on "click", () =>
                    @showFolderContents(folder, root_folder.url)
            @content.append($folder_box)

    SpritesDialog::findOrCreateFolder = (folders_path) ->
        topmost = @root_folder
        path = "/"
        for folder in folders_path
            break if folder is ""
            path = "{0}{1}/".f(path, folder)
            if topmost.folders[folder]? # if folder exists, go deeper
                topmost = topmost.folders[folder]
            else # or create a new one and exit
                topmost = topmost.folders[folder] = {
                    url: path
                    folders: {}
                    sprites: {}
                }
                break
        return topmost

    return SpritesDialog