"use strict"

html = 
"""
<div class="top">
    <div class="top-ornament"></div>
    <div class="top-menu-bar">
        <div class="top-menu-button options-system"></div>
        <div class="top-menu-button messages"></div>
        <div class="top-menu-button renking-ladders"></div>
        <div class="top-menu-button news"></div>
        <img src="img/top-resources/gold.png" class="top-resources">
        <span id="resource-info-balance" class="top-resources-input"></span>
        <img src="img/top-resources/water.png" class="top-resources">
        <span id="resource-info-water" class="top-resources-input"></span>
        <span id="resource-info-food" class="top-resources-input-right-first"></span>
        <img src="img/top-resources/food.png" class="top-resources-right">
        <span id="resource-info-iron" class="top-resources-input-right"></span>
        <img src="img/top-resources/iron.png" class="top-resources-right">
        <span id="resource-info-timber" class="top-resources-input-right"></span>
        <img src="img/top-resources/wood.png" class="top-resources-right">
        <!-- <div class="full-screen-button"></div> -->
        <div class="top-menu-name">
            <span id="name"></span>
        </div>
        <div class="top-menu-gradient-bar">
            <img src="img/top-interface/effects-daynight.png" class="daynight">
            <img src="img/top-interface/effects-frame.png" class="effects">
            <img src="img/top-interface/effects-frame.png" class="effects">
            <img src="img/top-interface/effects-frame.png" class="effects">
        </div>
    </div>
    <div class="top-menu-center-box">
        <div class="top-menu-center-l top-menu-center">
            <span id="title-a">Ruling Title</span>
        </div>
        <div class="top-menu-center-r top-menu-center">
            <span id="title-b">Achieved Title</span>
        </div>
    </div>
    <div class="top-menu-center-down">
        <span>Player Tribe</span>
    </div>
    <div class="advisor">
        <div class="advisor-ornament"></div>
        <div class="advisor-name">
            <span id="coords"><b>X:</b> 4518<b> Y:</b> 5512</span>
        </div>
        <div class="advisor-img"></div>
    </div>
</div>
"""

define ["soa/hud/dialogs/BaseDialog"], 

(BaseDialog) ->

    class TopMenu extends BaseDialog
        constructor: () ->
            super(html)

    # TopMenu::init = () ->
    #     @ResourcesInfo = @html.find("span[id^='resource-info']")

    # TopMenu::onPrologue = (hud) ->
    #     @hide()

    TopMenu::onEpilogue = (hud) ->
        @html.find("#name").text(amjad.user.getFullName())
        @html.find("#coords").html(
            "<b>X:</b> #{amjad.empire.settlement.x}<b> Y:</b> #{amjad.empire.settlement.y}"    
        )
        @on "SHOW", () ->
            @updateResources()

    TopMenu::updateResources = () ->
        for reskey, res of amjad.empire.resources
            obj = @html.find("span[id$='#{reskey}']")
            if reskey.length > 1 and obj.length
                ##console.debug "Updating resource #{reskey}"
                obj.text(res.amount)

    # TopMenu::show = () ->
    #     super({})
    #     

    return TopMenu