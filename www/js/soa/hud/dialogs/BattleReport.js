(function() {
  "use strict";
  var $all_reports_table, $battle_status, $battle_status_info, $loot_resources, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"battle-report draggable\">\n    <div class=\"dialog-name\">\n        <span id=\"caravan-name\">B A T T L E  &nbsp; R E P O R T</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"report-tabs\">\n        <div class=\"chat-tab\" id=\"commander-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Black List</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"agent-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Taunts</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"caravan-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Reports</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"trainer-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Mail</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n    </div>\n    <div id=\"tab\">\n        <div class=\"battle-report-content\">\n            <div id=\"battle-status\">{{{battle_status}}}</div>\n            <div class=\"battle-animation\">\n\n            </div>\n            <div id=\"battle-status-info\">{{{battle_status_info}}} </div>\n            <div id=\"loot-resources\"> {{{loot_resources}}} </div>\n            <div class=\"battle-report-button\">Back to Reports</div>\n        </div>\n        <div class=\"all-reports-content\">\n            <div class=\"all-reports-delete\">Delete Message</div>\n          \n            <div id=\"all-reports-table\">{{{all_reports_table}}}</div>\n            <div class=\"navigation\">\n                <img src=\"img/dialog/battle-report/backward-end.png\" class=\"navigation-icons\">\n                <img src=\"img/dialog/battle-report/backward.png\" class=\"navigation-icons\">\n                <span>1</span>\n                <img src=\"img/dialog/battle-report/forward.png\" class=\"navigation-icons\">\n                <img src=\"img/dialog/battle-report/forward-end.png\" class=\"navigation-icons\">\n            </div>\n        </div>\n       \n        <div class=\"messages-table\">\n            <table>\n                <tr>\n                    <th class=\"t230\"></th>\n                    <th class=\"t170\">From</th>\n                    <th class=\"t170\">Subject</th>\n                    <th class=\"t105\">Date</th>\n                    <th class=\"t105\">Time</th>\n                </tr>\n                <tr>\n                    <td class=\"t230\"><input type=\"checkbox\" /></td>\n                    <td class=\"t170\">Nikola Soro</td>\n                    <td class=\"t170\">Heloooo</td>\n                    <td class=\"t105\">12. 23. 2014.</td>\n                    <td class=\"t105\">15:54:21</td>\n                </tr>\n            </table>\n        </div>\n        <div class=\"navigation\">\n            <img src=\"img/dialog/battle-report/backward-end.png\" class=\"navigation-icons\">\n            <img src=\"img/dialog/battle-report/backward.png\" class=\"navigation-icons\">\n            <span>1</span>\n            <img src=\"img/dialog/battle-report/forward.png\" class=\"navigation-icons\">\n            <img src=\"img/dialog/battle-report/forward-end.png\" class=\"navigation-icons\">\n        </div>\n    </div>\n    <div class=\"black-list-content\">\n        <div class=\"all-reports-delete\">Remove</div>\n        \n        <div class=\"black-list-table\">\n            <table>\n                <tr>\n                    <th class=\"t230\"></th>\n                    <th class=\"t200\">Player Name</th>\n                    <th class=\"t200\">Alliance</th>\n                    <th class=\"t150\">Date</th>\n                </tr>\n                <tr>\n                    <td class=\"t230\"><input type=\"checkbox\" /></td>\n                    <td class=\"t200\">Nikola Soro</td>\n                    <td class=\"t200\">Hell Hol</td>\n                    <td class=\"t150\">12. 23. 2014.</td>\n                </tr>\n            </table>\n        </div>\n        <div class=\"navigation\">\n            <img src=\"img/dialog/battle-report/backward-end.png\" class=\"navigation-icons\">\n            <img src=\"img/dialog/battle-report/backward.png\" class=\"navigation-icons\">\n            <span>1</span>\n            <img src=\"img/dialog/battle-report/forward.png\" class=\"navigation-icons\">\n            <img src=\"img/dialog/battle-report/forward-end.png\" class=\"navigation-icons\">\n        </div>\n    </div>\n    <div class=\"compose-content\">\n        <div class=\"all-reports-delete\">Send Message</div>\n        <div class=\"compose-table\">\n            <span>To:</span>\n            <input type=\"text\" />\n            <span>Subject:</span>\n            <input type=\"text\" />\n            <span>Message:</span>\n            <textarea type=\"text\"></textarea> \n        </div>\n    </div>\n</div>";

  $battle_status = "<div class=\"dialog-name\">\n    <span id=\"caravan-name\">You were {{b_stat}}</span>\n</div>";

  $battle_status_info = "<div class=\"battle-stat-attack\">\n    <span><img src=\"img/dialog/battle-report/attack-icon.png\" class=\"br-attacker-icon\">  {{battleResult.attackCommander.playerName}}</span>\n    <span>{{commander_name}}  <img src=\"img/dialog/training/level.png\" class=\"br-level-icon\"> {{battleResult.attackCommander.playerLevel}}</span>\n    <div class=\"battle-table default-skin\">\n        <table>\n            <tr>\n                <th class=\"t100\"></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/all.png\" /></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/survived.png\" /></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/died.png\" /></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/wounded.png\" /></th>\n            </tr>\n            {{#each battleResult.attackers}}\n            <tr>\n                <td class=\"t100\"><img src={{concat_unit_avatar @key \"44x44\"}} class=\"br-type-img\" /></td>\n                <td class=\"t60\">{{all}}</td>\n                <td class=\"t60\">{{survived}}</td>\n                <td class=\"t60\">{{died}}</td>\n                <td class=\"t60\">{{wounded}}</td>\n            </tr>\n            {{/each}}\n        </table>\n    </div>\n</div>\n<div class=\"battle-stat-def\">\n    <span><img src=\"img/dialog/battle-report/defender-icon.png\" class=\"br-defender-icon\">  {{battleResult.defenderCommander.playerName}}</span>\n    <span>{{commander_name}}  <img src=\"img/dialog/training/level.png\" class=\"br-level-icon\"> {{battleResult.defenderCommander.playerLevel}}</span>\n    <div class=\"battle-table default-skin\">\n        <table>\n            <tr>\n                <th class=\"t100\"></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/all.png\" /></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/survived.png\" /></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/died.png\" /></th>\n                <th class=\"t60\"><img src=\"img/dialog/battle-report/wounded.png\" /></th>\n            </tr>\n            {{#each battleResult.defenders}}\n            <tr>\n                <td class=\"t100\"><img src={{concat_unit_avatar @key \"44x44\"}} class=\"br-type-img\" /></td>\n                <td class=\"t60\">{{all}}</td>\n                <td class=\"t60\">{{survived}}</td>\n                <td class=\"t60\">{{died}}</td>\n                <td class=\"t60\">{{wounded}}</td>\n            </tr>\n            {{/each}}\n        </table>\n    </div>\n</div>";

  $loot_resources = "<div class=\"loot-resources\">\n    <img src=\"img/top-resources/gold.png\" class=\"top-resources\">\n    <span id=\"resource-info-balance\" class=\"top-resources-input\">{{gold}}</span>\n    <img src=\"img/top-resources/water.png\" class=\"top-resources\">\n    <span id=\"resource-info-water\" class=\"top-resources-input\">{{water}}</span>\n    <img src=\"img/top-resources/food.png\" class=\"top-resources\">\n    <span id=\"resource-info-food\" class=\"top-resources-input\">{{food}}</span>\n    <img src=\"img/top-resources/iron.png\" class=\"top-resources\">\n    <span id=\"resource-info-iron\" class=\"top-resources-input\">{{iron}}</span>\n    <img src=\"img/top-resources/wood.png\" class=\"top-resources\">\n    <span id=\"resource-info-timber\" class=\"top-resources-input\">{{wood}}</span>\n</div>";

  $all_reports_table = "<div class=\"all-reports-table\">\n    <table>\n        <tr>\n            <th class=\"t230\"></th>\n            <th class=\"t150\">Date</th>\n            <th class=\"t250\">Type</th>\n            <th class=\"t150\">View</th>\n        </tr>\n        <tr>\n            <td class=\"t230\"><input type=\"checkbox\" /></td>\n            <td class=\"t150\">{{date}}</td>\n            <td class=\"t250\">{{type}}</td>\n            <td class=\"t150\"><a href=\"#\">{{reports}}</a></td>\n        </tr>\n    </table>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var BattleReport;
    BattleReport = (function(_super) {
      __extends(BattleReport, _super);

      function BattleReport() {
        BattleReport.__super__.constructor.call(this, html);
      }

      return BattleReport;

    })(BaseDialog);
    BattleReport.prototype.onPrologue = function() {
      var _this = this;
      this.setTemplates({
        "battle_status": $battle_status,
        "all_reports_table": $all_reports_table,
        "loot_resources": $loot_resources,
        "battle_status_info": $battle_status_info
      });
      HAL.on("FIGHT_FINISHED", function(pdata) {
        return _this.show(pdata);
      });
      this.closeable();
      return this.draggable();
    };
    BattleReport.prototype.show = function(data) {
      var pdata,
        _this = this;
      pdata = this.calculateBattleOutcome(data);
      this.setData("dbattle_status", pdata.battle_status);
      this.setData("dbattle_status_info", pdata.battle_status_info);
      this.setData("dloot_resources", pdata.loot_resources);
      this.setData("dall_reports_table", pdata.all_reports_table);
      this.compile("battle_status", "dbattle_status").then(function(data) {
        return _this.refresh("battle-status", data);
      });
      this.compile("battle_status_info", "dbattle_status_info").then(function(data) {
        _this.refresh("battle-status-info", data);
        return _this.html.find(".battle-table").customScrollbar();
      });
      this.compile("loot_resources", "dloot_resources").then(function(data) {
        return _this.refresh("loot-resources", data);
      });
      this.compile("all_reports_table", "dall_reports_table").then(function(data) {
        return _this.refresh("all-reports-table", data);
      });
      return BattleReport.__super__.show.call(this, {});
    };
    BattleReport.prototype.calculateBattleOutcome = function(data) {
      var art, battle_status, bs, bsi, commanderObjName, commanderValue, lr, unitKey, unitName, unitVal, _ref, _ref1, _ref2, _ref3, _ref4;
      bsi = {
        attackCommander: {},
        defenderCommander: {},
        attackers: {},
        defenders: {}
      };
      bsi['attackCommander']['playerName'] = data.attackers.playerName;
      bsi['attackCommander']['playerLevel'] = data.attackers.playerLevel;
      bsi['defenderCommander']['playerName'] = data.defenders.playerName;
      bsi['defenderCommander']['playerLevel'] = data.defenders.playerLevel;
      _ref = data.attackers.start;
      for (commanderObjName in _ref) {
        commanderValue = _ref[commanderObjName];
        _ref1 = commanderValue.troops;
        for (unitName in _ref1) {
          unitVal = _ref1[unitName];
          bsi['attackers'][unitName] = {
            'all': unitVal,
            'survived': data.attackers.finish[commanderObjName].troops[unitName],
            'died': unitVal - data.attackers.finish[commanderObjName].troops[unitName],
            'wounded': 0
          };
        }
      }
      _ref2 = data.defenders.start;
      for (commanderObjName in _ref2) {
        commanderValue = _ref2[commanderObjName];
        _ref3 = commanderValue.troops;
        for (unitName in _ref3) {
          unitVal = _ref3[unitName];
          bsi['defenders'][unitName] = {
            'all': unitVal,
            'survived': data.defenders.finish[commanderObjName].troops[unitName],
            'died': unitVal - data.defenders.finish[commanderObjName].troops[unitName],
            'wounded': 0
          };
        }
      }
      battle_status = 'Defeated';
      _ref4 = bsi.attackers;
      for (unitKey in _ref4) {
        unitVal = _ref4[unitKey];
        if (unitVal.survived > 0) {
          battle_status = 'Victorious';
        }
      }
      bs = {
        b_stat: battle_status
      };
      lr = {
        gold: 885588,
        water: 11112,
        food: 2221,
        iron: 124154,
        wood: 454525
      };
      art = {
        date: "14.12.2013. 14:56:23",
        type: "Attack on Castle",
        reports: "Reports"
      };
      data = {
        battle_status: bs,
        battle_status_info: {
          battleResult: bsi,
          commanderName: "KUrac palac"
        },
        loot_resources: lr,
        all_reports_table: art
      };
      return data;
    };
    return BattleReport;
  });

}).call(this);
