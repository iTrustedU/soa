(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader", "soa/Agent", "soa/Trader", "soa/Commander", "soa/Trainer"], function(StructureLoader, Agent, Trader, Commander, Trainer) {
    var AGENT_EXPERIENCE_MODIFIER, Heroes, ShortMap, TRADER_EXPERIENCE_MODIFIER, TRAINER_EXPERIENCE_MODIFIER;
    AGENT_EXPERIENCE_MODIFIER = 800;
    TRAINER_EXPERIENCE_MODIFIER = 600;
    TRADER_EXPERIENCE_MODIFIER = 500;
    ShortMap = {
      "commanders": function(meta) {
        var k, out, v;
        out = {};
        for (k in meta) {
          v = meta[k];
          out[k] = this.addNewCommander(v);
        }
        return out;
      },
      "trainers": function(meta) {
        var k, out, v;
        out = {};
        for (k in meta) {
          v = meta[k];
          out[k] = this.addNewTrainer(v);
        }
        return out;
      },
      "traders": function(meta) {
        var k, out, v;
        out = {};
        for (k in meta) {
          v = meta[k];
          out[k] = this.addNewTrader(v);
        }
        return out;
      },
      "agents": function(meta) {
        var k, out, v;
        out = {};
        for (k in meta) {
          v = meta[k];
          out[k] = this.addNewAgent(v);
        }
        return out;
      }
    };
    Heroes = (function(_super) {
      __extends(Heroes, _super);

      function Heroes() {
        this.commanders = {};
        this.trainers = {};
        this.traders = {};
        this.agents = {};
        Heroes.__super__.constructor.call(this, ShortMap);
      }

      Heroes.prototype.initListeners = function() {
        return this.on("DONE_LOADING", function() {
          var HeroesQueue, h, k, _ref, _ref1, _ref2, _results;
          HeroesQueue = SOA.Hud.getDialog("HEROES_QUEUE");
          _ref = this.traders;
          for (k in _ref) {
            h = _ref[k];
            HeroesQueue.putHero(h);
          }
          _ref1 = this.agents;
          for (k in _ref1) {
            h = _ref1[k];
            HeroesQueue.putHero(h);
          }
          _ref2 = this.commanders;
          _results = [];
          for (k in _ref2) {
            h = _ref2[k];
            _results.push(HeroesQueue.putHero(h));
          }
          return _results;
        });
      };

      Heroes.prototype.addNewCommander = function(meta) {
        var cmd;
        cmd = new Commander({
          "*": "*",
          "commander_id": "id"
        });
        cmd.setType("commander");
        cmd.load(meta);
        this.commanders[cmd.id] = cmd;
        return cmd;
      };

      Heroes.prototype.addNewTrainer = function(meta) {
        var cmd;
        cmd = new Trainer({
          "*": "*",
          "trainer_id": "id"
        });
        cmd.setType("trainer");
        cmd.load(meta);
        this.trainers[cmd.id] = cmd;
        return cmd;
      };

      Heroes.prototype.addNewAgent = function(meta) {
        var cmd;
        cmd = new Agent({
          "*": "*",
          "agent_id": "id"
        });
        cmd.setType("agent");
        cmd.load(meta);
        this.agents[cmd.id] = cmd;
        return cmd;
      };

      Heroes.prototype.addNewTrader = function(meta) {
        var cmd;
        cmd = new Trader({
          "*": "*",
          "caravan_id": "id"
        });
        cmd.setType("trader");
        cmd.load(meta);
        this.traders[cmd.id] = cmd;
        return cmd;
      };

      Heroes.prototype.getCountByType = function(type) {
        var count;
        count = 0;
        return 0;
      };

      Heroes.prototype.getCountAll = function() {
        var count;
        count = this.getCountByType('traders') + this.getCountByType('commanders') + this.getCountByType('trainers') + this.getCountByType('agents');
        return count;
      };

      return Heroes;

    })(StructureLoader);
    return Heroes;
  });

}).call(this);
