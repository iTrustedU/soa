(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/constructions/Construction"], function(Construction) {
    var Science, _ref;
    Science = (function(_super) {
      __extends(Science, _super);

      function Science() {
        _ref = Science.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return Science;

    })(Construction);
    Science.prototype.init = function() {
      this.technologies = [];
      return this.sciences = [];
    };
    Science.prototype.epilogue = function() {
      var science, tech, _i, _j, _len, _len1, _ref1, _ref2, _results;
      _ref1 = this.production;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        science = _ref1[_i];
        this.researchScience(science);
        SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(science, "Research");
      }
      _ref2 = this.production;
      _results = [];
      for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
        tech = _ref2[_j];
        this.researchTech(tech);
        _results.push(SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(tech, "Research"));
      }
      return _results;
    };
    Science.prototype.beginScienceResearch = function(science, res) {
      amjad.empire.resources.take(res);
      return this.assignScienceResearch(science);
    };
    Science.prototype.assignScienceResearch = function(science) {
      this.production.push(science);
      this.researchScience(science);
      return SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(science, "Research", true);
    };
    Science.prototype.beginTechResearch = function(tech, res) {
      amjad.empire.resources.take(res);
      return this.assignTechResearch(tech);
    };
    Science.prototype.assignTechResearch = function(tech) {
      this.production.push(tech);
      return SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(tech, "Research");
    };
    Science.prototype.researchTech = function(tech) {
      tech.buildTime = function() {
        var time;
        time = XMLStore.technologies[tech.type][tech.level].time;
        return 60 * (+time + 0.1) * 1000;
      };
      tech.timeLeft = function() {
        var a, b;
        a = new Date(this.start_research);
        b = new Date();
        return this.buildTime() - (b - a);
      };
      tech.timeLeftPercentage = function() {
        return (1 - (this.timeLeft() / this.buildTime())).toFixed(2);
      };
      return tech.researchEnds = function() {
        var a, b, c;
        a = new Date(this.start_production);
        b = new Date(this.start_production);
        a.setMilliseconds(a.getMilliseconds() + this.buildTime());
        c = b - a;
        return [a.toLocaleString(), c];
      };
    };
    Science.prototype.researchScience = function(science) {
      science.buildTime = function() {
        var time;
        time = XMLStore.science[science.type].resources[science.level].time;
        return 60 * (+time + 0.1) * 1000;
      };
      science.timeLeft = function() {
        var a, b;
        a = new Date(this.start_research);
        b = new Date();
        return this.buildTime() - (b - a);
      };
      science.timeLeftPercentage = function() {
        return (1 - (this.timeLeft() / this.buildTime())).toFixed(2);
      };
      return science.researchEnds = function() {
        var a, b, c;
        a = new Date(this.start_production);
        b = new Date(this.start_production);
        a.setMilliseconds(a.getMilliseconds() + this.buildTime());
        c = b - a;
        return [a.toLocaleString(), c];
      };
    };
    return Science;
  });

}).call(this);
