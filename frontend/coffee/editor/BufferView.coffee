"use strict"

html = """
    <table class="buffer-view">
      <tr>
        <td><div id="0"></div></td>
        <td><div id="1"></div></td>
        <td><div id="2"></div></td>
      </tr>
      <tr>
        <td><div id="3"></div></td>
        <td><div id="4"></div></td>
        <td><div id="5"></div></td>
      </tr>
      <tr>
        <td><div id="6"></div></td>
        <td><div id="7"></div></td>
        <td><div id="8"></div></td>
      </tr>
    </table>
"""

define ["EventDispatcher"],

(EventDispatcher) ->

    class BufferView extends EventDispatcher
        constructor: (@map) -> 
            super()
            @init()

    BufferView::init = () ->
        @html = $(html)
        rmngr = @map.region_mngr

        rmngr.on "SECTION_CHANGED", () =>
            console.debug "omg"
            @html.find("img").remove()
            @update()

        @current_region = rmngr.current_region
        @update()

    BufferView::update = () ->
        for section, index in @current_region.sections
            for buffer in section.buffers
                img = HAL.ImageUtils.pixelBufferToImg(buffer)
                @html.find("##{index}").append(img)

    return BufferView