"use strict"

html =
"""
<div class="warehouse draggable">
	<div class="dialog-name">
		<span>Warehouse</span>
		<div class="close-button"></div>
	</div>
	<div class="resource-overview">
		<div class="ro-name">
			<span>Resource Overview</span>
		</div>
		<table>
			<tr>
				<td>
					<span class="statistics">h /</span><span>25</span>
				</td>
				<td>
					<span class="statistics">32 /</span><span>50</span>
				</td>
				<td>
					<img src="img/top-resources/food.png">
				</td>
			</tr>
			<tr>
				<td>
					<span class="statistics">h /</span><span>25</span>
				</td>
				<td>
					<span class="statistics">32 /</span><span>50</span>
				</td>
				<td>
					<img src="img/top-resources/water.png">
				</td>
			</tr>
			<tr>
				<td>
					<span class="statistics">h /</span><span>25</span>
				</td>
				<td>
					<span class="statistics">32 /</span><span>50</span>
				</td>
				<td>
					<img src="img/top-resources/wood.png">
				</td>
			</tr>
			<tr>
				<td>
					<span class="statistics">h /</span><span>25</span>
				</td>
				<td>
					<span class="statistics">32 /</span><span>50</span>
				</td>
				<td>
					<img src="img/top-resources/iron.png">
				</td>
			</tr>
			<tr>
				<td>
					<span class="statistics">h /</span><span>25</span>
				</td>
				<td>
					<span class="statistics">32 /</span><span>50</span>
				</td>
				<td>
					<img src="img/top-resources/gold.png">
				</td>
			</tr>
		</table>
	</div>
	<div class="storage-capacity">
		<div class="ro-name">
			<span>Storage Capacity</span>
		</div>
		<span class="total-capacity-name">Total Capacity</span>
		<div class="storage-exp-bar-box">
			<div class="commander-bar">
				<div class="commander-bar-full"></div>
			</div>
		</div>
		{{{capacity}}}
		<br />
		<div class="single-capacity">
			<span class="single-max-min">Max</span>
			<div class="storage-sub-exp-bar-box">
				<div class="wh-slider-box">	
					<div class="inner-slider2"></div>
				</div>
			</div>
			<span class="single-max-min">Min</span>
			<img src="img/dialog/commander/dates-s.png" />
		</div>
		<div class="single-capacity">
			<span class="single-max-min">Max</span>
			<div class="storage-sub-exp-bar-box">
				<div class="wh-slider-box">	
					<div class="inner-slider2"></div>
				</div>
			</div>
			<span class="single-max-min">Min</span>
			<img src="img/dialog/commander/dates-s.png" />
		</div>
		<div class="single-capacity">
			<span class="single-max-min">Max</span>
			<div class="storage-sub-exp-bar-box">
				<div class="wh-slider-box">	
					<div class="inner-slider2"></div>
				</div>
			</div>
			<span class="single-max-min">Min</span>
			<img src="img/dialog/commander/dates-s.png" />
		</div>
		<div class="single-capacity">
			<span class="single-max-min">Max</span>
			<div class="storage-sub-exp-bar-box">
				<div class="wh-slider-box">	
					<div class="inner-slider2"></div>
				</div>
			</div>
			<span class="single-max-min">Min</span>
			<img src="img/dialog/commander/dates-s.png" />
		</div>
		<div class="single-capacity">
			<span class="single-max-min">Max</span>
			<div class="storage-sub-exp-bar-box">
				<div class="wh-slider-box">	
					<div class="inner-slider2"></div>
				</div>
			</div>
			<span class="single-max-min">Min</span>
			<img src="img/dialog/commander/dates-s.png" />
		</div>
		<div class="upgrade-building move-button">Store</div>
	</div>
	<div class="war-building-info">
        <img src="assets/sprites/amjad/dialogs/constructions/civil//warehouse.png" class="dialog-building-img">
        <div class="upgrade-price">
            <img src="img/top-resources/wood.png">
            <span>200000</span>
            <img src="img/top-resources/iron.png">
            <span>200000</span>
            <img src="img/top-resources/gold.png">
            <span>200000</span>
            <img src="img/top-resources/food.png">
            <span>200000</span>
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
        <div id="move-building" class="upgrade-building move-button">Move</div>
    </div> 
</div>
"""


$capacity = 
"""
<div>
	<span class="max-capacity">{{max_capacity}}</span>
	<span class="min-capacity">{{min_capacity}}</span>
</div>
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->

    class WarehouseDialog extends BaseDialog
        constructor: () ->
            super(html)

    WarehouseDialog::onPrologue = () ->
        @upgradeable()
        @moveable()
        @closeable()
        @draggable()

    WarehouseDialog::onEpilogue = () ->
        @on "PRE_SHOW", _me_ = () ->
            @html
            .find(".dialog-building-img")
            .attr("src", Handlebars.helpers.concat_dialog_construction("warehouse"))
            @remove "PRE_SHOW", _me_

        @on "SHOW", () ->
            resourcesData =
                food_per_hour: 100
                water_per_hour: 200
                wood_per_hour: 300
                iron_per_hour: 400
                money_per_hour: 500
                current_food: 100
                max_food: 10000
                current_water: 100
                max_water: 10000
                current_wood: 100
                max_wood: 10000
                current_iron: 100
                max_iron: 10000
                current_money: 100
                max_money: 10000

            capacityData =
                max_capacity: 50000
                min_capacity: 10


    return WarehouseDialog