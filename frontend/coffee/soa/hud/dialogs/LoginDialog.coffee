"use strict"

html = 
"""
<div class="index-body">
<div class="login-content">
    <div class="login-box">
    <div class="dialog-name">
        <span>Login</span>
    </div>
    <span class="register-name">Username:</span>
    <input class="register-input" type="text" id="username"/>
    <span class="register-name">Password:</span>
    <input class="register-input" id="password" type="password" />
    <input class="check-button" name="choice" type="checkbox" />
    <span class="remember-me">Keep me logged in</span>
    <a href="#" class="forgot-user-id">Forgot your User ID or Password</a>
    <div id="loginbtn" class="register-button">LOGIN</div>
    <br />
    <span class="register-name">No, I'm new here!</span>
    <div id="regbtn" class="register-button">REGISTER</div>
</div>
</div>
</div>
"""

define ["soa/hud/dialogs/BaseDialog", "soa/Rest"],

(BaseDialog, Rest) ->

    class LoginDialog extends BaseDialog
        constructor: () ->
            super(html)

    LoginDialog::init = () ->
        @LoginButton            = @html.find("#loginbtn")
        @SignupButton           = @html.find("#regbtn")

    LoginDialog::onPrologue = () ->
        @SignupButton.click () =>
            SOA.Hud.showDialog("REGISTER")
            @hide()

        @LoginButton.click () =>
            usn     = @html.find("#username").val()
            pass    = @html.find("#password").val()
            SOA.Session.tryLogin(usn, pass)
            .done (data) ->
                SOA.logMeIn()
            .fail (err) ->
                console.debug err

    return LoginDialog




