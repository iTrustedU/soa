"use strict"

define ["soa/constructions/Construction"],

(Construction) ->

	class Science extends Construction

	Science::init = () ->
		@technologies = []
		@sciences = []

	Science::epilogue = () ->

		#kako razaznati ove dve?????
		for science in @production
			@researchScience(science)
			SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(science, "Research")

		for tech in @production
			@researchTech(tech)
			SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(tech, "Research")

	Science::beginScienceResearch = (science, res) ->
		amjad.empire.resources.take(res)
		@assignScienceResearch(science)

	Science::assignScienceResearch = (science) ->
		@production.push(science)
		@researchScience(science)
		SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(science, "Research", true)

	Science::beginTechResearch = (tech, res) ->
		amjad.empire.resources.take(res)
		@assignTechResearch(tech)

	Science::assignTechResearch = (tech) ->
		@production.push(tech)
		SOA.Hud.getDialog("BUILD_ORDERS").addToResearchQueue(tech, "Research")

	Science::researchTech = (tech) ->
		tech.buildTime = () ->
			time = XMLStore.technologies[tech.type][tech.level].time
			return 60*(+time+0.1)*1000 #1 hour

		tech.timeLeft = () ->
			# a = new Date(@start_production)
			a = new Date(@start_research)
			b = new Date()
			return @buildTime() - (b - a)

		tech.timeLeftPercentage = () ->
			return (1 - (@timeLeft() / @buildTime())).toFixed(2)
		
		tech.researchEnds = () ->
			a = new Date(@start_production)
			b = new Date(@start_production)
			a.setMilliseconds(a.getMilliseconds() + @buildTime())
			c = (b - a)
			return [a.toLocaleString(), c]

	# amjad.finishProduction(cn.construction_id).done (data) ->
	#     return
	#     ##console.debug data
	# .fail () ->
	#     console.error "omg. failed to finish tech research"

	Science::researchScience = (science) ->
		science.buildTime = () ->
			time = XMLStore.science[science.type].resources[science.level].time
			return 60*(+time+0.1)*1000 #1 hour hardfuckingcoded

		science.timeLeft = () ->
			# a = new Date(@start_production)
			a = new Date(@start_research)
			b = new Date()
			return @buildTime() - (b - a)

		science.timeLeftPercentage = () ->
			return (1 - (@timeLeft() / @buildTime())).toFixed(2)
		
		science.researchEnds = () ->
			a = new Date(@start_production)
			b = new Date(@start_production)
			a.setMilliseconds(a.getMilliseconds() + @buildTime())
			c = (b - a)
			return [a.toLocaleString(), c]

	##console.debug science.buildTime()

	# amjad.finishProduction(cn.construction_id).done (data) ->
	#     return
	#     ##console.debug data
	# .fail () ->
	#     console.error "omg. failed to finish science research"

	return Science