(function() {
  "use strict";
  define([], function() {
    var lang, langs;
    langs = {
      en: {
        "caravan_discipline": "Caravan Discipline",
        "offensive_tactics": "Offensive Tactics",
        "wood_hauling": "Wood Hauling",
        "worker_education": "Worker Education",
        "load_management": "Load Management",
        "mathematics": "Mathematics",
        "literary_and_oratory_science": "Literary and Oratory Science",
        "medicine": "Medicine",
        "astronomy_and_geography": "Astronomy and Geography",
        "art_of_war": "Art of War",
        "engineering": "Engineering",
        "skirmisher": "Skirmisher",
        "swordsman": "Swordsman",
        "heavy_swordsman": "Heavy Swordsman",
        "bowman": "Bowman",
        "faris": "Faris",
        "heavy_faris": "Heavy Faris",
        "haggan": "Haggan",
        "heavy_haggan": "Heavy Haggan",
        "balance": "Gold",
        "cow": "Cow",
        "food": "Food",
        "goat": "Goat",
        "iron": "Iron",
        "majaheem": "Majaheem",
        "milk": "Milk",
        "sheep": "Sheep",
        "skin": "Skin",
        "timber": "Timber",
        "water": "Water",
        "wool": "Wool",
        "main_building": "Headquarters",
        "tavern": "Tavern",
        "home": "Home",
        "home_info_text": "Home building gives you money based on the population value. Both upgrading it and building more homes raises the population value. Maximum number of homes for Civil players is 6.",
        "war": "Military",
        "training_grounds": "Training Grounds",
        "warehouse": "Warehouse",
        "science": "House of Wisdom",
        "majlis": "Majlis",
        "trade_station": "Trade Station",
        "workshop": "Workshop",
        "market": "Souq",
        "arabian_medicine_tent": "Hospital",
        "tower": "Tower",
        "garrison": "Garrison",
        "date_farm": "Date Farm",
        "water_collector": "Water Collector",
        "woodcutter": "Woodcutter",
        "gold_encampment": "Gold Mine",
        "iron_mine": "Iron Mine"
      }
    };
    return lang = langs.en;
  });

}).call(this);
