mongoose = require "mongoose"
# Resource = require "resource"

schema = mongoose.Schema
	x:
		type: Number
		required: true

	y:
		type: Number
		required: true

	name:
		type: String
		required: true

module.exports = mongoose.model("Settlement", schema)

	# resources:
	# 	type: ObjectId
	# 	ref: Resource