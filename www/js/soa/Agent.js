(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/Hero"], function(Hero) {
    var Agent, _ref;
    Agent = (function(_super) {
      __extends(Agent, _super);

      function Agent() {
        _ref = Agent.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      Agent.prototype.initListeners = function() {};

      return Agent;

    })(Hero);
    return Agent;
  });

}).call(this);
