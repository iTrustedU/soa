(function() {
  "use strict";
  var $active_trainer_template, $available_troops_template, $other_trainers_template, $selected_troop_stat_template, $selected_troop_template, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"training draggable\">\n    <div class=\"dialog-name\">\n        <span>T R A I N I N G &nbsp; G R O U N D S</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div id=\"active-trainer\">{{{active_trainer}}}</div>\n    <div id=\"other-trainers\">{{{other_trainers}}}</div>\n    <div class=\"training-troop\">\n        <div id=\"active-troop\">{{{select_troops}}}</div>\n        <div id=\"available-troops\">{{{available_troops_info}}}</div>\n        <div class=\"slider-amount\">\n            <input type=\"text\" class=\"troop-amount\" />\n            <span class=\"min-max\">Min</span>\n            <div class=\"troop-slider\">\n                <div class=\"inner-slider-box\">\n                    <div class=\"inner-slider ui-slider-handle\"></div>\n                </div> \n            </div>\n            <span class=\"min-max\">Max</span>\n        </div>\n    </div>\n    <div class=\"train-button-box\">Train</div>\n    <div class=\"war-building-info\">\n        <img src=\"assets/sprites/amjad/dialogs/constructions/civil//training_grounds.png\" class=\"dialog-building-img\">\n        <div class=\"upgrade-price\">\n            <img src=\"img/top-resources/wood.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/iron.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/gold.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/food.png\">\n            <span>200000</span>\n        </div>\n        <div id=\"upgrade-building\" class=\"upgrade-building\">Upgrade</div>\n        <div id=\"move-building\" class=\"upgrade-building move-button\">Move</div>\n    </div>\n</div>";

  $active_trainer_template = "<div class=\"trainer-statistic\">\n    <img src={{concat_hero_avatar \"trainer\" \"m\"}} id={{id}} class=\"trainer-avatar\">\n    <div class=\"trainers-name-level\">\n        <span class=\"traniers-name\">{{name}}</span>\n        <img src=\"img/dialog/training/level.png\" class=\"level-icon\">\n        <span class=\"traniers-level-value\">{{level}}</span>\n        <div class=\"tranier-stat\">  \n            <div class=\"tranier-stat-name\"> \n                <span>Brawn:</span>\n                <span>Reflexes:</span>\n                <span>Endurance:</span>\n                <span>Experience:</span>\n            </div>\n            <div class=\"tranier-stat-value\">    \n                <span>{{brawn}}</span>\n                <span>{{reflexes}}</span>\n                <span>{{endurance}}</span>\n                <span>{{experience}}</span>\n            </div>\n        </div>\n    </div>\n</div>    ";

  $other_trainers_template = "{{#each other_trainers}}\n<img src={{concat_hero_avatar \"trainer\" \"s\"}} id={{id}} class=\"training-reserve disable\" />\n{{/each}}";

  $available_troops_template = "<div class=\"select-troops\">\n    <div class=\"select-troops-box\">\n        {{#each troops}}\n            <img id={{@key}} src={{concat_unit_avatar @key \"44x44\"}} class=\"list-troop\"/>\n        {{/each}}\n    </div>\n    <div id=\"selected-troop-stat\">{{{selected_troop_stat}}}</div>\n</div>";

  $selected_troop_stat_template = "<div class=\"available-unit-info\">\n    <div class=\"unit-info-name\">\n        <div class=\"unit-info-box\">\n            <img src=\"img/dialog/training/health.png\" class=\"training-stats-icon\">\n            <span>{{stats.health}}</span>\n        </div>\n        <div class=\"unit-info-box\">\n            <img src=\"img/dialog/training/attack.png\" class=\"training-stats-icon\">\n            <span>{{stats.attack}}</span>\n        </div>\n        <div class=\"unit-info-box\">\n            <img src=\"img/dialog/training/defense.png\" class=\"training-stats-icon\">\n            <span>{{stats.defense}}</span>\n        </div>\n        <div class=\"unit-info-box\">\n            <img src=\"img/dialog/training/upkeep.png\" class=\"training-stats-icon\">\n            <span>{{stats.upkeep}}</span>\n        </div>\n    </div>\n    <div class=\"unit-info-name2\">\n        <div class=\"unit-info-box\">\n            <img src=\"img/dialog/training/pillage.png\" class=\"training-stats-icon\">\n            <span>{{stats.pillage}}</span>\n        </div>\n        <div class=\"unit-info-box\">\n            <img src=\"img/dialog/training/raze.png\" class=\"training-stats-icon\">\n            <span>{{stats.raze}}</span>\n        </div>\n        <div class=\"unit-info-box\">\n            <img src=\"img/dialog/training/steal.png\" class=\"training-stats-icon\">\n            <span>{{stats.steal}}</span>\n        </div>\n    </div>\n</div>";

  $selected_troop_template = "<div class=\"selected-troop\">\n    <img src={{concat_unit_avatar type \"portrait\"}} class=\"selected-troop-img\" />\n    <div class=\"troop-price\">\n        <img src=\"img/top-resources/gold.png\" class=\"top-resources\">\n        <span id=\"selected-troop-balance\" class=\"value\">{{price.balance}}</span>\n        <img src=\"img/top-resources/food.png\" class=\"top-resources\">\n        <span id=\"selected-troop-food\" class=\"value\">{{price.food}}</span>\n        <img src=\"img/top-resources/iron.png\" class=\"top-resources\">\n        <span id=\"selected-troop-iron\" class=\"value\">{{price.iron}}</span>\n    </div>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var TrainingGroundsDialog;
    TrainingGroundsDialog = (function(_super) {
      __extends(TrainingGroundsDialog, _super);

      function TrainingGroundsDialog() {
        TrainingGroundsDialog.__super__.constructor.call(this, html);
      }

      return TrainingGroundsDialog;

    })(BaseDialog);
    TrainingGroundsDialog.prototype.onPrologue = function() {
      var _me_;
      this.setTemplates({
        "active_trainer_template": $active_trainer_template,
        "selected_troop_template": $selected_troop_template,
        "available_troops_template": $available_troops_template,
        "other_trainers_template": $other_trainers_template,
        "selected_troop_stat_template": $selected_troop_stat_template
      });
      this.upgradeable();
      this.moveable();
      this.closeable();
      this.draggable();
      this.on("PRE_SHOW", _me_ = function() {
        this.html.find(".dialog-building-img").attr("src", Handlebars.helpers.concat_dialog_construction("training_grounds"));
        return this.remove("PRE_SHOW", _me_);
      });
      this.clickTriggers({
        ".trainer-avatar/#id": "SELECTED_TRAINER",
        ".training-reserve/#id": "SELECTED_TRAINER",
        ".list-troop/#id": "SELECTED_TROOP",
        ".train-button-box/#": "TRAIN_CLICKED"
      });
      this.on("SELECTED_TRAINER", function(id) {
        this.set("ACTIVE_TRAINER", this.ctx.getTrainers()[id]);
        this.set("OTHER_TRAINERS", this.otherTrainers(id));
        return this.recompile();
      });
      this.on("SELECTED_TROOP", function(id) {
        this.set("ACTIVE_TROOP", this.ctx.units[id]);
        return this.recompile();
      });
      this.on("TRAIN_CLICKED", function() {
        var amount, trainer, troop, xhr,
          _this = this;
        trainer = this.get("ACTIVE_TRAINER");
        troop = this.get("SELECTED_TROOP");
        amount = this.get("AMOUNT");
        return xhr = Rest.post("construction.start_production", {
          cid: this.ctx.construction_id,
          ty: troop,
          amt: amount
        }).done(function(data) {
          var res;
          res = {
            "balance": _this.get("ACTIVE_BALANCE").text(),
            "food": _this.get("ACTIVE_FOOD").text(),
            "iron": _this.get("ACTIVE_IRON").text()
          };
          data["type"] = troop;
          data["amount"] = amount;
          data["construction_id"] = _this.ctx.construction_id;
          _this.ctx.beginUnitProduction(data, res);
          return _this.hide();
        }).fail(function(data) {
          console.error("Production ordering of " + amount + " of " + troop + " units failed");
          return console.error(data);
        });
      });
      return this.on("SHOW", function() {
        var id, trainers, trids, uid;
        trainers = this.ctx.getTrainers();
        trids = Object.keys(trainers);
        if (trids.length === 0) {
          alert("You don't own any trainers at the moment");
          this.disable();
          return;
        }
        id = trids[0];
        uid = Object.keys(this.ctx.units)[0];
        this.set("SELECTED_TROOP", uid);
        this.set("AVAILABLE_TROOPS", {
          "troops": this.ctx.units
        });
        this.set("ACTIVE_TROOP", this.ctx.units[uid]);
        this.set("ACTIVE_TRAINER", trainers[id]);
        this.set("OTHER_TRAINERS", this.otherTrainers(id));
        return this.recompile();
      });
    };
    TrainingGroundsDialog.prototype.initSliderAndInput = function() {
      var _this = this;
      this.set("ACTIVE_MAX_AMOUNT", this.findMaxToTrain(this.get("ACTIVE_TROOP").price));
      this.set("AMOUNT", 1);
      this.slider = this.html.find(".troop-slider").slider({
        min: 1,
        max: this.get("ACTIVE_MAX_AMOUNT") - 1,
        orientation: "horizontal",
        slide: function(ev, ui) {
          _this.validateInput(+ui.value);
          return _this.get("INPUT_AMOUNT").val(ui.value);
        }
      });
      this.slider.slider('value', 1);
      this.set("INPUT_AMOUNT", this.html.find(".troop-amount"));
      this.get("INPUT_AMOUNT").val(1);
      return this.get("INPUT_AMOUNT").on("keyup", function(ev) {
        var amount;
        amount = _this.get("INPUT_AMOUNT").val();
        if (!_this.validateInput(amount)) {
          return _this.get("INPUT_AMOUNT").val(_this.get("ACTIVE_MAX_AMOUNT"));
        }
      });
    };
    TrainingGroundsDialog.prototype.recompile = function() {
      var _this = this;
      this.setData("active_trainer", this.get("ACTIVE_TRAINER"));
      this.setData("troop_info", {
        price: (this.get("ACTIVE_TROOP")).price,
        type: this.get("SELECTED_TROOP")
      });
      this.setData("available_troops", this.get("AVAILABLE_TROOPS"));
      this.setData("other_trainers", this.get("OTHER_TRAINERS"));
      this.setData("active_troop", this.get("ACTIVE_TROOP"));
      this.compile("selected_troop_template", "troop_info").then(function(data) {
        return _this.refresh("active-troop", data);
      });
      this.compile("available_troops_template", "available_troops").then(function(data) {
        return _this.refresh("available-troops", data);
      });
      this.compile("other_trainers_template", "other_trainers").then(function(data) {
        return _this.refresh("other-trainers", data);
      });
      this.compile("selected_troop_stat_template", "active_troop").then(function(data) {
        _this.refresh("selected-troop-stat", data);
        _this.set("ACTIVE_BALANCE", _this.html.find("#selected-troop-balance"));
        _this.set("ACTIVE_FOOD", _this.html.find("#selected-troop-food"));
        _this.set("ACTIVE_IRON", _this.html.find("#selected-troop-iron"));
        _this.set("ACTIVE_MAX_AMOUNT", _this.findMaxToTrain(_this.get("ACTIVE_TROOP").price));
        return _this.initSliderAndInput();
      });
      return this.compile("active_trainer_template", "active_trainer").then(function(data) {
        _this.refresh("active-trainer", data);
        return _this.initSliderAndInput();
      });
    };
    TrainingGroundsDialog.prototype.otherTrainers = function(id) {
      var k, out, v, _ref;
      out = {};
      _ref = this.ctx.getTrainers();
      for (k in _ref) {
        v = _ref[k];
        if (k !== id) {
          out[k] = v;
        }
      }
      return {
        "other_trainers": out
      };
    };
    TrainingGroundsDialog.prototype.calculateTotalMax = function(price, amount) {
      var key, mul, val;
      mul = {};
      for (key in price) {
        val = price[key];
        if (key === "time") {
          continue;
        }
        mul[key] = val * amount;
      }
      return mul;
    };
    TrainingGroundsDialog.prototype.findMaxToTrain = function(price) {
      var amt, k, max, max_of_this, my_res, v;
      max = Number.MAX_VALUE;
      amt = 0;
      for (k in price) {
        v = price[k];
        if (k === "time") {
          continue;
        }
        my_res = amjad.empire.resources[k].amount;
        max_of_this = my_res / (+v);
        if (max_of_this < max) {
          amt = max_of_this;
          max = max_of_this;
        }
      }
      return Math.round(amt);
    };
    TrainingGroundsDialog.prototype.validateInput = function(amount) {
      var max, res_check;
      max = this.calculateTotalMax(this.get("ACTIVE_TROOP").price, amount);
      res_check = amjad.empire.resources.hasEnough(max);
      if (res_check.length !== 0) {
        alert("You're missing some resources");
        return;
      }
      this.get("ACTIVE_BALANCE").text(max['balance']);
      this.get("ACTIVE_FOOD").text(max['food']);
      this.get("ACTIVE_IRON").text(max['iron']);
      this.set("AMOUNT", amount);
      return amount <= this.get("ACTIVE_MAX_AMOUNT");
    };
    return TrainingGroundsDialog;
  });

}).call(this);
