"use strict"

###
@todo neka opcija da se sacuvaju lokalno, sa version brojem
###

define ["soa/StructureLoader"], 

(StructureLoader) ->

    # These two should have equal keys, always!
    URIMap = 
        "constructions": "construction.load_constr_xml"
        "units": "army.load_units_xml"
        "tradegoods": "caravan.load_tradegoods_xml"
        "equipment": "army.load_items_xml"
        "science": "settlement.load_sciences_xml"
        "technologies": "settlement.load_technologies_xml"

    ShortMap =
        "*": ["*", (data) -> return data]

    class XMLStore extends StructureLoader
        fetch: (type) ->
            if not URIMap[type]?
                console.error "Request not allowed"
                return
            if not @[type]?
                Rest.get(URIMap[type])
                .done (data) =>
                    temp = {}
                    temp[type] = data
                    @load(temp)
                    @trigger "LOADED_#{type.toUpperCase()}_XML", data
                .fail (data) =>
                    console.error data

        get: (type) ->
            if not @[type]?
                @fetch(type)
            else
                return @[type]

    return (window.XMLStore = new XMLStore(ShortMap))
