(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader"], function(StructureLoader) {
    var ShortMap, User;
    ShortMap = {
      "fr": "fraction",
      "fn": "last_name",
      "n": "name",
      "trb": "tribe",
      "usid": "user_id",
      "usn": "username",
      "arch": "architecture"
    };
    User = (function(_super) {
      __extends(User, _super);

      function User() {
        User.__super__.constructor.call(this, ShortMap);
      }

      User.prototype.getFullName = function() {
        return "" + this.name + " " + this.last_name;
      };

      User.prototype.initListeners = function() {
        return this.on("DONE_LOADING", function() {
          if (this.architecture === "arabian") {
            this.architecture === "civil";
            this.fraction === "civil";
          }
          this.ASSETS_URI = "assets/sprites/";
          this.CONSTRUCTION_SPRITES_URI = "amjad/constructions/" + this.architecture + "/";
          this.TECH_RESEARCH_SPRITES_URI = "amjad/technologies/";
          this.SCIENCE_RESEARCH_SPRITES_URI = "amjad/science/";
          this.DIALOG_CONSTRUCTIONS_URI = "amjad/dialogs/constructions/" + this.architecture + "/";
          if (this.architecture === "bedouin") {
            return delete XMLStore.constructions.city["home"];
          }
        });
      };

      return User;

    })(StructureLoader);
    return User;
  });

}).call(this);
