(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/constructions/Construction"], function(Construction) {
    var EncampedPOI, _ref;
    EncampedPOI = (function(_super) {
      __extends(EncampedPOI, _super);

      function EncampedPOI() {
        _ref = EncampedPOI.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return EncampedPOI;

    })(Construction);
    return EncampedPOI;
  });

}).call(this);
