(function() {
  "use strict";
  define(["handlebars", "soa/Lang"], function(Handlebars, lang) {
    Handlebars.registerHelper("concat_sprite", function(src) {
      return "assets/sprites/" + src + ".png";
    });
    Handlebars.registerHelper("concat_construction_icon", function(construction, disabled) {
      var icon;
      icon = "" + construction + "_icon.png";
      if (disabled) {
        icon = "" + construction + "_dis_icon.png";
      }
      return "assets/sprites/amjad/constructions/" + amjad.user.architecture + "/" + icon;
    });
    Handlebars.registerHelper("concat_science", function(src) {
      return "assets/sprites/amjad/science/" + src + ".png";
    });
    Handlebars.registerHelper("concat_technology", function(src) {
      return "assets/sprites/amjad/technologies/" + src + ".png";
    });
    Handlebars.registerHelper("lang", function(word) {
      return lang[word];
    });
    Handlebars.registerHelper("concat_hero_avatar", function(src, type) {
      var avuri;
      avuri = "amjad/hero_avatars/" + src + type;
      return Handlebars.helpers.concat_sprite(avuri);
    });
    Handlebars.registerHelper("concat_construction", function(type) {
      var constr;
      constr = "" + amjad.user.CONSTRUCTION_SPRITES_URI + "/" + type;
      return Handlebars.helpers.concat_sprite(constr);
    });
    Handlebars.registerHelper("concat_research", function(type, typeOfResearch) {
      var constr;
      constr = "" + amjad.user.TECH_RESEARCH_SPRITES_URI + "/" + type;
      if (typeOfResearch) {
        constr = "" + amjad.user.SCIENCE_RESEARCH_SPRITES_URI + "/" + type;
      }
      return Handlebars.helpers.concat_sprite(constr);
    });
    Handlebars.registerHelper("concat_dialog_construction", function(type) {
      var constr;
      constr = "" + amjad.user.DIALOG_CONSTRUCTIONS_URI + "/" + type;
      return Handlebars.helpers.concat_sprite(constr);
    });
    Handlebars.registerHelper("concat_resource", function(type) {
      var resuri;
      resuri = "amjad/resources/" + type;
      return Handlebars.helpers.concat_sprite(resuri);
    });
    Handlebars.registerHelper("concat_unit_avatar", function(src, type) {
      var unuri;
      unuri = "amjad/units/" + amjad.user.architecture + "/" + type + "/" + src;
      return Handlebars.helpers.concat_sprite(unuri);
    });
    Handlebars.registerHelper("calc_experience_percentage", function(hero) {
      if (hero.experience == null) {
        return "";
      }
      return "            style=width:" + (((hero.experience / hero.nextLevelExperience()) * 100).toFixed()) + "%!important        ";
    });
    Handlebars.registerHelper("concat_dialog_construction", function(type) {
      var constr;
      constr = "" + amjad.user.DIALOG_CONSTRUCTIONS_URI + "/" + type;
      return Handlebars.helpers.concat_sprite(constr);
    });
    Handlebars.registerHelper("concat_poi", function(poi_type) {
      var poi_uri;
      poi_uri = "amjad/poi/" + poi_type;
      return Handlebars.helpers.concat_sprite(poi_uri);
    });
    Handlebars.registerHelper("capitalize_word", function(word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    });
    return Handlebars.registerHelper("each_skip_null", function(context, options) {
      var it, key, out;
      out = "";
      for (key in context) {
        it = context[key];
        if (it == null) {
          continue;
        }
        out = out + options.fn(it);
      }
      return out;
    });
  });

}).call(this);
