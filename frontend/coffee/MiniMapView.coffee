"use strict"

define ["Geometry", "MapConfig"], 

(Geometry, MapConfig) ->

    class MiniMapView
        ### @todo Ovo sranjce ce da leti odavde ###
        # @dom_minimap  = HAL.Dom.viewport.querySelector("#minimap")
        constructor: (@map) ->
            @width = 600
            @height = 600
            @viewport_size = new PIXI.Point()
            @sections = (new PIXI.Graphics() for x in [0...9])
            @section_container = new PIXI.DisplayObjectContainer()
            @sections_pivot = new PIXI.Point()
            @envelope_bbox_g = new PIXI.Graphics()
            @envelope_bbox = new PIXI.Rectangle()

            @margin = {
                left: 5
                top: 5
            }
            
            @section_size = new PIXI.Point()
            @init()
            
    MiniMapView::pointToLocalSpace = (out, x, y) ->
        out.x = (x / @map.world_bounds.x) * @width
        out.y = (y / @map.world_bounds.y) * @height

    MiniMapView::rectToLocalSpace = (rectOut, rectIn) ->
        rectOut.x = (rectIn.x / @map.world_bounds.x) * @width
        rectOut.y = (rectIn.y / @map.world_bounds.y) * @height
        rectOut.width = (rectIn.width / @map.world_bounds.x) * @width
        rectOut.height = (rectIn.height / @map.world_bounds.y) * @height

    MiniMapView::initSections = () ->
        ind = 0
        for row in [0...3]
            for col in [0...3]
                section = @sections[ind++]
                section.beginFill(0x2005DF, 0.2)
                section.lineStyle(1, 0x2F05CC, 1)
                section.drawRect(row*@section_size.x, col*@section_size.y, @section_size.x, @section_size.y)
                section.endFill()
                @world.addChild(section)

        @rectToLocalSpace(@envelope_bbox, @regmn.envelope.bbox)
        @envelope_bbox_g.clear()
        @envelope_bbox_g.beginFill(0xFF0c10, 0.4)
        @envelope_bbox_g.lineStyle(1, 0x1F0c10, 1)
        @envelope_bbox_g.drawRect(
            @envelope_bbox.x, 
            @envelope_bbox.y, 
            @envelope_bbox.width, 
            @envelope_bbox.height
        )
        @envelope_bbox_g.endFill()

        @world.addChild(@envelope_bbox_g)

    MiniMapView::updateSections = () ->
        @rectToLocalSpace(@envelope_bbox, @regmn.envelope.bbox)
        console.debug @envelope_bbox
        @envelope_bbox_g.clear()
        @envelope_bbox_g.beginFill(0xFF0c10, 0.4)
        @envelope_bbox_g.lineStyle(1, 0x1F0c10, 1)
        @envelope_bbox_g.drawRect(
            @envelope_bbox.x, 
            @envelope_bbox.y, 
            @envelope_bbox.width, 
            @envelope_bbox.height
        )
        @envelope_bbox_g.endFill()

        # v = new PIXI.Point(0, 0)
        # section.clear() for section in @sections
        # for section, i in @regmn.envelope.sections
        #     @pointToLocalSpace(v, section.x + section.parent_region.x, section.y + section.parent_region.y)
        #     @sections[i].clear()
        #     @sections[i].beginFill(0x2005DF, 0.2)
        #     @sections[i].lineStyle(1, 0x2F05CC, 1)
        #     @sections[i].drawRect(v.x, v.y, @section_size.x, @section_size.y)
        #     @sections[i].endFill()

    MiniMapView::init = () ->
        @viewport = new PIXI.Graphics()
        @world = new PIXI.Graphics()
        # @sections = new PIXI.Graphics()
        # @sections.lineStyle(1, 0x00CC00, 1)
        # @sections.drawRect(1, 1, regmn.sw)

        @regmn = @map.region_mngr
        
        # regmn.on "SECTIONS_LOADED", () =>
        #     minX = Number.MAX_VALUE
        #     minY = Number.MAX_VALUE
        #     for sect in regmn.current_region.sections
        #         minX = Math.min(minX, sect.x)
        #         minY = Math.min(minY, sect.y)

        @map.scene.on "KEY_UP", (ev) =>
            if ev.keyCode is 32 #space
                @world.visible = not @world.visible

        @map.on "HIT_AREA_CHANGED", (hit_area) =>
            @pointToLocalSpace(@viewport.position, hit_area.x, hit_area.y)
            @pointToLocalSpace(@viewport_size, hit_area.width, hit_area.height)

            @viewport.clear()
            @viewport.lineStyle(1, 0xFF0000, 1)
            @viewport.drawRect(0, 0, @viewport_size.x, @viewport_size.y)

        @regmn.on "REGION_CHANGED", () =>
            # console.debug "hello"
            # console.debug @regmn.envelope.pivot
            @updateSections()

        @regmn.on "TILE_PLACED", (region, section, row, col, value) =>
            pos = @map.getTilePosition(row, col)
            @pointToLocalSpace(pos, pos.x, pos.y)
            @world.beginFill(value, 1)
            @world.lineStyle(1, 0xFFFFFF^value<<8, 1)
            @world.drawRect(pos.x-1.5, pos.y-1.5, 3, 3)
            @world.endFill()

        @regmn.on "LOADED", () =>
            @world.position.x = @map.scene.screen_size.x - @height - @margin.left
            @world.position.y = @margin.top
            @viewport_size.x = (@map.scene.screen_size.x / @map.world_bounds.x) * @width
            @viewport_size.y = (@map.scene.screen_size.y / @map.world_bounds.y) * @height

            @viewport.position.x = 0
            @viewport.position.y = 0
            @viewport.lineStyle(1, 0xFF0000, 1)
            @viewport.drawRect(0, 0, @viewport_size.x, @viewport_size.y)

            @world.beginFill(0x0C0C0C, 0.3)
            @world.drawRect(0, 0, @width, @height)
            @world.endFill()
            @world.lineStyle(1, 0xFFFFFF, 0.8)
            
            rw = @regmn.rwidth
            rh = @regmn.rheight
            scaledrw = @width / @regmn.columns
            scaledrh = @height / @regmn.rows
            @section_size.x = scaledrw / 3
            @section_size.y = scaledrh / 3

            console.debug @regmn.rows, @regmn.columns

            for row in [0...~~@regmn.rows]
                y = row * scaledrh
                for col in [0...~~@regmn.columns]
                    x = col * scaledrw
                    @world.drawRect(x, y, scaledrw, scaledrh)
            

            @initSections()
            @world.addChild(@viewport)
            @map.scene.stage.addChild(@world)

    return MiniMapView