"use strict"

define [],

() ->

    class DOMManager
        constructor: () ->
            @renderspace                = document.getElementById("renderspace")
            @hud                        = document.getElementById("viewport")
            @viewport                   = document.getElementById("viewport")
            @game                       = document.getElementById("game")
            
            @area                       = @viewport.getBoundingClientRect()
            @default_zindex             = 1000
            @canvases                   = {}
            @in_fullscreen              = false
            @screen_w                   = window.screen.width  #availWidth
            @screen_h                   = window.screen.height #availHeight
            @fullscreen_scale           = [1.0, 1.0]

    DOMManager::createCanvas = (width = @area.width, height = @area.height, z=0, transp) ->
        ind             = @default_zindex + z
        canvas          = document.createElement("canvas")
        canvas.width    = width
        canvas.height   = height      
        if not transp
            canvas.style["background"] = "white"
        else 
            canvas.style["background-color"] = "transparent"
            canvas.style["background"] = "transparent"
        canvas.style["z-index"] = ind
        return canvas

    DOMManager::createCanvasLayer = (width = @area.width, height = @area.height, z, transp) ->
        ind = @default_zindex + z
        if @canvases[ind]
            return @canvases[ind]
        canvas = @createCanvas(width, height, z, transp)
        return canvas

    DOMManager::addCanvas = (canvas, x = 0, y = 0) ->
        z = canvas.style["z-index"]
        canvas.style["left"] = "#{x}px"
        canvas.style["top"]  = "#{y}px"            
        if @canvases[z]
            console.warn("Canvas with z-index of #{z} already exists")
            return
        @canvases[z] = canvas
        @viewport.appendChild(canvas)
        return canvas

    DOMManager::removeCanvasLayer = (z) ->
        ind = @default_zindex + (+z)
        console.info("Removing canvas layer at z-index: #{ind} / #{z}")
        @viewport.removeChild(@canvases[ind])
        delete @canvases[ind]

    DOMManager::clearInfoBox = () ->
        spans = @viewport.getElementsByClassName("infotext")
        spans = [].slice.call(spans) 
        spans.forEach (span) => @viewport.removeChild(span)

    DOMManager::updateTextOn = (publisher, type, text) ->
        span = document.createElement("p")
        span.setAttribute("class", "infotext")
        publisher.on type, (args...) ->
            span.innerHTML = text.f(args)
        @viewport.appendChild(span)

    return DOMManager