"use strict"

html =
"""
<div class="trade-point draggable">
    <div class="dialog-name">
        <span>T R A D E &nbsp; P O I N T</span>
        <div class="close-button"></div>
    </div>
    <div id="trade-info"> {{{trade_info}}} </div>
    <div class="war-building-info">
        <img style="height:112px" src="assets/sprites/amjad/poi/trade.png" class="dialog-building-img" />
    </div>
    <div class="merchant">
        <div class="sale-trade-good">
            <img src="assets/sprites/amjad/hero_avatars/traderl.png">
        </div>
        <div id="own-goods" class="trade-point-inventory2 default-skin">
            {{{own_goods}}}
        </div>
        <input type="text" id="trader-input-amount"/>
        <div class="trade-point-slider-bar">
            <div class="slider-bar-middle trader-slider">
                <div class="inner-slider2 ui-slider-handle"></div>
            </div>
        </div>
        <div class="sell-all sell-button">Sell All</div>
        <div class="sell sell-button">Sell</div>
    </div>
    <div class="trade-p">
        <div class="sale-trade-good good2">
            <img id="poi-merchant"/>
        </div>
        <div id="trade-goods" class="trade-point-inventory default-skin">
            {{{trade_goods}}}
        </div>
        <input type="text" id="merchant-input-amount"/>
        <div class="trade-point-slider-bar">
            <div class="slider-bar-middle merchant-slider">
                <div class="inner-slider2 ui-slider-handle"></div>
            </div>
        </div>
        <div class="buy sell-button">Buy</div>
    </div>
</div>
"""

$trade_info_template = 
"""
<div class="load-money">
    <span><img src="img/top-resources/gold.png"></span>
    <span>{{getBalance}}</span>
    <span><img src="img/dialog/trade-station/load.png"></span>
    <span>{{getLoad}}/{{capacity}}</span>
</div>
"""

$own_goods_template =
"""
{{#each bag}}
    <div class="poi-good7" id="{{@key}}">
        <img class="trader-good" src="{{concat_resource @key}}"/>
        <span class="goods-amount2">{{this}}</span>
    </div>
{{/each}}
"""

$trade_goods_template = 
"""
{{#each goods}}
    <img class="poi-good merchant-good" id="{{@key}}" src="{{concat_resource @key}}"/>
{{/each}}
"""

define ["soa/hud/dialogs/BaseDialog"],
(BaseDialog) ->

    class TradeDialog extends BaseDialog
        constructor: () ->
            super(html)

    TradeDialog::onPrologue = () ->
        #@Dialog = Hud.dm.compileAndAddDialog("TRADE", html)

        @setTemplates
            "trade_info_template": $trade_info_template
            "trade_goods_template": $trade_goods_template
            "own_goods_template": $own_goods_template

        @clickTriggers
            ".poi-good/#id": "POI_GOOD_SELECTED"
            ".poi-good7/#id": "TRADER_GOOD_SELECTED"
            ".buy/#": "BUY"
            ".sell-all/#": "SELL_ALL"
            ".sell/#": "SELL"

        @on "TRADER_GOOD_SELECTED", (id) ->
            @html.find(".poi-good7").removeClass("trade-point-inventory-a")
            @html.find(".poi-good7##{id}").addClass("trade-point-inventory-a")
            @setData "parked_trader", @ctx
            @setData "trade_goods", @loadGoods()
            @setData "trader_goods", @traderGoods()
            @set "TRADER_SELECTED_GOOD", id
            @calcMaxLoad(@get("TRADER_SELECTED_GOOD"))
            #@get("TRADER_INPUT").val(1)
            #TradeDialog::set "TRADER_INPUT_AMOUNT", 1
            @initSliderAndInput()

        @on "POI_GOOD_SELECTED", (id) ->
            @html.find(".merchant-good").removeClass("trade-point-inventory-a")
            @html.find(".merchant-good##{id}").addClass("trade-point-inventory-a")
            @set "MERCHANT_SELECTED_GOOD", id
            @setData "parked_trader", @ctx
            @setData "trade_goods", @loadGoods()
            @setData "trader_goods", @traderGoods()
            @calcMaxLoad(@get("MERCHANT_SELECTED_GOOD"))
            #TradeDialog::get("MERCHANT_INPUT").val(0)
            #TradeDialog::set "MERCHANT_INPUT_AMOUNT", 0
            @initSliderAndInput()
            
        @on "SELL_ALL", () =>
            good = @get "TRADER_SELECTED_GOOD"
            amnt = @get "TRADER_INPUT_AMOUNT"
            ##console.debug good
            ##console.debug amnt
            ##console.debug "#{good}/#{amnt}"
            #resource = {}
            #resource[good] = amnt
            Rest.post "caravan.sell",
                cid: @ctx.caravan_id
                tid: @ctx.trader_id
                res: @traderGoods().bag
            .done (data) =>
                ##console.debug "You sold all your shit"
                @ctx.emptyBag()
                @recompile()
            .fail (data) ->
                console.error data

        @on "BUY", () =>
            good = @get "MERCHANT_SELECTED_GOOD"
            amnt = @get "MERCHANT_INPUT_AMOUNT"
            ##console.debug good
            ##console.debug amnt
            ##console.debug "#{good}/#{amnt}"
            resource = {}
            resource[good] = amnt
            Rest.post "caravan.buy",
                cid: @ctx.caravan_id
                res: resource
            .done (data) =>
                ##console.debug "You bought some shit"
                @ctx.putInBag(good, amnt, +XMLStore.tradegoods[good].value)
                @recompile()
            .fail (data) ->
                console.error data

        @on "SELL", () =>
            good = @get "TRADER_SELECTED_GOOD"
            amnt = @get "TRADER_INPUT_AMOUNT"
            ##console.debug good
            ##console.debug amnt
            ##console.debug "#{good}/#{amnt}"
            resource = {}
            resource[good] = amnt
            Rest.post "caravan.sell",
                cid: @ctx.caravan_id
                tid: @ctx.trader_id
                res: resource
            .done (data) =>
                ##console.debug "You sold some shit"
                @ctx.takeFromBag(good, amnt, +XMLStore.tradegoods[good].value)
                @recompile()
            .fail (data) ->
                console.error data

        @on "PRE_SHOW", _me_ = () =>     
            @html
            .find("#building-avatar")
            .attr("src", Handlebars.helpers.concat_poi("trade"))

            ### @todo odkomentuj###
            ##console.debug @ctx
            if not @ctx? #or not @ctx?
                console.error "No parked hero"
                @hide()
            @loadGoods()
            @remove "PRE_SHOW", _me_

        @on "SHOW", () =>
            t = SOA.Zone.map.getTile(@ctx.o_coord_x, @ctx.o_coord_y)
            #trader = @ctx.trader #(@ctx.getLayers().filter (t) -> return t.type is "trader")[0]
            #console.debug trader
            if not @ctx?
                @disable()
                return
            @set "MERCHANT_SELECTED_GOOD", "dates"
            @alertedNoMoney = false
            #@ctx = trader #.meta

            @recompile()

            #super(@ctx)
            #@compile("own_goods_template", "own_goods").then
        
            #@set "MERCHANT_INPUT_AMOUNT", @html.find("")

        @closeable()
        @draggable()

    TradeDialog::recompile = () ->
        @setData "parked_trader", @ctx
        @setData "trade_goods", @loadGoods()
        @setData "trader_goods", @traderGoods()

        @compile("trade_info_template", "parked_trader").then (data) =>
            @refresh("trade-info", data)
            @html.find(".trade-point-inventory").customScrollbar({
                skin: "default-skin-red", vScroll: true, wheelSpeed: 60
            })
            @initSliderAndInput()

        @compile("trade_goods_template", "trade_goods").then (data) =>
            @refresh("trade-goods", data)
            @html.find(".trade-point-inventory").customScrollbar({
                skin: "default-skin-red", vScroll: true, wheelSpeed: 60
            })
            @initSliderAndInput()

        @compile("own_goods_template", "trader_goods").then (data) =>
            @refresh("own-goods", data)
            @html.find(".trade-point-inventory2").customScrollbar({
                skin: "default-skin-red", vScroll: true, wheelSpeed: 60
            })
            @initSliderAndInput()

        @html.find(".trade-point-inventory").customScrollbar({
            skin: "default-skin-red", vScroll: true, wheelSpeed: 60
        })
        @html.find(".trade-point-inventory").customScrollbar({
            skin: "default-skin-red", vScroll: true, wheelSpeed: 45
        })

    TradeDialog::traderGoods = () ->
        out = {}
        for k, v of @ctx.bag
            continue if k is "balance"
            out[k] = v
        return "bag": out

    TradeDialog::loadGoods = () ->
        goods = {}
        for k, good of XMLStore.get "tradegoods"
            guri = "assets/sprites/amjad/resources/#{k}.png".trim()
            spr = PIXI.TextureCache[guri]
            if spr?
                goods[k] = good
        return "goods": goods

    TradeDialog::initSliderAndInput = () ->
        @set "MERCHANT_INPUT_AMOUNT", 0
        @set "TRADER_INPUT_AMOUNT", 1
        @set "MERCHANT_INPUT", @html.find("#merchant-input-amount")
        @set "TRADER_INPUT", @html.find("#trader-input-amount")
        @get("MERCHANT_INPUT").val(0)
        @get("TRADER_INPUT").val(1)

        self = @

        merchSlider = @html.find(".merchant-slider")
        merchSlider.slider(
            min: 0
            max: self.calcMaxLoad(@get("MERCHANT_SELECTED_GOOD"))
            orientation: "horizontal"
            slide: (ev, ui) =>
                @get("MERCHANT_INPUT").val(ui.value)
                @set "MERCHANT_INPUT_AMOUNT", ui.value
                #TradeDialog::updateInputField()
        )
        merchSlider.slider('value', 0)

        tradeSlider = @html.find(".trader-slider")
        tradeSlider.slider(
            min: 1
            max: @ctx.bag[self.get("TRADER_SELECTED_GOOD")]
            orientation: "horizontal"
            slide: (ev, ui) =>
                @get("TRADER_INPUT").val(ui.value)
                @set "TRADER_INPUT_AMOUNT", ui.value
                #TradeDialog::updateInputField(ui.value)
        )
        tradeSlider.slider('value', 1)

    TradeDialog::updateInputField = () ->
        x = @get "MERCHANT_INPUT_AMOUNT"
        y = @get "TRADER_INPUT_AMOUNT"

    TradeDialog::calcMaxLoad = (good) ->
        cap = @ctx.getCapacity()
        diff = cap.capacity - cap.load
        weight = +XMLStore.tradegoods[good].weight
        console.debug "weight is: #{weight}"
        maxW = Math.floor(diff / weight)
        console.debug "Maxw: #{maxW}"
        balance = @ctx.getBalance()
        price = (+XMLStore.tradegoods[good].value)
        if balance < price and not @alertedNoMoney
            @alertedNoMoney = true
            alert("There's not enough gold on your caravan")
        maxB = Math.floor(balance / price)
        return Math.min(maxW, maxB)

    TradeDialog::calcCostValue = (good) ->
        val = +XMLStore.tradegoods[good].value
        return @get("MERCHANT_INPUT_AMOUNT") * val

    return TradeDialog