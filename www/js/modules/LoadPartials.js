(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["jquery", "EventDispatcher", "handlebars"], function($, EventDispatcher) {
    var LoadPartials, PARTIALS_LIST, PARTIAL_DIR, instance;
    PARTIAL_DIR = "hbpartials/";
    PARTIALS_LIST = "" + PARTIAL_DIR + "partials.list";
    instance = null;
    LoadPartials = (function(_super) {
      __extends(LoadPartials, _super);

      function LoadPartials() {
        LoadPartials.__super__.constructor.call(this);
        if (instance != null) {
          return instance;
        }
        instance = this;
        window.HBPartials = instance;
        this.loaded = false;
        return this.load();
      }

      return LoadPartials;

    })(EventDispatcher);
    LoadPartials.prototype.load = function() {
      var $deferred,
        _this = this;
      $deferred = new $.Deferred();
      if (this.loaded) {
        $deferred.resolve();
      } else {
        $.get(PARTIALS_LIST).done(function(data) {
          return _this.loadAllPartials(data.split("\n"), $deferred);
        });
      }
      return $deferred.promise();
    };
    LoadPartials.prototype.loadAllPartials = function(list, $deferred) {
      var len, loaded_so_far, url, _i, _len, _results,
        _this = this;
      len = list.length;
      loaded_so_far = 0;
      _results = [];
      for (_i = 0, _len = list.length; _i < _len; _i++) {
        url = list[_i];
        if (url === "") {
          _results.push(loaded_so_far++);
        } else {
          _results.push(this.loadPartial(url).done(function() {
            loaded_so_far++;
            if (loaded_so_far === len) {
              _this.loaded = true;
              $deferred.resolve();
              return _this.trigger("LOADED_ALL_PARTIALS", _this);
            }
          }));
        }
      }
      return _results;
    };
    LoadPartials.prototype.loadPartial = function(url) {
      var _this = this;
      return $.get("" + PARTIAL_DIR + url).done(function(html) {
        var name;
        _this.trigger("LOADED_PARTIAL", _this, html);
        name = url.substring(0, url.lastIndexOf(".handlebars"));
        return Handlebars.registerPartial(name, html);
      });
    };
    return LoadPartials;
  });

}).call(this);
