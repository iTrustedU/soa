"use strict"

define ["MathUtils", "PIXI"], 

(MathUtils, PIXI) ->

    POOL_SIZE       = 25000
    Vec2            = {}
    MathUtils.Vec2  = Vec2
    FreeList        = []
    Vec2.free       = POOL_SIZE
    
    for i in [0...POOL_SIZE]
        v = new PIXI.Point() #new MathUtils.ARRAY_TYPE(2)
        # v.x = 0
        # v.y = 0
        FreeList.push(v)

    Vec2.release = (vec) ->
        if Vec2.free > POOL_SIZE
            throw new Error("You released a vector which you didn't acquired")
        Vec2.free++
        Vec2.set(vec, 0, 0)
        return FreeList.push(vec)

    Vec2.acquire = () ->
        if Vec2.free <= 0
            throw new Error("No more free vectors in pool")
        Vec2.free--
        return FreeList.pop()

    Vec2.create = () ->
        return Vec2.acquire()

    Vec2.newFrom = (a) ->
        v = Vec2.acquire()
        v.x = a.x
        v.y = a.y
        return v

    Vec2.clone = (a) ->
        v = Vec2.acquire()
        v.x = a.x
        v.y = a.y
        return v

    Vec2.copy = (out, a) ->
        out.x = a.x
        out.y = a.y
        return out

    Vec2.from = (x, y) ->
        v = Vec2.acquire()
        v.x = x
        v.y = y
        return v

    Vec2.set = (out, x, y) ->
        out.x = x
        out.y = y
        return out

    Vec2.add = (out, a, b) ->
        out.x = a.x + b.x
        out.y = a.y + b.y
        return out

    Vec2.sub = (out, a, b) ->
        out.x = a.x - b.x
        out.y = a.y - b.y
        return out

    Vec2.mul = (out, a, b) ->
        out.x = a.x * b.x
        out.y = a.y * b.y
        return out

    Vec2.divide = (out, a, b) ->
        out.x = a.x / b.x
        out.y = a.y / b.y
        return out

    Vec2.min = (out, a, b) ->
        out.x = Math.min(a.x, b.x)
        out.y = Math.min(a.y, b.y)
        return out

    Vec2.max = (out, a, b) ->
        out.x = Math.max(a.x, b.x)
        out.y = Math.max(a.y, b.y)
        return out

    Vec2.scale = (out, a, b) ->
        out.x = a.x * b
        out.y = a.y * b
        return out

    Vec2.scaleAndAdd = (out, a, b, scale) ->
        out.x = a.x + (b.x * scale)
        out.y = a.y + (b.y * scale)
        return out

    Vec2.addAndScale = (out, a, b, scale) ->
        out.x = (a.x + b.x) * scale
        out.y = (a.y + b.y) * scale
        return out

    Vec2.distance = (a, b) ->
        x = b.x - a.x
        y = b.y - a.y
        return Math.sqrt(x*x + y*y)

    Vec2.sqDistance = (a, b) ->
        x = b.x - a.x
        y = b.y - a.y
        return (x*x + y*y)

    Vec2.length = (a) ->
        [x, y] = a
        return Math.sqrt(x*x + y*y)

    Vec2.sqLength = (a) ->
        [x, y] = a
        return (x*x + y*y)

    Vec2.negate = (out, a) ->
        out.x = -a.x
        out.y = -a.y
        return out

    Vec2.normalize = (out, a) ->
        [x, y] = a
        len = x*x + y*y
        if len > 0
            len = 1/Math.sqrt(len)
            out.x = a.x * len
            out.y = a.y * len
        return out

    Vec2.dot = (a, b) ->
        return a.x * b.x + a.y * b.y

    Vec2.lerp = (out, a, b, t) ->
        # [ax, ay] = a
        out.x = a.x + t * (b.x - a.x)
        out.y = a.y + t * (b.y - a.y)
        return out

    Vec2.random = (out, scale) ->
        scale ?= 1
        r = Math.random() * 2.0 * Math.PI
        out.x = Math.cos(r) * scale
        out.y = Math.sin(r) * scale
        return out
        
    Vec2.transformMat3 = (out, a, m) ->
        out.x = m[0] * a.x + m[1] * a.y + m[2]
        out.y = m[3] * a.y + m[4] * a.y + m[5]
        return out

    Vec2.perpendicular = (out, a) ->
        out.x = a.y
        out.y = -a.x
        return out
         
    Vec2.str = (a) ->
        return "Vec2(#{a.x.toFixed(2)}, #{a.y.toFixed(2)})"

    return Vec2