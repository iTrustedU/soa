#!/bin/bash
# cget.sh: Simple utility script to get JSON data via command line
# @author amilic
usage() {
	echo "Usage: $0 route/to/api" && exit 1
}
[[ $# -lt 1 ]] && usage
curl -i -H "Accept: application/json" http://localhost:3000/$1 | grep '}' | python -mjson.tool 
