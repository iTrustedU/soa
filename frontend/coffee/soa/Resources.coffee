"use strict"

define ["soa/StructureLoader", "soa/hud/IntervalQueue"], (StructureLoader, IntervalQueue) ->
    ShortMap = 
        "*" : "*"
    
    class Resource extends StructureLoader

    UPDATE_INTERVAL = 60*1000
    class Resources extends StructureLoader
        constructor: () ->
            super (
                "*" : [
                    "*", 
                    (meta) -> 
                        new Resource(ShortMap).load(meta)
                ]
            )
            
        load: (meta) ->
            super(meta)

        initListeners: () ->
            @on "DONE_LOADING", () ->
                ##console.debug "RESOURCES LOADED"
                ##console.debug @
                @scheduleUpdate()

    Resources::scheduleUpdate = () ->
        IntervalQueue.addToQueue(() =>
            Rest.post("settlement.calculate_all_resources").then (data) =>
                @add(data)
            .fail (data) ->
                console.error(data)
        , UPDATE_INTERVAL)

    Resources::take = (res) ->
        ##console.debug "Taking resources"
        ##console.debug res
        if not res? 
            console.error "You can't take null resources"
        for key, val of res
            @[key].amount -= (+val)
        amjad.trigger "RESOURCES_UPDATED"
        SOA.Hud.getDialog("TOP_MENU").updateResources()

    Resources::add = (res) ->
        for k, v of res
            @[k].amount += v if @[k]?
        amjad.trigger "RESOURCES_UPDATED"
        SOA.Hud.getDialog("TOP_MENU").updateResources()

    Resources::hasEnough = (res) ->
        out = []
        for key, amount of res
            if not @[key]?
                out.push "Resource doesnt exist: #{key}"
            if (+@[key].amount) < (+amount)
                out.push "Not enough #{key} resources, amount required: #{(+amount).toFixed(0)}, amount available: #{(+@[key].amount).toFixed(0)}"
        return out

    Resources::getTradeResources = () ->
        out = {}
        for k, good of XMLStore.get "tradegoods"
            guri = "assets/sprites/amjad/resources/#{k}.png"
            spr = PIXI.TextureCache[guri]
            if spr? and @[k]?
                out[k] = @[k]
        return out

    #Method returns resources needed for science of selected level
    #Data is pulled from XMLStore
    Resources::getScienceResources = (science, level) ->
        xmlSciences = XMLStore.get("science")
        level = 1 if level == 0
        resourceAmounts = new String(xmlSciences[science].resources['level'+level].amount).split(',')
        resourceTypes = new String(xmlSciences[science].resources['level'+level].resource_type).split(',')
        
        resources = {}
        for num,index in resourceAmounts
            resources[resourceTypes[index]] = num

        return resources

    #Method returns resources needed for technology of selected level
    #Data is pulled from XMLStore
    Resources::getTechResources = (tech, level) ->
        xmlTechnologies = XMLStore.get("technologies");
        level = 1 if level == 0
        resourceAmounts = new String(xmlTechnologies[tech]['level'+level].resources.amount).split(',')
        resourceTypes = new String(xmlTechnologies[tech]['level'+level].resources.resource_type).split(',')
        
        resources = {}
        for num,index in resourceAmounts
           resources[resourceTypes[index]] = num
        
        return resources

    #Method returns resources needed upgrading the construction
    #Data is pulled from XMLStore
    Resources::getConstructionUpgradeResources = (constr, level) ->
        xmlConstructions = XMLStore.get("constructions");
        return xmlConstructions.city[constr]['level'+level].requirements

    Resources::getEmpireResources = () ->
        out = {}
        for key, val of amjad.empire.resources
            if @[key].hasOwnProperty "resource_type"
                out[key]=@[key]
        return out
    return Resources