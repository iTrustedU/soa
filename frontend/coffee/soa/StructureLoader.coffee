"use strict"

define ["soa/SOAEventDispatcher"], 

(SOAEventDispatcher) ->

    class ResourceLoader extends SOAEventDispatcher
        constructor: (@ShortMap) ->
            super()
            @initListeners()
            @init()
            return
    
    ResourceLoader::init = () ->
        return
    
    ResourceLoader::initListeners = () ->
        return

    ResourceLoader::load = (meta) ->
        keys = Object.keys(meta)
        for key, val of @ShortMap
            prop = meta[key]
            entry_tr = val
            is_arr = val instanceof Array and val.length is 2
            if is_arr and prop?
                entry_tr = val[0]
                fn = val[1]
                res = fn.call(@, prop, entry_tr, key)
                @[entry_tr] = res if res?
            else if typeof entry_tr is "function"
                res = entry_tr.call(@, meta[key])
                @[key] = res if res?
            else if key is "*" and is_arr
                if typeof val[1] is "function"
                    fn = val[1]
                    for key, val of meta
                        res = fn.call(@, val, key)
                        @[key] = res if res?
                # else
                #     @[keys[i]] = meta[keys[i]]
            else if key is "*" and val is "*"
                for key, val of meta
                    @[key] = val if val?
            else if prop?
                @[entry_tr] = prop if prop?
        
        @trigger "DONE_LOADING", @
        return @

    return ResourceLoader

