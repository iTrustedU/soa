(function() {
  "use strict";
  var $trader_goods, $trader_info, $trader_item_info, $weight_money, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"trader-screen draggable\">\n    <div class=\"dialog-name\">\n        <span id=\"caravan-name\">T R A D E R &nbsp; &nbsp; {{name}}</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"commander-info\">\n        <div id=\"trader-info-2\">{{{trader_info}}}</div>\n    </div>\n    <div class=\"trader-img\" style=\"left:-7px!important\">\n        <img src=\"{{concat_hero_avatar \"trader\" \"l\"}}\" />\n    </div>\n    <div class=\"trader-inventory\" id=\"own-goods\">\n        {{{own_goods}}}\n    </div>\n    <div class=\"trader-bar-box\">\n        <div class=\"trader-bar\">\n            <div class=\"trader-bar-full\" {{calc_experience_percentage this}}></div>\n        </div>\n    </div>\n    <div id=\"weight-money\">{{{weight_money}}}</div>\n    <div id=\"trader-item-info\">{{{trader_item_info}}}</div>\n</div>";

  $trader_info = "<div class=\"commander-statistics\"> \n    <span class=\"commander-level-name\"><img src=\"img/dialog/training/level.png\" /></span>\n    <span class=\"commander-level-value\">{{level}}</span>\n    <span class=\"commander-expirience-name color-text\">Experience:</span>\n    <span class=\"commander-current-expirience\">{{experience}} / {{nextLevelExperience}}</span>\n    <span class=\"commander-level-name color-text\">Capacity:</span>\n    <span class=\"commander-level-value\">{{capacity}}</span>\n    <span class=\"commander-level-name color-text\">Speed:</span>\n    <span class=\"commander-level-value\">{{speed}}</span>\n    <span class=\"commander-level-name color-text\">Tongue:</span>\n    <span class=\"commander-level-value\">{{tongue}}</span>\n</div>";

  $trader_item_info = "<div class=\"trader-inventory-info\">\n    <span>{{info}}</span>\n</div>";

  $weight_money = "<div class=\"weight-money\">\n    <span>Weight:</span>\n    <span>{{getLoad}}</span>\n    <span> / {{capacity}}</span></br>\n    <span>Money:</span>\n    <span>{{getBalance}}</span>\n</div>";

  $trader_goods = "{{#each goods}}\n    <div class=\"goods-box4\">\n        <img class=\"poi-good trader-good\" id=\"{{@key}}\" src=\"{{concat_resource @key}}\"/>\n        <span class=\"goods-amount4\">{{this}}</span>\n    </div>\n{{/each}}";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var TraderDialog;
    TraderDialog = (function(_super) {
      __extends(TraderDialog, _super);

      function TraderDialog() {
        TraderDialog.__super__.constructor.call(this, html);
      }

      return TraderDialog;

    })(BaseDialog);
    TraderDialog.prototype.onPrologue = function() {
      var trader_item_info;
      trader_item_info = {
        info: "Lorems ipsum dolor sit amet, consectetur adipisicing elit, sed ko eiusmod tempor"
      };
      this.setTemplates({
        "weight_money": $weight_money,
        "trader_info": $trader_info,
        "trader_item_info": $trader_item_info,
        "trader_goods": $trader_goods
      });
      return this.on("SHOW", function() {
        var _this = this;
        this.setData("trader_info", this.ctx);
        this.setData("trader_goods", {
          "goods": this.ctx.getGoods()
        });
        this.setData("trader_item_info", trader_item_info);
        this.compileHtml(this.ctx);
        this.compile("trader_info", "trader_info").then(function(data) {
          return _this.refresh("trader-info-2", data);
        });
        this.compile("weight_money", "trader_info").then(function(data) {
          return _this.refresh("weight-money", data);
        });
        this.compile("trader_item_info", "trader_item_info").then(function(data) {
          return _this.refresh("trader-item-info", data);
        });
        this.compile("trader_goods", "trader_goods").then(function(data) {
          return _this.refresh("own-goods", data);
        });
        this.draggable();
        return this.closeable();
      });
    };
    return TraderDialog;
  });

}).call(this);
