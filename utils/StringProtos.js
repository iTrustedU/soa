(function() {
  module.exports = function() {
    String.prototype.endsWith = function(str) {
      return this.lastIndexOf(str) + str.length === this.length;
    };
    return String.prototype.format = String.prototype.f = function() {
      var args, i, s;
      if (arguments[0] instanceof Array) {
        args = arguments[0];
      } else {
        args = arguments;
      }
      i = args.length;
      s = this;
      while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), args[i]);
      }
      return s;
    };
  };

}).call(this);
