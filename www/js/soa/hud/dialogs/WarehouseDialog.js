(function() {
  "use strict";
  var $capacity, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"warehouse draggable\">\n	<div class=\"dialog-name\">\n		<span>Warehouse</span>\n		<div class=\"close-button\"></div>\n	</div>\n	<div class=\"resource-overview\">\n		<div class=\"ro-name\">\n			<span>Resource Overview</span>\n		</div>\n		<table>\n			<tr>\n				<td>\n					<span class=\"statistics\">h /</span><span>25</span>\n				</td>\n				<td>\n					<span class=\"statistics\">32 /</span><span>50</span>\n				</td>\n				<td>\n					<img src=\"img/top-resources/food.png\">\n				</td>\n			</tr>\n			<tr>\n				<td>\n					<span class=\"statistics\">h /</span><span>25</span>\n				</td>\n				<td>\n					<span class=\"statistics\">32 /</span><span>50</span>\n				</td>\n				<td>\n					<img src=\"img/top-resources/water.png\">\n				</td>\n			</tr>\n			<tr>\n				<td>\n					<span class=\"statistics\">h /</span><span>25</span>\n				</td>\n				<td>\n					<span class=\"statistics\">32 /</span><span>50</span>\n				</td>\n				<td>\n					<img src=\"img/top-resources/wood.png\">\n				</td>\n			</tr>\n			<tr>\n				<td>\n					<span class=\"statistics\">h /</span><span>25</span>\n				</td>\n				<td>\n					<span class=\"statistics\">32 /</span><span>50</span>\n				</td>\n				<td>\n					<img src=\"img/top-resources/iron.png\">\n				</td>\n			</tr>\n			<tr>\n				<td>\n					<span class=\"statistics\">h /</span><span>25</span>\n				</td>\n				<td>\n					<span class=\"statistics\">32 /</span><span>50</span>\n				</td>\n				<td>\n					<img src=\"img/top-resources/gold.png\">\n				</td>\n			</tr>\n		</table>\n	</div>\n	<div class=\"storage-capacity\">\n		<div class=\"ro-name\">\n			<span>Storage Capacity</span>\n		</div>\n		<span class=\"total-capacity-name\">Total Capacity</span>\n		<div class=\"storage-exp-bar-box\">\n			<div class=\"commander-bar\">\n				<div class=\"commander-bar-full\"></div>\n			</div>\n		</div>\n		{{{capacity}}}\n		<br />\n		<div class=\"single-capacity\">\n			<span class=\"single-max-min\">Max</span>\n			<div class=\"storage-sub-exp-bar-box\">\n				<div class=\"wh-slider-box\">	\n					<div class=\"inner-slider2\"></div>\n				</div>\n			</div>\n			<span class=\"single-max-min\">Min</span>\n			<img src=\"img/dialog/commander/dates-s.png\" />\n		</div>\n		<div class=\"single-capacity\">\n			<span class=\"single-max-min\">Max</span>\n			<div class=\"storage-sub-exp-bar-box\">\n				<div class=\"wh-slider-box\">	\n					<div class=\"inner-slider2\"></div>\n				</div>\n			</div>\n			<span class=\"single-max-min\">Min</span>\n			<img src=\"img/dialog/commander/dates-s.png\" />\n		</div>\n		<div class=\"single-capacity\">\n			<span class=\"single-max-min\">Max</span>\n			<div class=\"storage-sub-exp-bar-box\">\n				<div class=\"wh-slider-box\">	\n					<div class=\"inner-slider2\"></div>\n				</div>\n			</div>\n			<span class=\"single-max-min\">Min</span>\n			<img src=\"img/dialog/commander/dates-s.png\" />\n		</div>\n		<div class=\"single-capacity\">\n			<span class=\"single-max-min\">Max</span>\n			<div class=\"storage-sub-exp-bar-box\">\n				<div class=\"wh-slider-box\">	\n					<div class=\"inner-slider2\"></div>\n				</div>\n			</div>\n			<span class=\"single-max-min\">Min</span>\n			<img src=\"img/dialog/commander/dates-s.png\" />\n		</div>\n		<div class=\"single-capacity\">\n			<span class=\"single-max-min\">Max</span>\n			<div class=\"storage-sub-exp-bar-box\">\n				<div class=\"wh-slider-box\">	\n					<div class=\"inner-slider2\"></div>\n				</div>\n			</div>\n			<span class=\"single-max-min\">Min</span>\n			<img src=\"img/dialog/commander/dates-s.png\" />\n		</div>\n		<div class=\"upgrade-building move-button\">Store</div>\n	</div>\n	<div class=\"war-building-info\">\n        <img src=\"assets/sprites/amjad/dialogs/constructions/civil//warehouse.png\" class=\"dialog-building-img\">\n        <div class=\"upgrade-price\">\n            <img src=\"img/top-resources/wood.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/iron.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/gold.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/food.png\">\n            <span>200000</span>\n        </div>\n        <div id=\"upgrade-building\" class=\"upgrade-building\">Upgrade</div>\n        <div id=\"move-building\" class=\"upgrade-building move-button\">Move</div>\n    </div> \n</div>";

  $capacity = "<div>\n	<span class=\"max-capacity\">{{max_capacity}}</span>\n	<span class=\"min-capacity\">{{min_capacity}}</span>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var WarehouseDialog;
    WarehouseDialog = (function(_super) {
      __extends(WarehouseDialog, _super);

      function WarehouseDialog() {
        WarehouseDialog.__super__.constructor.call(this, html);
      }

      return WarehouseDialog;

    })(BaseDialog);
    WarehouseDialog.prototype.onPrologue = function() {
      this.upgradeable();
      this.moveable();
      this.closeable();
      return this.draggable();
    };
    WarehouseDialog.prototype.onEpilogue = function() {
      var _me_;
      this.on("PRE_SHOW", _me_ = function() {
        this.html.find(".dialog-building-img").attr("src", Handlebars.helpers.concat_dialog_construction("warehouse"));
        return this.remove("PRE_SHOW", _me_);
      });
      return this.on("SHOW", function() {
        var capacityData, resourcesData;
        resourcesData = {
          food_per_hour: 100,
          water_per_hour: 200,
          wood_per_hour: 300,
          iron_per_hour: 400,
          money_per_hour: 500,
          current_food: 100,
          max_food: 10000,
          current_water: 100,
          max_water: 10000,
          current_wood: 100,
          max_wood: 10000,
          current_iron: 100,
          max_iron: 10000,
          current_money: 100,
          max_money: 10000
        };
        return capacityData = {
          max_capacity: 50000,
          min_capacity: 10
        };
      });
    };
    return WarehouseDialog;
  });

}).call(this);
