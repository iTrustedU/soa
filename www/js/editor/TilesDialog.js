(function() {
  "use strict";
  var delete_btn_html, tile_html, tile_tpl,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  tile_html = "<li id={{id}} class=\"selectable-box tile\" title={{tooltip}}>\n    <img id={{id}} src={{img_url}} class=\"tile-img\">\n    <span id={{id}} class=\"selectable-title\">\n        {{title}}\n    </span>\n</li>";

  delete_btn_html = "<i id=\"map-delete\" class=\"fa fa-times tile-delete-btn\"></i>";

  tile_tpl = Handlebars.compile(tile_html);

  define(["editor/BaseDialog", "MapConfig", "TileMetaStorage", "editor/TileEditDialog"], function(BaseDialog, MapConfig, TileMetaStorage, TileEditDialog) {
    var API, TilesDialog;
    API = {
      TILES_LIST: "/tiles",
      TILE_SAVE: "/tiles/new",
      TILE_DELETE: "/tile/{0}/delete"
    };
    TilesDialog = (function(_super) {
      __extends(TilesDialog, _super);

      function TilesDialog() {
        TilesDialog.__super__.constructor.call(this, "Tiles", "tiles-dialog");
      }

      return TilesDialog;

    })(BaseDialog);
    TilesDialog.prototype.init = function() {
      var _this = this;
      this.storage = new TileMetaStorage();
      this.current_selected = -1;
      this.showTiles();
      this.addTools();
      this.content.droppable({
        accept: ".draggable-sprite",
        activeClass: "border-active",
        drop: function(ev, ui) {
          var tile_spr_wrapper;
          tile_spr_wrapper = ui.draggable.clone();
          tile_spr_wrapper.addClass("border-active");
          return _this.edit.dropSprite(tile_spr_wrapper);
        }
      });
      this.edit = new TileEditDialog();
      this.edit.html.hide();
      this.delete_btn = $(delete_btn_html);
      this.delete_btn.on("click", function() {
        var id;
        id = _this.current_selected;
        return $.post(API.TILE_DELETE.f(id)).then(function() {
          return _this.html.find("li#" + id).remove();
        });
      });
      return this.addToToolbox(this.delete_btn);
    };
    TilesDialog.prototype.initListeners = function() {
      var _this = this;
      TilesDialog.__super__.initListeners.call(this);
      return this.edit.on("TILE_SAVED", function(tile) {
        return $.post(API.TILE_SAVE, tile).then(function(tile) {
          return _this.addTile(tile);
        });
      });
    };
    TilesDialog.prototype.showTiles = function() {
      var _this = this;
      return $.get(API.TILES_LIST).done(function(tiles) {
        var tile, _i, _len, _ref, _results;
        _this.tiles = tiles;
        _this.content.empty();
        _ref = _this.tiles;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          tile = _ref[_i];
          _results.push(_this.addTile(tile));
        }
        return _results;
      });
    };
    TilesDialog.prototype.addTile = function(tile) {
      var $tile_box, tooltip,
        _this = this;
      this.storage.addTile(tile);
      tooltip = tile.name;
      $tile_box = $(tile_tpl({
        id: tile.seedId,
        title: tile.name,
        tooltip: tooltip,
        img_url: tile.sprite
      }));
      $tile_box.on("click", function(ev, ui) {
        var el;
        el = $(ev.target);
        _this.current_selected = el.attr("id");
        _this.trigger("TILE_SELECTED", _this, _this.storage.findByID(_this.current_selected));
        _this.html.find(".tile-img").removeClass("border-active");
        return el.addClass("border-active");
      });
      $tile_box.on("dblclick", function(ev, ui) {
        var el;
        el = $(ev.target);
        el.removeClass("border-active");
        return _this.edit.showTile(el.parent());
      });
      return this.content.append($tile_box);
    };
    TilesDialog.prototype.addTools = function() {
      return this.addToToolbox(this.createLayerCircleButtons());
    };
    TilesDialog.prototype.createLayerCircleButtons = function() {
      var i, out, _i, _ref;
      out = "";
      for (i = _i = 0, _ref = MapConfig.MAX_LAYERS; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
        out += "<div id=\"layer\" layer='" + i + "' class='circle'>\n    <span id=\"layer-text\">" + i + "</span>\n</div>";
      }
      return out;
    };
    return TilesDialog;
  });

}).call(this);
