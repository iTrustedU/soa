(function() {
  "use strict";
  var html, media_preview_html, registration_confirmation_html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"index-body\">\n<div class=\"login-content\">\n    <div id=\"register-dialog\" class=\"register-box\">\n        <div class=\"dialog-name\">\n            <span>Sign Up</span>\n        </div>\n        <form method=\"post\" action=\"register\" id=\"regform\"> \n            <span class=\"register-name\">Username:</span>\n            <input name=\"username\" id=\"username\" class=\"register-input\" type=\"text\" minlength=\"5\" maxlength=\"30\" value=\"\" required>\n            <span class=\"register-name\">First Name:</span>\n            <input  name=\"firstname\" id=\"firstname\" class=\"register-input\" type=\"text\" minlength=\"2\" maxlength=\"50\" value=\"\" required>\n            <span class=\"register-name\">Family Name:</span>\n            <input name=\"familyname\" id=\"familyname\" class=\"register-input\" type=\"text\" minlength=\"2\" maxlength=\"50\" value=\"\" required>\n            <span class=\"register-name\">Email:</span>\n            <input id=\"email\" name=\"email\" class=\"register-input\" type=\"email\" value=\"\" required>\n            <span class=\"register-name\">Verify Email:</span>\n            <input id=\"email-verify\" name=\"email-verify\" class=\"register-input\" type=\"email\" value=\"\" required>\n            <span class=\"register-name\">Password:</span>\n            <input name=\"password\" id=\"password\" class=\"register-input\" type=\"password\" value=\"\" required>\n            <span class=\"register-name\">Verify Password:</span>\n            <input name=\"password-verify\" id=\"password-verify\" class=\"register-input\" type=\"password\" value=\"\" required>\n            <span class=\"register-name\">Civil:</span>\n            <input id=\"faction\" class=\"radio-button\" name=\"faction\" type=\"radio\" value=\"civil\">\n            <div class=\"sub-choices disable\" id=\"sub-choices\">\n                <span class=\"register-name\">Arabian:</span>\n                <input id=\"architecture\" checked=\"checked\" class=\"radio-button\" name=\"architecture\" type=\"radio\" value=\"arabian\">\n                <span class=\"register-name\">Southern:</span>\n                <input id=\"architecture\" class=\"radio-button\" name=\"architecture\" type=\"radio\" value=\"southern\">\n            </div>\n            <span class=\"register-name\">Bedouin:</span>\n            <input id=\"faction\" class=\"radio-button\" name=\"faction\" checked=\"checked\" type=\"radio\" value=\"bedouin\">\n            <label for=\"\" class=\"error validation-message\"></label>\n            <input type=\"submit\" class=\"register-button\" value=\"REGISTER\"/>\n            <br />\n            <div class=\"back-button\">BACK</div>\n        </form>\n    </div>\n</div>\n</div>";

  media_preview_html = "<div class=\"media-preview-box\">\n    <div class=\"dialog-name\">\n        <span>Preview</span>\n    </div>\n</div>";

  registration_confirmation_html = "<div class=\"register-box\">\n    <div class=\"dialog-name\">\n        <span>Confirmation</span>\n    </div>\n    <div>\n        <span class=\"validation-message\">\n            <p>Thank You for registering for Sheiks of Arabia!</p>\n            <p>A confirmation mail has been sent to the e-mail you provided.</p>\n            <p>Open your e-mail and click on the confirmation link to complete the registration.</p>\n        </span>\n    </div>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var RegisterDialog;
    RegisterDialog = (function(_super) {
      __extends(RegisterDialog, _super);

      function RegisterDialog() {
        RegisterDialog.__super__.constructor.call(this, html);
      }

      return RegisterDialog;

    })(BaseDialog);
    RegisterDialog.prototype.init = function() {
      this.RegisterDialogContent = this.html.find(".login-content");
      this.RegisterDialogBox = this.html.find("#register-dialog");
      this.MediaPreviewBox = $(media_preview_html);
      this.ConfirmationDialog = $(registration_confirmation_html);
      this.CivilChoice = this.html.find("#civil");
      this.BedouinChoice = this.html.find("#bedouin");
      this.CivilChoiceSubmenu = this.html.find(".sub-choices");
      this.RegisterButton = this.html.find(".register-button");
      this.BackButton = this.html.find(".back-button");
      return this.ValidationMessage = this.html.find(".validation-message");
    };
    RegisterDialog.prototype.onPrologue = function() {
      var _this = this;
      this.RegisterDialogContent.append(this.MediaPreviewBox);
      return this.BackButton.click(function() {
        _this.hide();
        return SOA.Hud.showDialog("LOGIN");
      });
    };
    return RegisterDialog;
  });

}).call(this);
