(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/Rest", "soa/StructureLoader", "soa/Resources", "soa/constructions/Constructions", "soa/Heroes", "soa/Settlement"], function(Rest, StructureLoader, Resources, Constructions, Heroes, Settlement) {
    var Empire, ShortMap;
    ShortMap = {
      "resources": [
        "resources", function(meta) {
          return new Resources().load(meta);
        }
      ],
      "cons_and_units": [
        "constructions", function(meta) {
          return new Constructions().load(meta);
        }
      ],
      "settlement": [
        "settlement", function(meta) {
          return new Settlement().load(meta);
        }
      ],
      "units": [
        "units", function(meta) {
          return this["units"] = meta;
        }
      ],
      "sciences": [
        "sciences", function(meta) {
          return this["sciences"] = meta;
        }
      ],
      "technologies": [
        "technologies", function(meta) {
          return this["technologies"] = meta;
        }
      ],
      "heroes": [
        "heroes", function(meta) {
          return new Heroes().load(meta);
        }
      ]
    };
    Empire = (function(_super) {
      __extends(Empire, _super);

      function Empire() {
        Empire.__super__.constructor.call(this, ShortMap);
        return this;
      }

      return Empire;

    })(StructureLoader);
    Empire.prototype.load = function(meta) {
      Empire.__super__.load.call(this, meta);
    };
    Empire.prototype.generateCommandersList = function() {
      return Rest.get("army.generate_commanders");
    };
    Empire.prototype.loadCommandersList = function() {
      return Rest.get("army.load_commanders");
    };
    Empire.prototype.generateTrainersList = function() {
      return Rest.get("construction.generate_trainers");
    };
    Empire.prototype.loadTrainersList = function() {
      return Rest.get("construction.load_trainers");
    };
    Empire.prototype.generateAgentsList = function() {
      return Rest.get("army.generate_agents");
    };
    Empire.prototype.loadAgentsList = function() {
      return Rest.get("army.load_agents");
    };
    Empire.prototype.generateCaravansList = function() {
      return Rest.get("trader.generate_traders");
    };
    Empire.prototype.loadCaravansList = function() {
      return Rest.get("trader.load_traders");
    };
    Empire.prototype.employCommander = function(id) {
      return Rest.post("army.assign_commander_to_user", {
        comid: id
      });
    };
    Empire.prototype.employTrainer = function(tid, cid) {
      return Rest.post("construction.add_trainer_to_construction", {
        tid: tid,
        cid: cid
      });
    };
    Empire.prototype.employTrader = function(id) {
      return Rest.post("trader.assign_trader_to_user", {
        tid: id
      });
    };
    Empire.prototype.employAgent = function(id) {
      return Rest.post("army.assign_agent_to_user", {
        gaid: id
      });
    };
    Empire.prototype.startScienceResearch = function(id, level) {
      return Rest.post("settlement.start_science_research", {
        ty: id,
        lvl: level
      });
    };
    Empire.prototype.startTechResearch = function(id, level) {
      return Rest.post("settlement.start_tech_research", {
        ty: id,
        lvl: level
      });
    };
    Empire.prototype.initListeners = function() {
      return this.on("DONE_LOADING", function() {});
    };
    return Empire;
  });

}).call(this);
