(function() {
  "use strict";
  define(["EventDispatcher", "Ajax"], function(EventDispatcher, Ajax) {
    var TileMetaStorage;
    TileMetaStorage = (function() {
      function TileMetaStorage() {
        this.tile_layer_map = {};
        this.tile_name_map = {};
        this.tile_id_map = {};
      }

      return TileMetaStorage;

    })();
    TileMetaStorage.prototype.load = function(file_uri) {
      var _this = this;
      return Ajax.get(file_uri).success(function(tiles) {
        tiles = JSON.parse(tiles);
        return _this.loadTiles(tiles);
      });
    };
    TileMetaStorage.prototype.onComplete = function() {};
    TileMetaStorage.prototype.loadTiles = function(tiles) {
      var i, t;
      for (i in tiles) {
        t = tiles[i];
        this.addTile(t);
      }
      return this.onComplete(tiles);
    };
    TileMetaStorage.prototype.addTile = function(tile) {
      var t;
      t = this.tile_name_map[tile.name];
      if (t != null) {
        delete this.tile_layer_map[t.layer][t.name];
      }
      this.tile_name_map[tile.name] = tile;
      if (this.tile_id_map[tile.seedId] != null) {
        throw new Error("Duplicate tile meta id detected: " + tile.name + "/" + tile.seedId);
      }
      this.tile_id_map[tile.seedId] = tile;
      if (this.tile_layer_map[tile.layer] == null) {
        this.tile_layer_map[tile.layer] = {};
      }
      return this.tile_layer_map[tile.layer][tile.name] = tile;
    };
    TileMetaStorage.prototype.getAllByLayer = function(layer) {
      return this.tile_layer_map[layer] || {};
    };
    TileMetaStorage.prototype.findByName = function(name) {
      var t;
      t = this.tile_name_map[name];
      return t;
    };
    TileMetaStorage.prototype.findByID = function(id) {
      var t;
      t = this.tile_id_map[id];
      return t;
    };
    TileMetaStorage.prototype.removeByName = function(name) {
      var t;
      t = this.tile_name_map[name];
      delete this.tile_layer_map[t.layer][t.name];
      delete this.tile_name_map[t.name];
      delete this.tile_id_map[t.seedId];
      return t = null;
    };
    TileMetaStorage.prototype.removeById = function(id) {
      var t;
      t = this.tile_id_map[id];
      delete this.tile_layer_map[t.layer][t.name];
      delete this.tile_id_map[t.seedId];
      delete this.tile_name_map[t.name];
      return t = null;
    };
    return TileMetaStorage;
  });

}).call(this);
