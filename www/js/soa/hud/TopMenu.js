(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"top\">\n    <div class=\"top-ornament\"></div>\n    <div class=\"top-menu-bar\">\n        <div class=\"top-menu-button options-system\"></div>\n        <div class=\"top-menu-button messages\"></div>\n        <div class=\"top-menu-button renking-ladders\"></div>\n        <div class=\"top-menu-button news\"></div>\n        <img src=\"img/top-resources/gold.png\" class=\"top-resources\">\n        <span id=\"resource-info-balance\" class=\"top-resources-input\"></span>\n        <img src=\"img/top-resources/water.png\" class=\"top-resources\">\n        <span id=\"resource-info-water\" class=\"top-resources-input\"></span>\n        <span id=\"resource-info-food\" class=\"top-resources-input-right-first\"></span>\n        <img src=\"img/top-resources/food.png\" class=\"top-resources-right\">\n        <span id=\"resource-info-iron\" class=\"top-resources-input-right\"></span>\n        <img src=\"img/top-resources/iron.png\" class=\"top-resources-right\">\n        <span id=\"resource-info-timber\" class=\"top-resources-input-right\"></span>\n        <img src=\"img/top-resources/wood.png\" class=\"top-resources-right\">\n        <!-- <div class=\"full-screen-button\"></div> -->\n        <div class=\"top-menu-name\">\n            <span id=\"name\"></span>\n        </div>\n        <div class=\"top-menu-gradient-bar\">\n            <img src=\"img/top-interface/effects-daynight.png\" class=\"daynight\">\n            <img src=\"img/top-interface/effects-frame.png\" class=\"effects\">\n            <img src=\"img/top-interface/effects-frame.png\" class=\"effects\">\n            <img src=\"img/top-interface/effects-frame.png\" class=\"effects\">\n        </div>\n    </div>\n    <div class=\"top-menu-center-box\">\n        <div class=\"top-menu-center-l top-menu-center\">\n            <span id=\"title-a\">Ruling Title</span>\n        </div>\n        <div class=\"top-menu-center-r top-menu-center\">\n            <span id=\"title-b\">Achieved Title</span>\n        </div>\n    </div>\n    <div class=\"top-menu-center-down\">\n        <span>Player Tribe</span>\n    </div>\n    <div class=\"advisor\">\n        <div class=\"advisor-ornament\"></div>\n        <div class=\"advisor-name\">\n            <span id=\"coords\"><b>X:</b> 4518<b> Y:</b> 5512</span>\n        </div>\n        <div class=\"advisor-img\"></div>\n    </div>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var TopMenu;
    TopMenu = (function(_super) {
      __extends(TopMenu, _super);

      function TopMenu() {
        TopMenu.__super__.constructor.call(this, html);
      }

      return TopMenu;

    })(BaseDialog);
    TopMenu.prototype.onEpilogue = function(hud) {
      this.html.find("#name").text(amjad.user.getFullName());
      this.html.find("#coords").html("<b>X:</b> " + amjad.empire.settlement.x + "<b> Y:</b> " + amjad.empire.settlement.y);
      return this.on("SHOW", function() {
        return this.updateResources();
      });
    };
    TopMenu.prototype.updateResources = function() {
      var obj, res, reskey, _ref, _results;
      _ref = amjad.empire.resources;
      _results = [];
      for (reskey in _ref) {
        res = _ref[reskey];
        obj = this.html.find("span[id$='" + reskey + "']");
        if (reskey.length > 1 && obj.length) {
          _results.push(obj.text(res.amount));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    return TopMenu;
  });

}).call(this);
