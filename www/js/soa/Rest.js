(function() {
  "use strict";
  define(["jquery"], function($) {
    var Rest, api_url;
    api_url = "api/";
    Rest = {
      setSessionKey: function(session) {
        this.session = session;
      },
      prepareRequest: function(obj) {
        return "session=" + this.session + "&jparams=" + (encodeURI(JSON.stringify(obj)));
      },
      post: function(url, data, okay) {
        if (data == null) {
          data = {};
        }
        url = "" + api_url + url + "?";
        data = this.prepareRequest(data);
        return $.post(url, data, okay).promise();
      },
      get: function(url, data, okay) {
        if (data == null) {
          data = {};
        }
        url = "" + api_url + url + "?";
        data = this.prepareRequest(data);
        return $.get(url + data, okay).promise();
      }
    };
    return (window.Rest = Rest);
  });

}).call(this);
