(function() {
  "use strict";
  var $build_template, $progress_template, $research_template, $training_template, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"build-order\">\n    <div id=\"build\">{{{build_template}}}</div>\n    <div id=\"research\">{{{research_template}}}</div>\n    <div id=\"training\">{{{training_template}}}</div>\n</div>";

  $build_template = "<div class=\"build-order-content\">\n    <div class=\"build-order-box build-box\">\n        <div class=\"order-info\">\n            <span class=\"name\">{{lang name}}</span>\n            <span class=\"status\">{{title}}</span><br/>\n            <span id=\"time-left\" class=\"time\">{{time}}</span>\n            <div class=\"order-progress-bar\">\n                <div class=\"order-inner-progress-bar\">\n                    {{{time_left}}}\n                </div>\n            </div>\n        </div>\n        <div class=\"order-content\">\n            <img width=\"89px\" src={{sprite}}>\n            <div class=\"order-button\">\n                <span>Finish</span>\n            </div>\n            <div class=\"order-button\">\n                <span>Cancel</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"build-order-tab tab-build\"></div>\n</div>";

  $research_template = "<div class=\"build-order-content\">\n    <div class=\"build-order-box research-box\">\n        <div class=\"order-info\">\n            <span class=\"name\">{{lang name}}</span>\n            <span class=\"status\">{{title}}</span><br/>\n            <span id=\"time-left\" class=\"time\">{{time}}</span>\n            <div class=\"order-progress-bar\">\n                <div class=\"order-inner-progress-bar\">\n                    {{{time_left}}}\n                </div>\n            </div>\n        </div>\n        <div class=\"order-content\">\n            <img width=\"89px\" src={{sprite}}>\n            <div class=\"order-button\">\n                <span>Finish</span>\n            </div>\n            <div class=\"order-button\">\n                <span>Cancel</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"build-order-tab tab-research\"></div>\n</div>";

  $training_template = "<!-- training -->\n<div class=\"build-order-content\">\n    <div class=\"build-order-box training-box\">\n        <div class=\"order-info\">\n            <span class=\"name\">{{lang name}}</span>\n            <span class=\"status\">{{title}}</span><br/>\n            <span id=\"time-left\" class=\"time\">{{time}}</span>\n            <div class=\"order-progress-bar\">\n                <div class=\"order-inner-progress-bar\">\n                    {{{time_left}}}\n                </div>\n            </div>\n        </div>\n        <div class=\"order-content\">\n            <img width=\"89px\" src={{sprite}} />\n            <div class=\"order-button\">\n                <span>Finish</span>\n            </div>\n            <div class=\"order-button\">\n                <span>Cancel</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"build-order-tab tab-training\"></div>\n</div>";

  $progress_template = "<div id=\"progress-bar\" style=\"width:{{time_left}}%\"></div>";

  define(["jquery-ui", "soa/hud/IntervalQueue", "soa/hud/dialogs/BaseDialog"], function($, IntervalQueue, BaseDialog) {
    var BuildOrders, REFRESH_TIME;
    REFRESH_TIME = 1000;
    BuildOrders = (function(_super) {
      __extends(BuildOrders, _super);

      function BuildOrders() {
        BuildOrders.__super__.constructor.call(this, html);
      }

      return BuildOrders;

    })(BaseDialog);
    BuildOrders.prototype.init = function() {
      return this.setTemplates({
        "training_template": $training_template,
        "build_template": $build_template,
        "research_template": $research_template,
        "progress_template": $progress_template
      });
    };
    BuildOrders.prototype.attachListeners = function() {
      var build_tab, dlg, research_tab, training_tab;
      dlg = this.html;
      build_tab = dlg.find(".tab-build");
      build_tab.unbind("click");
      build_tab.click(function() {
        dlg.find(".build-box").toggle();
        dlg.find(".tab-build").toggleClass("tab-build-click");
        dlg.find(".research-box").hide();
        dlg.find(".tab-research").removeClass("tab-research-click");
        dlg.find(".workshop-box").hide();
        dlg.find(".tab-workshop").removeClass("tab-workshop-click");
        dlg.find(".training-box").hide();
        dlg.find(".tab-training").removeClass("tab-training-click");
        dlg.find(".tab-training").removeClass("tab-training2");
        return dlg.find(".tab-workshop").removeClass("tab-workshop2");
      });
      training_tab = dlg.find(".tab-training");
      training_tab.unbind("click");
      training_tab.click(function() {
        dlg.find(".training-box").toggle();
        dlg.find(".tab-training").toggleClass("tab-training-click");
        dlg.find(".build-box").hide();
        dlg.find(".tab-build").removeClass("tab-build-click");
        dlg.find(".workshop-box").hide();
        dlg.find(".tab-workshop").removeClass("tab-workshop-click");
        dlg.find(".research-box").hide();
        dlg.find(".tab-research").removeClass("tab-research-click");
        dlg.find(".tab-training").removeClass("tab-training2");
        dlg.find(".tab-workshop").toggleClass("tab-workshop2");
        return dlg.find(".tab-workshop").removeClass("tab-workshop3");
      });
      research_tab = dlg.find(".tab-research");
      research_tab.unbind("click");
      return research_tab.click(function() {
        dlg.find(".research-box").toggle();
        dlg.find(".tab-research").toggleClass("tab-research-click");
        dlg.find(".build-box").hide();
        dlg.find(".tab-build").removeClass("tab-build-click");
        dlg.find(".workshop-box").hide();
        dlg.find(".tab-workshop").removeClass("tab-workshop-click");
        dlg.find(".training-box").hide();
        dlg.find(".tab-training").removeClass("tab-training-click");
        dlg.find(".tab-training").toggleClass("tab-training2");
        return dlg.find(".tab-workshop").toggleClass("tab-workshop3");
      });
    };
    BuildOrders.prototype.formatMillSeconds = function(msecs) {
      var date, hours, minutes, seconds;
      date = new Date(0);
      date.setHours(0);
      date.setMilliseconds(msecs);
      minutes = "" + (date.getMinutes());
      if (minutes.length === 1) {
        minutes = "0" + minutes;
      }
      hours = "" + (date.getHours());
      if (hours.length === 1) {
        hours = "0" + hours;
      }
      seconds = "" + (date.getSeconds());
      if (seconds.length === 1) {
        seconds = "0" + seconds;
      }
      return "" + hours + ":" + minutes + ":" + seconds;
    };
    BuildOrders.prototype.formatSeconds = function(secs) {
      var res;
      res = this.formatMillSeconds(secs * 1000);
      return res;
    };
    BuildOrders.prototype.attachTimerFunctors = function(obj, duration, buildTime, callb) {
      var decDur;
      obj.decreaseDuration = decDur = function() {
        duration--;
        if (duration <= 0) {
          callb.call(obj);
          return IntervalQueue.clear(1000, decDur);
        }
      };
      obj.timeLeft = function() {
        return duration;
      };
      obj.timeLeftPercentage = function() {
        var out;
        out = (1 - (this.timeLeft() / buildTime) * 1000).toFixed(2);
        return out;
      };
      return obj;
    };
    BuildOrders.prototype.addToQueue = function(obj, spriteURI, name, duration, buildTime, tempName, title) {
      var $deferred, dataN, ref, tleft_data,
        _this = this;
      $deferred = new $.Deferred();
      obj = this.attachTimerFunctors(obj, duration, buildTime, function() {
        return $deferred.resolve();
      });
      IntervalQueue.addToQueue(obj.decreaseDuration, 1000);
      tleft_data = this.compileTemplate("progress_template", "time_left", obj.timeLeftPercentage());
      ref = this.get("refresh_" + tempName);
      if (ref != null) {
        IntervalQueue.clear(REFRESH_TIME, ref);
      }
      dataN = "" + tempName + "_queue";
      this.setData(dataN, {
        name: name,
        title: title,
        time: this.formatSeconds(obj.timeLeft()),
        sprite: spriteURI,
        time_left: tleft_data
      });
      this.compile("" + tempName + "_template", dataN).then(function(data) {
        var $prbar, $tleft, refresh;
        _this.refresh(tempName, data);
        _this.attachListeners();
        _this.show();
        $tleft = $(_this.html.find("#time-left"));
        $prbar = $(_this.html.find("#" + tempName + " #progress-bar"));
        refresh = function(id) {
          var tleft;
          tleft = obj.timeLeftPercentage() * 100;
          if (tleft >= 100) {
            IntervalQueue.clear(REFRESH_TIME, refresh);
            _this.html.find("#" + tempName).empty();
          }
          $prbar.css("width", tleft + "%");
          return $tleft.text(_this.formatSeconds(obj.timeLeft()));
        };
        _this.set("refresh_" + tempName, refresh);
        return IntervalQueue.addToQueue(refresh, REFRESH_TIME);
      });
      return $deferred.promise();
    };
    BuildOrders.prototype.addConstructionToBuildQueue = function(constr, title) {
      var buildTime;
      buildTime = 0.2 * 60 * 1000;
      return this.addToQueue(constr, Handlebars.helpers.concat_construction(constr.c_type), constr.c_type, constr.duration, buildTime, "build", title);
    };
    BuildOrders.prototype.addConstructionToUpgradeQueue = function(constr, title) {
      var buildTime;
      buildTime = 0.2 * 60 * 1000;
      return this.addToQueue(constr, Handlebars.helpers.concat_construction(constr.c_type), constr.c_type, constr.duration, buildTime, "build", title);
    };
    BuildOrders.prototype.addPOIToEncampmentQueue = function(poi, title) {
      var buildTime;
      buildTime = 0.2 * 60 * 1000;
      return this.addToQueue(poi, Handlebars.helpers.concat_poi(poi.c_type), poi.c_type, poi.duration, buildTime, "build", title);
    };
    BuildOrders.prototype.addUnitToTrainingQueue = function(unit, title) {
      var buildTime;
      buildTime = 0.2 * 60 * 1000;
      return this.addToQueue(unit, Handlebars.helpers.concat_unit_avatar(unit.type, "44x44"), unit.type, unit.duration, buildTime, "training", title);
    };
    BuildOrders.prototype.addScienceToResearchQueue = function(science, bldtime, title) {
      return this.addToQueue(unit, Handlebars.helpers.concat_unit_avatar(unit.type, "44x44"), science.type, science.duration, science.buildTime, "training", title);
    };
    BuildOrders.prototype.addToResearchQueue = function(research, title, typeOfResearch) {
      var ref, tleft_data,
        _this = this;
      if (typeOfResearch == null) {
        typeOfResearch = null;
      }
      tleft_data = this.compileTemplate("progress_template", "time_left", research.timeLeftPercentage() * 100);
      ref = this.get("refresh_research");
      if (ref != null) {
        IntervalQueue.clear(REFRESH_TIME, ref);
      }
      this.setData("research_queue", {
        name: research.name,
        title: title,
        time: this.formatMillSeconds(research.timeLeft()),
        sprite: Handlebars.helpers.concat_research(research.name, typeOfResearch),
        time_left: tleft_data
      });
      return this.compile("research_template", "research_queue").then(function(data) {
        var $prbar, $tleft, refresh;
        _this.refresh("research", data);
        _this.attachListeners();
        _this.show();
        $tleft = $(_this.html.find("#time-left"));
        $prbar = $(_this.html.find("#research #progress-bar"));
        refresh = function(id) {
          var res, tleft;
          tleft = research.timeLeftPercentage() * 100;
          if (tleft >= 100) {
            IntervalQueue.clear(REFRESH_TIME, refresh);
            _this.html.find("#research").empty();
            if (typeOfResearch) {
              res = amjad.finishScienceResearch(research.name);
              res.done(function(data) {
                var science;
                science = SOA.Hud.getDialog("SCIENCE");
                if (science.opened) {
                  return science.show(amjad.empire.constructions.science);
                }
              });
            } else {
              res = amjad.finishTechResearch(research.name);
              res.done(function(data) {
                var science;
                science = SOA.Hud.getDialog("SCIENCE");
                if (science.opened) {
                  return science.show(amjad.empire.constructions.science);
                }
              });
            }
          }
          $prbar.css("width", tleft + "%");
          return $tleft.text(_this.formatMillSeconds(research.timeLeft()));
        };
        _this.set("refresh_research", refresh);
        return IntervalQueue.addToQueue(refresh, REFRESH_TIME);
      });
    };
    return BuildOrders;
  });

}).call(this);
