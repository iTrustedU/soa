(function() {
  "use strict";
  define(["soa/hud/dialogs/RegisterDialog", "soa/hud/dialogs/LoginDialog", "soa/hud/BottomMenu", "soa/hud/HeroesQueue", "soa/hud/TopMenu", "soa/hud/LiveStock", "soa/hud/ActionBox", "soa/hud/BuildOrders", "soa/hud/dialogs/TrainingGroundsDialog", "soa/hud/dialogs/TradeStationDialog", "soa/hud/dialogs/TavernDialog", "soa/hud/dialogs/TraderDialog", "soa/hud/dialogs/TradeDialog", "soa/hud/dialogs/CommanderDialog", "soa/hud/dialogs/WarDialog", "soa/hud/dialogs/MainDialog", "soa/hud/dialogs/ScienceDialog", "soa/hud/dialogs/BattleReport", "soa/hud/dialogs/ExchangeDialog", "soa/hud/dialogs/HomeDialog", "soa/hud/dialogs/CityInfoDialog", "soa/hud/dialogs/TradeDialog", "soa/hud/dialogs/WarehouseDialog"], function(RegisterDialog, LoginDialog, BottomMenu, HeroesQueue, TopMenu, LiveStock, ActionBox, BuildOrders, TrainingGroundsDialog, TradeStationDialog, TavernDialog, TraderDialog, TradeDialog, CommanderDialog, WarDialog, MainDialog, ScienceDialog, BattleReport, ExchangeDialog, HomeDialog, CityInfoDialog, TradePOIDialog, WarehouseDialog) {
    var DialogsLoader;
    DialogsLoader = (function() {
      function DialogsLoader() {
        SOA.Hud.insert("REGISTER", RegisterDialog);
        SOA.Hud.insert("LOGIN", LoginDialog);
        SOA.Hud.insert("BOTTOM_MENU", BottomMenu);
        SOA.Hud.insert("TOP_MENU", TopMenu);
        SOA.Hud.insert("BUILD_ORDERS", BuildOrders);
        SOA.Hud.insert("TAVERN", TavernDialog);
        SOA.Hud.insert("WAR", WarDialog);
        SOA.Hud.insert("HEROES_QUEUE", HeroesQueue);
        SOA.Hud.insert("TRAINING_GROUNDS", TrainingGroundsDialog);
        SOA.Hud.insert("TRADE_STATION", TradeStationDialog);
        SOA.Hud.insert("MAIN_BUILDING", MainDialog);
        SOA.Hud.insert("SCIENCE", ScienceDialog);
        SOA.Hud.insert("ACTION_BOX", ActionBox);
        SOA.Hud.insert("BATTLE_REPORT", BattleReport);
        SOA.Hud.insert("EXCHANGE_DIALOG", ExchangeDialog);
        SOA.Hud.insert("HOME", HomeDialog);
        SOA.Hud.insert("CITY_INFO", CityInfoDialog);
        SOA.Hud.insert("COMMANDER", CommanderDialog);
        SOA.Hud.insert("TRADER", TraderDialog);
        SOA.Hud.insert("TRADE_POI", TradePOIDialog);
        SOA.Hud.insert("WAREHOUSE", WarehouseDialog);
      }

      return DialogsLoader;

    })();
    return DialogsLoader;
  });

}).call(this);
