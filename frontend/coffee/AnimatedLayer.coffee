"use strict"

define ["PIXI"], 

(PIXI) ->
	TILE_HEIGHT 	= 64
	TILE_WIDTH 		= 128

	class AnimatedLayer extends PIXI.MovieClip
		constructor: (@parentTile, meta) ->
			### @todo 
				Meta textures could become just an array of strings
			###
			super(meta.textures)
			@z 	= meta.layer
			@id = meta.id
			@animationSpeed = 0.3
			@init()
		
	AnimatedLayer::init = () ->
		@y_off 		 	= (@getBounds().height - TILE_HEIGHT) * 0.5
		@position.x  	= @parentTile.position.x
		@position.y 	= @parentTile.position.y #- @y_off
		@interactive 	= true
		@_pass_me_pixi_ = true
		@hitArea 	 	= @_bounds.clone()
	
	AnimatedLayer::mouseover = () ->
		@onMouseOver() if @onMouseOver?

	AnimatedLayer::mouseout = () ->
		@onMouseOut() if @onMouseOut?

	AnimatedLayer::onClick = (btn) ->
		if btn is 0
			@onLeftClick() if @onLeftClick?
		else 
			@onRightClick() if @onRightClick?

	AnimatedLayer::changeTexture = (tex_name) ->
		texture = PIXI.TextureCache[tex_name]
		if not texture?
			throw new Error("No such texture #{tex_name}!")
		@texture 		= texture
		@y_off          = (@getBounds().height - TILE_HEIGHT) * 0.5
		@position.x     = @parentTile.position.x
		@position.y     = @parentTile.position.y - @y_off

	AnimatedLayer::changeParentTile = (parentTile, isPlaying) ->
		if @playing
			@stop()
		delete @parentTile.layers[@z]
		@parentTile = parentTile
		@parentTile.layers[@z] = @
		@y_off 		 	= (@getBounds().height - TILE_HEIGHT) * 0.5
		@position.x  	= @parentTile.position.x
		@position.y  	= @parentTile.position.y - @y_off
		@play()
	return AnimatedLayer