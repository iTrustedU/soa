"use strict"

define [
    "soa/Rest"
    "soa/StructureLoader"
], 

(Rest, StructureLoader) ->

    ShortMap =
        "*": "*"

    class Settlement extends StructureLoader
        constructor: () ->
            super(ShortMap)
            @neighs = []
            return @

    Settlement::load = (meta) ->
        super(meta)
        return @

    Settlement::initListeners = () ->
        @on "DONE_LOADING", () ->
            #console.debug "SETTLEMENT LOADED"
            #console.debug @

        HAL.on "ZONE_MAP_LOADED", () =>
            @layer = amjad.putCityOnZone(@x, @y, "city", "amjad/constructions/#{amjad.user.architecture}/")
            @neighs = @findNeighbours()
            @layer.onLeftClick = () ->
                SOA.goToCityView()
            HAL.trigger "MY_SETTLEMENT_LOADED", @
            #SOA.centerOnZone()

    Settlement::putHeroNearby = (hero_texture) ->
        tile = @neighs.pop()
        return SOA.Zone.map.addAnimatedLayer(tile.row, tile.col, hero_texture)
    
    Settlement::isInNeighbourhood = (row, col) ->
        for neigh in @findNeighbours()
            if neigh.row is row and neigh.col is col
                return true
        return false
    
    Settlement::findNeighbours = () ->
        size = ~~((@layer.width/128)+1)

        # SOA.Zone.map.getSpanArea(
        #     SOA.Zone.map.getTile(row, col), new Array(size*size+1).join().split('')
        #     .map(-> return 1).join().replace(/,/g,'')
        # )

        out = []
        pivot = SOA.Zone.map.getTile(@x + 1, @y)
        sw = SOA.Zone.map.findInDirectionOf(pivot, "northwest", size)
        se = SOA.Zone.map.findInDirectionOf(pivot, "northeast", size)
        lw = sw[sw.length-1]
        le = se[se.length-1]
        swe = SOA.Zone.map.findInDirectionOf(lw, "northeast", size)
        sew = SOA.Zone.map.findInDirectionOf(le, "northwest", size)
        out = out.concat(sw).concat(swe).concat(sew).concat(se)
        dupes = []
        for t in out
            if not dupes[t.id]? 
                dupes[t.id] = t
                out.push(t)
        # SOA.Zone.map.tintSpan(filt, 2)
        return out

    return Settlement
