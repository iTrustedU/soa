"use strict"

tile_html =
"""
<li id={{id}} class="selectable-box tile" title={{tooltip}}>
    <img id={{id}} src={{img_url}} class="tile-img">
    <span id={{id}} class="selectable-title">
        {{title}}
    </span>
</li>
"""

delete_btn_html = """
    <i id="map-delete" class="fa fa-times tile-delete-btn"></i>
"""

tile_tpl = Handlebars.compile(tile_html)

define ["editor/BaseDialog", "MapConfig", "TileMetaStorage", "editor/TileEditDialog"],

(BaseDialog, MapConfig, TileMetaStorage, TileEditDialog) ->

    API =
        TILES_LIST: "/tiles"
        TILE_SAVE: "/tiles/new"
        TILE_DELETE: "/tile/{0}/delete"

    class TilesDialog extends BaseDialog
        constructor: () ->
            super("Tiles", "tiles-dialog")

    TilesDialog::init = () ->
        @storage            = new TileMetaStorage()
        @current_selected   = -1

        @showTiles()
        @addTools()
        @content.droppable(
            accept: ".draggable-sprite"
            activeClass: "border-active"
            drop: (ev, ui) =>
                tile_spr_wrapper = ui.draggable.clone()
                tile_spr_wrapper.addClass "border-active"
                @edit.dropSprite(tile_spr_wrapper)
        )

        @edit = new TileEditDialog()
        @edit.html.hide()

        @delete_btn = $(delete_btn_html)
        @delete_btn.on "click", () =>
            id = @current_selected
            $.post(API.TILE_DELETE.f(id))
            .then () =>
                @html.find("li##{id}").remove()
        @addToToolbox(@delete_btn)

    TilesDialog::initListeners = () ->
        super()
        @edit.on "TILE_SAVED", (tile) =>
            $.post(API.TILE_SAVE, tile)
            .then (tile) => @addTile(tile)

    TilesDialog::showTiles = () ->
        $.get(API.TILES_LIST)
        .done (@tiles) =>
            @content.empty()
            for tile in @tiles
                @addTile(tile)

    TilesDialog::addTile = (tile) ->
        @storage.addTile(tile)
        tooltip = tile.name
        
        $tile_box = $(tile_tpl(
            id: tile.seedId
            title: tile.name
            tooltip: tooltip
            img_url: tile.sprite
        ))

        $tile_box.on "click", (ev, ui) =>
            el = $(ev.target)
            @current_selected = el.attr("id")
            @trigger "TILE_SELECTED", @, @storage.findByID(@current_selected)
            @html.find(".tile-img").removeClass "border-active"
            el.addClass "border-active"

        $tile_box.on "dblclick", (ev, ui) =>
            el = $(ev.target)
            el.removeClass "border-active"
            @edit.showTile(el.parent())

        @content.append($tile_box)

    TilesDialog::addTools = () ->
        @addToToolbox(@createLayerCircleButtons())

    TilesDialog::createLayerCircleButtons = () ->
        out = ""
        for i in [0...MapConfig.MAX_LAYERS]
            out +=
            """
                <div id="layer" layer='#{i}' class='circle'>
                    <span id="layer-text">#{i}</span>
                </div>
            """
        return out
    
    return TilesDialog
