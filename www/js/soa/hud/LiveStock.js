(function() {
  "use strict";
  var $camels, $livestock, html;

  html = "<div class=\"live-stock\">\n    <div class=\"live-stock-content\">\n        <div class=\"live-stock-box\">\n            <div class=\"live-stock-tabs\">\n                <div class=\"chat-tab\">\n                    <div class=\"chat-tab-l\"></div>\n                    <span class=\"chat-tab-middle\">Livestock</span>\n                    <div class=\"chat-tab-r\"></div>\n                </div>\n                <div class=\"chat-tab\">\n                    <div class=\"chat-tab-l\"></div>\n                    <span class=\"chat-tab-middle\">Horses</span>\n                    <div class=\"chat-tab-r\"></div>\n                </div>\n                <div class=\"chat-tab\">\n                    <div class=\"chat-tab-l\"></div>\n                    <span class=\"chat-tab-middle\">Camel</span>\n                    <div class=\"chat-tab-r\"></div>\n                </div>\n            </div>\n            {{{livestock}}}\n            {{{camels}}}\n        </div>\n        <div class=\"live-stock-tab\">\n            <div class=\"chat-tab-t\"></div>\n            <span class=\"chat-tab-middle-v\">L<br />i<br />v<br />e<br /><br />s<br />t<br />o<br />c<br />k</span>\n            <div class=\"chat-tab-b\"></div>\n        </div>\n    </div>\n</div>";

  $camels = "<div class=\"camels\">\n    <div class=\"camels-button\">Stats</div>\n    <table>\n        <tr>\n            <th>Weekly Growth</th>\n            <th>Food Rate</th>\n            <th>Number</th>\n        </tr>\n        <tr>\n            <td>{{weekly_growth}}<br /> Per Week</td>\n            <td>{{food_rate}}<br /> Per Hour</td>\n            <td>+ {{animal_type}} {{animals}}/1000</td>\n        </tr>\n    </table>\n</div>";

  $livestock = "<div class=\"livestock\">\n    <table>\n        <tr>\n            <th>Goods</th>\n            <th>Weekly Growth</th>\n            <th>Food Rate</th>\n            <th>Animals</th>\n        </tr>\n        <tr>\n            <td>\n                {{goods_1}}<br />\n                {{goods_2}}<br />\n                {{goods_3}}<br />\n                {{goods_4}}<br />\n            </td>\n            <td>{{weekly_growth}}<br /> Per Week</td>\n            <td>{{food_rate}}<br /> Per Hour</td>\n            <td>+ {{animal_type}} {{animals}}/1000</td>\n        </tr>\n    </table>\n</div>";

  define(["jquery"], function($) {
    var LiveStock;
    LiveStock = (function() {
      function LiveStock() {}

      return LiveStock;

    })();
    return LiveStock.prototype.constructor = function() {
      var $LiveStock, camels, cm, livestock, ls, template;
      camels = {
        weekly_growth: 9,
        food_rate: 199,
        animals: 499,
        animal_type: "Majaheem"
      };
      livestock = {
        weekly_growth: 9,
        food_rate: 199,
        animals: 499,
        animal_type: "Sheep",
        goods_1: "Milk",
        goods_2: "Skin",
        goods_3: "Wool",
        goods_4: "Meet"
      };
      template = Handlebars.compile($camels);
      cm = template(camels);
      template = Handlebars.compile($livestock);
      ls = template(livestock);
      template = Handlebars.compile(html);
      $LiveStock = $(template({
        camels: cm,
        livestock: ls
      }));
      Hal.trigger("DOM_ADD", function(domlayer) {
        $(domlayer).append($LiveStock);
        return $LiveStock.find(".live-stock-tab").click(function() {
          return $LiveStock.find(".live-stock-box").toggle("fast", function() {
            var content;
            content = $LiveStock.find(".live-stock-content");
            if (content.css("width") === "24px") {
              return content.css("width", "375px");
            } else {
              return content.css("width", "24px");
            }
          });
        });
      });
      return Hal.on("OPEN_LIVE_STOCK", function(livestock) {
        return $LiveStock.show();
      });
    };
  });

}).call(this);
