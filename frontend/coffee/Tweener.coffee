"use strict"

define [], () ->

	Tweeners = 
		Linear: (t, start, rate, duration) ->
			return rate*t/duration + start

		EaseInQuad: (t, start, rate, duration) ->
			t /= duration
			return rate*t*t + start

		EaseInCubic: (t, start, rate, duration) ->
			t /= duration
			return rate*t*t*t + start

	class Tweener
		constructor: () ->
			@t 			 = performance.now()
			@delta 		 = 0
			@old_t 		 = 0
			@num_tweens  = 0
			@cur_time 	 = 0
			@to_wait     = 0
			@animating   = false
			@paused      = false
			@done_clb    = null
			@frameID 	 = null
			@clb_ids     = []
			@tweenQueue  = []
			@tween_chain = []
			return @

		tween: (meta, callb) ->
			@num_tweens++
			meta.callb = callb
			if @to_wait > 0
				@tween_chain.push(meta)
			@animating = true
			finnclb = () =>
				ind = @clb_ids.indexOf(finnclb)
				@clb_ids.splice(ind, 1)
				@num_tweens--
				if @to_wait > 0 and @num_tweens isnt 0 and not @paused
					@to_wait--
					@tween(@tween_chain.pop())
					@num_tweens--
				if @num_tweens <= 0 and @done_clb? and not @paused
					@done_clb.call(@obj)
				if @num_tweens <= 0 and @to_wait is 0
					@animating = false
			@clb_ids.push(finnclb)
			@update(meta, finnclb)
			return @

		update: (meta, finnclb) ->
			meta.finnclb = finnclb
			if not meta.method?
				meta.method = Tweeners.Linear
			
			#sign = +((meta.from - meta.to) < 0)
			#meta.to = meta.to * sign
			meta.val = meta.from
			# if meta.from > meta.to
			# 	c = meta.to
			# 	meta.to = meta.from
			# 	meta.from = c
			# 	meta.val = c
			@tweenQueue.push(meta)
			@t = performance.now()
			@update_()

		update_: () ->
			return if @paused
			if @tweenQueue.length is 0
				cancelAnimationFrame(@frameID)
				@cur_time = 0
				return
			@old_t = @t
			@t = performance.now()
			@delta = (@t - @old_t) * 0.001
			@cur_time += @delta
			for up in @tweenQueue
				up.val = up.method(@t, up.val, @delta, up.duration)
				if up.val >= up.to
					up.repeat--
					up.callb(up.to)
					if up.repeat <= 0
						ind = @tweenQueue.indexOf(up)
						@tweenQueue.splice(ind, 1)
						@cur_time = 0
						up.finnclb()
						return
					else
						up.val = up.from
				else
					up.callb(up.val)
			@frameID = requestAnimFrame(() => @update_())
			return @
		
		wait: (wait_clb, msecs) ->
			@to_wait++
			return @
		
		pause: () ->
			@paused = true
			return @

		resume: () ->
			@paused = false
			@t = performance.now()
			return @

		stop: () ->
			@tweenQueue = []
			@clb_ids = []
			@num_tweens = 0
			@to_wait = 0
			@animating = false
			@wait_clb = null
			@done_clb(@) if @done_clb?
			@done_clb = null
			@tween_chain = []
			@cur_time = 0
			return @

		done: (@done_clb) ->
			return @

	Tweener.Tweeners = Tweeners
	return Tweener