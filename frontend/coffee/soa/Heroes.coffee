"use strict"

define [
    "soa/StructureLoader"
    "soa/Agent"
    "soa/Trader"
    "soa/Commander"
    "soa/Trainer"
],

(StructureLoader, Agent, Trader, Commander, Trainer) ->
    
    AGENT_EXPERIENCE_MODIFIER       = 800
    TRAINER_EXPERIENCE_MODIFIER     = 600
    TRADER_EXPERIENCE_MODIFIER      = 500

    ShortMap = 
        "commanders": 
            (meta) ->
                out = {}
                for k, v of meta
                    out[k] = @addNewCommander(v)
                return out
        "trainers": 
            (meta) ->
                out = {}
                for k, v of meta
                    out[k] = @addNewTrainer(v)
                return out
        "traders": 
            (meta) ->
                out = {}
                for k, v of meta
                    out[k] = @addNewTrader(v)
                return out

        "agents": 
            (meta) ->
                out = {}
                for k, v of meta
                    out[k] = @addNewAgent(v)
                return out

    class Heroes extends StructureLoader
        constructor: () ->
            @commanders     = {}
            @trainers       = {}
            @traders        = {}
            @agents         = {}
            super(ShortMap)

        initListeners: () ->
            @on "DONE_LOADING", () ->
                ##console.debug "HEROES LOADED"
                HeroesQueue = SOA.Hud.getDialog("HEROES_QUEUE")
                HeroesQueue.putHero(h) for k, h of @traders
                HeroesQueue.putHero(h) for k, h of @agents
                HeroesQueue.putHero(h) for k, h of @commanders

        addNewCommander: (meta) ->
            cmd = new Commander("*":"*", "commander_id": "id")
            cmd.setType "commander"
            cmd.load(meta)
            #cmd.troops = {}
            @commanders[cmd.id] = cmd
            return cmd

        addNewTrainer: (meta) ->
            cmd = new Trainer("*":"*", "trainer_id":"id")
            cmd.setType "trainer"
            cmd.load(meta)
            @trainers[cmd.id] = cmd
            return cmd

        addNewAgent: (meta) ->
            cmd = new Agent("*":"*", "agent_id":"id")
            cmd.setType "agent"
            cmd.load(meta)
            @agents[cmd.id] = cmd
            return cmd
                    
        addNewTrader: (meta) ->
            cmd = new Trader("*":"*", "caravan_id":"id")
            cmd.setType "trader"
            cmd.load(meta)
            @traders[cmd.id] = cmd
            return cmd

        getCountByType: (type) ->
            count = 0
            # heroes = @commander
            # switch type
            #     heroes = @trainers when "trainers"
            # heroes = @trainers if type is "trainers"
            # heroes = @traders if type is "traders"
            # heroes = @agents if type is "agents"
            
            return 0 #Object.keys(heroes).length

        getCountAll: () ->
            count = @getCountByType('traders')+
                    @getCountByType('commanders')+
                    @getCountByType('trainers')+
                    @getCountByType('agents')
            return count

    return Heroes
