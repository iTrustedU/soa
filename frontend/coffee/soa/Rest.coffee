"use strict"

define ["jquery"], ($) ->
    
    api_url = "api/"

    Rest = 
        setSessionKey: (@session) -> return
        prepareRequest: (obj) ->
            return "session=#{@session}&jparams=#{encodeURI(JSON.stringify(obj))}"

        post: (url, data = {}, okay) ->
            url = "#{api_url}#{url}?"
            ##console.debug "Post url: #{url}"
            data = @prepareRequest(data)
            ##console.debug "Data sent: #{decodeURI(data)}"
            return $.post(url, data, okay).promise()

        get: (url, data = {}, okay) ->
            url = "#{api_url}#{url}?"
            ##console.debug "Get url: #{url}"
            data = @prepareRequest(data)
            ##console.debug "Data sent: #{decodeURI(data)}"
            return $.get(url + data, okay).promise()

    return (window.Rest = Rest)