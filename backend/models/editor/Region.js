(function() {
  var Section, mongoose, schema;

  mongoose = require("mongoose");

  Section = require("./Section");

  schema = mongoose.Schema({
    x: {
      type: Number,
      required: true
    },
    y: {
      type: Number,
      required: true
    },
    sections: [Section]
  });

  module.exports = schema;

}).call(this);
