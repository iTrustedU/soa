(function() {
  "use strict";
  var $own_goods_template, $trade_goods_template, $trade_info_template, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"trade-point draggable\">\n    <div class=\"dialog-name\">\n        <span>T R A D E &nbsp; P O I N T</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div id=\"trade-info\"> {{{trade_info}}} </div>\n    <div class=\"war-building-info\">\n        <img style=\"height:112px\" src=\"assets/sprites/amjad/poi/trade.png\" class=\"dialog-building-img\" />\n    </div>\n    <div class=\"merchant\">\n        <div class=\"sale-trade-good\">\n            <img src=\"assets/sprites/amjad/hero_avatars/traderl.png\">\n        </div>\n        <div id=\"own-goods\" class=\"trade-point-inventory2 default-skin\">\n            {{{own_goods}}}\n        </div>\n        <input type=\"text\" id=\"trader-input-amount\"/>\n        <div class=\"trade-point-slider-bar\">\n            <div class=\"slider-bar-middle trader-slider\">\n                <div class=\"inner-slider2 ui-slider-handle\"></div>\n            </div>\n        </div>\n        <div class=\"sell-all sell-button\">Sell All</div>\n        <div class=\"sell sell-button\">Sell</div>\n    </div>\n    <div class=\"trade-p\">\n        <div class=\"sale-trade-good good2\">\n            <img id=\"poi-merchant\"/>\n        </div>\n        <div id=\"trade-goods\" class=\"trade-point-inventory default-skin\">\n            {{{trade_goods}}}\n        </div>\n        <input type=\"text\" id=\"merchant-input-amount\"/>\n        <div class=\"trade-point-slider-bar\">\n            <div class=\"slider-bar-middle merchant-slider\">\n                <div class=\"inner-slider2 ui-slider-handle\"></div>\n            </div>\n        </div>\n        <div class=\"buy sell-button\">Buy</div>\n    </div>\n</div>";

  $trade_info_template = "<div class=\"load-money\">\n    <span><img src=\"img/top-resources/gold.png\"></span>\n    <span>{{getBalance}}</span>\n    <span><img src=\"img/dialog/trade-station/load.png\"></span>\n    <span>{{getLoad}}/{{capacity}}</span>\n</div>";

  $own_goods_template = "{{#each bag}}\n    <div class=\"poi-good7\" id=\"{{@key}}\">\n        <img class=\"trader-good\" src=\"{{concat_resource @key}}\"/>\n        <span class=\"goods-amount2\">{{this}}</span>\n    </div>\n{{/each}}";

  $trade_goods_template = "{{#each goods}}\n    <img class=\"poi-good merchant-good\" id=\"{{@key}}\" src=\"{{concat_resource @key}}\"/>\n{{/each}}";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var TradeDialog;
    TradeDialog = (function(_super) {
      __extends(TradeDialog, _super);

      function TradeDialog() {
        TradeDialog.__super__.constructor.call(this, html);
      }

      return TradeDialog;

    })(BaseDialog);
    TradeDialog.prototype.onPrologue = function() {
      var _me_,
        _this = this;
      this.setTemplates({
        "trade_info_template": $trade_info_template,
        "trade_goods_template": $trade_goods_template,
        "own_goods_template": $own_goods_template
      });
      this.clickTriggers({
        ".poi-good/#id": "POI_GOOD_SELECTED",
        ".poi-good7/#id": "TRADER_GOOD_SELECTED",
        ".buy/#": "BUY",
        ".sell-all/#": "SELL_ALL",
        ".sell/#": "SELL"
      });
      this.on("TRADER_GOOD_SELECTED", function(id) {
        this.html.find(".poi-good7").removeClass("trade-point-inventory-a");
        this.html.find(".poi-good7#" + id).addClass("trade-point-inventory-a");
        this.setData("parked_trader", this.ctx);
        this.setData("trade_goods", this.loadGoods());
        this.setData("trader_goods", this.traderGoods());
        this.set("TRADER_SELECTED_GOOD", id);
        this.calcMaxLoad(this.get("TRADER_SELECTED_GOOD"));
        return this.initSliderAndInput();
      });
      this.on("POI_GOOD_SELECTED", function(id) {
        this.html.find(".merchant-good").removeClass("trade-point-inventory-a");
        this.html.find(".merchant-good#" + id).addClass("trade-point-inventory-a");
        this.set("MERCHANT_SELECTED_GOOD", id);
        this.setData("parked_trader", this.ctx);
        this.setData("trade_goods", this.loadGoods());
        this.setData("trader_goods", this.traderGoods());
        this.calcMaxLoad(this.get("MERCHANT_SELECTED_GOOD"));
        return this.initSliderAndInput();
      });
      this.on("SELL_ALL", function() {
        var amnt, good;
        good = _this.get("TRADER_SELECTED_GOOD");
        amnt = _this.get("TRADER_INPUT_AMOUNT");
        return Rest.post("caravan.sell", {
          cid: _this.ctx.caravan_id,
          tid: _this.ctx.trader_id,
          res: _this.traderGoods().bag
        }).done(function(data) {
          _this.ctx.emptyBag();
          return _this.recompile();
        }).fail(function(data) {
          return console.error(data);
        });
      });
      this.on("BUY", function() {
        var amnt, good, resource;
        good = _this.get("MERCHANT_SELECTED_GOOD");
        amnt = _this.get("MERCHANT_INPUT_AMOUNT");
        resource = {};
        resource[good] = amnt;
        return Rest.post("caravan.buy", {
          cid: _this.ctx.caravan_id,
          res: resource
        }).done(function(data) {
          _this.ctx.putInBag(good, amnt, +XMLStore.tradegoods[good].value);
          return _this.recompile();
        }).fail(function(data) {
          return console.error(data);
        });
      });
      this.on("SELL", function() {
        var amnt, good, resource;
        good = _this.get("TRADER_SELECTED_GOOD");
        amnt = _this.get("TRADER_INPUT_AMOUNT");
        resource = {};
        resource[good] = amnt;
        return Rest.post("caravan.sell", {
          cid: _this.ctx.caravan_id,
          tid: _this.ctx.trader_id,
          res: resource
        }).done(function(data) {
          _this.ctx.takeFromBag(good, amnt, +XMLStore.tradegoods[good].value);
          return _this.recompile();
        }).fail(function(data) {
          return console.error(data);
        });
      });
      this.on("PRE_SHOW", _me_ = function() {
        _this.html.find("#building-avatar").attr("src", Handlebars.helpers.concat_poi("trade"));
        /* @todo odkomentuj*/

        if (_this.ctx == null) {
          console.error("No parked hero");
          _this.hide();
        }
        _this.loadGoods();
        return _this.remove("PRE_SHOW", _me_);
      });
      this.on("SHOW", function() {
        var t;
        t = SOA.Zone.map.getTile(_this.ctx.o_coord_x, _this.ctx.o_coord_y);
        if (_this.ctx == null) {
          _this.disable();
          return;
        }
        _this.set("MERCHANT_SELECTED_GOOD", "dates");
        _this.alertedNoMoney = false;
        return _this.recompile();
      });
      this.closeable();
      return this.draggable();
    };
    TradeDialog.prototype.recompile = function() {
      var _this = this;
      this.setData("parked_trader", this.ctx);
      this.setData("trade_goods", this.loadGoods());
      this.setData("trader_goods", this.traderGoods());
      this.compile("trade_info_template", "parked_trader").then(function(data) {
        _this.refresh("trade-info", data);
        _this.html.find(".trade-point-inventory").customScrollbar({
          skin: "default-skin-red",
          vScroll: true,
          wheelSpeed: 60
        });
        return _this.initSliderAndInput();
      });
      this.compile("trade_goods_template", "trade_goods").then(function(data) {
        _this.refresh("trade-goods", data);
        _this.html.find(".trade-point-inventory").customScrollbar({
          skin: "default-skin-red",
          vScroll: true,
          wheelSpeed: 60
        });
        return _this.initSliderAndInput();
      });
      this.compile("own_goods_template", "trader_goods").then(function(data) {
        _this.refresh("own-goods", data);
        _this.html.find(".trade-point-inventory2").customScrollbar({
          skin: "default-skin-red",
          vScroll: true,
          wheelSpeed: 60
        });
        return _this.initSliderAndInput();
      });
      this.html.find(".trade-point-inventory").customScrollbar({
        skin: "default-skin-red",
        vScroll: true,
        wheelSpeed: 60
      });
      return this.html.find(".trade-point-inventory").customScrollbar({
        skin: "default-skin-red",
        vScroll: true,
        wheelSpeed: 45
      });
    };
    TradeDialog.prototype.traderGoods = function() {
      var k, out, v, _ref;
      out = {};
      _ref = this.ctx.bag;
      for (k in _ref) {
        v = _ref[k];
        if (k === "balance") {
          continue;
        }
        out[k] = v;
      }
      return {
        "bag": out
      };
    };
    TradeDialog.prototype.loadGoods = function() {
      var good, goods, guri, k, spr, _ref;
      goods = {};
      _ref = XMLStore.get("tradegoods");
      for (k in _ref) {
        good = _ref[k];
        guri = ("assets/sprites/amjad/resources/" + k + ".png").trim();
        spr = PIXI.TextureCache[guri];
        if (spr != null) {
          goods[k] = good;
        }
      }
      return {
        "goods": goods
      };
    };
    TradeDialog.prototype.initSliderAndInput = function() {
      var merchSlider, self, tradeSlider,
        _this = this;
      this.set("MERCHANT_INPUT_AMOUNT", 0);
      this.set("TRADER_INPUT_AMOUNT", 1);
      this.set("MERCHANT_INPUT", this.html.find("#merchant-input-amount"));
      this.set("TRADER_INPUT", this.html.find("#trader-input-amount"));
      this.get("MERCHANT_INPUT").val(0);
      this.get("TRADER_INPUT").val(1);
      self = this;
      merchSlider = this.html.find(".merchant-slider");
      merchSlider.slider({
        min: 0,
        max: self.calcMaxLoad(this.get("MERCHANT_SELECTED_GOOD")),
        orientation: "horizontal",
        slide: function(ev, ui) {
          _this.get("MERCHANT_INPUT").val(ui.value);
          return _this.set("MERCHANT_INPUT_AMOUNT", ui.value);
        }
      });
      merchSlider.slider('value', 0);
      tradeSlider = this.html.find(".trader-slider");
      tradeSlider.slider({
        min: 1,
        max: this.ctx.bag[self.get("TRADER_SELECTED_GOOD")],
        orientation: "horizontal",
        slide: function(ev, ui) {
          _this.get("TRADER_INPUT").val(ui.value);
          return _this.set("TRADER_INPUT_AMOUNT", ui.value);
        }
      });
      return tradeSlider.slider('value', 1);
    };
    TradeDialog.prototype.updateInputField = function() {
      var x, y;
      x = this.get("MERCHANT_INPUT_AMOUNT");
      return y = this.get("TRADER_INPUT_AMOUNT");
    };
    TradeDialog.prototype.calcMaxLoad = function(good) {
      var balance, cap, diff, maxB, maxW, price, weight;
      cap = this.ctx.getCapacity();
      diff = cap.capacity - cap.load;
      weight = +XMLStore.tradegoods[good].weight;
      console.debug("weight is: " + weight);
      maxW = Math.floor(diff / weight);
      console.debug("Maxw: " + maxW);
      balance = this.ctx.getBalance();
      price = +XMLStore.tradegoods[good].value;
      if (balance < price && !this.alertedNoMoney) {
        this.alertedNoMoney = true;
        alert("There's not enough gold on your caravan");
      }
      maxB = Math.floor(balance / price);
      return Math.min(maxW, maxB);
    };
    TradeDialog.prototype.calcCostValue = function(good) {
      var val;
      val = +XMLStore.tradegoods[good].value;
      return this.get("MERCHANT_INPUT_AMOUNT") * val;
    };
    return TradeDialog;
  });

}).call(this);
