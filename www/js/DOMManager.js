(function() {
  "use strict";
  var __slice = [].slice;

  define([], function() {
    var DOMManager;
    DOMManager = (function() {
      function DOMManager() {
        this.renderspace = document.getElementById("renderspace");
        this.hud = document.getElementById("viewport");
        this.viewport = document.getElementById("viewport");
        this.game = document.getElementById("game");
        this.area = this.viewport.getBoundingClientRect();
        this.default_zindex = 1000;
        this.canvases = {};
        this.in_fullscreen = false;
        this.screen_w = window.screen.width;
        this.screen_h = window.screen.height;
        this.fullscreen_scale = [1.0, 1.0];
      }

      return DOMManager;

    })();
    DOMManager.prototype.createCanvas = function(width, height, z, transp) {
      var canvas, ind;
      if (width == null) {
        width = this.area.width;
      }
      if (height == null) {
        height = this.area.height;
      }
      if (z == null) {
        z = 0;
      }
      ind = this.default_zindex + z;
      canvas = document.createElement("canvas");
      canvas.width = width;
      canvas.height = height;
      if (!transp) {
        canvas.style["background"] = "white";
      } else {
        canvas.style["background-color"] = "transparent";
        canvas.style["background"] = "transparent";
      }
      canvas.style["z-index"] = ind;
      return canvas;
    };
    DOMManager.prototype.createCanvasLayer = function(width, height, z, transp) {
      var canvas, ind;
      if (width == null) {
        width = this.area.width;
      }
      if (height == null) {
        height = this.area.height;
      }
      ind = this.default_zindex + z;
      if (this.canvases[ind]) {
        return this.canvases[ind];
      }
      canvas = this.createCanvas(width, height, z, transp);
      return canvas;
    };
    DOMManager.prototype.addCanvas = function(canvas, x, y) {
      var z;
      if (x == null) {
        x = 0;
      }
      if (y == null) {
        y = 0;
      }
      z = canvas.style["z-index"];
      canvas.style["left"] = "" + x + "px";
      canvas.style["top"] = "" + y + "px";
      if (this.canvases[z]) {
        console.warn("Canvas with z-index of " + z + " already exists");
        return;
      }
      this.canvases[z] = canvas;
      this.viewport.appendChild(canvas);
      return canvas;
    };
    DOMManager.prototype.removeCanvasLayer = function(z) {
      var ind;
      ind = this.default_zindex + (+z);
      console.info("Removing canvas layer at z-index: " + ind + " / " + z);
      this.viewport.removeChild(this.canvases[ind]);
      return delete this.canvases[ind];
    };
    DOMManager.prototype.clearInfoBox = function() {
      var spans,
        _this = this;
      spans = this.viewport.getElementsByClassName("infotext");
      spans = [].slice.call(spans);
      return spans.forEach(function(span) {
        return _this.viewport.removeChild(span);
      });
    };
    DOMManager.prototype.updateTextOn = function(publisher, type, text) {
      var span;
      span = document.createElement("p");
      span.setAttribute("class", "infotext");
      publisher.on(type, function() {
        var args;
        args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
        return span.innerHTML = text.f(args);
      });
      return this.viewport.appendChild(span);
    };
    return DOMManager;
  });

}).call(this);
