"use strict"

define ["EventDispatcher", "Ajax"], 

(EventDispatcher, Ajax) ->

	class TileMetaStorage
		constructor: () ->
			@tile_layer_map = {}
			@tile_name_map  = {}
			@tile_id_map    = {}
			
	TileMetaStorage::load = (file_uri) ->
		Ajax.get(file_uri).success (tiles) =>
			#console.debug "Loaded tiles from file: #{file_uri}"
			tiles = JSON.parse(tiles)
			@loadTiles(tiles)

	TileMetaStorage::onComplete = () ->
		#console.debug "Tile metas downloaded"
		return

	TileMetaStorage::loadTiles = (tiles) ->
		for i, t of tiles
			@addTile(t)
		@onComplete(tiles)

	TileMetaStorage::addTile = (tile) ->
		t = @tile_name_map[tile.name]
		if t?
			delete @tile_layer_map[t.layer][t.name]
		@tile_name_map[tile.name] = tile
		if @tile_id_map[tile.seedId]?
			throw new Error("Duplicate tile meta id detected: #{tile.name}/#{tile.seedId}")
		@tile_id_map[tile.seedId] = tile
		if not @tile_layer_map[tile.layer]?
			@tile_layer_map[tile.layer] = {}
		@tile_layer_map[tile.layer][tile.name] = tile

	TileMetaStorage::getAllByLayer = (layer) ->
		return @tile_layer_map[layer] || {}

	TileMetaStorage::findByName = (name) ->
		t = @tile_name_map[name]
		return t

	TileMetaStorage::findByID = (id) ->
		t = @tile_id_map[id]
		return t

	TileMetaStorage::removeByName = (name) ->
		t = @tile_name_map[name]
		delete @tile_layer_map[t.layer][t.name]
		delete @tile_name_map[t.name]
		delete @tile_id_map[t.seedId]
		t = null

	TileMetaStorage::removeById = (id) ->
		t = @tile_id_map[id]
		delete @tile_layer_map[t.layer][t.name]
		delete @tile_id_map[t.seedId]
		delete @tile_name_map[t.name]
		t = null

	return TileMetaStorage
	