"use strict"

define ["soa/hud/HUDManager", "soa/Session", "soa/XMLStore", "soa/Amjad", "MapConfig", "soa/hud/dialogs/SpinnerOverlay"], 

(HUDManager, Session, XMLStore, Amjad, MapConfig, SpinnerOverlay) ->
	
	MAPS_URIS = [
		{
			"name": "Zone",
			"url": "map/74_75"
		},
		{
			"name": "City"
			"url": "map/10_20"
		}
	]

	SPRITES_URI      = "assets/sprites.list"
	TILES_META_URI  = "map/tiles.list"
	POI_SPRITES_URI = "amjad/poi/"

	class SOAManager
		constructor: () ->
			@init()
	
	SOAManager::init = () ->
		@currentMap     = null
		@Hud            = new HUDManager()
		@Zone           = new HAL.IsometricScene("Zone")
		@City           = new HAL.IsometricScene("City")
		@Session        = new Session()
		@getDialog      = Function.prototype.bind.call(@Hud.getDialog, @Hud)
		
		@Config         = MapConfig
		@Maps           = {}
		@Settlements    = {}
		@sessionData    = null
		@attachedLayer  = null
		@selectedHero   = null

		@pois = {}

		@initListeners()

	SOAManager::onAssetProgress = (asset) ->
		HAL.trigger "ASSETS_DOWNLOAD_PROGRESSED"

	SOAManager::onAssetsComplete = () ->
		HAL.trigger "ASSETS_DOWNLOADED"
		HAL.Animations.load()

	SOAManager::enableDebug = () ->
		HAL.Dom.putTextInInfoBox("MOUSE_MOVED", "Mouse position: %0, %1")
		HAL.Dom.putTextInInfoBox("MOUSE_WORLD_UPDATED", "Mouse world position: %0, %1")
		HAL.Dom.putTextInInfoBox("MOUSE_OVER_TILE", "Tile: %0, %1")
		
	###
		Downloads all necessary resources 
		-sprites/spritesheets/animations
		-tilemetas
		-map files
		-markers
		-xml (and json) meta files
	###
	SOAManager::start = () ->
		SOA.Hud.prologue()
		window.amjad    = new Amjad()
		@_spinner_      = new SpinnerOverlay()
		@logMeIn()

	SOAManager::initListeners = () ->
		HAL.on "ZONE_MAP_LOADED", () =>
			@putAllPOIS()
			@createClouds()

		HAL.on "MOUSE_CLICK", (data) =>
			if @currentMap isnt @Zone or not @selectedHero?
				return
			if data.originalEvent.button is 2
				@selectedHero.showActionBox(data)

	SOAManager::logMeIn = () ->
		$deferredSpinner = new $.Deferred()
		$deferred = new $.Deferred()
		@_spinner_.start(@Hud.domElement[0], $deferredSpinner)
		@validateSession()
		.done((sessionData) =>
			@sessionData = sessionData
			@downloadResources()
			.done () => @downloadXML()
			.done () => $deferred.resolve()
		).fail(() =>
			$deferred.reject("Couldn't login to game")
			$deferredSpinner.resolve()
			@Hud.showDialog("LOGIN")
		)
		$deferredA = new $.Deferred()
		$deferred.done () =>
			@logMeIntoGame()
			.done () =>
				$deferredA.resolve()
				$deferredSpinner.resolve()
		return $deferredA.promise()

	SOAManager::downloadResources = () ->
		$deferred = new $.Deferred()
		@downloadAssets()
		.done () => @downloadTilesMeta()
		.done () => @downloadMaps()
		.done () =>
			$deferred.resolve()
		return $deferred.promise()

	### Download assets ###
	SOAManager::downloadAssets = () ->
		$request = $.get(SPRITES_URI)
		$deferred = new $.Deferred()
		$request.done (data) =>
			sheetLoader = new PIXI.AssetLoader(data.split("\n"))
			sheetLoader.onProgress = (item) =>
				@onAssetProgress(item)
				$deferred.progress(item)
			sheetLoader.onComplete = () => 
				@onAssetsComplete()
				$deferred.resolve()
			sheetLoader.load()
		.fail () =>
			$deferred.reject("Download of #{SPRITES_URI} failed")
		return $deferred.promise()

	### Download tile metas ###
	SOAManager::downloadTilesMeta = () ->
		$deferred = new $.Deferred()
		$request = $.getJSON(TILES_META_URI)
		$request
		.done (tile_metas) =>
			HAL.TileMetaStorage.loadTiles(tile_metas)
			$deferred.resolve()
		.fail (tile_metas) =>
			$deferred.reject("Download of tiles meta failed")
		return $deferred.promise()

	### Download zone/city maps ###
	SOAManager::downloadMaps = () ->
		map_list = MAPS_URIS.slice()
		num_to_download = map_list.length
		$deferred = new $.Deferred()
		while (to_download = map_list.pop())?
			url = to_download.url
			map_name = to_download.name
			do (url, map_name) =>
				$.get(url)
				.done (mapData) =>
					num_to_download--
					### convert stringy ID's to integers ###
					@Maps[map_name] = mapData.split(",").map (t) -> return +t
					HAL.trigger "MAP_DOWNLOADED", map_name, url
					if num_to_download is 0
						$deferred.resolve()
				.fail () =>
					$deferred.reject("Downloading of map #{map_name} failed")
		return $deferred.promise()

	SOAManager::downloadXML = () ->
		done = 0
		$deferred = new $.Deferred()
		load_xmls = ["units", "constructions", "tradegoods", "equipment", "science", "technologies"]
		
		XMLStore.on "LOADED_CONSTRUCTIONS_XML", () => loadXML()
		XMLStore.on "LOADED_UNITS_XML", () => loadXML()
		XMLStore.on "LOADED_TRADEGOODS_XML", () => loadXML()
		XMLStore.on "LOADED_EQUIPMENT_XML", () => loadXML()
		XMLStore.on "LOADED_SCIENCE_XML", () => loadXML()
		XMLStore.on "LOADED_TECHNOLOGIES_XML", () => loadXML()

		for store in load_xmls
			XMLStore.get(store)

		loadXML = () ->
			done++
			if done isnt load_xmls.length
				return
			$deferred.resolve()
		
		return $deferred.promise()

	SOAManager::logMeIntoGame = () ->
		amjad.load(@sessionData)
		amjad.initialize()
		
		@Empire         = amjad.empire
		@Constructions  = amjad.empire.constructions
		@Heroes         = amjad.empire.heroes
		@Settlement     = amjad.empire.settlement
		@Resources      = amjad.empire.resources
		@Units          = amjad.empire.units

		HAL.trigger "SOA_LOADED"
		
		@loadZoneMap().done () =>
			@currentMap = @Zone
			@loadCityMap()
			@Hud.epilogue()
			@Hud.hideAllDialogs()
			@Hud.showDialog("BOTTOM_MENU")
			@Hud.showDialog("TOP_MENU")
			@Hud.showDialog("BUILD_ORDERS")
			@Hud.showDialog("HEROES_QUEUE")
			@Hud.showDialog("TOP_MENU")
			@Hud.fadeInViewport()
			@Zone.map.tile_shape.alpha = 0
			@City.map.tile_shape.alpha = 0
			@centerOnZone()

	SOAManager::validateSession = () ->
		$deferred = new $.Deferred()
		$.get("/load")
		.done (data) =>
			alert("success")
			$deferred.resolve(data)
		.fail (err) =>
			alert("fail")
			$deferred.reject(err)
		return $deferred.promise()

	SOAManager::goToCityView = () ->
		HAL.IsometricScene.stopRendering(@Zone)
		HAL.IsometricScene.startRendering(@City)
		@Zone.pause()
		@sortCityConstructions()
		@Hud.fadeInViewport().done () =>
			@currentMap = @City
			@City.resume()
		return

	SOAManager::sortCityConstructions = () ->
		@City.map.layers[3].children.sort (a, b) ->
			return a.position.y - b.position.y

	SOAManager::goToZoneView = () ->
		HAL.IsometricScene.stopRendering(@City)
		HAL.IsometricScene.startRendering(@Zone)
		@City.pause()
		@Hud.fadeInViewport().done () =>
			@currentMap = @Zone
			@Zone.resume()
		return

	SOAManager::toggleViews = () ->
		if @isInCityView()
			@goToZoneView()
		else
			@goToCityView()

	SOAManager::isInCityView = () ->
		return @currentMap is @City

	SOAManager::isInZoneView = () ->
		return @currentMap is @Zone

	SOAManager::loadZoneMap = () ->
		@Zone.init()
		@Hud.domElement.append(@Zone.renderer.view)
		HAL.IsometricScene.startRendering(@Zone)
		$deferred = new $.Deferred()

		@loadAllPOIS()
		.done () =>
			@Zone.map.createMapFromBitmap(@Maps.Zone)
			Rest.get("user.all_settlements")
			.done (data) => 
				@loadSettlements(data)
				HAL.trigger "ZONE_MAP_LOADED", @Zone
				$deferred.resolve()
		.fail (data) =>
			$deferred.reject(data)
		return $deferred.promise()

	SOAManager::loadCityMap = () ->
		@City.initFrom(@Zone)
		@City.map.createMapFromBitmap(@Maps["City"])
		HAL.trigger "CITY_MAP_LOADED", @City

	SOAManager::attachConstructionToMouse = (map_name, construction) ->
		layer = construction.layer
		return if not layer?
		grab_top =  (layer.texture.frame.height - MapConfig.TILE_HEIGHT) * 0.5
		grab_left = layer.texture.frame.width*0.5
		layer.interactive = false
		@attachedLayer = layer
		MAP = @[map_name].map
		tweeners = []

		MAP.top_layer.mousemove = (data) =>
			x = data.global.x
			y = data.global.y - grab_top
			pos = MAP.scene.toLocalObject(x, y)
			layer.position.x = pos.x
			layer.position.y = pos.y

		mouse_over_clb = (row, col) =>
			return if not @attachedLayer? or @currentMap isnt MAP.scene
			collisions = @Constructions.testSpanArea(
				construction.getSpanFromTile(MAP.scene.tile_under), 
				layer.id
			)

			if collisions.length is 0
				for tween in tweeners
					tween.target.tint = 0xffffff
					tween.progress(1.0) 
					tween.kill()
			else
				construction.onCollision(collisions, () ->
					tweeners.push(
						TweenMax.fromTo(
							@layer, 1.1,
							{
								alpha: 0.1, 
								repeat: 1
								tint: 0xff0000
							},
							{
								alpha: 1
								repeat: 1
								tint: 0xff0000
							}
						)
					)
				)

		HAL.on "MOUSE_OVER_TILE", mouse_over_clb
		prev_callb = MAP.top_layer.click

		MAP.top_layer.click = () =>
			if not @attachedLayer
				MAP.top_layer.click = prev_callb
				HAL.removeTrigger "MOUSE_OVER_TILE", mouse_over_clb

			tA = @attachedLayer.parentTile
			tB = MAP.scene.tile_under
			row = tB.row
			col = tB.col
			Rest.post("construction.relocate", {
				cid: construction.construction_id
				x: row
				y: col
			}).done () =>
				tB.copyLayer(@attachedLayer)
				layer.interactive       = true
				MAP.top_layer.mousemove = null
				@attachedLayer          = null
				MAP.top_layer.click     = prev_callb
				SOA.sortCityConstructions()
			.fail (data) =>
				console.error(data)
			HAL.removeTrigger "MOUSE_OVER_TILE", mouse_over_clb

	SOAManager::loadSettlements = (data) ->
		$deferred = new $.Deferred()
		for k, v of data
			continue if k is amjad.user.username
			layer = amjad.putCityOnZone(v.coord_x, v.coord_y, "city", "amjad/constructions/#{v.type}/")
			continue if not layer?
			@Settlements[k] 		= v
			@Settlements[k].city 	= layer
			layer.type 				= "city"
		$deferred.resolve()
		return $deferred.promise()

	# ovde neku veliku vatru zapaliti, sjesti i popiti caj#
	SOAManager::loadAllPOIS = () ->
		$deferred = new $.Deferred()
		
		Rest.get("user.all_pois")
		.done (data) =>
			@pois = data
			$deferred.resolve()
		.fail (data) =>
			$deferred.reject(data)

		return $deferred.promise()

	SOAManager::putAllPOIS = () ->
		for k, pois of @pois
			for id, poi of pois
				tile = @Zone.map.getTile(poi.x_coord, poi.y_coord)
				type = "poi"
				type = "trade" if k is "trade"
				for c, l of tile?.layers
					l.type = type
					l.subtype = k
					l.poid = id

	SOAManager::getPOIS = () ->
		out = []
		for k, pois of @pois
			for id, poi of pois
				ls =  @Zone.map.getTile(poi.x_coord, poi.y_coord).layers
				for k, l of ls
					out.push(l)
		return out

	SOAManager::createClouds = (num = 50) ->
		clouds = [
					PIXI.TextureCache["assets/sprites/amjad/clouds/cloud1.png"],
					PIXI.TextureCache["assets/sprites/amjad/clouds/cloud2.png"]
				]
		zbnds = @Zone.map.world_bounds.clone()
		for i in [0...num]
			startY = ~~(Math.random() * zbnds.y)
			cloud = clouds[+(Math.random() > 0.5)]
			startX = -cloud.width
			spr = new PIXI.Sprite(cloud)
			spr.position.x = startX
			spr.position.y = startY
			upOrDown = 1
			randScale = Math.random() + 1
			spr.scale.x = randScale
			spr.scale.y = randScale
			@Zone.map.layers[6].addChild(spr)
			sideFactor = Math.random()
			spr.rotation = -Math.PI/15
			spr.alpha = 0.5 + (Math.random() *  0.40)
			rndFact = upOrDown*(~~(sideFactor*1050))
			whereY = startY + rndFact
			# console.debug startY, whereY, rndFact
			tween = TweenMax.fromTo(
				spr.position,
				~~(Math.random() * 100) + 100,
				{
					x: startX
					y: startY
					repeat: -1
				},
				{
					x: zbnds.x
					y: whereY
					repeat: -1
				}
			)

	SOAManager::createStorms = (numStorms, stormSize, stormPoints, stormSpeed = 100, stormColor = 0xf2f299ff) ->
		texts = Object.keys(PIXI.TextureCache).filter (en) ->
			return en.indexOf("storm") != -1
		texts = texts.map (texname) ->
			return PIXI.TextureCache[texname]

		for j in [0...numStorms]
			startRow = ~~(@Zone.map.NROWS * Math.random())
			startCol = ~~(@Zone.map.NCOLUMNS * Math.random())
			hazard = @Zone.map.addAnimatedLayer(startRow, startCol, texts)
			hazard.scale.x = hazard.scale.y = stormSize * Math.random()
			points = []
			for i in [0...stormPoints]
				row = ~~(Math.random() * @Zone.map.NROWS)
				col = ~~(Math.random() * @Zone.map.NCOLUMNS)
				points.push(@Zone.map.getTile(row, col).position)
			hazard.tint = stormColor
			tweener = TweenMax.to(hazard.position, 200 + ~~(stormSpeed * Math.random()), {bezier: points.slice(), type: "cubic", paused: true, curviness: 2.5, repeat: -1})
			rottweener = TweenMax.fromTo(hazard, 3, {rotation: 0, repeat: -1}, {rotation: 2*Math.PI, repeat: -1})
			tweener.play()
			#rottweener.play()
			hazard.play()

	SOAManager::setSelectedHero = (hero) ->
		return if not hero?
		@selectedHero = hero

	SOAManager::centerOnZone = () ->
		# x = @Settlement.layer.position.x
		# y = @Settlement.layer.position.y
		# @Zone.world.position.x = -x + @Zone.screen_center.x
		# @Zone.world.position.y = -y + @Zone.screen_center.y
		# @Zone.camera.updateTransform()
		# @Zone.world.updateTransform()
		# @Zone.map.updateHitAreaPosition()
		@Zone.centerOn(@Settlement.layer.position)
		return
	
	SOAManager::getPOI = (row, col) ->
		tile = @Zone.map.getTile(row, col)
		return if not tile?
		for v, target of tile.layers
			if not target? or not target.poid?
				throw new Error("No poi here")
			poi_id = target.poid
			subtype = target.subtype
			poi = SOA.pois[subtype][poi_id]
			return poi
		return null

	return SOAManager