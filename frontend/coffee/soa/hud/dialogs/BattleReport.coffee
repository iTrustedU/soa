"use strict"

html =
"""
<div class="battle-report draggable">
    <div class="dialog-name">
        <span id="caravan-name">B A T T L E  &nbsp; R E P O R T</span>
        <div class="close-button"></div>
    </div>
    <div class="report-tabs">
        <div class="chat-tab" id="commander-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Black List</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="agent-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Taunts</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="caravan-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Reports</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="trainer-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Mail</span>
            <div class="chat-tab-r"></div>
        </div>
    </div>
    <div id="tab">
        <div class="battle-report-content">
            <div id="battle-status">{{{battle_status}}}</div>
            <div class="battle-animation">

            </div>
            <div id="battle-status-info">{{{battle_status_info}}} </div>
            <div id="loot-resources"> {{{loot_resources}}} </div>
            <div class="battle-report-button">Back to Reports</div>
        </div>
        <div class="all-reports-content">
            <div class="all-reports-delete">Delete Message</div>
          
            <div id="all-reports-table">{{{all_reports_table}}}</div>
            <div class="navigation">
                <img src="img/dialog/battle-report/backward-end.png" class="navigation-icons">
                <img src="img/dialog/battle-report/backward.png" class="navigation-icons">
                <span>1</span>
                <img src="img/dialog/battle-report/forward.png" class="navigation-icons">
                <img src="img/dialog/battle-report/forward-end.png" class="navigation-icons">
            </div>
        </div>
       
        <div class="messages-table">
            <table>
                <tr>
                    <th class="t230"></th>
                    <th class="t170">From</th>
                    <th class="t170">Subject</th>
                    <th class="t105">Date</th>
                    <th class="t105">Time</th>
                </tr>
                <tr>
                    <td class="t230"><input type="checkbox" /></td>
                    <td class="t170">Nikola Soro</td>
                    <td class="t170">Heloooo</td>
                    <td class="t105">12. 23. 2014.</td>
                    <td class="t105">15:54:21</td>
                </tr>
            </table>
        </div>
        <div class="navigation">
            <img src="img/dialog/battle-report/backward-end.png" class="navigation-icons">
            <img src="img/dialog/battle-report/backward.png" class="navigation-icons">
            <span>1</span>
            <img src="img/dialog/battle-report/forward.png" class="navigation-icons">
            <img src="img/dialog/battle-report/forward-end.png" class="navigation-icons">
        </div>
    </div>
    <div class="black-list-content">
        <div class="all-reports-delete">Remove</div>
        
        <div class="black-list-table">
            <table>
                <tr>
                    <th class="t230"></th>
                    <th class="t200">Player Name</th>
                    <th class="t200">Alliance</th>
                    <th class="t150">Date</th>
                </tr>
                <tr>
                    <td class="t230"><input type="checkbox" /></td>
                    <td class="t200">Nikola Soro</td>
                    <td class="t200">Hell Hol</td>
                    <td class="t150">12. 23. 2014.</td>
                </tr>
            </table>
        </div>
        <div class="navigation">
            <img src="img/dialog/battle-report/backward-end.png" class="navigation-icons">
            <img src="img/dialog/battle-report/backward.png" class="navigation-icons">
            <span>1</span>
            <img src="img/dialog/battle-report/forward.png" class="navigation-icons">
            <img src="img/dialog/battle-report/forward-end.png" class="navigation-icons">
        </div>
    </div>
    <div class="compose-content">
        <div class="all-reports-delete">Send Message</div>
        <div class="compose-table">
            <span>To:</span>
            <input type="text" />
            <span>Subject:</span>
            <input type="text" />
            <span>Message:</span>
            <textarea type="text"></textarea> 
        </div>
    </div>
</div>
"""

$battle_status = 
"""
<div class="dialog-name">
    <span id="caravan-name">You were {{b_stat}}</span>
</div>
"""

$battle_status_info =
"""
<div class="battle-stat-attack">
    <span><img src="img/dialog/battle-report/attack-icon.png" class="br-attacker-icon">  {{battleResult.attackCommander.playerName}}</span>
    <span>{{commander_name}}  <img src="img/dialog/training/level.png" class="br-level-icon"> {{battleResult.attackCommander.playerLevel}}</span>
    <div class="battle-table default-skin">
        <table>
            <tr>
                <th class="t100"></th>
                <th class="t60"><img src="img/dialog/battle-report/all.png" /></th>
                <th class="t60"><img src="img/dialog/battle-report/survived.png" /></th>
                <th class="t60"><img src="img/dialog/battle-report/died.png" /></th>
                <th class="t60"><img src="img/dialog/battle-report/wounded.png" /></th>
            </tr>
            {{#each battleResult.attackers}}
            <tr>
                <td class="t100"><img src={{concat_unit_avatar @key "44x44"}} class="br-type-img" /></td>
                <td class="t60">{{all}}</td>
                <td class="t60">{{survived}}</td>
                <td class="t60">{{died}}</td>
                <td class="t60">{{wounded}}</td>
            </tr>
            {{/each}}
        </table>
    </div>
</div>
<div class="battle-stat-def">
    <span><img src="img/dialog/battle-report/defender-icon.png" class="br-defender-icon">  {{battleResult.defenderCommander.playerName}}</span>
    <span>{{commander_name}}  <img src="img/dialog/training/level.png" class="br-level-icon"> {{battleResult.defenderCommander.playerLevel}}</span>
    <div class="battle-table default-skin">
        <table>
            <tr>
                <th class="t100"></th>
                <th class="t60"><img src="img/dialog/battle-report/all.png" /></th>
                <th class="t60"><img src="img/dialog/battle-report/survived.png" /></th>
                <th class="t60"><img src="img/dialog/battle-report/died.png" /></th>
                <th class="t60"><img src="img/dialog/battle-report/wounded.png" /></th>
            </tr>
            {{#each battleResult.defenders}}
            <tr>
                <td class="t100"><img src={{concat_unit_avatar @key "44x44"}} class="br-type-img" /></td>
                <td class="t60">{{all}}</td>
                <td class="t60">{{survived}}</td>
                <td class="t60">{{died}}</td>
                <td class="t60">{{wounded}}</td>
            </tr>
            {{/each}}
        </table>
    </div>
</div>
"""

$loot_resources =
"""
<div class="loot-resources">
    <img src="img/top-resources/gold.png" class="top-resources">
    <span id="resource-info-balance" class="top-resources-input">{{gold}}</span>
    <img src="img/top-resources/water.png" class="top-resources">
    <span id="resource-info-water" class="top-resources-input">{{water}}</span>
    <img src="img/top-resources/food.png" class="top-resources">
    <span id="resource-info-food" class="top-resources-input">{{food}}</span>
    <img src="img/top-resources/iron.png" class="top-resources">
    <span id="resource-info-iron" class="top-resources-input">{{iron}}</span>
    <img src="img/top-resources/wood.png" class="top-resources">
    <span id="resource-info-timber" class="top-resources-input">{{wood}}</span>
</div>
"""

$all_reports_table =
"""
<div class="all-reports-table">
    <table>
        <tr>
            <th class="t230"></th>
            <th class="t150">Date</th>
            <th class="t250">Type</th>
            <th class="t150">View</th>
        </tr>
        <tr>
            <td class="t230"><input type="checkbox" /></td>
            <td class="t150">{{date}}</td>
            <td class="t250">{{type}}</td>
            <td class="t150"><a href="#">{{reports}}</a></td>
        </tr>
    </table>
</div>
"""


define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->  

    class BattleReport extends BaseDialog
        constructor: () ->
            super(html)

    BattleReport::onPrologue = () ->
        @setTemplates
            "battle_status": $battle_status
            "all_reports_table": $all_reports_table
            "loot_resources": $loot_resources
            "battle_status_info": $battle_status_info

        HAL.on "FIGHT_FINISHED", (pdata) =>
            ##console.debug pdata
            @show(pdata)

        @closeable()
        @draggable()

    BattleReport::show = (data) ->

        # battle_report = 
        #     attackers:
        #         commanderName : 'Shake Milk'
        #         commanderLevel : 3
        #         finish:
        #             31:
        #                 army_id: 53
        #                 arrival: null
        #                 attack_type: null
        #                 color: "white"
        #                 commander_id: 31
        #                 d_coord_x: null
        #                 d_coord_y: null
        #                 deff_ost: null
        #                 deff_sett: null
        #                 departure: null
        #                 experience: 0
        #                 img_id: 10
        #                 leadership: 7
        #                 level: 1
        #                 name: "Scot"
        #                 o_coord_x: 16
        #                 o_coord_y: 21
        #                 points: null
        #                 readiness: 11
        #                 speed: 2
        #                 troops:
        #                     bowman: 101
        #                     heavy_faris: 68
        #                     heavy_haggan: 20
        #                     heavy_swordsman: 41
        #                     skirmisher: 146
        #                     swordsman: 64
        #         start:
        #             31:
        #                 army_id: 53
        #                 arrival: null
        #                 attack_type: null
        #                 color: "white"
        #                 commander_id: 31
        #                 d_coord_x: null
        #                 d_coord_y: null
        #                 deff_ost: null
        #                 deff_sett: null
        #                 departure: null
        #                 experience: 0
        #                 img_id: 10
        #                 leadership: 7
        #                 level: 1
        #                 name: "Scot"
        #                 o_coord_x: 16
        #                 o_coord_y: 21
        #                 points: null
        #                 readiness: 11
        #                 speed: 2
        #                 troops:
        #                     bowman: 101
        #                     heavy_faris: 68
        #                     heavy_haggan: 20
        #                     heavy_swordsman: 41
        #                     skirmisher: 146
        #                     swordsman: 64
        #     defenders:
        #         commanderName:'Shake Butty'
        #         commanderLevel : 2
        #         finish:
        #             31:
        #                 army_id: 53
        #                 arrival: null
        #                 attack_type: null
        #                 color: "white"
        #                 commander_id: 31
        #                 d_coord_x: null
        #                 d_coord_y: null
        #                 deff_ost: null
        #                 deff_sett: null
        #                 departure: null
        #                 experience: 0
        #                 img_id: 10
        #                 leadership: 7
        #                 level: 1
        #                 name: "Scot"
        #                 o_coord_x: 16
        #                 o_coord_y: 21
        #                 points: null
        #                 readiness: 11
        #                 speed: 2
        #                 troops:
        #                     bowman: 0
        #                     heavy_faris: 0
        #                     heavy_haggan: 0
        #                     heavy_swordsman: 0
        #                     skirmisher: 0
        #                     swordsman: 0
        #         start:
        #             31:
        #                 army_id: 53
        #                 arrival: null
        #                 attack_type: null
        #                 color: "white"
        #                 commander_id: 31
        #                 d_coord_x: null
        #                 d_coord_y: null
        #                 deff_ost: null
        #                 deff_sett: null
        #                 departure: null
        #                 experience: 0
        #                 img_id: 10
        #                 leadership: 7
        #                 level: 1
        #                 name: "Scot"
        #                 o_coord_x: 16
        #                 o_coord_y: 21
        #                 points: null
        #                 readiness: 11
        #                 speed: 2
        #                 troops:
        #                     bowman: 101
        #                     heavy_faris: 68
        #                     heavy_haggan: 20
        #                     heavy_swordsman: 41
        #                     skirmisher: 146
        #                     swordsman: 64
                            
        pdata = @calculateBattleOutcome(data)
        #console.debug data
        
        @setData "dbattle_status", pdata.battle_status
        @setData "dbattle_status_info", pdata.battle_status_info
        @setData "dloot_resources", pdata.loot_resources
        @setData "dall_reports_table", pdata.all_reports_table

        @compile("battle_status", "dbattle_status").then (data) =>
            @refresh("battle-status", data)

        @compile("battle_status_info", "dbattle_status_info").then (data) =>
            @refresh("battle-status-info", data)
            @html.find(".battle-table").customScrollbar()

        @compile("loot_resources", "dloot_resources").then (data) =>
            @refresh("loot-resources", data)

        @compile("all_reports_table", "dall_reports_table").then (data) =>
            @refresh("all-reports-table", data)
                      
        super({})

    BattleReport::calculateBattleOutcome = (data) ->
        #assumtion: we are always attackers
        bsi = 
            attackCommander : {}
            defenderCommander : {}
            attackers : {}
            defenders : {}
        #iterate through all commanders involved in battle
        #battle report supplies information how many troops you still have
        
        bsi['attackCommander']['playerName'] = data.attackers.playerName
        bsi['attackCommander']['playerLevel'] = data.attackers.playerLevel

        bsi['defenderCommander']['playerName'] = data.defenders.playerName
        bsi['defenderCommander']['playerLevel'] = data.defenders.playerLevel

        for commanderObjName,commanderValue of data.attackers.start
            for unitName, unitVal of commanderValue.troops
                    bsi['attackers'][unitName] =
                        'all' : 
                            unitVal
                        'survived' :
                            data.attackers.finish[commanderObjName].troops[unitName]
                        'died' :
                            unitVal-data.attackers.finish[commanderObjName].troops[unitName]
                        'wounded' : 0

        for commanderObjName,commanderValue of data.defenders.start
            for unitName, unitVal of commanderValue.troops
                    bsi['defenders'][unitName] =
                        'all' : 
                            unitVal
                        'survived' :
                            data.defenders.finish[commanderObjName].troops[unitName]
                        'died' :
                            unitVal-data.defenders.finish[commanderObjName].troops[unitName]
                        'wounded' : 0

        #determine if battle is won or lost
        battle_status = 'Defeated'
        for unitKey, unitVal of bsi.attackers
            if unitVal.survived > 0
                battle_status = 'Victorious'

        bs =
            b_stat: battle_status

        lr =
            gold: 885588
            water: 11112
            food: 2221
            iron: 124154
            wood: 454525

        art =
            date: "14.12.2013. 14:56:23"
            type: "Attack on Castle"
            reports: "Reports"

        data = 
            battle_status: bs
            battle_status_info: 
                battleResult: bsi
                commanderName: "KUrac palac"
            loot_resources: lr
            all_reports_table: art

        return data

    return BattleReport