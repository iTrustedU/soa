"use strict"

html = """
<div class="fountainG-container">
	<div id="fountainG">
		<div id="fountainG_1" class="fountainG">
		</div>
		<div id="fountainG_2" class="fountainG">
		</div>
		<div id="fountainG_3" class="fountainG">
		</div>
		<div id="fountainG_4" class="fountainG">
		</div>
		<div id="fountainG_5" class="fountainG">
		</div>
		<div id="fountainG_6" class="fountainG">
		</div>
		<div id="fountainG_7" class="fountainG">
		</div>
		<div id="fountainG_8" class="fountainG">
		</div>
	</div>
</div>
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->

	class SpinnerOverlay extends BaseDialog
		constructor: () ->
			super(html)

	SpinnerOverlay::start = (dialog, deferred) ->
		@html.hide(0)
		@html.prependTo(dialog)
		@html.show(200)
		deferred.done (msg) =>
			@finish(msg)
		.fail (msg) =>
			@error(msg)
		return deferred.promise()

	SpinnerOverlay::finish = () ->
		@html.fadeOut(1500) #.delay(15000)

	SpinnerOverlay::error = (msg) ->
		@html.fadeOut(1500) #.delay(15000)

	return SpinnerOverlay