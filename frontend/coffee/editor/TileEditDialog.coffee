"use strict"

form_html = 
"""
<div class="keyval">
    <div>
        <label for="name">Name</label>
        <input id="tile-name" type="text" value="{{name}}"></input>
    </div>

    <div>
        <label for="layer">Layer</label>
        <select id="tile-layer">
            {{create_options layers}}
        </select>
    </div>

    <div>
        <label for="minigrid"> Size </label>
        {{{minigrid}}}
    </div>
</div>
"""

save_btn_html =
"""
<button id="save-tile" type="button" class="tile-edit-save-btn loader-load-button"> Save </button>
"""

cancel_btn_html =
"""
<button id="cancel-tile" type="button" class="tile-edit-cancel-btn loader-load-button"> Cancel </button>
"""

buttons_html = 
"""
<div class="buttons-holder">
</div>
"""

define ["editor/BaseDialog", "MapConfig", "TileMetaStorage"],

(BaseDialog, MapConfig, TileMetaStorage) ->

    API =
        TILES_LIST: "/tiles"
        TILE_SAVE: "/tile/new"

    Handlebars.registerHelper "create_options", (values, options) ->
        out = ""
        values.forEach (elem) ->
            out += "<option value='#{elem}'>#{elem}</option>"
        return new Handlebars.SafeString(out)

    class TileEditDialog extends BaseDialog
        constructor: () ->
            super("Edit tile...", "tile-edit-dialog")
            @html.css(
                "left": "50%"
                "top": "50%"
                "height": "400px"
                "width": "400px"
                "z-index": "5000"
                "position": "absolute"
                "margin-left": "450px"
                "margin-top": "-300px"
            )
            $holder = @html.find(".holder")
            $holder.css(
                "width": "inherit"
                "height": "inherit"
            )
            $buttons_holder = $(buttons_html)
            @html.append($buttons_holder)

            @html.find(".title-container").remove()

            @save_btn = $(save_btn_html)
            @save_btn.appendTo($buttons_holder)

            @save_btn.on "click", () =>
                @trigger "TILE_SAVED", @, @getCurrentTile()
                @html.hide(200)
            
            @cancel_btn = $(cancel_btn_html)
            @cancel_btn.appendTo($buttons_holder)
            @cancel_btn.on "click", () =>
                @html.hide(200)

    TileEditDialog::getCurrentTile = () ->
        @current_tile.sprite = @current_tile.sprite
        @current_tile.name = @content.find("#tile-name").val()
        @current_tile.layer = +@content.find("#tile-layer").find("option:selected").text()
        @current_tile.span = @parseMiniGridSize()
        @current_tile.minigrid = ""
        return @current_tile

    TileEditDialog::showTile = ($sprite) ->
        @content.empty()
        tpl_tile_form = Handlebars.compile(form_html)
        @current_tile =
            seedId: $sprite.attr("id")
            sprite: $sprite.find("img").attr("src")
            name: $sprite.find("img").attr("src")
            minigrid: @createMiniGrid($sprite, "1")
            layers: new Array(MapConfig.MAX_LAYERS).join(0).split(0).map (_,i) -> i
        $form = $(tpl_tile_form(@current_tile))
        @content.append($form)
        $form.find(".minigrid").first().click (ev) ->
            $(ev.target).toggleClass("minigrid-active-cell")
        @html.show(200)

    TileEditDialog::dropSprite = ($sprite) ->
        @content.empty()
        tpl_tile_form = Handlebars.compile(form_html)
        @current_tile =
            seedId: $sprite.attr("id")
            sprite: $sprite.find("img").attr("src")
            name: $sprite.find("img").attr("src")
            minigrid: @createMiniGrid($sprite, "1")
            layers: new Array(MapConfig.MAX_LAYERS).join(0).split(0).map (_,i) -> i
        $form = $(tpl_tile_form(@current_tile))
        @content.append($form)
        $form.find(".minigrid").first().click (ev) ->
            $(ev.target).toggleClass("minigrid-active-cell")
        @html.show(200)
        
    TileEditDialog::parseMiniGridSize = () ->
        out = []
        $.each(@content.find(".minigrid").children(), 
          (k, v) ->
            out[k] = 0
            if $(v).hasClass("minigrid-active-cell")
              out[k] = 1
        )
        binaryString = out.toString().replace(/,/g,'')
        return binaryString

    # $form.find("#tile-layer option[value=#{tile.layer}]").attr("selected", "selected")
    TileEditDialog::createMiniGrid = (spr, encodednum) ->
        src         = spr.find("img").attr("src")
        height      = Editor.sprites_dialog.sprites[src].height

        h           = Math.pow(2, ~~(Math.log(height-1)/Math.LN2) + 1)
        factor      = 16
        size        = 128
        w           = Math.max(h, height) * 2
        numrows     = w / size
        numcols     = w / size
        diagonal    = (Math.sqrt(2*size*size) * numrows) / (size/factor)
        diff        = diagonal - (numcols*factor)
        
        imgcl       = spr.find("img").clone()
        
        $wrapper = $("<div/>", 
          "width": (diagonal) + "px"
          "height": (diagonal / 2) + "px"
          "class" :"minigrid-wrapper"
        )

        $parent = $("<div/>",
          "class": "minigrid"
          "width": numcols * factor
          "height": numrows * factor
          "css":
            "left": (diff * 0.5 - 1) + "px"
            "top" : -(diff * 0.5 - (numrows*5) / 2 + (numrows / 2 + 1)) + "px"
        )

        k = 0
        bin = encodednum.split('')
        for i in [0...numrows]
          for j in [0...numcols]
            $cell = $("<div/>",
              "id": "minigrid-cell"
              "css":
                "float": "left"
              "width": factor - 1
              "height": factor - 1
            )
            if +bin[k]
              $cell.addClass("minigrid-active-cell")
            k++
            $cell.appendTo($parent)
        $parent.appendTo($wrapper)

        cl = spr.find("img").clone()
        cl.css("position", "absolute")
        cl.css("-webkit-transform", "rotate(-45deg)")
        cl.css("height", "100%")
        cl.css("width", "100%")
        cl.css("left", "0px")
        cl.css("top", "0px")
        cl.css("opacity", "0.9")
        cl.css("z-index", -1)
        $parent.append(cl)

        return new Handlebars.SafeString($wrapper[0].outerHTML)       

    return TileEditDialog