(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader", "soa/XMLStore", "soa/User", "soa/Empire"], function(StructureLoader, XMLStore, User, Empire) {
    var Amjad, ShortMap;
    ShortMap = {
      "us": [
        "user", function(meta) {
          var user;
          user = new User();
          user.load(meta);
          return user;
        }
      ],
      "e": [
        "empire", function(meta) {
          var empire;
          empire = new Empire();
          empire.load(meta);
          return empire;
        }
      ]
    };
    Amjad = (function(_super) {
      __extends(Amjad, _super);

      function Amjad() {
        this.loaded = false;
        this.selected_hero = null;
        this.current_map = null;
        this.moved_camera = false;
        Amjad.__super__.constructor.call(this, ShortMap);
      }

      Amjad.prototype.load = function(meta) {
        return Amjad.__super__.load.call(this, meta);
      };

      Amjad.prototype.initListeners = function() {
        Amjad.__super__.initListeners.call(this);
        return this.on("DONE_LOADING", function() {
          this.trigger("AMJAD_LOADED");
          return this.loaded = true;
        });
      };

      Amjad.prototype.layerMetaFromSprite = function(spriteuri) {
        return HAL.TileMetaStorage.findByName(this.splaySpriteURI(spriteuri));
      };

      Amjad.prototype.splaySpriteURI = function(spriteuri) {
        return spriteuri.replace(/\//g, "_");
      };

      Amjad.prototype.putConstructionInCity = function(row, col, type, isPoi) {
        var meta, name, res;
        if (isPoi) {
          name = amjad.user.CONSTRUCTION_SPRITES_URI;
        }
        name = amjad.user.CONSTRUCTION_SPRITES_URI + type;
        meta = this.layerMetaFromSprite(name);
        if (meta == null) {
          console.error("Meta file not found for " + name);
          return;
        }
        if (meta.sprite.indexOf("assets/sprites") !== 0) {
          meta.sprite = "assets/sprites/" + meta.sprite;
        }
        res = SOA.City.map.addLayerToTileByID(row, col, meta.id);
        if (res == null) {
          console.debug("Couldn't add layer " + meta.name + " on " + row + ", " + col);
        } else {
          console.debug("Construction " + type + " placed on city map at " + row + ", " + col);
        }
        return res;
      };

      Amjad.prototype.putCityOnZone = function(row, col, type, spriteuri) {
        var meta, name, res;
        if (spriteuri == null) {
          spriteuri = amjad.user.CONSTRUCTION_SPRITES_URI;
        }
        name = spriteuri + type;
        meta = this.layerMetaFromSprite(name);
        if (meta == null) {
          console.error("Meta file not found for " + name);
          return;
        }
        res = SOA.Zone.map.addLayerSpriteToTile(row, col, meta.layer, "assets/sprites/" + meta.sprite);
        if (res == null) {

        } else {

        }
        return res;
      };

      return Amjad;

    })(StructureLoader);
    Amjad.prototype.initialize = function() {};
    Amjad.prototype.finishProduction = function(id) {
      return Rest.post("construction.finish_production", {
        cid: id
      });
    };
    Amjad.prototype.finishTechResearch = function(type) {
      return Rest.post("settlement.finish_tech_research", {
        ty: type
      });
    };
    Amjad.prototype.finishScienceResearch = function(type) {
      return Rest.post("settlement.finish_science_research", {
        ty: type
      });
    };
    return Amjad;
  });

}).call(this);
