"use strict"

html =
"""
<div class="main-tent draggable">
    <div class="dialog-name">
        <span>Main Tent</span>
        <div class="close-button"></div>
    </div>
    <div class="science-tabs">
        <div id="overview-tab" class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Overview</span>
            <div class="chat-tab-r"></div>
        </div>
        <div id="resources-tab" class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Resources</span>
            <div class="chat-tab-r"></div>
        </div>
        <div id="pois-tab" class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">POI's</span>
            <div class="chat-tab-r"></div>
        </div>
        <div id="buildings-tab" class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Buildings</span>
            <div class="chat-tab-r"></div>
        </div>
    </div>
    <div id="tab_content_overview_template" class="main-tent-content overview">
        {{{overview_content}}}
    </div>
    <div id="tab_content_resources_template" class="main-tent-content resources default-skin">
       {{{resources_content}}}
    </div>
    <div id="tab_content_pois_template" class="main-tent-content pois default-skin">
       {{{pois_content}}} 
    </div>
    <div id="tab_content_buildings_template"  class="main-tent-content main-buildings default-skin">
        {{{buildings_content}}} 
    </div>
    <div class="war-building-info">
        <img class="dialog-building-img" />
        <div id="upgrade-price" class="upgrade-price">
           {{{upgrade_price}}}
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
    </div>
</div>
"""

$upgrade_price = """
        {{#each data.resources}}
            <img src="img/top-resources/{{@key}}.png">
            <span>{{this}}</span>
        {{/each}}
        
        """

$overview_content = 
"""
<div id="overview-tab" class="main-tent-content main-overview">
    <span class="par">City Name:</span><span class="main-tent-content-value nepar">{{data.city_name}}</span><br />
    <span class="par">Player Name:</span><span class="main-tent-content-value nepar">{{data.player_name}}</span><br />
    <span class="par">Ruling Title:</span><span class="main-tent-content-value nepar">{{data.ruling_title}}</span><br />
    <span class="par">Achieved Title:</span><span class="main-tent-content-value nepar">{{data.achived_title}}</span><br />
    <span class="par">Poi's:</span><span class="main-tent-content-value nepar">{{data.pois}}</span><br />
    <span class="par">Glory:</span><span class="main-tent-content-value nepar">{{data.glory}}</span><br />
    <span class="par">Rank:</span><span class="main-tent-content-value nepar">{{data.rank}}</span><br />
    <span class="par">Heroes:</span><span class="main-tent-content-value nepar">{{data.heroes}}</span><br />
    <span class="par">Army:</span><span class="main-tent-content-value nepar">{{data.army}}</span><br />
    <span class="par">Camels:</span><span class="main-tent-content-value nepar">{{data.camels}}</span><br />
    <span class="par">Horses:</span><span class="main-tent-content-value nepar">{{data.horses}}</span><br />
    <span class="par">Cows:</span><span class="main-tent-content-value nepar">{{data.cows}}</span><br />
    <span class="par">Sheeps:</span><span class="main-tent-content-value nepar">{{data.sheeps}}</span><br />
    <span class="par">Goats:</span><span class="main-tent-content-value nepar">{{data.goats}}</span><br />
</div>
"""

$resources_content = 
"""
<div id="resources-tab" class="main-tent-resources-box default-skin">
    {{#each data}}
    <div class="resources-inner-box">
        <img src="img/dialog/commander/timber.png" class="resources-image">
        <div class="resources-shadow-box">
            <div class="main-resources-stats">
                <span class="color-text">{{lang @key}}</span>
                <img src="img/dialog/commander/income.png">
                <span>12</span>
            </div>
        </div>
    </div>
    {{/each}}
</div>
"""

$pois_content = 
"""
{{#each data}}
<div id="pois-tab" class="pois-box">
    <img src="{{concat_poi c_type}}" class="poi-image">
    <div class="poi-shadow-box">
        <div class="main-poi-stats">
            <span class="color-text">{{lang c_type}}</span>
            <span class="poi-coordinates"><span class="color-text">X</span><span>{{x}}</span>
            <span class="color-text">Y</span><span>{{y}}</span></span><br />
            <!-- <img src="{{concat_poi c_type}}" class="poi-resource-img"> -->
            <img src="img/dialog/commander/income.png">
            <span class="level-value-2">152</span>
        </div>
    </div>
</div>
{{/each}}
"""

$buildings_content = 
"""
{{#each data}}
<div class="main-buildings-box">
    <img src="{{concat_construction_icon c_type}}" />
    <div class="buildings-shadow-box">
        <div class="main-buildings-stats">
            <span class="color-text">{{lang c_type}}</span>
            <div class="main-building-level-icon"></div>
            <span class="level-value-2">{{level}}</span><br />
            <span class="building-status">Idle</span>
        </div>
        <div class="main-buildings-stats-2">
            {{#each resources.resources}}
                <img src="img/top-resources/{{@key}}.png">
                <span>{{this}}</span><br />
            {{/each}}
        </div>
        <div id="{{construction_id}}" class="building-upgrade-button">Upgrade</div>
    </div>
</div>
{{/each}}
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->

    class MainDialog extends BaseDialog
        constructor: () ->
            super(html)

    MainDialog::onPrologue = () ->
        @setTemplates
            "overview_template" : $overview_content
            "resources_template" : $resources_content
            "pois_template" : $pois_content
            "buildings_template" : $buildings_content
            "upgrade_template" : $upgrade_price


        @upgradeable()

        @closeable()
        @draggable()

    MainDialog::onEpilogue = (hud) ->
        @on "PRE_SHOW", _me_ = () ->
            @html
            .find(".dialog-building-img")
            .attr("src", Handlebars.helpers.concat_dialog_construction("main"))
            @remove "PRE_SHOW", _me_

        @on "SELECTED_TAB", (val) ->
            template = "overview_template"
            if val is "overview-tab"
                datakey = "overview_content"
            else if val is "resources-tab"
                template = "resources_template"
                datakey = "resources_content"
            else if val is "pois-tab"
                template = "pois_template"
                datakey = "pois_content"
            else if val is "buildings-tab"
                template = "buildings_template"
                datakey = "buildings_content"

            #hide all tabs content
            $("div[id^=tab_content_").css('display', 'none')
            #show current content
            $('.science-tabs #tab_content_'+datakey).css('display', 'block') 

            @compile(template, datakey).then (data) =>  
                    @refresh "tab_content_"+template, data
                    @html.find("#tab_content_"+template).customScrollbar();

            @UpgradeBtn = @html.find(".building-upgrade-button")
            @UpgradeBtn.unbind "click"
            @UpgradeBtn.on "click", () ->
                @buildingId = $(@).attr('id')
                @building = amjad.empire.constructions.getByID(@buildingId)
                @building.on "UPGRADED", () =>
                    SOA.getDialog("MAIN_BUILDING").trigger "SELECTED_TAB", "buildings-tab"
                @building.upgrade()

        @on "SHOW", () ->

            @ctx.on "UPGRADED", () =>
                @trigger "SELECTED_TAB", "overview-tab"

            empireResources = amjad.empire.resources.getEmpireResources()

            overviewData =
                city_name: "Citonija"
                player_name: amjad.user.getFullName()
                ruling_title: "Ruler"
                achived_title: "Achiver"
                glory: 12
                rank: 600
                army: 56
                camels: empireResources['majaheem'].amount
                heroes: amjad.empire.heroes.getCountAll()
                horses: empireResources['majaheem'].amount
                cows: empireResources['cow'].amount
                sheeps: empireResources['sheep'].amount
                goats: empireResources['goat'].amount
                pois: amjad.empire.constructions.pois.length

            mainBuildingUpgradeResources = amjad.empire.resources.getConstructionUpgradeResources('main_building', @ctx.level+1)
            #debugger
            poisData = amjad.empire.constructions.pois

            buildings = amjad.empire.constructions.getAll()
            buildingsData = {}
            for value, key in buildings
                upgradeResources = amjad.empire.resources.getConstructionUpgradeResources(value.c_type, value.level+1)
                buildingsData[value.construction_id] = value
                buildingsData[value.construction_id]['resources'] = upgradeResources

            @setData "overview_content", {data : overviewData}
            @setData "resources_content", {data :  empireResources }
            @setData "pois_content", {data : poisData}
            @setData "buildings_content", {data : buildingsData}
            @setData "upgrade_price", {data : mainBuildingUpgradeResources}

            @html.find(".main-tent-content").customScrollbar()

            @compile("upgrade_template", "upgrade_price").then (data) =>  
                @refresh "upgrade-price", data 

            @set("SELECTED_TAB", "overview-tab")
            @trigger "SELECTED_TAB", "overview-tab"

        @clickTriggers
            ".chat-tab/#id" : "SELECTED_TAB"

    return MainDialog