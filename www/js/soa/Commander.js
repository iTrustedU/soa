(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/Hero"], function(Hero) {
    var COMMANDER_EXPERIENCE_MODIFIER, Commander, SPEED_PER_TILE, _ref;
    COMMANDER_EXPERIENCE_MODIFIER = 1000;
    SPEED_PER_TILE = 1.5;
    Commander = (function(_super) {
      __extends(Commander, _super);

      function Commander() {
        _ref = Commander.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return Commander;

    })(Hero);
    Commander.prototype.initListeners = function() {
      Commander.__super__.initListeners.call(this);
      this.EXPERIENCE_MODIFIER = COMMANDER_EXPERIENCE_MODIFIER;
      return this.on("ARRIVAL", function() {
        return this.finishArriving();
      });
    };
    Commander.prototype.doAction = function(action) {
      var col, row,
        _this = this;
      Commander.__super__.doAction.call(this, action);
      row = this.target.parentTile.row;
      col = this.target.parentTile.col;
      return Rest.post("army.move_commander", {
        comid: this.commander_id,
        x: row,
        y: col,
        mt: action
      }).done(function(data) {
        _this.arrival = data.arrival;
        _this.departure = data.departure;
        _this.duration = data.duration;
        _this.move_type = action;
        return _this.moveTo(row, col, data);
      }).fail(function(data) {
        return alert(data);
      });
    };
    Commander.prototype.onAction = function(target) {
      var actBox;
      actBox = SOA.getDialog("ACTION_BOX");
      actBox.setMoveAsDefault();
      actBox.MoveBtn.show();
      console.debug("Target: ", target);
      if (target.type != null) {
        if (target.type === "city") {
          actBox.setMoveAsToCity();
          actBox.AttackBtn.show();
        }
        if (target.type === "poi" && !this.isMyPOI(this.target.parentTile.row, this.target.parentTile.col)) {
          return actBox.CampBtn.show();
        }
      }
    };
    Commander.prototype.isMyPOI = function(row, col) {
      var poi;
      poi = SOA.getPOI(row, col);
      return (poi != null) && poi.user_id === amjad.user.user_id;
    };
    Commander.prototype.finishArriving = function() {
      var _this = this;
      Rest.post("army.arrive", {
        usid: amjad.user.user_id,
        arid: this.army_id
      }).done(function(data) {
        _this.o_coord_x = _this.d_coord_x;
        _this.o_coord_y = _this.d_coord_y;
        return _this.resolveArrival(data);
      }).fail(function(data) {
        return alert(data);
      });
      this.o_coord_x = this.d_coord_x;
      this.o_coord_y = this.d_coord_y;
      return this.layer.stop();
    };
    Commander.prototype.resolveArrival = function(data) {
      console.debug("resolving arrival", data);
      if (this.move_type === "attack") {
        this.doFight(data);
      } else if (this.move_type === "camp") {
        this.doEncampment(data);
      } else {
        this.doMove(data);
      }
      return this.departure = null;
    };
    Commander.prototype.doEncampment = function(data) {
      var k, l, poi, poi_id, subtype, tile, _ref1,
        _this = this;
      if (this.target == null) {
        tile = SOA.Zone.map.getTile(this.d_coord_x, this.d_coord_y);
        _ref1 = tile.layers;
        for (k in _ref1) {
          l = _ref1[k];
          if (l.poid != null) {
            this.target = l;
            break;
          }
        }
      }
      poi_id = this.target.poid;
      subtype = this.target.subtype;
      poi = SOA.pois[subtype][poi_id];
      Rest.post("construction.create_poi", {
        poid: poi.poi_id,
        x: poi.x_coord,
        y: poi.y_coord
      }).done(function(data) {
        var poicn;
        poi.user_id = amjad.user.user_id;
        poicn = SOA.Constructions.loadNewConstruction(data);
        SOA.Constructions.placePOIEncampment(poicn);
        return poicn.epilogue();
      }).fail(function(data) {
        return console.error("PLease no!@!");
      });
      HAL.trigger("CAMP_ESTABLISHED", data);
      return console.debug("I just camped");
    };
    Commander.prototype.doFight = function(data) {
      HAL.trigger("FIGHT_FINISHED", this, data);
      return console.debug("I just fought");
    };
    Commander.prototype.doMove = function(data) {
      HAL.trigger("COMMANDER_MOVED", data);
      return console.debug("I just moved");
    };
    Commander.prototype.getUnits = function() {
      var k, out, v, _ref1;
      out = {};
      _ref1 = this.troops;
      for (k in _ref1) {
        v = _ref1[k];
        if (v == null) {
          continue;
        }
        out[k] = v;
      }
      return out;
    };
    Commander.prototype.addUnit = function(k, amnt) {
      if (this.troops[k] != null) {
        return this.troops[k] += amnt;
      } else {
        return this.troops[k] = amnt;
      }
    };
    Commander.prototype.removeUnit = function(k, amnt) {
      if (this.troops[k] != null) {
        this.troops[k] -= amnt;
        if (this.troops[k] <= 0) {
          return delete this.troops[k];
        }
      }
    };
    return Commander;
  });

}).call(this);
