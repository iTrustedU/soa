(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/Hero"], function(Hero) {
    var SPEED_PER_TILE, TRADER_EXPERIENCE_MODIFIER, Trader, _ref;
    TRADER_EXPERIENCE_MODIFIER = 1000;
    SPEED_PER_TILE = 1.5;
    Trader = (function(_super) {
      __extends(Trader, _super);

      function Trader() {
        _ref = Trader.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return Trader;

    })(Hero);
    Trader.prototype.initListeners = function() {
      Trader.__super__.initListeners.call(this);
      this.EXPERIENCE_MODIFIER = TRADER_EXPERIENCE_MODIFIER;
      return this.on("ARRIVAL", function() {
        return this.finishArriving();
      });
    };
    Trader.prototype.doAction = function(action) {
      var col, row,
        _this = this;
      Trader.__super__.doAction.call(this, action);
      row = this.target.parentTile.row;
      col = this.target.parentTile.col;
      return Rest.post("caravan.move_caravan", {
        cid: this.caravan_id,
        x: row,
        y: col,
        mt: action
      }).done(function(data) {
        _this.arrival = data.arrival;
        _this.departure = data.departure;
        _this.move_type = action;
        return _this.moveTo(row, col, data);
      }).fail(function(data) {
        return alert(data);
      });
    };
    Trader.prototype.moveTo = function(d_coord_x, d_coord_y, data) {
      this.d_coord_x = d_coord_x;
      this.d_coord_y = d_coord_y;
      return this.onMoveTo();
    };
    Trader.prototype.onAction = function(target) {
      var actBox;
      actBox = SOA.getDialog("ACTION_BOX");
      actBox.setMoveAsDefault();
      actBox.MoveBtn.show();
      if (target.type != null) {
        if (target.type === "trade" && !this.isMyPOI(this.target.parentTile.row, this.target.parentTile.col)) {
          return actBox.TradeBtn.show();
        }
      }
    };
    Trader.prototype.isMyPOI = function(row, col) {
      var poi;
      poi = SOA.getPOI(row, col);
      return (poi != null) && poi.user_id === amjad.user.user_id;
    };
    Trader.prototype.finishArriving = function() {
      var _this = this;
      Rest.post("caravan.arrive", {
        usid: amjad.user.user_id,
        cid: this.caravan_id
      }).done(function(data) {
        _this.o_coord_x = _this.d_coord_x;
        _this.o_coord_y = _this.d_coord_y;
        return _this.resolveArrival(data);
      }).fail(function(data) {
        return console.error(data);
      });
      this.o_coord_x = this.d_coord_x;
      this.o_coord_y = this.d_coord_y;
      return this.layer.stop();
    };
    Trader.prototype.resolveArrival = function(data) {
      if (this.move_type === "trade") {
        this.doTrade(data);
      } else {
        this.doMove(data);
      }
      return this.departure = null;
    };
    Trader.prototype.doTrade = function(data) {
      var trpoi;
      console.debug(data);
      trpoi = SOA.getDialog("TRADE_POI");
      return trpoi.show(this);
    };
    Trader.prototype.doMove = function(data) {
      return HAL.trigger("TRADER_MOVED", data);
    };
    Trader.prototype.getCapacity = function() {
      return {
        capacity: this.capacity,
        load: this.getLoad()
      };
    };
    Trader.prototype.getLoad = function() {
      var amount, k, trload, weight, _ref1;
      trload = 0;
      _ref1 = this.bag;
      for (k in _ref1) {
        amount = _ref1[k];
        if (k === "balance") {
          continue;
        }
        weight = +XMLStore.tradegoods[k].weight;
        console.debug(k, amount, weight, amount * weight);
        trload += amount * weight;
        console.debug(trload);
      }
      return trload;
    };
    Trader.prototype.getGoods = function() {
      var k, out, v, _ref1;
      out = {};
      _ref1 = this.bag;
      for (k in _ref1) {
        v = _ref1[k];
        if (k === "balance" || v === 0) {
          continue;
        }
        out[k] = v;
      }
      return out;
    };
    Trader.prototype.getBalance = function() {
      return this.bag.balance;
    };
    Trader.prototype.putInBag = function(good, amnt, price) {
      if (this.bag[good] == null) {
        this.bag[good] = 0;
      }
      this.bag[good] += amnt;
      return this.bag["balance"] -= amnt * price;
    };
    Trader.prototype.takeFromBag = function(good, amnt, price) {
      if (this.bag[good] == null) {
        console.error("You don't have " + good + " good");
        return;
      }
      this.bag[good] -= amnt;
      if (this.bag[good] === 0) {
        delete this.bag[good];
      }
      return this.bag["balance"] += amnt * price;
    };
    Trader.prototype.emptyBag = function() {
      var k, price, total, v, _ref1;
      total = 0;
      _ref1 = this.bag;
      for (k in _ref1) {
        v = _ref1[k];
        if (k === "balance") {
          continue;
        }
        price = +XMLStore.tradegoods[k].value;
        this.takeFromBag(k, v, price);
        total += v * price;
      }
      return this.bag["balance"] += total;
    };
    return Trader;
  });

}).call(this);
