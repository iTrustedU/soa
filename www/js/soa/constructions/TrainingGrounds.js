(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/constructions/Construction"], function(Construction) {
    var TrainingGrounds, _ref;
    TrainingGrounds = (function(_super) {
      __extends(TrainingGrounds, _super);

      function TrainingGrounds() {
        _ref = TrainingGrounds.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return TrainingGrounds;

    })(Construction);
    TrainingGrounds.prototype.init = function() {
      var hspruri, k, spr, unit, _ref1, _results;
      this.units = {};
      _ref1 = XMLStore.get("units");
      _results = [];
      for (k in _ref1) {
        unit = _ref1[k];
        hspruri = "assets/sprites/amjad/units/" + amjad.user.architecture + "/44x44/" + k + ".png";
        spr = PIXI.TextureCache[hspruri];
        if ((unit.price != null) && (spr != null)) {
          _results.push(this.units[k] = unit);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    TrainingGrounds.prototype.epilogue = function() {
      if (this.action_type === "produce") {
        alert(this.u_type);
        return alert();
      }
    };
    TrainingGrounds.prototype.beginUnitProduction = function(unit, res) {
      amjad.empire.resources.take(res);
      return this.assignUnitProduction(unit);
    };
    TrainingGrounds.prototype.assignUnitProduction = function(unit) {
      var _this = this;
      this.production.push(unit);
      return SOA.Hud.getDialog("BUILD_ORDERS").addUnitToTrainingQueue(unit, "Training").done(function() {
        return amjad.finishProduction(_this.construction_id).done(function(data) {
          var war, warcnt;
          console.debug(data);
          warcnt = (amjad.empire.constructions.getAllOfType("war")[0]);
          warcnt.addUnit(unit);
          console.debug(warcnt);
          war = SOA.Hud.getDialog("WAR");
          if (war.opened) {
            return war.show(warcnt);
          }
        });
      });
    };
    TrainingGrounds.prototype.addToMap = function() {
      return this.addToCityMap();
    };
    TrainingGrounds.prototype.getTrainers = function() {
      return amjad.empire.heroes.trainers;
    };
    TrainingGrounds.prototype.finishUnitProduction = function() {
      return amjad.finishProduction(this.construction_id).done(function(data) {
        console.debug("Finished unit production");
        return console.debug(data);
      }).fail(function() {
        return alert("omg. failed to finish unit production");
      });
    };
    return TrainingGrounds;
  });

}).call(this);
