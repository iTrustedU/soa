(function() {
  "use strict";
  var $commander_inventory, $commander_name, $commander_troops, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"commander draggable\">\n    <div class=\"dialog-name\">\n        <span>C O M M A N D E R</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"commander-info\">\n        <div class=\"commander-statistics\"> \n            <span class=\"commander-level-name\"><img src=\"img/dialog/training/level.png\" /></span>\n            <span class=\"commander-level-value\">{{level}}</span>\n            <span class=\"commander-expirience-name color-text\">Experience:</span>\n            <span class=\"commander-current-expirience\">{{experience}} / {{nextLevelExperience}}</span>\n            <span class=\"commander-level-name color-text\">Readiness:</span>\n            <span class=\"commander-level-value\">{{readiness}}</span>\n            <span class=\"commander-level-name color-text\">Speed:</span>\n            <span class=\"commander-level-value\">{{speed}}</span>\n            <span class=\"commander-level-name color-text\">Leadership:</span>\n            <span class=\"commander-level-value\">{{leadership}}</span>\n        </div>\n    </div>\n    <div id=\"commander-name\" class=\"commander-name\">\n        <span>{{name}}</span>\n    </div>\n    <div class=\"commander-inventory\">\n    </div>\n    <div class=\"commander-equipment\">\n        <img src=\"img/dialog/commander/equipslika.png\" class=\"equip-img\" />\n        <img src=\"img/dialog/commander/equipment-holder.png\" class=\"equipment-holder\" />\n        <div class=\"equipment-box-b head\"></div>\n        <div class=\"equipment-box-b back\"></div>\n        <div class=\"equipment-box-b torso\"></div>\n        <div class=\"equipment-box-b r-hand\"></div>\n        <div class=\"equipment-box-b l-hand\"></div>\n        <div class=\"equipment-box-b legs\"></div>\n        <div class=\"equipment-box-b boots\"></div>\n        <div class=\"equipment-box-s neck\"></div>\n        <div class=\"equipment-box-s l-ring\"></div>\n        <div class=\"equipment-box-s r-ring\"></div>\n    </div>\n    <div class=\"commander-exp-bar-box\">\n        <div class=\"commander-bar\">\n            <div class=\"commander-bar-full\" {{calc_experience_percentage this}}></div>\n        </div>\n    </div>\n    <div id=\"commander-troops\" class=\"commander-troops\">\n        {{{commander_troops}}}\n    </div>\n    <div class=\"attach-agent\">\n    </div>\n</div>";

  $commander_name = "<span>{{name}}</span>";

  $commander_inventory = "<img src=\"{{concat_sprite sprite}}\" id=\"{{name}}\" type=\"{{type}}\">";

  $commander_troops = "{{#each getUnits}}\n    <div class=\"unit-box3\">\n        <img id=\"{{key}}\" src={{concat_unit_avatar @key \"borderless\"}}>\n        <span class=\"units-amount2\">{{this}}</span>\n    </div>\n{{/each}}";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var CommanderDialog;
    CommanderDialog = (function(_super) {
      __extends(CommanderDialog, _super);

      function CommanderDialog() {
        CommanderDialog.__super__.constructor.call(this, html);
      }

      return CommanderDialog;

    })(BaseDialog);
    CommanderDialog.prototype.onPrologue = function() {
      this.setTemplates({
        "commander_troops": $commander_troops,
        "commander_window": html
      });
      return this.on("SHOW", function() {
        var _this = this;
        this.setData("commander", this.ctx);
        this.compileHtml(this.ctx);
        this.compile("commander_troops", "commander").then(function(data) {
          return _this.refresh("commander-troops", data);
        });
        this.closeable();
        return this.draggable();
      });
    };
    return CommanderDialog;
  });

}).call(this);
