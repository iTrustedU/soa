mongoose = require "mongoose"
Settlement = require "./Settlement"
Commander = require "./heroes/Commander"

schema = mongoose.Schema
	name:
		type: String
		required: true

	settlements:
		type: ObjectId
		ref: Settlement
		required: true

	heroes:
		commanders:
			type: ObjectId
			ref: Commander
			default: []

module.exports = mongoose.model("Empire", schema)