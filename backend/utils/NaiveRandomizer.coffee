# Generates random numbers in clamped range from a known starting seed
# Used for an optimization on TileMeta caching in Map model

fs = require "fs"
module.exports = (generating_seed) ->
	numc = Math.sin(generating_seed++) * 10000
	return (numc - Math.floor(numc))