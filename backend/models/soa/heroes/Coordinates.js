(function() {
  var mongoose, schema;

  mongoose = require("mongoose");

  schema = {
    ox: {
      type: Number,
      required: true
    },
    oy: {
      type: Number,
      required: true
    },
    dx: {
      type: Number,
      required: true
    },
    dy: {
      type: Number,
      required: true
    }
  };

  module.exports.Coordinates = mongoose.model("Coordinates", schema);

}).call(this);
