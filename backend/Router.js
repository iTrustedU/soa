(function() {
  var und, validator, wrench;

  validator = require("express-validator");

  wrench = require("wrench");

  und = require("underscore");

  module.exports = function(app, passport) {
    return wrench.readdirSyncRecursive("./backend/controllers").filter(function(cntrl) {
      return cntrl.endsWith(".js");
    }).forEach(function(cntl) {
      return require("../backend/controllers/" + cntl)(app);
    });
  };

}).call(this);
