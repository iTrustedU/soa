"use strict"

define [], 
() ->

	class ImageUtils
		constructor: () ->
			# i should specify this
			@hit_ctx = @createCanvas(1, 1).getContext("2d")
			### 
			 @todo 
				Ovo treba biti maks velicine
			###
			# @tint_ctx = @createCanvas(1024, 1024).getContext("2d")

		createCanvas: (w, h) ->
			canvas = document.createElement("canvas")
			canvas.width = w
			canvas.height = h
			return canvas

		clipImage: (img, area) ->
			canvas  = @createCanvas(area.w, area.h)
			ctx     = canvas.getContext("2d")
			ctx.drawImage(img, area.x, area.y, area.w, area.h, 0, 0, area.w, area.h)
			img     = new Image()
			img.src = canvas.toDataURL("image/png")
			return img            

		isTransparent: (img, x, y) ->
			@hit_ctx.drawImage(img, x, y, 1, 1, 0, 0, 1, 1)
			data = @hit_ctx.getImageData(0, 0, 1, 1).data
			@hit_ctx.clearRect(0, 0, 1, 1)
			return data[3] < 150
		
		getPixelAtHitImg: (img, x, y) ->
			@hit_ctx.drawImage(img, x, y, 1, 1, 0, 0, 1, 1)
			data = @hit_ctx.getImageData(0, 0, 1, 1).data
			pos = (x + y) * 4
			return [
				data[pos], 
				data[pos+1], 
				data[pos+2], 
				data[pos+3]
			]

		putPixelToBuffer: (buffer, x, y, color_pack) ->
			pos = (x + buffer.width * y) * 4
			console.debug color_pack
			buffer.data[pos] = color_pack[0]
			buffer.data[pos+1] = color_pack[1]
			buffer.data[pos+2] = color_pack[2]
			buffer.data[pos+3] = 0xFF #color_pack[3]
			return buffer

		getPixelAtBuffer: (buffer, x, y) ->
			pos = (x + buffer.width * y) * 4
			return [
				buffer.data[pos], 
				buffer.data[pos+1], 
				buffer.data[pos+2], 
				buffer.data[pos+3]
			]

		hexToRGBString: (hex) ->
			blue = (hex >> 16 & 0xFF)
			green = (hex >> 8 & 0xFF)
			red = (hex & 0xFF)
			return "rgba("+red+","+green+","+blue+",1)"

		hexToRGBAString: (hex) ->
			red = (hex >> 24 & 0xFF)
			green = (hex >> 16 & 0xFF)
			blue = (hex >> 8 & 0xFF)
			alpha = (hex & 0xFF)
			return "rgba("+red+","+green+","+blue+","+alpha+")"

		putPixelAtImg: (img, color) -> #rgba format
			return

		savePixelBufferToImg: (buffer) -> return

		readPixelBufferFromImg: (image) -> return

		readPixelBufferFromCanvas: (canvas) -> return

		createPixelBuffer: (w, h, fill_color=0xFFFFFFFF) ->
			canvas = @createCanvas(w, h)
			ctx = canvas.getContext("2d")
			ctx.fillStyle = @hexToRGBAString(fill_color)
			ctx.fillRect(0, 0, w, h)
			return ctx.getImageData(0, 0, w, h)

		pixelBufferToImg: (buffer) ->
			canvas = @createCanvas(buffer.width, buffer.height)
			ctx = canvas.getContext("2d")
			ctx.mozImageSmoothingEnabled = false
			ctx.webkitImageSmoothingEnabled = false
			ctx.imageSmoothingEnabled = false
			ctx.putImageData(buffer, 0, 0)
			img = new Image()
			img.src = canvas.toDataURL("image/png", 1)
			return img

		encode64Buffer: (buffer) ->
			canvas = @createCanvas(buffer.width, buffer.height)
			ctx = canvas.getContext("2d")
			ctx.mozImageSmoothingEnabled = false
			ctx.webkitImageSmoothingEnabled = false
			ctx.imageSmoothingEnabled = false
			ctx.putImageData(buffer, 0, 0)
			return canvas.toDataURL("image/png", 1)

		imgToPixelBuffer: (img) ->
			canvas = @createCanvas(img.width, img.height)
			ctx = canvas.getContext("2d")
			ctx.mozImageSmoothingEnabled = false
			ctx.webkitImageSmoothingEnabled = false
			ctx.imageSmoothingEnabled = false
			ctx.drawImage(img, 0, 0)
			return ctx.getImageData(0, 0, img.width, img.height)

		base64ToPixelBuffer: (base64data) ->
			img = new Image()
			img.src = "data:image/png;base64,#{base64data}"
			return @imgToPixelBuffer(img)

		colorChannelToInt: (channel) ->
			red = channel[0]
			green = channel[1]
			blue = channel[2]
			alpha = if channel[3]? then channel[3] else 255
			
		tintImage: (img, color, opacity) ->
			console.warn("Not implemented")
			return
			tint_buff = @createCanvas(img.width, img.height)
			tint_ctx = tint_buff.getContext("2d")
			tint_ctx.globalAlpha = 1.0
			tint_ctx.drawImage(img, 0, 0)
			tint_ctx.globalAlpha = opacity
			tint_ctx.globalCompositeOperation = 'source-atop'        
			tint_ctx.fillStyle = color
			tint_ctx.fillRect(0, 0, img.width, img.height)
			return tint_buff
			
	return ImageUtils