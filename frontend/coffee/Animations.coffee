"use strict"

define [], () ->

	class Animations
		constructor: () -> return #@init()

	Animations::load = () ->
		@trader =
			east: [
				PIXI.Texture.fromFrame("trader/e/e_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/e/e_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/e/e_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/e/e_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/e/e_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/e/e_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/e/e_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/e/e_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/e/e_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/e/e_0009_Layer-1.png")
			]
			north: [
				PIXI.Texture.fromFrame("trader/n/n_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/n/n_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/n/n_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/n/n_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/n/n_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/n/n_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/n/n_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/n/n_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/n/n_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/n/n_0009_Layer-1.png")
			]
			northeast: [
				PIXI.Texture.fromFrame("trader/ne/ne_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/ne/ne_0009_Layer-1.png")
			]
			northwest: [
				PIXI.Texture.fromFrame("trader/nw/nw_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/nw/nw_0009_Layer-1.png")
			]
			south: [
				PIXI.Texture.fromFrame("trader/s/s_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/s/s_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/s/s_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/s/s_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/s/s_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/s/s_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/s/s_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/s/s_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/s/s_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/s/s_0009_Layer-1.png")
			]
			southeast: [
				PIXI.Texture.fromFrame("trader/se/se_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/se/se_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/se/se_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/se/se_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/se/se_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/se/se_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/se/se_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/se/se_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/se/se_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/se/se_0009_Layer-1.png")
			]
			southwest: [
				PIXI.Texture.fromFrame("trader/sw/sw_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/sw/sw_0009_Layer-1.png")
			]
			west: [
				PIXI.Texture.fromFrame("trader/w/w_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("trader/w/w_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("trader/w/w_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("trader/w/w_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("trader/w/w_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("trader/w/w_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("trader/w/w_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("trader/w/w_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("trader/w/w_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("trader/w/w_0009_Layer-1.png")
			]

		@agent =
			east: [
				PIXI.Texture.fromFrame("agent/walk/e/1.png")
				PIXI.Texture.fromFrame("agent/walk/e/2.png")
				PIXI.Texture.fromFrame("agent/walk/e/3.png")
				PIXI.Texture.fromFrame("agent/walk/e/4.png")
				PIXI.Texture.fromFrame("agent/walk/e/5.png")
				PIXI.Texture.fromFrame("agent/walk/e/6.png")
				PIXI.Texture.fromFrame("agent/walk/e/7.png")
				PIXI.Texture.fromFrame("agent/walk/e/8.png")
				PIXI.Texture.fromFrame("agent/walk/e/9.png")
				PIXI.Texture.fromFrame("agent/walk/e/10.png")
			]
			north: [
				PIXI.Texture.fromFrame("agent/walk/n/1.png")
				PIXI.Texture.fromFrame("agent/walk/n/2.png")
				PIXI.Texture.fromFrame("agent/walk/n/3.png")
				PIXI.Texture.fromFrame("agent/walk/n/4.png")
				PIXI.Texture.fromFrame("agent/walk/n/5.png")
				PIXI.Texture.fromFrame("agent/walk/n/6.png")
				PIXI.Texture.fromFrame("agent/walk/n/7.png")
				PIXI.Texture.fromFrame("agent/walk/n/8.png")
				PIXI.Texture.fromFrame("agent/walk/n/9.png")
				PIXI.Texture.fromFrame("agent/walk/n/10.png")
			]
			northeast: [
				PIXI.Texture.fromFrame("agent/walk/ne/1.png")
				PIXI.Texture.fromFrame("agent/walk/ne/2.png")
				PIXI.Texture.fromFrame("agent/walk/ne/3.png")
				PIXI.Texture.fromFrame("agent/walk/ne/4.png")
				PIXI.Texture.fromFrame("agent/walk/ne/5.png")
				PIXI.Texture.fromFrame("agent/walk/ne/6.png")
				PIXI.Texture.fromFrame("agent/walk/ne/7.png")
				PIXI.Texture.fromFrame("agent/walk/ne/8.png")
				PIXI.Texture.fromFrame("agent/walk/ne/9.png")
				PIXI.Texture.fromFrame("agent/walk/ne/10.png")
			]
			northwest: [
				PIXI.Texture.fromFrame("agent/walk/nw/1.png")
				PIXI.Texture.fromFrame("agent/walk/nw/2.png")
				PIXI.Texture.fromFrame("agent/walk/nw/3.png")
				PIXI.Texture.fromFrame("agent/walk/nw/4.png")
				PIXI.Texture.fromFrame("agent/walk/nw/5.png")
				PIXI.Texture.fromFrame("agent/walk/nw/6.png")
				PIXI.Texture.fromFrame("agent/walk/nw/7.png")
				PIXI.Texture.fromFrame("agent/walk/nw/8.png")
				PIXI.Texture.fromFrame("agent/walk/nw/9.png")
				PIXI.Texture.fromFrame("agent/walk/nw/10.png")
			]
			south: [
				PIXI.Texture.fromFrame("agent/walk/s/1.png")
				PIXI.Texture.fromFrame("agent/walk/s/2.png")
				PIXI.Texture.fromFrame("agent/walk/s/3.png")
				PIXI.Texture.fromFrame("agent/walk/s/4.png")
				PIXI.Texture.fromFrame("agent/walk/s/5.png")
				PIXI.Texture.fromFrame("agent/walk/s/6.png")
				PIXI.Texture.fromFrame("agent/walk/s/7.png")
				PIXI.Texture.fromFrame("agent/walk/s/8.png")
				PIXI.Texture.fromFrame("agent/walk/s/9.png")
				PIXI.Texture.fromFrame("agent/walk/s/10.png")
			]
			southeast: [
				PIXI.Texture.fromFrame("agent/walk/se/1.png")
				PIXI.Texture.fromFrame("agent/walk/se/10.png")
				PIXI.Texture.fromFrame("agent/walk/se/2.png")
				PIXI.Texture.fromFrame("agent/walk/se/3.png")
				PIXI.Texture.fromFrame("agent/walk/se/4.png")
				PIXI.Texture.fromFrame("agent/walk/se/5.png")
				PIXI.Texture.fromFrame("agent/walk/se/6.png")
				PIXI.Texture.fromFrame("agent/walk/se/7.png")
				PIXI.Texture.fromFrame("agent/walk/se/8.png")
				PIXI.Texture.fromFrame("agent/walk/se/9.png")
			]
			southwest: [
				PIXI.Texture.fromFrame("agent/walk/sw/1.png")
				PIXI.Texture.fromFrame("agent/walk/sw/10.png")
				PIXI.Texture.fromFrame("agent/walk/sw/2.png")
				PIXI.Texture.fromFrame("agent/walk/sw/3.png")
				PIXI.Texture.fromFrame("agent/walk/sw/4.png")
				PIXI.Texture.fromFrame("agent/walk/sw/5.png")
				PIXI.Texture.fromFrame("agent/walk/sw/6.png")
				PIXI.Texture.fromFrame("agent/walk/sw/7.png")
				PIXI.Texture.fromFrame("agent/walk/sw/8.png")
				PIXI.Texture.fromFrame("agent/walk/sw/9.png")
			]
			west: [
				PIXI.Texture.fromFrame("agent/walk/w/1.png")
				PIXI.Texture.fromFrame("agent/walk/w/10.png")
				PIXI.Texture.fromFrame("agent/walk/w/2.png")
				PIXI.Texture.fromFrame("agent/walk/w/3.png")
				PIXI.Texture.fromFrame("agent/walk/w/4.png")
				PIXI.Texture.fromFrame("agent/walk/w/5.png")
				PIXI.Texture.fromFrame("agent/walk/w/6.png")
				PIXI.Texture.fromFrame("agent/walk/w/7.png")
				PIXI.Texture.fromFrame("agent/walk/w/8.png")
				PIXI.Texture.fromFrame("agent/walk/w/9.png")
			]
		
		@commander =
			east: [
				PIXI.Texture.fromFrame("commander/e/e_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/e/e_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/e/e_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/e/e_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/e/e_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/e/e_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/e/e_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/e/e_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/e/e_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/e/e_0009_Layer-1.png")
			]
			north: [
				PIXI.Texture.fromFrame("commander/n/n_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/n/n_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/n/n_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/n/n_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/n/n_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/n/n_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/n/n_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/n/n_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/n/n_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/n/n_0009_Layer-1.png")
			]
			northeast: [
				PIXI.Texture.fromFrame("commander/ne/ne_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/ne/ne_0009_Layer-1.png")
			]
			northwest: [
				PIXI.Texture.fromFrame("commander/nw/nw_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/nw/nw_0009_Layer-1.png")
			]
			south: [
				PIXI.Texture.fromFrame("commander/s/s_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/s/s_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/s/s_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/s/s_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/s/s_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/s/s_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/s/s_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/s/s_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/s/s_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/s/s_0009_Layer-1.png")
			]
			southeast: [
				PIXI.Texture.fromFrame("commander/se/se_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/se/se_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/se/se_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/se/se_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/se/se_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/se/se_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/se/se_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/se/se_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/se/se_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/se/se_0009_Layer-1.png")
			]
			southwest: [
				PIXI.Texture.fromFrame("commander/sw/sw_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/sw/sw_0009_Layer-1.png")
			]
			west: [
				PIXI.Texture.fromFrame("commander/w/w_0000_Layer-10.png"),
				PIXI.Texture.fromFrame("commander/w/w_0001_Layer-9.png"),
				PIXI.Texture.fromFrame("commander/w/w_0002_Layer-8.png"),
				PIXI.Texture.fromFrame("commander/w/w_0003_Layer-7.png"),
				PIXI.Texture.fromFrame("commander/w/w_0004_Layer-6.png"),
				PIXI.Texture.fromFrame("commander/w/w_0005_Layer-5.png"),
				PIXI.Texture.fromFrame("commander/w/w_0006_Layer-4.png"),
				PIXI.Texture.fromFrame("commander/w/w_0007_Layer-3.png"),
				PIXI.Texture.fromFrame("commander/w/w_0008_Layer-2.png"),
				PIXI.Texture.fromFrame("commander/w/w_0009_Layer-1.png")
			]
	return Animations