{
    "baseUrl": "js",
    
    "paths": {
        "requireLib" : "../vendor/requirejs/require",
        "PIXI": "../vendor/pixi/bin/pixi.dev",
        "jquery": "../vendor/jquery/jquery.min",
        "jquery-ui": "../vendor/jquery-ui/ui/minified/jquery-ui.min",
        "handlebars": "../vendor/handlebars/handlebars.min",
        "jquery.cookie": "../vendor/jquery-cookie/jquery.cookie",
        "jquery-perfect-scrollbar": "../vendor/jquery-custom-scrollbar/jquery.custom-scrollbar",
        "tweener": "../vendor/tween.min"
    },
    "shim": {
        "jquery": {
            exports: "$"
        },
        "jquery.cookie": {
            exports: "$",
            deps: ["jquery"]
        },
        "jquery-ui": {
            deps: ["jquery"],
            exports: "$"
        },
        "jquery-perfect-scrollbar": {
            deps: ["jquery"],
            exports: "$"
        },
        "PIXI": {
            exports: "PIXI"        
        },
        "handlebars": {
            exports: "Handlebars"
        }
    },
    "removeCombined": true,
    "findNestedDependencies": true,
    "name" : "soa/main",
    "out" : "build/soa.js"
}
