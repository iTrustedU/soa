(function() {
  "use strict";
  define(["MathUtils", "PIXI"], function(MathUtils, PIXI) {
    var FreeList, POOL_SIZE, Vec2, i, v, _i;
    POOL_SIZE = 25000;
    Vec2 = {};
    MathUtils.Vec2 = Vec2;
    FreeList = [];
    Vec2.free = POOL_SIZE;
    for (i = _i = 0; 0 <= POOL_SIZE ? _i < POOL_SIZE : _i > POOL_SIZE; i = 0 <= POOL_SIZE ? ++_i : --_i) {
      v = new PIXI.Point();
      FreeList.push(v);
    }
    Vec2.release = function(vec) {
      if (Vec2.free > POOL_SIZE) {
        throw new Error("You released a vector which you didn't acquired");
      }
      Vec2.free++;
      Vec2.set(vec, 0, 0);
      return FreeList.push(vec);
    };
    Vec2.acquire = function() {
      if (Vec2.free <= 0) {
        throw new Error("No more free vectors in pool");
      }
      Vec2.free--;
      return FreeList.pop();
    };
    Vec2.create = function() {
      return Vec2.acquire();
    };
    Vec2.newFrom = function(a) {
      v = Vec2.acquire();
      v.x = a.x;
      v.y = a.y;
      return v;
    };
    Vec2.clone = function(a) {
      v = Vec2.acquire();
      v.x = a.x;
      v.y = a.y;
      return v;
    };
    Vec2.copy = function(out, a) {
      out.x = a.x;
      out.y = a.y;
      return out;
    };
    Vec2.from = function(x, y) {
      v = Vec2.acquire();
      v.x = x;
      v.y = y;
      return v;
    };
    Vec2.set = function(out, x, y) {
      out.x = x;
      out.y = y;
      return out;
    };
    Vec2.add = function(out, a, b) {
      out.x = a.x + b.x;
      out.y = a.y + b.y;
      return out;
    };
    Vec2.sub = function(out, a, b) {
      out.x = a.x - b.x;
      out.y = a.y - b.y;
      return out;
    };
    Vec2.mul = function(out, a, b) {
      out.x = a.x * b.x;
      out.y = a.y * b.y;
      return out;
    };
    Vec2.divide = function(out, a, b) {
      out.x = a.x / b.x;
      out.y = a.y / b.y;
      return out;
    };
    Vec2.min = function(out, a, b) {
      out.x = Math.min(a.x, b.x);
      out.y = Math.min(a.y, b.y);
      return out;
    };
    Vec2.max = function(out, a, b) {
      out.x = Math.max(a.x, b.x);
      out.y = Math.max(a.y, b.y);
      return out;
    };
    Vec2.scale = function(out, a, b) {
      out.x = a.x * b;
      out.y = a.y * b;
      return out;
    };
    Vec2.scaleAndAdd = function(out, a, b, scale) {
      out.x = a.x + (b.x * scale);
      out.y = a.y + (b.y * scale);
      return out;
    };
    Vec2.addAndScale = function(out, a, b, scale) {
      out.x = (a.x + b.x) * scale;
      out.y = (a.y + b.y) * scale;
      return out;
    };
    Vec2.distance = function(a, b) {
      var x, y;
      x = b.x - a.x;
      y = b.y - a.y;
      return Math.sqrt(x * x + y * y);
    };
    Vec2.sqDistance = function(a, b) {
      var x, y;
      x = b.x - a.x;
      y = b.y - a.y;
      return x * x + y * y;
    };
    Vec2.length = function(a) {
      var x, y;
      x = a[0], y = a[1];
      return Math.sqrt(x * x + y * y);
    };
    Vec2.sqLength = function(a) {
      var x, y;
      x = a[0], y = a[1];
      return x * x + y * y;
    };
    Vec2.negate = function(out, a) {
      out.x = -a.x;
      out.y = -a.y;
      return out;
    };
    Vec2.normalize = function(out, a) {
      var len, x, y;
      x = a[0], y = a[1];
      len = x * x + y * y;
      if (len > 0) {
        len = 1 / Math.sqrt(len);
        out.x = a.x * len;
        out.y = a.y * len;
      }
      return out;
    };
    Vec2.dot = function(a, b) {
      return a.x * b.x + a.y * b.y;
    };
    Vec2.lerp = function(out, a, b, t) {
      out.x = a.x + t * (b.x - a.x);
      out.y = a.y + t * (b.y - a.y);
      return out;
    };
    Vec2.random = function(out, scale) {
      var r;
      if (scale == null) {
        scale = 1;
      }
      r = Math.random() * 2.0 * Math.PI;
      out.x = Math.cos(r) * scale;
      out.y = Math.sin(r) * scale;
      return out;
    };
    Vec2.transformMat3 = function(out, a, m) {
      out.x = m[0] * a.x + m[1] * a.y + m[2];
      out.y = m[3] * a.y + m[4] * a.y + m[5];
      return out;
    };
    Vec2.perpendicular = function(out, a) {
      out.x = a.y;
      out.y = -a.x;
      return out;
    };
    Vec2.str = function(a) {
      return "Vec2(" + (a.x.toFixed(2)) + ", " + (a.y.toFixed(2)) + ")";
    };
    return Vec2;
  });

}).call(this);
