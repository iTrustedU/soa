"use strict"

define ["soa/StructureLoader"], (StructureLoader) ->

	ShortMap =
		"fr": "fraction"
		"fn": "last_name"
		"n": "name"
		"trb": "tribe"
		"usid": "user_id"
		"usn": "username"
		"arch": "architecture"

	class User extends StructureLoader
		constructor: () ->
			super(ShortMap)

		getFullName: () ->
			return "#{@name} #{@last_name}"

		initListeners: () ->
			@on "DONE_LOADING", () ->
				if @architecture is "arabian"
					@architecture is "civil"
					@fraction is "civil"
				@ASSETS_URI = "assets/sprites/"
				@CONSTRUCTION_SPRITES_URI = "amjad/constructions/#{@architecture}/"	
				@TECH_RESEARCH_SPRITES_URI = "amjad/technologies/"	
				@SCIENCE_RESEARCH_SPRITES_URI = "amjad/science/"	
				@DIALOG_CONSTRUCTIONS_URI = "amjad/dialogs/constructions/#{@architecture}/"
				
				# kad beduini nemaju home, we take care of it
				if @architecture is "bedouin"
					delete XMLStore.constructions.city["home"]
	return User

