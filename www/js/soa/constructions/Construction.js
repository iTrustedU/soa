(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader"], function(StructureLoader) {
    var Construction;
    Construction = (function(_super) {
      __extends(Construction, _super);

      function Construction(meta) {
        this.loaded = false;
        this.layer = null;
        this.production = [];
        this.CONSTRUCTION_LEVELING_DIVISOR = 1 / 5;
        Construction.__super__.constructor.call(this, meta);
      }

      return Construction;

    })(StructureLoader);
    Construction.prototype.epilogue = function() {
      var _this = this;
      if (this.action_type === "build") {
        if (this.duration > 0) {
          if (this.poi_id != null) {
            this.addOnEncampQueue();
          } else {
            this.addOnBuildQueue();
          }
        } else {
          alert("ovo ne bi trebalo nikada da se desi");
          this.finishBuild().done(function(data) {
            _this.load(data);
            return amjad.trigger("CONSTRUCTIONS_UPDATED");
          }).fail(function(err) {
            return console.error(err);
          });
        }
      }
    };
    Construction.prototype.initListeners = function() {
      Construction.__super__.initListeners.call(this);
      return this.on("DONE_LOADING", function() {
        console.debug("Loading new construction " + this.c_type);
        return this.loaded = true;
      });
    };
    Construction.prototype.upgrade = function() {
      var next_level, okay,
        _this = this;
      next_level = (this.level + 1) * this.CONSTRUCTION_LEVELING_DIVISOR;
      console.debug(next_level);
      okay = true;
      if (next_level % 1 === 0) {
        if (!this.canBeUpgraded(next_level + 1)) {
          alert("You'll have to relocate the building");
          okay = false;
        }
      }
      if (okay) {
        return Rest.post("construction.upgrade_building", {
          cid: this.construction_id,
          ty: this.c_type,
          lvl: this.level + 1
        }).done(function(data) {
          _this.duration = data.duration;
          return _this.addOnUpgradeQueue();
        }).fail(function(data) {
          return console.error(data);
        });
      }
    };
    Construction.prototype.changeLevel = function() {
      var normalized, tex_name;
      this.level += 1;
      normalized = Math.floor(this.CONSTRUCTION_LEVELING_DIVISOR * this.level) + 1;
      console.debug("NORMALIZED LEVEL " + normalized);
      if (this.canBeUpgraded(normalized)) {
        tex_name = "assets/sprites/" + amjad.user.CONSTRUCTION_SPRITES_URI + (this.getLevelNormalizedName()) + ".png";
        return this.layer.changeTexture(tex_name);
      }
    };
    Construction.prototype.getNormalizedLevel = function() {
      return Math.floor(this.CONSTRUCTION_LEVELING_DIVISOR * this.level) + 1;
    };
    Construction.prototype.getLevelNormalizedName = function() {
      return "" + this.c_type + (this.getNormalizedLevel());
    };
    Construction.prototype.getSpan = function(to_level) {
      var leveled_meta, lvl_meta_name, span, span_size;
      if (to_level == null) {
        to_level = this.getNormalizedLevel();
      }
      lvl_meta_name = amjad.splaySpriteURI("" + amjad.user.CONSTRUCTION_SPRITES_URI + this.c_type + to_level);
      leveled_meta = HAL.TileMetaStorage.findByName(lvl_meta_name);
      if (leveled_meta == null) {
        return null;
      }
      span_size = leveled_meta.size;
      span = SOA.City.map.getSpanArea(this.layer.parentTile, span_size);
      return span;
    };
    Construction.prototype.getSpanFromTile = function(tile, to_level) {
      var leveled_meta, lvl_meta_name, span, span_size;
      if (to_level == null) {
        to_level = this.getNormalizedLevel();
      }
      lvl_meta_name = amjad.splaySpriteURI("" + amjad.user.CONSTRUCTION_SPRITES_URI + this.c_type + to_level);
      leveled_meta = HAL.TileMetaStorage.findByName(lvl_meta_name);
      if (leveled_meta == null) {
        return null;
      }
      span_size = leveled_meta.size;
      span = SOA.City.map.getSpanArea(tile, span_size);
      return span;
    };
    Construction.prototype.drawSpan = function(level, color) {
      var span;
      if (color == null) {
        color = 0x0292CD;
      }
      span = this.getSpan(level);
      return SOA.City.map.tintSpan(span, 2, color);
    };
    Construction.prototype.canBeUpgraded = function(to_level) {
      /*
      				@todo treba pronaci span za sledeci nivo
      				fillovati matricu za sve gradjevionCollisionne 
      				zatim proveriti ima li preseka u bitovima
      				za gradjevine posmatramo samo 1 i 0
      */

      var collisions, span;
      span = this.getSpan(to_level);
      collisions = amjad.empire.constructions.testSpanArea(span, this.construction_id);
      if (collisions.length > 0) {
        this.onCollision(collisions, function() {
          return TweenMax.fromTo(this.layer, 1.1, {
            alpha: 0.1,
            repeat: 5
          }, {
            alpha: 1,
            repeat: 5
          });
        });
        return false;
      }
      return true;
    };
    Construction.prototype.addToMap = function() {
      return this.addToCityMap();
    };
    Construction.prototype.finishBuild = function() {
      this.layer.alpha = 1;
      return Rest.post("construction.finish_build", {
        cid: this.construction_id,
        spec: "spec1"
      });
    };
    Construction.prototype.assignLayer = function(layer) {
      var _this = this;
      if (layer == null) {
        throw new Error("Layer couldn't be assigned to construction " + this.c_type + " at " + this.x + ", " + this.y);
      }
      this.layer = layer;
      this.layer.onLeftClick = function() {
        return SOA.Hud.showDialog(_this.c_type.toUpperCase(), _this);
      };
      if (this.level === 0) {
        this.level = 1;
      }
      if (this.action_type === "build") {
        return this.layer.alpha = 0.5;
      }
    };
    Construction.prototype.addToCityMap = function() {
      var layer;
      console.debug("Added to city map", this);
      layer = amjad.putConstructionInCity(this.x, this.y, this.getLevelNormalizedName());
      this.assignLayer(layer);
      if (this.duration > 0) {
        return this.addOnBuildQueue();
      }
    };
    Construction.prototype.addOnBuildQueue = function() {
      var _this = this;
      return SOA.Hud.getDialog("BUILD_ORDERS").addConstructionToBuildQueue(this, "Constructing").done(function() {
        return _this.finishBuild().done(function(data) {
          return _this.load(data);
        });
      });
    };
    Construction.prototype.addOnUpgradeQueue = function() {
      var _this = this;
      return SOA.Hud.getDialog("BUILD_ORDERS").addConstructionToUpgradeQueue(this, "Upgrading to level " + (this.level + 1)).done(function() {
        _this.changeLevel();
        return _this.trigger("UPGRADED", _this.level);
      });
    };
    Construction.prototype.addOnEncampQueue = function() {
      return SOA.Hud.getDialog("BUILD_ORDERS").addPOIToEncampmentQueue(this, "Encamping " + (Handlebars.helpers.lang(this.c_type)));
    };
    Construction.prototype.onCollision = function(collisions, effect) {
      var all_cns, cn, cnid, collided, _i, _j, _len, _len1, _results;
      all_cns = [];
      for (_i = 0, _len = collisions.length; _i < _len; _i++) {
        collided = collisions[_i];
        cnid = SOA.Constructions.getSpanMark(collided.row, collided.col);
        cn = amjad.empire.constructions.getByID(cnid);
        if (all_cns.indexOf(cn) === -1) {
          all_cns.push(cn);
        }
      }
      _results = [];
      for (_j = 0, _len1 = all_cns.length; _j < _len1; _j++) {
        cn = all_cns[_j];
        _results.push(effect.call(cn));
      }
      return _results;
    };
    return Construction;
  });

}).call(this);
