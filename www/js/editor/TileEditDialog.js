(function() {
  "use strict";
  var buttons_html, cancel_btn_html, form_html, save_btn_html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  form_html = "<div class=\"keyval\">\n    <div>\n        <label for=\"name\">Name</label>\n        <input id=\"tile-name\" type=\"text\" value=\"{{name}}\"></input>\n    </div>\n\n    <div>\n        <label for=\"layer\">Layer</label>\n        <select id=\"tile-layer\">\n            {{create_options layers}}\n        </select>\n    </div>\n\n    <div>\n        <label for=\"minigrid\"> Size </label>\n        {{{minigrid}}}\n    </div>\n</div>";

  save_btn_html = "<button id=\"save-tile\" type=\"button\" class=\"tile-edit-save-btn loader-load-button\"> Save </button>";

  cancel_btn_html = "<button id=\"cancel-tile\" type=\"button\" class=\"tile-edit-cancel-btn loader-load-button\"> Cancel </button>";

  buttons_html = "<div class=\"buttons-holder\">\n</div>";

  define(["editor/BaseDialog", "MapConfig", "TileMetaStorage"], function(BaseDialog, MapConfig, TileMetaStorage) {
    var API, TileEditDialog;
    API = {
      TILES_LIST: "/tiles",
      TILE_SAVE: "/tile/new"
    };
    Handlebars.registerHelper("create_options", function(values, options) {
      var out;
      out = "";
      values.forEach(function(elem) {
        return out += "<option value='" + elem + "'>" + elem + "</option>";
      });
      return new Handlebars.SafeString(out);
    });
    TileEditDialog = (function(_super) {
      __extends(TileEditDialog, _super);

      function TileEditDialog() {
        var $buttons_holder, $holder,
          _this = this;
        TileEditDialog.__super__.constructor.call(this, "Edit tile...", "tile-edit-dialog");
        this.html.css({
          "left": "50%",
          "top": "50%",
          "height": "400px",
          "width": "400px",
          "z-index": "5000",
          "position": "absolute",
          "margin-left": "450px",
          "margin-top": "-300px"
        });
        $holder = this.html.find(".holder");
        $holder.css({
          "width": "inherit",
          "height": "inherit"
        });
        $buttons_holder = $(buttons_html);
        this.html.append($buttons_holder);
        this.html.find(".title-container").remove();
        this.save_btn = $(save_btn_html);
        this.save_btn.appendTo($buttons_holder);
        this.save_btn.on("click", function() {
          _this.trigger("TILE_SAVED", _this, _this.getCurrentTile());
          return _this.html.hide(200);
        });
        this.cancel_btn = $(cancel_btn_html);
        this.cancel_btn.appendTo($buttons_holder);
        this.cancel_btn.on("click", function() {
          return _this.html.hide(200);
        });
      }

      return TileEditDialog;

    })(BaseDialog);
    TileEditDialog.prototype.getCurrentTile = function() {
      this.current_tile.sprite = this.current_tile.sprite;
      this.current_tile.name = this.content.find("#tile-name").val();
      this.current_tile.layer = +this.content.find("#tile-layer").find("option:selected").text();
      this.current_tile.span = this.parseMiniGridSize();
      this.current_tile.minigrid = "";
      return this.current_tile;
    };
    TileEditDialog.prototype.showTile = function($sprite) {
      var $form, tpl_tile_form;
      this.content.empty();
      tpl_tile_form = Handlebars.compile(form_html);
      this.current_tile = {
        seedId: $sprite.attr("id"),
        sprite: $sprite.find("img").attr("src"),
        name: $sprite.find("img").attr("src"),
        minigrid: this.createMiniGrid($sprite, "1"),
        layers: new Array(MapConfig.MAX_LAYERS).join(0).split(0).map(function(_, i) {
          return i;
        })
      };
      $form = $(tpl_tile_form(this.current_tile));
      this.content.append($form);
      $form.find(".minigrid").first().click(function(ev) {
        return $(ev.target).toggleClass("minigrid-active-cell");
      });
      return this.html.show(200);
    };
    TileEditDialog.prototype.dropSprite = function($sprite) {
      var $form, tpl_tile_form;
      this.content.empty();
      tpl_tile_form = Handlebars.compile(form_html);
      this.current_tile = {
        seedId: $sprite.attr("id"),
        sprite: $sprite.find("img").attr("src"),
        name: $sprite.find("img").attr("src"),
        minigrid: this.createMiniGrid($sprite, "1"),
        layers: new Array(MapConfig.MAX_LAYERS).join(0).split(0).map(function(_, i) {
          return i;
        })
      };
      $form = $(tpl_tile_form(this.current_tile));
      this.content.append($form);
      $form.find(".minigrid").first().click(function(ev) {
        return $(ev.target).toggleClass("minigrid-active-cell");
      });
      return this.html.show(200);
    };
    TileEditDialog.prototype.parseMiniGridSize = function() {
      var binaryString, out;
      out = [];
      $.each(this.content.find(".minigrid").children(), function(k, v) {
        out[k] = 0;
        if ($(v).hasClass("minigrid-active-cell")) {
          return out[k] = 1;
        }
      });
      binaryString = out.toString().replace(/,/g, '');
      return binaryString;
    };
    TileEditDialog.prototype.createMiniGrid = function(spr, encodednum) {
      var $cell, $parent, $wrapper, bin, cl, diagonal, diff, factor, h, height, i, imgcl, j, k, numcols, numrows, size, src, w, _i, _j;
      src = spr.find("img").attr("src");
      height = Editor.sprites_dialog.sprites[src].height;
      h = Math.pow(2, ~~(Math.log(height - 1) / Math.LN2) + 1);
      factor = 16;
      size = 128;
      w = Math.max(h, height) * 2;
      numrows = w / size;
      numcols = w / size;
      diagonal = (Math.sqrt(2 * size * size) * numrows) / (size / factor);
      diff = diagonal - (numcols * factor);
      imgcl = spr.find("img").clone();
      $wrapper = $("<div/>", {
        "width": diagonal + "px",
        "height": (diagonal / 2) + "px",
        "class": "minigrid-wrapper"
      });
      $parent = $("<div/>", {
        "class": "minigrid",
        "width": numcols * factor,
        "height": numrows * factor,
        "css": {
          "left": (diff * 0.5 - 1) + "px",
          "top": -(diff * 0.5 - (numrows * 5) / 2 + (numrows / 2 + 1)) + "px"
        }
      });
      k = 0;
      bin = encodednum.split('');
      for (i = _i = 0; 0 <= numrows ? _i < numrows : _i > numrows; i = 0 <= numrows ? ++_i : --_i) {
        for (j = _j = 0; 0 <= numcols ? _j < numcols : _j > numcols; j = 0 <= numcols ? ++_j : --_j) {
          $cell = $("<div/>", {
            "id": "minigrid-cell",
            "css": {
              "float": "left"
            },
            "width": factor - 1,
            "height": factor - 1
          });
          if (+bin[k]) {
            $cell.addClass("minigrid-active-cell");
          }
          k++;
          $cell.appendTo($parent);
        }
      }
      $parent.appendTo($wrapper);
      cl = spr.find("img").clone();
      cl.css("position", "absolute");
      cl.css("-webkit-transform", "rotate(-45deg)");
      cl.css("height", "100%");
      cl.css("width", "100%");
      cl.css("left", "0px");
      cl.css("top", "0px");
      cl.css("opacity", "0.9");
      cl.css("z-index", -1);
      $parent.append(cl);
      return new Handlebars.SafeString($wrapper[0].outerHTML);
    };
    return TileEditDialog;
  });

}).call(this);
