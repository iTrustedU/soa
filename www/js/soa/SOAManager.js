(function() {
  "use strict";
  define(["soa/hud/HUDManager", "soa/Session", "soa/XMLStore", "soa/Amjad", "MapConfig", "soa/hud/dialogs/SpinnerOverlay"], function(HUDManager, Session, XMLStore, Amjad, MapConfig, SpinnerOverlay) {
    var MAPS_URIS, POI_SPRITES_URI, SOAManager, SPRITES_URI, TILES_META_URI;
    MAPS_URIS = [
      {
        "name": "Zone",
        "url": "map/74_75"
      }, {
        "name": "City",
        "url": "map/10_20"
      }
    ];
    SPRITES_URI = "assets/sprites.list";
    TILES_META_URI = "map/tiles.list";
    POI_SPRITES_URI = "amjad/poi/";
    SOAManager = (function() {
      function SOAManager() {
        this.init();
      }

      return SOAManager;

    })();
    SOAManager.prototype.init = function() {
      this.currentMap = null;
      this.Hud = new HUDManager();
      this.Zone = new HAL.IsometricScene("Zone");
      this.City = new HAL.IsometricScene("City");
      this.Session = new Session();
      this.getDialog = Function.prototype.bind.call(this.Hud.getDialog, this.Hud);
      this.Config = MapConfig;
      this.Maps = {};
      this.Settlements = {};
      this.sessionData = null;
      this.attachedLayer = null;
      this.selectedHero = null;
      this.pois = {};
      return this.initListeners();
    };
    SOAManager.prototype.onAssetProgress = function(asset) {
      return HAL.trigger("ASSETS_DOWNLOAD_PROGRESSED");
    };
    SOAManager.prototype.onAssetsComplete = function() {
      HAL.trigger("ASSETS_DOWNLOADED");
      return HAL.Animations.load();
    };
    SOAManager.prototype.enableDebug = function() {
      HAL.Dom.putTextInInfoBox("MOUSE_MOVED", "Mouse position: %0, %1");
      HAL.Dom.putTextInInfoBox("MOUSE_WORLD_UPDATED", "Mouse world position: %0, %1");
      return HAL.Dom.putTextInInfoBox("MOUSE_OVER_TILE", "Tile: %0, %1");
    };
    /*
    		Downloads all necessary resources 
    		-sprites/spritesheets/animations
    		-tilemetas
    		-map files
    		-markers
    		-xml (and json) meta files
    */

    SOAManager.prototype.start = function() {
      SOA.Hud.prologue();
      window.amjad = new Amjad();
      this._spinner_ = new SpinnerOverlay();
      return this.logMeIn();
    };
    SOAManager.prototype.initListeners = function() {
      var _this = this;
      HAL.on("ZONE_MAP_LOADED", function() {
        _this.putAllPOIS();
        return _this.createClouds();
      });
      return HAL.on("MOUSE_CLICK", function(data) {
        if (_this.currentMap !== _this.Zone || (_this.selectedHero == null)) {
          return;
        }
        if (data.originalEvent.button === 2) {
          return _this.selectedHero.showActionBox(data);
        }
      });
    };
    SOAManager.prototype.logMeIn = function() {
      var $deferred, $deferredA, $deferredSpinner,
        _this = this;
      $deferredSpinner = new $.Deferred();
      $deferred = new $.Deferred();
      this._spinner_.start(this.Hud.domElement[0], $deferredSpinner);
      this.validateSession().done(function(sessionData) {
        _this.sessionData = sessionData;
        return _this.downloadResources().done(function() {
          return _this.downloadXML().done(function() {
            return $deferred.resolve();
          });
        });
      }).fail(function() {
        $deferred.reject("Couldn't login to game");
        $deferredSpinner.resolve();
        return _this.Hud.showDialog("LOGIN");
      });
      $deferredA = new $.Deferred();
      $deferred.done(function() {
        return _this.logMeIntoGame().done(function() {
          $deferredA.resolve();
          return $deferredSpinner.resolve();
        });
      });
      return $deferredA.promise();
    };
    SOAManager.prototype.downloadResources = function() {
      var $deferred,
        _this = this;
      $deferred = new $.Deferred();
      this.downloadAssets().done(function() {
        return _this.downloadTilesMeta().done(function() {
          return _this.downloadMaps().done(function() {
            return $deferred.resolve();
          });
        });
      });
      return $deferred.promise();
    };
    /* Download assets*/

    SOAManager.prototype.downloadAssets = function() {
      var $deferred, $request,
        _this = this;
      $request = $.get(SPRITES_URI);
      $deferred = new $.Deferred();
      $request.done(function(data) {
        var sheetLoader;
        sheetLoader = new PIXI.AssetLoader(data.split("\n"));
        sheetLoader.onProgress = function(item) {
          _this.onAssetProgress(item);
          return $deferred.progress(item);
        };
        sheetLoader.onComplete = function() {
          _this.onAssetsComplete();
          return $deferred.resolve();
        };
        return sheetLoader.load();
      }).fail(function() {
        return $deferred.reject("Download of " + SPRITES_URI + " failed");
      });
      return $deferred.promise();
    };
    /* Download tile metas*/

    SOAManager.prototype.downloadTilesMeta = function() {
      var $deferred, $request,
        _this = this;
      $deferred = new $.Deferred();
      $request = $.getJSON(TILES_META_URI);
      $request.done(function(tile_metas) {
        HAL.TileMetaStorage.loadTiles(tile_metas);
        return $deferred.resolve();
      }).fail(function(tile_metas) {
        return $deferred.reject("Download of tiles meta failed");
      });
      return $deferred.promise();
    };
    /* Download zone/city maps*/

    SOAManager.prototype.downloadMaps = function() {
      var $deferred, map_list, map_name, num_to_download, to_download, url,
        _this = this;
      map_list = MAPS_URIS.slice();
      num_to_download = map_list.length;
      $deferred = new $.Deferred();
      while ((to_download = map_list.pop()) != null) {
        url = to_download.url;
        map_name = to_download.name;
        (function(url, map_name) {
          return $.get(url).done(function(mapData) {
            num_to_download--;
            /* convert stringy ID's to integers*/

            _this.Maps[map_name] = mapData.split(",").map(function(t) {
              return +t;
            });
            HAL.trigger("MAP_DOWNLOADED", map_name, url);
            if (num_to_download === 0) {
              return $deferred.resolve();
            }
          }).fail(function() {
            return $deferred.reject("Downloading of map " + map_name + " failed");
          });
        })(url, map_name);
      }
      return $deferred.promise();
    };
    SOAManager.prototype.downloadXML = function() {
      var $deferred, done, loadXML, load_xmls, store, _i, _len,
        _this = this;
      done = 0;
      $deferred = new $.Deferred();
      load_xmls = ["units", "constructions", "tradegoods", "equipment", "science", "technologies"];
      XMLStore.on("LOADED_CONSTRUCTIONS_XML", function() {
        return loadXML();
      });
      XMLStore.on("LOADED_UNITS_XML", function() {
        return loadXML();
      });
      XMLStore.on("LOADED_TRADEGOODS_XML", function() {
        return loadXML();
      });
      XMLStore.on("LOADED_EQUIPMENT_XML", function() {
        return loadXML();
      });
      XMLStore.on("LOADED_SCIENCE_XML", function() {
        return loadXML();
      });
      XMLStore.on("LOADED_TECHNOLOGIES_XML", function() {
        return loadXML();
      });
      for (_i = 0, _len = load_xmls.length; _i < _len; _i++) {
        store = load_xmls[_i];
        XMLStore.get(store);
      }
      loadXML = function() {
        done++;
        if (done !== load_xmls.length) {
          return;
        }
        return $deferred.resolve();
      };
      return $deferred.promise();
    };
    SOAManager.prototype.logMeIntoGame = function() {
      var _this = this;
      amjad.load(this.sessionData);
      amjad.initialize();
      this.Empire = amjad.empire;
      this.Constructions = amjad.empire.constructions;
      this.Heroes = amjad.empire.heroes;
      this.Settlement = amjad.empire.settlement;
      this.Resources = amjad.empire.resources;
      this.Units = amjad.empire.units;
      HAL.trigger("SOA_LOADED");
      return this.loadZoneMap().done(function() {
        _this.currentMap = _this.Zone;
        _this.loadCityMap();
        _this.Hud.epilogue();
        _this.Hud.hideAllDialogs();
        _this.Hud.showDialog("BOTTOM_MENU");
        _this.Hud.showDialog("TOP_MENU");
        _this.Hud.showDialog("BUILD_ORDERS");
        _this.Hud.showDialog("HEROES_QUEUE");
        _this.Hud.showDialog("TOP_MENU");
        _this.Hud.fadeInViewport();
        _this.Zone.map.tile_shape.alpha = 0;
        _this.City.map.tile_shape.alpha = 0;
        return _this.centerOnZone();
      });
    };
    SOAManager.prototype.validateSession = function() {
      var $deferred,
        _this = this;
      $deferred = new $.Deferred();
      $.get("/load").done(function(data) {
        alert("success");
        return $deferred.resolve(data);
      }).fail(function(err) {
        alert("fail");
        return $deferred.reject(err);
      });
      return $deferred.promise();
    };
    SOAManager.prototype.goToCityView = function() {
      var _this = this;
      HAL.IsometricScene.stopRendering(this.Zone);
      HAL.IsometricScene.startRendering(this.City);
      this.Zone.pause();
      this.sortCityConstructions();
      this.Hud.fadeInViewport().done(function() {
        _this.currentMap = _this.City;
        return _this.City.resume();
      });
    };
    SOAManager.prototype.sortCityConstructions = function() {
      return this.City.map.layers[3].children.sort(function(a, b) {
        return a.position.y - b.position.y;
      });
    };
    SOAManager.prototype.goToZoneView = function() {
      var _this = this;
      HAL.IsometricScene.stopRendering(this.City);
      HAL.IsometricScene.startRendering(this.Zone);
      this.City.pause();
      this.Hud.fadeInViewport().done(function() {
        _this.currentMap = _this.Zone;
        return _this.Zone.resume();
      });
    };
    SOAManager.prototype.toggleViews = function() {
      if (this.isInCityView()) {
        return this.goToZoneView();
      } else {
        return this.goToCityView();
      }
    };
    SOAManager.prototype.isInCityView = function() {
      return this.currentMap === this.City;
    };
    SOAManager.prototype.isInZoneView = function() {
      return this.currentMap === this.Zone;
    };
    SOAManager.prototype.loadZoneMap = function() {
      var $deferred,
        _this = this;
      this.Zone.init();
      this.Hud.domElement.append(this.Zone.renderer.view);
      HAL.IsometricScene.startRendering(this.Zone);
      $deferred = new $.Deferred();
      this.loadAllPOIS().done(function() {
        _this.Zone.map.createMapFromBitmap(_this.Maps.Zone);
        return Rest.get("user.all_settlements").done(function(data) {
          _this.loadSettlements(data);
          HAL.trigger("ZONE_MAP_LOADED", _this.Zone);
          return $deferred.resolve();
        });
      }).fail(function(data) {
        return $deferred.reject(data);
      });
      return $deferred.promise();
    };
    SOAManager.prototype.loadCityMap = function() {
      this.City.initFrom(this.Zone);
      this.City.map.createMapFromBitmap(this.Maps["City"]);
      return HAL.trigger("CITY_MAP_LOADED", this.City);
    };
    SOAManager.prototype.attachConstructionToMouse = function(map_name, construction) {
      var MAP, grab_left, grab_top, layer, mouse_over_clb, prev_callb, tweeners,
        _this = this;
      layer = construction.layer;
      if (layer == null) {
        return;
      }
      grab_top = (layer.texture.frame.height - MapConfig.TILE_HEIGHT) * 0.5;
      grab_left = layer.texture.frame.width * 0.5;
      layer.interactive = false;
      this.attachedLayer = layer;
      MAP = this[map_name].map;
      tweeners = [];
      MAP.top_layer.mousemove = function(data) {
        var pos, x, y;
        x = data.global.x;
        y = data.global.y - grab_top;
        pos = MAP.scene.toLocalObject(x, y);
        layer.position.x = pos.x;
        return layer.position.y = pos.y;
      };
      mouse_over_clb = function(row, col) {
        var collisions, tween, _i, _len, _results;
        if ((_this.attachedLayer == null) || _this.currentMap !== MAP.scene) {
          return;
        }
        collisions = _this.Constructions.testSpanArea(construction.getSpanFromTile(MAP.scene.tile_under), layer.id);
        if (collisions.length === 0) {
          _results = [];
          for (_i = 0, _len = tweeners.length; _i < _len; _i++) {
            tween = tweeners[_i];
            tween.target.tint = 0xffffff;
            tween.progress(1.0);
            _results.push(tween.kill());
          }
          return _results;
        } else {
          return construction.onCollision(collisions, function() {
            return tweeners.push(TweenMax.fromTo(this.layer, 1.1, {
              alpha: 0.1,
              repeat: 1,
              tint: 0xff0000
            }, {
              alpha: 1,
              repeat: 1,
              tint: 0xff0000
            }));
          });
        }
      };
      HAL.on("MOUSE_OVER_TILE", mouse_over_clb);
      prev_callb = MAP.top_layer.click;
      return MAP.top_layer.click = function() {
        var col, row, tA, tB;
        if (!_this.attachedLayer) {
          MAP.top_layer.click = prev_callb;
          HAL.removeTrigger("MOUSE_OVER_TILE", mouse_over_clb);
        }
        tA = _this.attachedLayer.parentTile;
        tB = MAP.scene.tile_under;
        row = tB.row;
        col = tB.col;
        Rest.post("construction.relocate", {
          cid: construction.construction_id,
          x: row,
          y: col
        }).done(function() {
          tB.copyLayer(_this.attachedLayer);
          layer.interactive = true;
          MAP.top_layer.mousemove = null;
          _this.attachedLayer = null;
          MAP.top_layer.click = prev_callb;
          return SOA.sortCityConstructions();
        }).fail(function(data) {
          return console.error(data);
        });
        return HAL.removeTrigger("MOUSE_OVER_TILE", mouse_over_clb);
      };
    };
    SOAManager.prototype.loadSettlements = function(data) {
      var $deferred, k, layer, v;
      $deferred = new $.Deferred();
      for (k in data) {
        v = data[k];
        if (k === amjad.user.username) {
          continue;
        }
        layer = amjad.putCityOnZone(v.coord_x, v.coord_y, "city", "amjad/constructions/" + v.type + "/");
        if (layer == null) {
          continue;
        }
        this.Settlements[k] = v;
        this.Settlements[k].city = layer;
        layer.type = "city";
      }
      $deferred.resolve();
      return $deferred.promise();
    };
    SOAManager.prototype.loadAllPOIS = function() {
      var $deferred,
        _this = this;
      $deferred = new $.Deferred();
      Rest.get("user.all_pois").done(function(data) {
        _this.pois = data;
        return $deferred.resolve();
      }).fail(function(data) {
        return $deferred.reject(data);
      });
      return $deferred.promise();
    };
    SOAManager.prototype.putAllPOIS = function() {
      var c, id, k, l, poi, pois, tile, type, _ref, _results;
      _ref = this.pois;
      _results = [];
      for (k in _ref) {
        pois = _ref[k];
        _results.push((function() {
          var _results1;
          _results1 = [];
          for (id in pois) {
            poi = pois[id];
            tile = this.Zone.map.getTile(poi.x_coord, poi.y_coord);
            type = "poi";
            if (k === "trade") {
              type = "trade";
            }
            _results1.push((function() {
              var _ref1, _results2;
              _ref1 = tile != null ? tile.layers : void 0;
              _results2 = [];
              for (c in _ref1) {
                l = _ref1[c];
                l.type = type;
                l.subtype = k;
                _results2.push(l.poid = id);
              }
              return _results2;
            })());
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };
    SOAManager.prototype.getPOIS = function() {
      var id, k, l, ls, out, poi, pois, _ref;
      out = [];
      _ref = this.pois;
      for (k in _ref) {
        pois = _ref[k];
        for (id in pois) {
          poi = pois[id];
          ls = this.Zone.map.getTile(poi.x_coord, poi.y_coord).layers;
          for (k in ls) {
            l = ls[k];
            out.push(l);
          }
        }
      }
      return out;
    };
    SOAManager.prototype.createClouds = function(num) {
      var cloud, clouds, i, randScale, rndFact, sideFactor, spr, startX, startY, tween, upOrDown, whereY, zbnds, _i, _results;
      if (num == null) {
        num = 50;
      }
      clouds = [PIXI.TextureCache["assets/sprites/amjad/clouds/cloud1.png"], PIXI.TextureCache["assets/sprites/amjad/clouds/cloud2.png"]];
      zbnds = this.Zone.map.world_bounds.clone();
      _results = [];
      for (i = _i = 0; 0 <= num ? _i < num : _i > num; i = 0 <= num ? ++_i : --_i) {
        startY = ~~(Math.random() * zbnds.y);
        cloud = clouds[+(Math.random() > 0.5)];
        startX = -cloud.width;
        spr = new PIXI.Sprite(cloud);
        spr.position.x = startX;
        spr.position.y = startY;
        upOrDown = 1;
        randScale = Math.random() + 1;
        spr.scale.x = randScale;
        spr.scale.y = randScale;
        this.Zone.map.layers[6].addChild(spr);
        sideFactor = Math.random();
        spr.rotation = -Math.PI / 15;
        spr.alpha = 0.5 + (Math.random() * 0.40);
        rndFact = upOrDown * (~~(sideFactor * 1050));
        whereY = startY + rndFact;
        _results.push(tween = TweenMax.fromTo(spr.position, ~~(Math.random() * 100) + 100, {
          x: startX,
          y: startY,
          repeat: -1
        }, {
          x: zbnds.x,
          y: whereY,
          repeat: -1
        }));
      }
      return _results;
    };
    SOAManager.prototype.createStorms = function(numStorms, stormSize, stormPoints, stormSpeed, stormColor) {
      var col, hazard, i, j, points, rottweener, row, startCol, startRow, texts, tweener, _i, _j, _results;
      if (stormSpeed == null) {
        stormSpeed = 100;
      }
      if (stormColor == null) {
        stormColor = 0xf2f299ff;
      }
      texts = Object.keys(PIXI.TextureCache).filter(function(en) {
        return en.indexOf("storm") !== -1;
      });
      texts = texts.map(function(texname) {
        return PIXI.TextureCache[texname];
      });
      _results = [];
      for (j = _i = 0; 0 <= numStorms ? _i < numStorms : _i > numStorms; j = 0 <= numStorms ? ++_i : --_i) {
        startRow = ~~(this.Zone.map.NROWS * Math.random());
        startCol = ~~(this.Zone.map.NCOLUMNS * Math.random());
        hazard = this.Zone.map.addAnimatedLayer(startRow, startCol, texts);
        hazard.scale.x = hazard.scale.y = stormSize * Math.random();
        points = [];
        for (i = _j = 0; 0 <= stormPoints ? _j < stormPoints : _j > stormPoints; i = 0 <= stormPoints ? ++_j : --_j) {
          row = ~~(Math.random() * this.Zone.map.NROWS);
          col = ~~(Math.random() * this.Zone.map.NCOLUMNS);
          points.push(this.Zone.map.getTile(row, col).position);
        }
        hazard.tint = stormColor;
        tweener = TweenMax.to(hazard.position, 200 + ~~(stormSpeed * Math.random()), {
          bezier: points.slice(),
          type: "cubic",
          paused: true,
          curviness: 2.5,
          repeat: -1
        });
        rottweener = TweenMax.fromTo(hazard, 3, {
          rotation: 0,
          repeat: -1
        }, {
          rotation: 2 * Math.PI,
          repeat: -1
        });
        tweener.play();
        _results.push(hazard.play());
      }
      return _results;
    };
    SOAManager.prototype.setSelectedHero = function(hero) {
      if (hero == null) {
        return;
      }
      return this.selectedHero = hero;
    };
    SOAManager.prototype.centerOnZone = function() {
      this.Zone.centerOn(this.Settlement.layer.position);
    };
    SOAManager.prototype.getPOI = function(row, col) {
      var poi, poi_id, subtype, target, tile, v, _ref;
      tile = this.Zone.map.getTile(row, col);
      if (tile == null) {
        return;
      }
      _ref = tile.layers;
      for (v in _ref) {
        target = _ref[v];
        if ((target == null) || (target.poid == null)) {
          throw new Error("No poi here");
        }
        poi_id = target.poid;
        subtype = target.subtype;
        poi = SOA.pois[subtype][poi_id];
        return poi;
      }
      return null;
    };
    return SOAManager;
  });

}).call(this);
