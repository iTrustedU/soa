(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher"], function(EventDispatcher) {
    var BaseDialog;
    BaseDialog = (function(_super) {
      __extends(BaseDialog, _super);

      function BaseDialog(title, id) {
        var template,
          _this = this;
        if (id == null) {
          id = "";
        }
        BaseDialog.__super__.constructor.call(this);
        template = Handlebars.compile(Handlebars.partials.editor_dialog);
        this.html = $(template({
          id: id,
          title: title
        }));
        this.content = this.html.find(".content");
        this.toolbox = this.html.find(".toolbox");
        this.show_btn = this.html.find("#toggle-show");
        this.show_btn.on("click", function() {
          var $holder;
          _this.trigger("TOGGLE_VISIBILITY", _this);
          $holder = _this.html.find(".holder");
          $holder.toggle("slide", {
            direction: "down"
          });
          return _this.show_btn.toggleClass("fa-minus-circle fa-plus-circle");
        });
        this.init();
        this.initListeners();
      }

      return BaseDialog;

    })(EventDispatcher);
    BaseDialog.prototype.addToToolbox = function(elem) {
      return this.toolbox.append(elem);
    };
    BaseDialog.prototype.init = function() {};
    BaseDialog.prototype.initListeners = function() {};
    return BaseDialog;
  });

}).call(this);
