(function() {
  "use strict";
  define([], function() {
    var MathUtils;
    MathUtils = {
      ARRAY_TYPE: typeof Float32Array !== 'undefined' ? Float32Array : Array,
      TAU: Math.PI * 2,
      EPSILON: 0.000001,
      DEGREE: Math.PI / 180,
      RADIAN: 180 / Math.PI
    };
    MathUtils.nextPOT = function(num) {
      throw new Error("Not implemented");
    };
    MathUtils.clamp = function(val, min, max) {
      if (val < min) {
        val = min;
      } else if (val > max) {
        val = max;
      }
      return val;
    };
    MathUtils.encode16 = function(vals) {
      return vals[0] << 16 | vals[1];
    };
    MathUtils.decode16 = function(val) {
      return [val >> 16 & 0xFFFF, val & 0xFFFF];
    };
    MathUtils.encode8 = function(r, g, b) {
      return ((r << 8 | g) << 8) | b;
    };
    MathUtils.decode8 = function(val) {
      return [val >> 16 & 0xFF, val >> 8 & 0xFF, val & 0xFF];
    };
    MathUtils.encode12 = function(a, b) {
      return a << 12 | b;
    };
    MathUtils.decode12 = function(val) {
      return [val >> 12 & 0xFFF, val & 0xFFF];
    };
    return MathUtils;
  });

}).call(this);
