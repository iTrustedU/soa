"use strict"

html =
"""
<div class="science-tent draggable">
    <div class="dialog-name">
        <span>Science-tent</span>
        <div class="close-button"></div>
    </div>
    <div class="science-tabs">
        <div id="science-tab" class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Science</span>
            <div class="chat-tab-r"></div>
        </div>
        <div id="infrastructure-tab"  class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Infrastructure</span>
            <div class="chat-tab-r"></div>
        </div>
    </div>
    <div id="tab_content_infrastructure_template" class="science-tent-content infrastructure-tab">
        {{{infrastructure_tab}}}
    </div>
    <div id="tab_content_science_template">{{{science_tab}}}</div>
    <div class="war-building-info">
        <img src="assets/sprites/amjad/dialogs/constructions/civil/science.png" class="dialog-building-img" />
        <div id="upgrade-price" class="upgrade-price">
            {{{upgrade_price}}}
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
        <div id="move-building" class="upgrade-building move-button">Move</div>
    </div>
</div>
"""
$upgrade_price = """

        {{#each data.resources}}
            <img src="img/top-resources/{{@key}}.png">
            <span>{{this}}</span>
        {{/each}}
        
        """

$infrastructure_tab = 
"""
{{#each data}}
<div class="science-content-tab">
     <span class="science-middle-deco">{{@key}}</span>
</div>
<div class="science-tent-content-box  resources-content-tab-sub-content">
    {{#each this.techs}}
        <div class="science-tab-box" id="{{@key}}">
            <div class="img-name">
                <img src="{{concat_technology @key}}" class="war-research-img" />
                <span class="science-name">{{lang @key}}</span>
                <div id={{@key}} class="science-research-button">Research</div>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <div class="science-info-icons science-level-icon"></div>
                    <span class="science-level">{{lvl}}</span>
                </div>
                <div class="st-time">
                    <div class="science-info-icons science-time-icon"></div>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    {{#each res}}
                    <img src="img/top-resources/{{@key}}.png" />
                    <span>{{this}}</span>
                    {{/each}}
                </div>
                <div class="st-info">
                    <span>{{info}}</span>
                </div>
            </div>
        </div>
    {{/each}}
    </div>
{{/each}}
"""

$science_tab = 
"""
<div id="science-tab-container" class="science-tent-content science-tab default-skin">
    {{#each data}}
    <div class="science-tab-box" id="{{@key}}">
        <div class="img-name">
            <img src="{{concat_science @key}}" class="war-research-img" />
            <span class="science-name">{{lang @key}}</span>
            <div id={{@key}} class="science-research-button">Research</div>
        </div>
        <div class="st-info-box">
            <div class="st-level">
                <div class="science-info-icons science-level-icon"></div>
                <span class="science-level">{{lvl}}</span>
            </div>
            <div class="st-time">
                <div class="science-info-icons science-time-icon"></div>
                <span>{{time}}</span>
            </div>
            <div class="st-price">
                {{#each res}}
                <img src="img/top-resources/{{@key}}.png">
                <span>{{this}}</span>
                {{/each}}
            </div>
            <div class="st-info">
                <span>{{info}}</span>
            </div>
        </div>
    </div>
    {{/each}}
</div>
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->

    class ScienceDialog extends BaseDialog
        constructor: () ->
            super(html)

    ScienceDialog::onPrologue = () ->
        @setTemplates
            "science_template": $science_tab
            "infrastructure_template": $infrastructure_tab
            "upgrade_template" : $upgrade_price
        
        @upgradeable()
        @moveable()
        @closeable()
        @draggable()
 
    # ovo se desava posle ucitavanja struktura
    ScienceDialog::onEpilogue = (hud) ->
        SELF = @html

        @on "SELECTED_TAB", (val) ->
            template = "science_template"
            if val is "science-tab"
                datakey = "science_tab"
            else if val is "infrastructure-tab"
                template = "infrastructure_template"
                datakey = "infrastructure_tab"
            else
                alert("not supported")
                return

            #hide all tabs content
            $("div[id^=tab_content_").css('display', 'none')
            #show current content
            $('.science-tabs #tab_content_'+datakey).css('display', 'block') 

            @compile(template, datakey).then (data) =>  
                @refresh "tab_content_"+template,data   
                @html.find("#science-tab-container").customScrollbar()                
                #toggle display items on infrastructure tab
                @html.find('.science-content-tab').click () -> 
                    SELF.find('.science-tent-content-box').hide()
                    $(this).next().toggle()

        @on "START_RESEARCH", () ->
            id = @get("START_RESEARCH")
            tab = @get("SELECTED_TAB")
            row = @html.find(".science-tab-box##{id}")
            level = @ctx.level
            defer = new $.Deferred()

            if not tab? or not id?
                defer.reject("TAB: #{tab}, ID: #{id}")
            else if tab is "science-tab"
                amjad.empire.startScienceResearch(id, level)
                .done (data) =>

                    #pull resources from xml store for current tech and lvl
                    res = amjad.empire.resources.getScienceResources(id, level)

                    data["name"] = @get "SELECTED_SCIENCE"
                    data["type"] = @get "SELECTED_SCIENCE"
                    data["amount"] = 1
                    data["construction_id"] = @ctx.construction_id
                    data["level"] = 'level'+level
                    @ctx.beginScienceResearch(data, res)
                .fail (err) ->
                    defer.reject(err)
            else if tab is "infrastructure-tab"
                amjad.empire.startTechResearch(id, level)
                .done (data) =>
                    #pull resources from xml store for current tech and lvl
                    res = amjad.empire.resources.getTechResources(id, level)

                    data["name"] = @get "SELECTED_SCIENCE"
                    data["type"] = @get "SELECTED_SCIENCE"
                    data["amount"] = 1
                    data["construction_id"] = @ctx.construction_id
                    data["level"] = 'level'+level
                    @ctx.beginTechResearch(data, res)
                .fail (err) ->
                    defer.reject(err)

            defer.promise()
            .done (research) ->
                return
                ##console.debug "research complete"
            .fail (err) ->
                alert(err.statusText)


        @on "SHOW", () ->
            scienceData = @getScienceData()
            @setData "science_tab", {data : scienceData}

            techData = @getTechData()
            @setData "infrastructure_tab",  {data : techData}
            
            #we are pulling needed resource for  
            #so we need to pass next level
            upgradeResources = amjad.empire.resources.getConstructionUpgradeResources('science', @ctx.level+1)
            @setData "upgrade_price", {data : upgradeResources}

            datakey = "upgrade_price"
            @compile("upgrade_template", datakey).then (data) =>  
                @refresh "upgrade-price",data 
            
            @set("SELECTED_TAB", "science-tab")
            @trigger "SELECTED_TAB", "science-tab"
            @html.find("#science-tab-container").customScrollbar()

        @clickTriggers
            ".chat-tab/#id": "SELECTED_TAB"
            ".science-research-button/#id": "START_RESEARCH"
            ".science-tab-box/#id": "SELECTED_SCIENCE"
        
        ScienceDialog::getScienceData = () ->
            #prepack science data
            #data is packed from xml and all prices and resources are taken from the level of particular science from database
            xmlSciences = XMLStore.get("science")

            scienceData = {}
            for key, value of xmlSciences
                currentLevel = if amjad.empire?.sciences?[key] then amjad.empire.sciences[key].level else 1

                resources = amjad.empire.resources.getScienceResources(key, currentLevel)

                scienceData[key] = {lvl: currentLevel, res : resources}

            return scienceData

        ScienceDialog::getTechData = () ->
            #prepack technologies from xml and database
            #we are repacking resources from xml
            xmlTechnologies = XMLStore.get("technologies");
            technologies = {}
            for key, value of xmlTechnologies
                currentLevel = if amjad.empire?.technologies?[key] then amjad.empire.technologies[key].level else 1

                resources = amjad.empire.resources.getTechResources(key, currentLevel)

                technologies[key] = {lvl: currentLevel, res : resources}
            
            #group the data in required structure for template
            techData = {
                trade: {
                    techs : {
                        caravan_discipline : technologies.caravan_discipline
                        load_management : technologies.load_management
                    }
                }
                resource: {
                    techs : {
                        wood_hauling : technologies.wood_hauling
                    }
                }
                construction: {
                    techs : {
                        worker_education : technologies.worker_education
                    }
                }
            }; 

            return techData
             

    return ScienceDialog

    # Hal.on "OPEN_SCIENCE_DIALOG", (science) ->
    #     $ScienceDialog.show()