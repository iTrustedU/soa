(function() {
  "use strict";
  var $agent_info, $commander_info, $trader_info, $trainer_info, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"tavern draggable\">\n    <div class=\"dialog-name\">\n        <span>T A V E R N</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"tavern-tabs\">\n        <div class=\"chat-tab\" id=\"commander-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Commander</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"agent-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Agent</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"caravan-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Caravan</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"trainer-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Trainer</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"mercenary-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Mercenary</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n    </div>\n    <div class=\"table-th\">\n        <span>Price</span>\n        <span>Statistics</span>\n        <span>Commander</span>\n    </div>\n    <div class=\"bars\"></div>\n    <div id=\"heroes-table\">\n        {{{hero_info}}}\n    </div>\n    <div class=\"war-building-info\">\n        <img src=\"img/bottom-middle/buildings/trade.png\" class=\"dialog-building-img\" />\n        <div class=\"upgrade-price\">\n            <img src=\"img/top-resources/wood.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/iron.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/gold.png\">\n            <span>200000</span>\n            <img src=\"img/top-resources/food.png\">\n            <span>200000</span>\n        </div>\n        <div id=\"upgrade-building\" class=\"upgrade-building\">Upgrade</div>\n        <div id=\"move-building\" class=\"upgrade-building move-button\">Move</div>\n    </div>\n    <div class=\"employ-button\">Employ</div>\n</div>";

  $commander_info = "<div class=\"tavern-info default-skin\">\n    <table>\n        {{#each gen}}\n            <tr class=\"commander-row\" id={{gen_commander_id}}>\n                <td id=\"price\">{{price}}</td>\n                <td>\n                    <span class=\"statistics\">Readiness</span><span>{{ready}}</span><br />\n                    <span class=\"statistics\">Speed</span><span>{{speed}}</span><br />\n                    <span class=\"statistics\">Leadership</span><span>{{leadership}}</span><br />\n                </td>\n                <td>\n                    <div class=\"tavern-hero-img\">\n                        <!-- <img src=\"{{concat_hero_avatar img_id}}\" /> -->\n                        <img src=\"{{concat_hero_avatar \"commander\" \"m\"}}\" />  \n                    </div><br />\n                    <span class=\"tavern-hero-table-name\">{{n}}</span>\n                </td>\n            </tr>\n        {{/each}}\n    </table>\n</div> ";

  $agent_info = "<div class=\"tavern-info default-skin\">\n    <table>\n        {{#each agents}}\n            <tr class=\"commander-row\" id={{gen_agent_id}}>\n                <td id=\"price\">{{price}}</td>\n                <td>\n                    <span class=\"statistics\">Resourcefulness</span><span>{{ress}}</span><br />\n                    <span class=\"statistics\">Stealth</span><span>{{st}}</span><br />\n                    <span class=\"statistics\">Wit</span><span>{{wit}}</span><br />\n                </td>\n                <td>\n                    <div class=\"tavern-hero-img\">\n                        <img src=\"{{concat_hero_avatar \"agent\" \"m\"}}\" />\n                    </div><br />\n                    <span class=\"tavern-hero-table-name\">{{name}}</span>\n                </td>\n            </tr>\n        {{/each}}\n    </table>\n</div> ";

  $trader_info = "<div class=\"tavern-info default-skin\">\n    <table>\n        {{#each traders}}\n            <tr class=\"commander-row\" id={{gen_trader_id}}>\n                <td id=\"price\">{{price}}</td>\n                <td>\n                    <span class=\"statistics\">Capacity</span><span>{{cap}}</span><br />\n                    <span class=\"statistics\">Speed</span><span>{{speed}}</span><br />\n                    <span class=\"statistics\">Tongue</span><span>{{tongue}}</span><br />\n                </td>\n                <td>\n                    <div class=\"tavern-hero-img\">\n                        <img src=\"{{concat_hero_avatar \"trader\" \"m\"}}\" />\n                    </div><br />\n                    <span class=\"tavern-hero-table-name\">{{name}}</span>\n                </td>\n            </tr>\n        {{/each}}\n    </table>\n</div> ";

  $trainer_info = "<div class=\"tavern-info default-skin\">\n    <table>\n        {{#each trainers}}\n            <tr class=\"commander-row\" id={{gen_trainer_id}}>\n                <td id=\"price\">{{price}}</td>\n                <td>\n                    <span class=\"statistics\">Brawn</span><span>{{brawn}}</span><br />\n                    <span class=\"statistics\">Reflexes</span><span>{{reflexes}}</span><br />\n                    <span class=\"statistics\">Endurance</span><span>{{endurance}}</span><br />\n                </td>\n                <td>\n                    <div class=\"tavern-hero-img\">\n                        <img src=\"{{concat_hero_avatar \"trainer\" \"m\"}}\" />\n                    </div><br />\n                    <span class=\"tavern-hero-table-name\">{{name}}</span>\n                </td>\n            </tr>\n        {{/each}}\n    </table>\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var HEROES_GENERATING_REFRESH_TIME, TavernDialog;
    HEROES_GENERATING_REFRESH_TIME = 5 * 60 * 1000;
    TavernDialog = (function(_super) {
      __extends(TavernDialog, _super);

      function TavernDialog() {
        TavernDialog.__super__.constructor.call(this, html);
      }

      return TavernDialog;

    })(BaseDialog);
    TavernDialog.prototype.onPrologue = function() {
      this.setTemplates({
        "trainer_template": $trainer_info,
        "trader_template": $trader_info,
        "commander_template": $commander_info,
        "agent_template": $agent_info
      });
      this.upgradeable();
      this.moveable();
      this.closeable();
      return this.draggable();
    };
    TavernDialog.prototype.onEpilogue = function(hud) {
      var _me_;
      this.on("PRE_SHOW", _me_ = function() {
        this.html.find(".dialog-building-img").attr("src", Handlebars.helpers.concat_dialog_construction("tavern"));
        return this.remove("PRE_SHOW", _me_);
      });
      this.on("SELECTED_TAB", function(val) {
        var datakey, template,
          _this = this;
        template = "commander_template";
        if (val === "commander-tab") {
          datakey = "commander_info";
        } else if (val === "caravan-tab") {
          template = "trader_template";
          datakey = "trader_info";
        } else if (val === "trainer-tab") {
          template = "trainer_template";
          datakey = "trainer_info";
        } else if (val === "agent-tab") {
          template = "agent_template";
          datakey = "agent_info";
        } else {
          alert("not supported");
          return;
        }
        return this.compile(template, datakey).then(function(data) {
          _this.refresh("heroes-table", data);
          _this.html.find(".tavern-info").customScrollbar();
          return $("tr").click(function() {
            $('tr').not(this).removeClass('tr-click-bg');
            return $(this).toggleClass("tr-click-bg");
          });
        });
      });
      this.on("EMPLOY_HERO", function() {
        var defer, id, price, row, tab, tg,
          _this = this;
        id = this.get("SELECTED_HERO");
        tab = this.get("SELECTED_TAB");
        row = this.html.find(".commander-row#" + id);
        price = row.find("#price").html();
        defer = new $.Deferred();
        /* @todo extensive preemptive check*/

        if ((tab == null) || (id == null)) {
          defer.reject("TAB: " + tab + ", ID: " + id);
        } else if (tab === "commander-tab") {
          amjad.empire.employCommander(id).done(function(data) {
            var cm;
            _this.setData("commander_info", amjad.empire.loadCommandersList);
            cm = amjad.empire.heroes.addNewCommander(data);
            cn.troops = {};
            return defer.resolve(cm);
          }).fail(function(err) {
            return defer.reject(err);
          });
        } else if (tab === "caravan-tab") {
          amjad.empire.employTrader(id).done(function(data) {
            var tr;
            _this.setData("trader_info", amjad.empire.loadCaravansList);
            tr = amjad.empire.heroes.addNewTrader(data);
            tr.bag = {
              balance: 0
            };
            return defer.resolve(tr);
          }).fail(function(err) {
            return defer.reject(err);
          });
        } else if (tab === "trainer-tab") {
          tg = amjad.empire.constructions.getAllOfType("training_grounds")[0];
          if (tg == null) {
            alert("No training grounds");
            defer.reject("No training grounds");
          } else {
            amjad.empire.employTrainer(id, tg.construction_id).done(function(data) {
              var tr;
              _this.setData("trainer_info", amjad.empire.loadTrainersList);
              tr = amjad.empire.heroes.addNewTrainer(data);
              return defer.resolve(tr);
            }).fail(function(err) {
              return defer.reject(err);
            });
          }
        } else if (tab === "agent-tab") {
          amjad.empire.employAgent(id).done(function(data) {
            var ag;
            _this.setData("agent_info", amjad.empire.loadAgentsList);
            ag = amjad.empire.heroes.addNewAgent(data);
            return defer.resolve(ag);
          }).fail(function(err) {
            return defer.reject(err);
          });
        } else {
          defer.reject("You can't employ this one");
        }
        return defer.promise().done(function(hero) {
          var HeroesQueue;
          amjad.empire.resources.take({
            balance: price
          });
          HeroesQueue = SOA.Hud.getDialog("HEROES_QUEUE");
          if (hero.getType() !== "trainer") {
            HeroesQueue.putHero(hero);
          }
          return row.remove();
        }).fail(function(err) {
          return alert(err.statusText);
        });
      });
      amjad.empire.generateCommandersList().done(function() {
        return amjad.empire.loadCommandersList();
      });
      amjad.empire.generateCaravansList().done(function() {
        return amjad.empire.loadCaravansList();
      });
      amjad.empire.generateTrainersList().done(function() {
        return amjad.empire.loadTrainersList();
      });
      amjad.empire.generateAgentsList().done(function() {
        return amjad.empire.loadAgentsList();
      });
      this.on("SHOW", function() {
        this.setData("commander_info", amjad.empire.loadCommandersList);
        this.setData("trader_info", amjad.empire.loadCaravansList);
        this.setData("trainer_info", amjad.empire.loadTrainersList);
        this.setData("agent_info", amjad.empire.loadAgentsList);
        this.set("SELECTED_TAB", "commander-tab");
        return this.trigger("SELECTED_TAB", "commander-tab");
      });
      this.clickTriggers({
        ".chat-tab/#id": "SELECTED_TAB",
        ".employ-button/#": "EMPLOY_HERO",
        ".commander-row/#id": "SELECTED_HERO"
      });
      return this.scheduleAsyncDataRefresh(HEROES_GENERATING_REFRESH_TIME, {
        "commander_info": amjad.empire.generateCommandersList,
        "trader_info": amjad.empire.generateCaravansList,
        "trainer_info": amjad.empire.generateTrainersList,
        "agent_info": amjad.empire.generateAgentsList
      });
    };
    return TavernDialog;
  });

}).call(this);
