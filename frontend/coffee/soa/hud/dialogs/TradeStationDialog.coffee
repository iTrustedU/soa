"use strict"

html =
"""
<div class="trade-station draggable">
    <div class="dialog-name">
        <span>T R A D E &nbsp; S T A T I O N</span>
        <div class="close-button"></div>
    </div>
    <div class="trader-info">
        <div class="trade-station-name">
            <span>Selected trader</span>
        </div>
        <div id="trader-info">{{{trader_info}}}</div>
        <div class="load-unload">
            <div class="unload"></div>
            <div class="load"></div>
        </div>
        <div id="gold-amount">{{{gold_amount}}}</div>
        <div id="trade-station-inventory">{{{trade_station_inventory}}}</div>
        <div id="load-text">{{{load_text}}}</div>
    </div>      
    <div class="available-traders">
        <span class="available-traders-name">Available Traders</span>
        <div id="available-traders">{{{available_traders}}}</div>
    </div>
    <div class="war-building-info">
        <img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img" />
        <div class="upgrade-price">
            <img src="img/top-resources/wood.png">
            <span>200000</span>
            <img src="img/top-resources/iron.png">
            <span>200000</span>
            <img src="img/top-resources/gold.png">
            <span>200000</span>
            <img src="img/top-resources/food.png">
            <span>200000</span>
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
        <div id="move-building" class="upgrade-building move-button">Move</div>
    </div>
</div>
"""

$trader_info_template = 
"""
<div class="trader">
    <img src="{{concat_hero_avatar "trader" "l"}}" />
    <span class="trader-name">{{name}}</span><br />
    <div class="slider-bar-middle">
        <div class="inner-slider-box2">
            <div style="top:0px!important" class="inner-slider2 ui-slider-handle"></div>
        </div>
    </div>
</div>
<div class="trade-goods-own">
    {{#each getGoods}}
        <div id="{{@key}}" class="poi-good">
            <img class="trader-good" id="{{@key}}" src="{{concat_resource @key}}"/>
            <span class="goods-amount5">{{this}}</span>
        </div>
    {{/each}}
</div>
"""

$trade_station_inventory_template =
"""
<div class="trade-goods-inventory">
    {{#each station_inventory}}
        <div id="{{@key}}" class="goods-box">
            <img src={{concat_resource @key}}>
            <span class="goods-amount">{{amount}}</span>
        </div>
    {{/each}}
</div>
"""

$gold_amount_template = 
"""
 <div class="gold-amount">
    <input id="gold-amount-value" value="{{gold}}"/>
    <img src="img/top-resources/gold.png" class="top-resources">
</div>
"""

$load_text_template = 
"""
<div class="load-text">
    <img src="img/dialog/trade-station/load.png" class="load-icon">
    <span>{{load}}</span>
    <span>/ {{capacity}}</span>
</div>
"""

$other_traders_template = 
"""
{{#each other_traders}}
<div class="available-traders-box" id="{{caravan_id}}">
    <img src={{concat_hero_avatar "trader" "m"}} />
    <div class="available-trader-info">
        <span class="trader-name-b">{{name}}</span><br />
        <span class="level-name-b">Level:</span>
        <span class="level-value-b">{{level}}</span>
    </div>
</div>
{{/each}}
"""

define ["soa/hud/dialogs/BaseDialog"],
(BaseDialog) ->

    class TradeStation extends BaseDialog
        constructor: () ->
            super(html)

    TradeStation::onPrologue = () -> 

        @setTemplates
            "trader_info_template": $trader_info_template
            "other_traders_template": $other_traders_template
            "load_text_template": $load_text_template
            "gold_amount_template": $gold_amount_template
            "trade_station_inventory_template": $trade_station_inventory_template

        @upgradeable()
        @moveable()
        @closeable()
        @draggable()

        @activeTrader   = null
        @stationGood    = null
        @traderGood     = null

        @on "PRE_SHOW", _me_ = () =>
            @html
            .find(".dialog-building-img")
            .attr("src", Handlebars.helpers.concat_dialog_construction("trade_station"))
            @remove "PRE_SHOW", _me_

        @clickTriggers
            ".available-traders-box/#id": "TRADER_CLICKED"
            ".unload/#": "UNLOAD"
            ".load/#": "LOAD"
            ".goods-box/#id": "STATION_GOOD_CLICKED"
            ".poi-good/#id": "TRADER_GOOD_CLICKED"

        @on "STATION_GOOD_CLICKED", (id) =>
            @stationGood = id
            @clearSelected()
            elem = @html.find(".goods-box##{id}")
            elem.addClass("goods-box-clicked")

        @on "TRADER_GOOD_CLICKED", (id) =>
            @traderGood = id
            @clearSelected()
            elem = @html.find(".poi-good##{id}")
            elem.addClass("goods-box-clicked")

        @on "LOAD", () =>
            if not @stationGood?
                alert("You didn't select the resource to unload")
            dlg = SOA.Hud.getDialog("EXCHANGE_DIALOG")
            caps = @activeTraderLoad()
            maxCaps = caps.capacity - caps.load
            amnt = 0
            if (@activeTrader.getGoods()[@stationGood])?
                amnt = (@activeTrader.getGoods()[@stationGood])
            dlg.open(
                {
                    minAmount: 0
                    amount: (amjad.empire.resources.getTradeResources())[@stationGood].amount
                    picture: Handlebars.helpers.concat_resource(@stationGood)
                },
                {
                    amount: amnt
                    maxAmount: maxCaps
                    picture: Handlebars.helpers.concat_resource(@stationGood)
                },
                (amount) =>
                    resources = {}
                    resources[@stationGood] = amount
                    Rest.post "caravan.load_caravan",
                        cid: @activeTrader.caravan_id
                        res: resources
                        mod: "plus"
                    .done (data) =>
                        amjad.empire.resources.take(resources)
                        @activeTrader.putInBag(@stationGood, amount, 0)
                        @recompile()
                    .fail (data) =>
                        alert("Loading failed")
            )

            # if not @stationGood?
            #   alert("You didn't select the resource to load")
            #   return
            # resources = {}
            # resources[@stationGood] = 1
            # Rest.post "caravan.load_caravan",
            #   cid: @activeTrader.caravan_id
            #   res: resources
            #   mod: "plus"
            # .done (data) =>
            #   amjad.empire.resources.add(resources)
            #   @activeTrader.putInBag(@stationGood, 1, 0)
            #   @recompile()
            # .fail (data) =>
            #   alert("Loading failed")
            # return
                
        @on "UNLOAD", () =>
            # trader = @get "ACTIVE_TRADER"
            # res = @get "TRADER_GOOD"
            if not @traderGood?
                alert("You didn't select the resource to unload")
            dlg = SOA.Hud.getDialog("EXCHANGE_DIALOG")
            caps = @activeTraderLoad()
            maxCaps = caps.capacity - caps.load
            #alert((@activeTrader.getGoods())[@traderGood]).amount)
            # console.debug(@activeTrader)
            # console.debug(@activeTrader.getGoods())
            # console.debug(@traderGood)
            dlg.open(
                {
                    minAmount: 0
                    amount: (@activeTrader.getGoods()[@traderGood])
                    picture: Handlebars.helpers.concat_resource(@traderGood)
                },
                {
                    amount: 0 #(amjad.empire.resources.getTradeResources())[@traderGood].amount
                    picture: Handlebars.helpers.concat_resource(@traderGood)
                    maxAmount: Number.MAX_VALUE
                },
                (amount) =>
                    resources = {}
                    resources[@traderGood] = amount
                    Rest.post "caravan.load_caravan",
                        cid: @activeTrader.caravan_id
                        res: resources
                        mod: "minus"
                    .done (data) =>
                        amjad.empire.resources.add(resources)
                        @activeTrader.takeFromBag(@traderGood, amount, 0)
                        @recompile()
                    .fail (data) =>
                        alert("Unloading failed")
            )
            return

        @on "TRADER_CLICKED", (id) =>
            new_trader = @getTraders()[id]
            if new_trader isnt @activeTrader
                @loadTraderBag("balance", @activeTrader.bag.balance)
                @activeTrader = new_trader
                @recompile()

        @on "SHOW", () =>
            trids = Object.keys(@getTraders())
            if trids.length is 0
                alert("You don't own any traders at the moment")
                @disable()
                return
            @activeTrader = @getTraders()[trids[0]]
            @recompile()
        
        @on "HIDE", () =>
            if @activeTrader?
                @loadTraderBag("balance", @activeTrader.bag.balance)
            return
    
    TradeStation::clearSelected = () ->
        trader = @html.find(".poi-good")
        station = @html.find(".goods-box")
        trader.removeClass("goods-box-clicked")
        station.removeClass("goods-box-clicked")

    TradeStation::stationInventory = () ->
        return "station_inventory": amjad.empire.resources.getTradeResources()

    TradeStation::recompile = () ->
        @setData "active_trader", @activeTrader
        @setData "other_traders", @otherTraders(@activeTrader.id)
        @setData "trader_load", @activeTraderLoad
        @setData "empire_gold", @empireGoldAmount
        @setData "trade_station_inventory", @stationInventory
        @setData "trader_balance", @activeTraderBalance

        @compile("trader_info_template", "active_trader").then (data) =>
            @refresh("trader-info", data)
            @initSlider()
            @compile("load_text_template", "trader_load").then (data) =>
                @refresh("load-text", data)
            @compile("gold_amount_template", "trader_balance").then (data) =>
                @refresh("gold-amount", data)
                @set "GOLD_AMOUNT_ELEMENT", @html.find("#gold-amount-value")

        @compile("other_traders_template", "other_traders").then (data) =>
            @refresh("available-traders", data)

        @compile("trade_station_inventory_template", "trade_station_inventory").then (data) =>
            @refresh("trade-station-inventory", data)

    TradeStation::otherTraders = (id) ->
        out = {}
        for k, v of @getTraders()
            if v.id isnt id
                out[k] = v
        return "other_traders": out

    TradeStation::initSlider = () ->
        @html.find(".slider-bar-middle").slider
            min: 0
            max: amjad.empire.resources.balance.amount
            value: @activeTrader.bag.balance
            orientation: "horizontal"
            slide: (ev, ui) => 
                @activeTrader.bag.balance = ui.value
                gold_am = @html.find("#gold-amount-value") #@get "GOLD_AMOUNT_ELEMENT"
                gold_am.val("#{@activeTrader.bag.balance}")

    TradeStation::loadTraderBag = (res, amount) ->
        id = @activeTrader.caravan_id
        resource = {}
        resource[res] = amount
        Rest.post "caravan.load_caravan",
            cid: id
            res: resource
        .done (data) ->
            return
            ##console.debug(data)
        .fail (data) ->
            console.error(data)

    TradeStation::activeTraderLoad = () ->
        @activeTrader.getCapacity()
        # trload = 0
        # for k, v of act_trader.bag
        #     continue if k is "balance"
        #     trload += v
        # return {
        #     capacity: act_trader.capacity
        #     load: trload
        # }

    TradeStation::activeTraderBalance = () ->
        return gold: @activeTrader.getBalance()

    TradeStation::empireGoldAmount = () ->
        return gold: amjad.empire.resources.balance.amount

    TradeStation::getTraders = () ->
        return amjad.empire.heroes.traders

    return TradeStation