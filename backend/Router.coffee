validator 	= require("express-validator")
wrench 		= require("wrench")
und 		= require("underscore")

#UserModel = require("./models/User")

module.exports = (app, passport) ->
	# Loads all controllers recursively!
	wrench.readdirSyncRecursive("./backend/controllers")
	.filter (cntrl) -> 
		return cntrl.endsWith(".js")
	.forEach (cntl) ->
		require("../backend/controllers/#{cntl}")(app)

	# app.post('/login', (req, res, next) ->
	# 	passport.authenticate("local",
	# 		(err, user, info) ->
	# 			if err
	# 				return next(err)
	# 			if not user
	# 				req.session.messages = [info.message]
	# 				return res.send(JSON.stringify(info))
	# 			req.logIn(user, (err) ->
	# 				if err
	# 					return next(err)
	# 				return res.send(JSON.stringify(info))
	# 			)
	# 	)(req, res, next)
	# )

	# app.post('/register', (req, res, next) ->
	# 	body = req.body
	# 	user = new UserModel(
	# 		username: body.username
	# 		firstName: body.firstname
	# 		lastName: body.familyname
	# 		email: body.email
	# 		password: body.password
	# 		faction: body.faction
	# 		architecture: body.architecture
	# 	)
		
	# 	user.save (err) ->
	# 		if err?
	# 			res
	# 			.status(422)
	# 			.send("Registering failed")
	# 		else
	# 			res
	# 			.status(200)
	# 			.send("Successfuly registered")
	# )

	# app.get("/load",
	# 	(req, res, next) ->
	# 		if req.isAuthenticated()
	# 			res
	# 			.status(200)
	# 			.send("Welcome")
	# 		else
	# 			res
	# 			.status(422)
	# 			.send("Fuck off")
	# )

	# isLoggedIn = (req, res, next) ->
	# 	if (req.isAuthenticated())
	# 		return next()
	# 	else
	# 		res.redirect("/")