"use strict"

define ["EventDispatcher", "MapConfig", "Geometry", "Vec2", "MathUtils"], 

(EventDispatcher, MapConfig, Geometry, Vec2, MathUtils) ->

	API =
		LOAD_REGION_BUFFERS: "/map/{0}/region/{1}/{2}/load"

	INCIDENCE_MATRIX = [
		[[-1,1], [0, 1], [1,1]],
		[[-1,0], [0,0], [1,0]],
		[[-1,-1],[0,-1], [1,-1]]
	]

	class RegionManager extends EventDispatcher
		constructor: (@map_manager) ->
			super()
			@init()

	# RegionManager::loadRenderableRegions = () ->
	# 	pos = Vec2.from(@map_manager.hit_area.x, @map_manager.hit_area.y)
	# 	reg = @getRegion(pos.x, pos.y)
	# 	x = reg.x
	# 	y = reg.y
	# 	out = []
	# 	for row in [0...3]
	# 		for col in [0...3]
	# 			section = @getSection(x, y)
	# 			console.debug x, y
	# 			out.push(section)
	# 			x += @swidth
	# 		x = reg.x
	# 		y += @sheight
	# 	@trigger "SECTIONS_LOADED", @
	# 	@current_region.sections = out
	# 	@current_region.pivot.x = pos.x
	# 	@current_region.pivot.y = pos.y
	# 	Vec2.release(pos)
	# 	return out

	# RegionManager::getRegion = (x, y) ->
	# 	rx = Math.floor(x / @rwidth)
	# 	ry = Math.floor(y / @rheight)
	# 	return @regions[rx+"_"+ry]

	# RegionManager::getSection = (x, y) ->
	# 	region = @getRegion(x, y)
	# 	sx = Math.floor((x - region.x) / @swidth)
	# 	sy = Math.floor((y - region.y) / @sheight)
	# 	return region.sections[sx + sy * 3]

	# RegionManager::getRegionKey = (region) ->
	# 	rx = Math.floor(region.x / @rwidth)
	# 	ry = Math.floor(region.y / @rheight)
	# 	return rx+"_"+ry

	# RegionManager::getSectionIndex = (region, section) ->
	# 	#region = @getRegion(section.x, section.y)
	# 	return region.sections.indexOf(section)

	# RegionManager::getTileAtSectionPosition = (x, y) ->
	# 	region = @getRegion(x, y)
	# 	sect = @getSection(x, y)
	# 	return @map_manager.getTileAtPosition(x - (region.x + sect.x), y - (region.y + sect.y))

	# RegionManager::getTileAtRegionPosition = (x, y) ->
	# 	region = @getRegion(x, y)
	# 	return @map_manager.getTileAtPosition(x - region.x, y - region.y)

	# RegionManager::getSectionGlobalCoordinates = (x, y) ->
	# 	section = @getRegionAtGlobalPosition(x, y)
	# 	sx = Math.floor((x - region.x) / @swidth)
	# 	sy = Math.floor((y - region.y) / @sheight)
	# 	return [sx, sy, region]

	RegionManager::findEnvelopeSections = () ->
		center = @getGlobalCenterSection()
		[crow, ccol] = @getSectionGlobalCoordinates(center.x + center.parent_region.x, center.parent_region.y + center.y)
		out = []
		console.debug "Center: ", crow, ccol

		minX = minY = Number.MAX_VALUE
		maxX = maxY = Number.MIN_VALUE

		for rows in INCIDENCE_MATRIX
			for node in rows
				sect = @getSectionAt(node[0] + crow, node[1] + ccol)
				continue if not sect?
				minX = Math.min(minX, sect.parent_region.x + sect.x)
				minY = Math.min(minY, sect.parent_region.y + sect.y)
				maxX = Math.max(maxX, sect.parent_region.x + sect.x)
				maxY = Math.max(maxY, sect.parent_region.y + sect.y)
				out.push(sect)
		
		@envelope.sections = out
		@envelope.bbox.x = minX
		@envelope.bbox.y = minY
		@envelope.bbox.width = (maxX - minX) + @swidth
		@envelope.bbox.height = (maxY - minY) + @sheight

		@loadEnvelopeBuffers()

		return out

	RegionManager::getGlobalCenterSection = () ->
		cx = @map_manager.hit_area.x + @map_manager.hit_area.width * 0.5
		cy = @map_manager.hit_area.y + @map_manager.hit_area.height * 0.5
		return @getSectionAtGlobalPosition(cx, cy)

	RegionManager::getRegionAtGlobalPosition = (x, y) ->
		rx = Math.floor(x / @rwidth)
		ry = Math.floor(y / @rheight)
		return null if rx < 0 or ry < 0
		return @regions[ry][rx]

	RegionManager::getSectionAtGlobalPosition = (x, y) ->
		region = @getRegionAtGlobalPosition(x, y)
		return null if not region?
		[sx, sy] = @getSectionLocalCoordinates(x, y)
		return region.sections[sx + sy * 3]

	RegionManager::getSectionLocalCoordinates = (x, y) ->
		region = @getRegionAtGlobalPosition(x, y)
		sx = Math.floor((x - region.x) / @swidth)
		sy = Math.floor((y - region.y) / @sheight)
		return [sx, sy, region]

	RegionManager::getSectionGlobalCoordinates = (x, y) ->
		section = @getSectionAtGlobalPosition(x, y)
		rrow = section.parent_region.row
		rcol = section.parent_region.col
		[row, col] = @getSectionLocalCoordinates(x, y)
		console.debug "region", rrow, rcol
		console.debug "section", col, row
		return [rcol * 3 + row, rrow * 3 + col]

	RegionManager::getSectionAt = (row, col) ->
		[px, py] = [row * @sheight, col * @swidth]
		return @getSectionAtGlobalPosition(px, py)

	RegionManager::getTileAtSectionPosition = (x, y) ->
		region = @getRegionAtGlobalPosition(x, y)
		sect = @getSectionAtGlobalPosition(x, y)
		return @map_manager.getTileAtPosition(x - (region.x + sect.x), y - (region.y + sect.y))

	RegionManager::init = () ->
		@map_manager.on "TILES_INITIALIZED", (map_meta) =>
			@load(map_meta)

		@map_manager.scene.on "MOUSE_WORLD_CHANGED", (x, y) =>
			rx = Math.floor(x / @rwidth)
			ry = Math.floor(y / @rheight)
			region = @regions[rx][ry]
			sx = Math.floor((x - region.x) / @swidth)
			sy = Math.floor((y - region.y) / @sheight)
			@trigger "MOUSE_OVER_REGION", @, rx, ry, sx, sy
			sect_pos = @getTileAtSectionPosition(x, y)

		@map_manager.on "HIT_AREA_CHANGED", (area) =>
			x = area.x + area.width
			y = area.y + area.height
			rx = Math.floor(x / @rwidth)
			ry = Math.floor(y / @rheight)
			new_region = @regions[rx][ry]

			if not Geometry.rectangleContainsRectangle(area, @envelope.bbox)
				console.debug "Hahah"
				@envelope.row = rx
				@envelope.col = ry
				@envelope.ref = new_region
				@envelope.sections = @findEnvelopeSections()
				@trigger "REGION_CHANGED", @, rx, ry

	# RegionManager::redrawStage = () ->
	# 	console.debug(@current_region.sections)
	# 	mx = @current_region.pivot.x
	# 	my = @current_region.pivot.y

	# 	for layer in @map_manager.layers
	# 		layer.position.x = mx #@map_manager.hit_area.x
	# 		layer.position.y = my #@map_manager.hit_area.y

	# 	@map_manager.clearLayers()
	# 	@loadRenderableRegions()
	# 	@initCurrentSectionBuffers()
	# 	@redrawCurrentSections()

	RegionManager::drawRegionBorders = () ->
		g = new PIXI.Graphics()
		colors = [
			0x00CC0F, 0x0F0C0D, 0xFEFA00,
			0x124C0F, 0xC0FC33, 0xF0CA94,
			0xCC33FF, 0x22029F, 0x90CF03
		]
		i = 0
		for rows in @regions
			for reg in rows
				g.lineStyle(
					4,
					0x000000,
					1
				)
				sw = @rwidth / 3
				sh = @rheight / 3
				for row in [0...3]
					for col in [0..3]
						color = colors[i % 9]
						g.lineStyle(
							1,
							color,
							1
						)
						g.drawRect(reg.x + (col * sw), reg.y + (row * sh), sw, sh)
						++i
				i = 0
		@map_manager.scene.world.addChild(g)
		return

	#gde lejer alternira, 0, 1 u zavisnosti od 4 bafera koja se koriste
	RegionManager::writeTileLayer = (buffer, x, y, layer, value) ->
		throw new Error("Layer out of bounds") if not 0 < layer < 2
		if value > ((2 << 11) - 1)
			throw new Error("Value #{val} exceeds the maximum allowed of #{2<<11 - 1}")
		colors_en = MathUtils.decode12(
			MathUtils.encode8(HAL.ImageUtils.getPixelAtBuffer(buffer, x, y))...
		)
		colors_en[layer] = value
		colors = MathUtils.decode8(MathUtils.encode12(colors_en...))
		buffer = HAL.ImageUtils.putPixelToBuffer(buffer, x, y, colors)
	
	RegionManager::readTileLayer = (buffer, x, y, layer) ->
		throw new Error("Layer out of bounds") if not 0 < layer < 2
		out = HAL.ImageUtils.getPixelAtBuffer(buffer, x, y)
		vals = MathUtils.decode12 MathUtils.encode8(out...)
		return vals[layer]

	
	# RegionManager::writeToSectionBufferAt = (row, col, layer, value) ->
	# 	pos = @map_manager.getTilePosition(row, col)
	# 	console.debug("Write tile position: ", row, col, pos.x, pos.y)
	# 	return @writeToSectionBufferAtPosition(pos.x, pos.y, layer, value)

	# RegionManager::writeToSectionBufferAtPosition = (x, y, layer, value) ->
	# 	tile = @getTileAtSectionPosition(x, y)
	# 	console.debug x, y
	# 	region = @getRegion(x, y)
	# 	section = @getSection(x, y)
	# 	console.debug (region.sections.indexOf(section))
	# 	bufferIndex = Math.floor(layer / 2)
	# 	lowHi = layer % 2
	# 	console.debug tile.row, tile.col, bufferIndex, lowHi
	# 	buffer = section.buffers[bufferIndex]
	# 	@writeToBufferAt(buffer, tile.col, tile.row, lowHi, value)
	# 	console.debug "Upisao vrednost #{value}", region, section
	# 	@trigger "SECTION_CHANGED", @
	# 	tcoords = @map_manager.getTileAtPosition(x, y)
	# 	@trigger "TILE_PLACED", @, region, section, tcoords.row, tcoords.col, value
	# 	return [region, section, buffer]

	# RegionManager::readFromSectionBufferAtPosition = (x, y, layer) ->
	# 	tile 		= @getTileAtSectionPosition(x, y)
	# 	section 	= @getSection(x, y)
	# 	bufferIndex = Math.floor(layer / 2)
	# 	lowHi 		= layer % 2
	# 	buffer 		= section.buffers[bufferIndex]
	# 	@readFromBufferAt(buffer, tile.col, tile.row, lowHi)

	# RegionManager::readFromSectionBufferAt = (row, col, layer) ->
	# 	pos = @map_manager.getTilePosition(row, col)
	# 	return @readFromSectionBufferAtPosition(pos.x, pos.y, layer)

	# RegionManager::writeToRegionAtPosition = (x, y) ->

	# RegionManager::encodeSectionBuffers = (section) ->
	# 	return (HAL.ImageUtils.encode64Buffer(buffer) for buffer in section.buffers)

	# RegionManager::debugCurrentSection = (section) ->
	# 	section = @current_region.ref.sections[section]
	# 	console.debug section

	# RegionManager::loadRegion = (regkey, callback) ->
	# 	region = @regions[regkey]
	# 	$.get(API.LOAD_REGION_BUFFERS.f(@map_manager.map_meta.name, region.row, region.col))
	# 	.done (loadedregion) =>
	# 		console.debug "Loaded another region"
	# 		for section, i in region.sections
	# 			loadedsection = loadedregion.sections[i]
	# 			for buffer, bufferIndex in loadedsection.buffers
	# 				section.buffers[bufferIndex] = HAL.ImageUtils.base64ToPixelBuffer(buffer)
	# 		callback(null, region)

	RegionManager::loadEnvelopeBuffers = () ->
		for section in @envelope.sections
			continue if section.loaded
			section.loaded = true
			section.buffers = (HAL.ImageUtils.base64ToPixelBuffer(buffer) for buffer in section.buffers)
	
	RegionManager::load = (map) ->
		@regions  	= []
		@rwidth 	= map.region_width
		@rheight 	= map.region_height
		@swidth 	= @rwidth / 3
		@sheight 	= @rheight / 3

		@numbuffs 	= Math.ceil(MapConfig.MAX_LAYERS / 2)

		for key, region of @map_manager.map_meta.regions
			rows = @regions[region.row] ?= []
			if not rows[region.col]?
				rows[region.col] = region

		@columns 	= (map.width / @rwidth)
		@rows 		= (map.height / @rheight)

		@envelope = 
			row: 0
			col: 0
			ref: @regions[0][0]
			tile_over: null
			sections: []
			pivot: new PIXI.Point(0, 0)
			bbox: new PIXI.Rectangle(0, 0, 0, 0)
	
		for rows in @regions
			for region in rows
				for section in region.sections
					section.parent_region = region

		@findEnvelopeSections()
		@drawRegionBorders()
		@trigger "LOADED"

	# @redrawStage()
	# RegionManager::redrawCurrentSections = () ->
	# 	for row in [0...MapConfig.NN_ROWS]
	# 		for col in [0...MapConfig.NN_COLUMNS]
	# 			for l in [0...MapConfig.MAX_LAYERS]
	# 				val = @readFromSectionBufferAt(row, col, l)
	# 				@map_manager.addTileLayerByID(row, col, val)
	# 				#pos = @map_manager.getTilePosition(row, col)
	# 				#@trigger "TILE_PLACED", @, @getRegion(pos.x, pos.y), @getSection(pos.x, pos.y), row, col, val

	return RegionManager