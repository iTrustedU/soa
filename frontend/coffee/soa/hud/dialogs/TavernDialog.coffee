"use strict"

html =
"""
<div class="tavern draggable">
    <div class="dialog-name">
        <span>T A V E R N</span>
        <div class="close-button"></div>
    </div>
    <div class="tavern-tabs">
        <div class="chat-tab" id="commander-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Commander</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="agent-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Agent</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="caravan-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Caravan</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="trainer-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Trainer</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="mercenary-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Mercenary</span>
            <div class="chat-tab-r"></div>
        </div>
    </div>
    <div class="table-th">
        <span>Price</span>
        <span>Statistics</span>
        <span>Commander</span>
    </div>
    <div class="bars"></div>
    <div id="heroes-table">
        {{{hero_info}}}
    </div>
    <div class="war-building-info">
        <img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img" />
        <div class="upgrade-price">
            <img src="img/top-resources/wood.png">
            <span>200000</span>
            <img src="img/top-resources/iron.png">
            <span>200000</span>
            <img src="img/top-resources/gold.png">
            <span>200000</span>
            <img src="img/top-resources/food.png">
            <span>200000</span>
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
        <div id="move-building" class="upgrade-building move-button">Move</div>
    </div>
    <div class="employ-button">Employ</div>
</div>
"""

$commander_info = 
"""
<div class="tavern-info default-skin">
    <table>
        {{#each gen}}
            <tr class="commander-row" id={{gen_commander_id}}>
                <td id="price">{{price}}</td>
                <td>
                    <span class="statistics">Readiness</span><span>{{ready}}</span><br />
                    <span class="statistics">Speed</span><span>{{speed}}</span><br />
                    <span class="statistics">Leadership</span><span>{{leadership}}</span><br />
                </td>
                <td>
                    <div class="tavern-hero-img">
                        <!-- <img src="{{concat_hero_avatar img_id}}" /> -->
                        <img src="{{concat_hero_avatar "commander" "m"}}" />  
                    </div><br />
                    <span class="tavern-hero-table-name">{{n}}</span>
                </td>
            </tr>
        {{/each}}
    </table>
</div> 
"""

$agent_info = 
"""
<div class="tavern-info default-skin">
    <table>
        {{#each agents}}
            <tr class="commander-row" id={{gen_agent_id}}>
                <td id="price">{{price}}</td>
                <td>
                    <span class="statistics">Resourcefulness</span><span>{{ress}}</span><br />
                    <span class="statistics">Stealth</span><span>{{st}}</span><br />
                    <span class="statistics">Wit</span><span>{{wit}}</span><br />
                </td>
                <td>
                    <div class="tavern-hero-img">
                        <img src="{{concat_hero_avatar "agent" "m"}}" />
                    </div><br />
                    <span class="tavern-hero-table-name">{{name}}</span>
                </td>
            </tr>
        {{/each}}
    </table>
</div> 
"""

$trader_info = 
"""
<div class="tavern-info default-skin">
    <table>
        {{#each traders}}
            <tr class="commander-row" id={{gen_trader_id}}>
                <td id="price">{{price}}</td>
                <td>
                    <span class="statistics">Capacity</span><span>{{cap}}</span><br />
                    <span class="statistics">Speed</span><span>{{speed}}</span><br />
                    <span class="statistics">Tongue</span><span>{{tongue}}</span><br />
                </td>
                <td>
                    <div class="tavern-hero-img">
                        <img src="{{concat_hero_avatar "trader" "m"}}" />
                    </div><br />
                    <span class="tavern-hero-table-name">{{name}}</span>
                </td>
            </tr>
        {{/each}}
    </table>
</div> 
"""

$trainer_info = 
"""
<div class="tavern-info default-skin">
    <table>
        {{#each trainers}}
            <tr class="commander-row" id={{gen_trainer_id}}>
                <td id="price">{{price}}</td>
                <td>
                    <span class="statistics">Brawn</span><span>{{brawn}}</span><br />
                    <span class="statistics">Reflexes</span><span>{{reflexes}}</span><br />
                    <span class="statistics">Endurance</span><span>{{endurance}}</span><br />
                </td>
                <td>
                    <div class="tavern-hero-img">
                        <img src="{{concat_hero_avatar "trainer" "m"}}" />
                    </div><br />
                    <span class="tavern-hero-table-name">{{name}}</span>
                </td>
            </tr>
        {{/each}}
    </table>
</div>
"""


define ["soa/hud/dialogs/BaseDialog"],
(BaseDialog) ->
    
    HEROES_GENERATING_REFRESH_TIME = 5*60*1000
    class TavernDialog extends BaseDialog
        constructor: () ->
            super(html)

    # prolog se desava pre ucitavanja i kreiranja 
    # struktura: empire, user, heroes, etc.
    TavernDialog::onPrologue = () ->
        @setTemplates
            "trainer_template": $trainer_info
            "trader_template": $trader_info
            "commander_template": $commander_info
            "agent_template": $agent_info

        @upgradeable()
        @moveable()
        @closeable()
        @draggable()

    # ovo se desava posle ucitavanja struktura
    TavernDialog::onEpilogue = (hud) ->
        @on "PRE_SHOW", _me_ = () ->
            @html
            .find(".dialog-building-img")
            .attr("src", Handlebars.helpers.concat_dialog_construction("tavern"))
            @remove "PRE_SHOW", _me_

        @on "SELECTED_TAB", (val) ->
            template = "commander_template"
            if val is "commander-tab"
                datakey = "commander_info"
            else if val is "caravan-tab"
                template = "trader_template"
                datakey = "trader_info"
            else if val is "trainer-tab"
                template = "trainer_template"
                datakey = "trainer_info"  
            else if val is "agent-tab"
                template = "agent_template"
                datakey = "agent_info"
            else
                alert("not supported")
                return

            @compile(template, datakey).then (data) =>
                @refresh("heroes-table", data)
                @html.find(".tavern-info").customScrollbar()
                $("tr").click () ->
                    $('tr').not(@).removeClass('tr-click-bg')
                    $(@).toggleClass("tr-click-bg")

        @on "EMPLOY_HERO", () ->
            id = @get("SELECTED_HERO")
            ##console.debug "ID = #{id}"
            tab = @get("SELECTED_TAB")
            row = @html.find(".commander-row##{id}")
            price = row.find("#price").html()
            defer = new $.Deferred()

            ### @todo extensive preemptive check ###
            if not tab? or not id?
                defer.reject("TAB: #{tab}, ID: #{id}")
            else if tab is "commander-tab"
                amjad.empire.employCommander(id)
                .done (data) =>
                    @setData "commander_info", amjad.empire.loadCommandersList
                    cm = amjad.empire.heroes.addNewCommander data
                    cn.troops = {}
                    defer.resolve(cm)
                .fail (err) ->
                    defer.reject(err)
            else if tab is "caravan-tab"
                amjad.empire.employTrader(id)
                .done (data) =>
                    @setData "trader_info", amjad.empire.loadCaravansList
                    tr = amjad.empire.heroes.addNewTrader data
                    tr.bag = {
                        balance: 0
                    }
                    defer.resolve(tr)
                .fail (err) ->
                    defer.reject(err)
            else if tab is "trainer-tab"
                tg = amjad.empire.constructions.getAllOfType("training_grounds")[0]
                if not tg?
                    alert("No training grounds")
                    defer.reject("No training grounds")
                else 
                    amjad.empire.employTrainer(id, tg.construction_id)
                    .done (data) =>
                        @setData "trainer_info", amjad.empire.loadTrainersList
                        tr = amjad.empire.heroes.addNewTrainer data
                        defer.resolve(tr)
                    .fail (err) ->
                        defer.reject(err)
            else if tab is "agent-tab"
                amjad.empire.employAgent(id)
                .done (data) =>
                    @setData "agent_info", amjad.empire.loadAgentsList
                    ag = amjad.empire.heroes.addNewAgent data
                    defer.resolve(ag)
                .fail (err) ->
                    defer.reject(err)
            else
                defer.reject "You can't employ this one"
            
            defer.promise()
            .done (hero) ->
                amjad.empire.resources.take(balance: price)
                HeroesQueue = SOA.Hud.getDialog("HEROES_QUEUE")
                if hero.getType() isnt "trainer"
                    HeroesQueue.putHero(hero)
                row.remove()
            .fail (err) ->
                alert(err.statusText)

        amjad.empire.generateCommandersList().done () ->
            amjad.empire.loadCommandersList()
    
        amjad.empire.generateCaravansList().done () ->
            amjad.empire.loadCaravansList()
        
        amjad.empire.generateTrainersList().done () ->
            amjad.empire.loadTrainersList()

        amjad.empire.generateAgentsList().done () ->
            amjad.empire.loadAgentsList()

        @on "SHOW", () ->
            @setData "commander_info", amjad.empire.loadCommandersList
            @setData "trader_info", amjad.empire.loadCaravansList
            @setData "trainer_info", amjad.empire.loadTrainersList
            @setData "agent_info", amjad.empire.loadAgentsList
            
            @set("SELECTED_TAB", "commander-tab")
            @trigger "SELECTED_TAB", "commander-tab"
        
        @clickTriggers
            ".chat-tab/#id": "SELECTED_TAB",
            ".employ-button/#": "EMPLOY_HERO",
            ".commander-row/#id": "SELECTED_HERO"

        @scheduleAsyncDataRefresh(HEROES_GENERATING_REFRESH_TIME,
            "commander_info": amjad.empire.generateCommandersList
            "trader_info": amjad.empire.generateCaravansList
            "trainer_info": amjad.empire.generateTrainersList
            "agent_info": amjad.empire.generateAgentsList
        )

    return TavernDialog