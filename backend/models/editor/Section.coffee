mongoose = require "mongoose"

schema = mongoose.Schema
    buffers:
        type: String
        default: [
            "", "", "", ""
        ]
    x:
        type: Number
    y:
        type: Number

module.exports = schema