(function() {
  "use strict";
  define([], function() {
    var MiniMapView;
    MiniMapView = (function() {
      /* @todo Ovo sranjce ce da leti odavde*/

      function MiniMapView() {}

      MiniMapView.dom_minimap = HAL.Dom.viewport.querySelector("#minimap");

      return MiniMapView;

    })();
    return MiniMapView;
  });

}).call(this);
