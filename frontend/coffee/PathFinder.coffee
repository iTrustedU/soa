"use strict"

define ["MinHeap"], 

(MinHeap) ->

    compareFs = (a, b) ->
        return a.f > b.f

    class PathFinder
        constructor: (@map) ->
            @diagonal_cost = 1.4
            @straight_cost = 1.0

        tracePath: (dest) ->
            out = []
            while dest.parent?
                out.push(dest)
                dest = dest.parent
            return out

        ###
            @throws
        ###
        find: (from, to) ->
            open = new MinHeap(null, compareFs)
            closed = {}
            open_map = {}

            if not from? or not to?
                throw new Error("no start or end node given")

            from_ = {
                row: from.row
                col: from.col
                id: from.id
                f: 0
                g: 0
                h: 0
                parent: null
            };

            open.push(from_)

            open_map[from.id] = from
            while open.size() > 0
                cur = open.pop()
                open_map[cur.id] = null

                if cur.row == to.row and cur.col == to.col
                    out = @tracePath(cur)
                    out.push(from_)
                    return out

                closed[cur.id] = cur

                neighs = @map.getNeighbours(@map.getTileAt(cur.row, cur.col))

                for t in neighs
                    if (t.row == cur.row) or (t.col == cur.col)
                        g = @straight_cost
                    else 
                        g = @diagonal_cost

                    if closed[t.id]?
                        continue

                    if not open_map[t.id]?
                        #ovo kao dobro balansira putanju
                        #h = (Math.abs(to.row - t.row) + Math.abs((to.col - t.col)/2-(t.col%2))) * @straight_cost
                        
                        #ovo ok radi
                        #h = (Math.abs(to.row - t.row) + Math.abs((to.col - t.col)/2-(t.col%2))) * @diagonal_cost

                        #ovo definitivno uvek trazi najkracu
                        xDiff = Math.abs(to.position.x - t.position.x)
                        yDiff = Math.abs(to.position.y - t.position.y)
            
                        if (t.col % 2)
                            cost = @straight_cost
                        else 
                            cost = @diagonal_cost

                        h = Math.sqrt((yDiff*yDiff) + (xDiff*xDiff)) * cost

                        #eksperimentalno se pokazalo da ovo najbolje sljaka, al
                        #ima discrepancy pri ivicama, fix za ovo jos nisam otkrio
                        #h = (Math.abs(to.row - t.row) + Math.abs(t.col - to.col)) * cost

                        open.push(
                            h: h
                            row: t.row
                            id: t.id
                            col: t.col
                            g: cur.g + g
                            f: h + g
                            parent: cur
                        )
                        open_map[t.id] = t
                    else 
                        tt = open_map[t.id]
                        if cur.g + g < tt.g
                            tt.g = cur.g + g
                            tt.f = tt.h + tt.g
                            tt.parent = cur

    return PathFinder