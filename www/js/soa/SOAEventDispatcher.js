(function() {
  "use strict";
  define([], function() {
    var EventDispatcher;
    EventDispatcher = (function() {
      function EventDispatcher() {
        this.__listeners__ = {};
      }

      return EventDispatcher;

    })();
    EventDispatcher.prototype.on = function(type, clb) {
      var ind, t, _i, _len;
      if (type instanceof Array) {
        for (_i = 0, _len = type.length; _i < _len; _i++) {
          t = type[_i];
          if (this.__listeners__[t] == null) {
            this.__listeners__[t] = [];
          }
          this.__listeners__[t].push(clb);
        }
      } else {
        if (this.__listeners__[type] == null) {
          this.__listeners__[type] = [];
        }
        this.__listeners__[type].push(clb);
        ind = this.__listeners__[type].indexOf(clb);
      }
      return clb;
    };
    EventDispatcher.prototype.remove = function(type, clb) {
      var ind;
      if (this.__listeners__[type] != null) {
        ind = this.__listeners__[type].indexOf(clb);
        if (ind !== -1) {
          this.__listeners__[type].splice(ind, 1);
        }
        return clb = null;
      }
    };
    EventDispatcher.prototype.removeAll = function(type) {
      var key, keys, list, _i, _len, _results;
      if (type) {
        return delete this.__listeners__[type];
      } else {
        keys = Object.keys(this.__listeners__);
        _results = [];
        for (_i = 0, _len = keys.length; _i < _len; _i++) {
          key = keys[_i];
          _results.push((function() {
            var _j, _len1, _ref, _results1;
            _ref = this.__listeners__[key];
            _results1 = [];
            for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
              list = _ref[_j];
              _results1.push(this.remove(key, list));
            }
            return _results1;
          }).call(this));
        }
        return _results;
      }
    };
    EventDispatcher.prototype.trigger = function(type, msg) {
      var clb, _i, _len, _ref, _results;
      if (!this.__listeners__[type]) {
        return;
      }
      _ref = this.__listeners__[type];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        clb = _ref[_i];
        if (clb != null) {
          _results.push(clb.call(this, msg, clb));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    return EventDispatcher;
  });

}).call(this);
