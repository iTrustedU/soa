"use strict"

define ["soa/constructions/Construction"],

(Construction) ->

	class WarTent extends Construction

	WarTent::init = () ->	
		@units = []
		@commanders = []

	WarTent::epilogue = () ->
		@addUnit(un) for un in amjad.empire.units
		for cmndr in amjad.empire.heroes.commanders
			@addCommander(cmndr)

	WarTent::addUnit = (un) ->
		f = (@units.filter (unit) -> return un.type is unit.type)[0]
		if f?
			f.amount += un.amount
		else 
			@units.push(un)
		@trigger "REFRESH_UNITS"
	
	WarTent::removeUnit = (type, amnt) ->
		f = (@units.filter (unit) -> return unit.type is type)[0]
		throw new Error("You can't remove the unit which you don't even have") if not f?
		f["amount"] -= amnt
		if f["amount"] <= 0
			ind = @units.indexOf(f)
			@units.splice(ind, 1)
		@trigger "REFRESH_UNITS"

	WarTent::addCommander = (comm) ->
		if cmndr.isInOwnCity()
			@commanders.push(comm)

	# alert war.construction_id
	# amjad.finishProduction(war.construction_id).done (data) ->
	#     console.error data
	# .fail () ->
	#     alert "omg"

	return WarTent