"use strict"

define [
	"soa/StructureLoader"
	"soa/constructions/Construction"
	"soa/constructions/TrainingGrounds"
	"soa/constructions/WarTent"
	"soa/constructions/Science"
	"soa/constructions/TradeStation"
	"soa/constructions/EncampedPOI"
],
(
	StructureLoader
	Construction
	TrainingGrounds
	WarTent
	Science
	TradeStation
	EncampedPOI
) ->

	ShortMap =
		"*" : "*"

	CONSTRUCTIONS_COLLISION_LAYER   = 6
	ID_MAP                          = {}
	TYPE_MAP                        = {}
	
	collisionReport = (collided) ->
		outStr = "Collisions at: \n"
		for cl in collided
			outStr += "#{cl.row},#{cl.col}\n"
		return outStr

	class Constructions extends StructureLoader
		constructor: () ->
			@buildings = []
			@pois = []
			@available_constructions = {}
			super(
				"*": [
					"*", 
					(meta) ->
						try 
							@loadNewConstruction(meta)
						catch e
							console.error e
						return null
				]
			)

	Constructions::loadNewConstruction = (meta) ->
		cn = null
		switch meta.c_type
			when "training_grounds"
				cn = new TrainingGrounds(ShortMap).load(meta)
			when "war"
				cn = new WarTent(ShortMap).load(meta)
			when "science"
				cn = new Science(ShortMap).load(meta)
			when "trade_station"
				cn = new TradeStation(ShortMap).load(meta)
			else
				if meta.poi_id?
					cn = new EncampedPOI(ShortMap).load(meta)
					@pois.push(cn)
				else
					cn = new Construction(ShortMap).load(meta)
		if not cn?
			throw new Error("No construction implementation for #{meta.c_type}")

		if not meta.poi_id?
			@buildings.push(cn)
			
		if not TYPE_MAP[cn.c_type]?
			TYPE_MAP[cn.c_type] = []
		TYPE_MAP[cn.c_type].push cn
		return cn

	Constructions::getIDMap = () ->
		return ID_MAP

	Constructions::getSpanMark = (row, col) ->
		return @span_area[row*@span_area_h + col]

	Constructions::fillSpanArea = (spans, id) ->
		out = []
		if not spans?
			return out #throw new Error("Span not available!!!")
		for spmark in spans
			index = spmark.row*@span_area_h + spmark.col
			if @span_area[index] is 0
				@span_area[index] = id
			else if @span_area[index] isnt id
				out.push({row: spmark.row, col: spmark.col})
		return out

	Constructions::testSpanArea = (spans, id) ->
		out = []
		if not spans?
			return out #throw new Error("Span not available!!!")
		for spmark in spans
			index = spmark.row*@span_area_h + spmark.col
			if @span_area[index] isnt id and @span_area[index] isnt 0
				out.push({row: spmark.row, col: spmark.col})
		return out

	Constructions::fromSpanMark = (row, col) ->
		id = @getSpanMark(row, col)
		return ID_MAP[id]

	Constructions::canBePlacedOn = (cnA, cnB) ->
		return @testSpanArea(cnA.getSpan(), cnB.getSpan())
	
	Constructions::initSpanArea = () ->
		@span_area_w = SOA.City.map.NROWS
		@span_area_h = SOA.City.map.NCOLUMNS
		@span_area = []
		for i in [0...@span_area_w]
			for j in [0...@span_area_h]
				@span_area[i*@span_area_h + j] = 0

	Constructions::constructionFulfilsRequirements = (requirements) ->
		out = []
		hasResources = amjad.empire.resources.hasEnough(requirements.resources)
		out = out.concat hasResources
		out = out.concat amjad.empire.constructions.hasConstructions(requirements.constructions, 
			(myconstr, xml_level) ->
				return (myconstr?.level >= xml_level)
		)
		return out

	Constructions::initListeners = () ->
		super()
		@on "DONE_LOADING", () ->
			console.debug "CONSTRUCTIONS LOADED"

		HAL.on "SOA_LOADED", () =>
			for cn in @buildings
				cn.epilogue()
			@findAvailableConstructions(XMLStore.get("constructions"))
			SOA.Hud.getDialog("BOTTOM_MENU").listBuildings()

		HAL.on "CITY_MAP_LOADED", () =>
			@initSpanArea()
			@placeConstructions()
			@placePOIEncampments()

		amjad.on ["CONSTRUCTIONS_UPDATED", "RESOURCES_UPDATED"], () =>
			console.debug "Constructions/resources updated"
			@updateConstructions()

	Constructions::updateConstructions = () ->
		#mark previously disabled constructions
		dis = []
		for k, v of @available_constructions
			if v.type is "disabled"
				dis.push k
		@findAvailableConstructions(XMLStore.get("constructions"))
		SOA.Hud.getDialog("BOTTOM_MENU").listBuildings()
		
		#trigger animation on bottom menu
		for k, cnt of @available_constructions
			if cnt.type isnt "disabled" and dis.indexOf(k) isnt -1
				bottom_img =
					$(".img-frame.bottom-menu-building.ui-draggable##{k}")
				bottom_img.addClass("construction-built")
				bottom_img.removeClass("construction-built", 5000)

	Constructions::getAllOfType = (type) ->
		return TYPE_MAP[type]

	Constructions::getAll = (type) ->
		return @buildings

	Constructions::getByID = (id) ->
		return ID_MAP[id]

	Constructions::hasConstructions = (types, filter = -> return) ->
		out = []
		for type, val of types
			if not @hasConstruction(type, val) and not filter(@getAllOfType(type)?[0], val)
				out.push "Missing required construction: #{type}, level: #{val}"
		return out

	Constructions::hasConstruction = (type, level = 1) ->
		return false if type is "home"
		return TYPE_MAP[type]? and TYPE_MAP[type].length > 0
		# out = false
		# @buildings.forEach (cnt) ->
		#     if cnt.c_type is type 
		#         out = true
		#         return
		# return out #and @[type].level >= level

	Constructions::buildConstruction = (type, constr) ->
		if @hasConstruction(type)
			throw new Error("You already have #{type} construction")
		return @loadNewConstruction(constr)

	Constructions::canBeConstructed = (key, constr_req) ->
		out = []
		exists = @hasConstruction(key)
		out.push "Construction exists #{key}" if exists
		out = out.concat @constructionFulfilsRequirements(constr_req)
		return out.length is 0

	### @todo na istu foru prodji kroz ostale, samo ih taguj ###
	Constructions::findAvailableConstructions = (xml_constructions = XMLStore.get("constructions")) ->
		for key, constr of xml_constructions.city
			# norm_name = @[key]?.getLevelNormalizedName()
			continue if @hasConstruction(key) # if norm_name? or 
			sprite_uri = amjad.user.CONSTRUCTION_SPRITES_URI + key
			can_construct = @canBeConstructed(key, constr.level1.requirements)
			is_supported_by_us = amjad.layerMetaFromSprite("#{sprite_uri}1")?

			if can_construct and is_supported_by_us
				@available_constructions[key] = 
					sprite: "assets/sprites/#{sprite_uri}1"
					type: key
			else if can_construct and not is_supported_by_us
				continue
			else if is_supported_by_us
				@available_constructions[key] = 
					sprite: sprite_uri + "_dis"
					type: "disabled"

	###
	@action Constructs a building
	@string Building type
	###
	Constructions::construct = (building) ->
		constr_xml = XMLStore.get("constructions").city[building.type]["level1"]
		if not constr_xml?
			throw new Error("You can't construct this building")

		meta_uri = amjad.splaySpriteURI("#{amjad.user.CONSTRUCTION_SPRITES_URI}")
		layermeta = amjad.layerMetaFromSprite("#{meta_uri}#{building.type}1")
		row = SOA.currentMap.tile_under.row
		col = SOA.currentMap.tile_under.col

		result =
		Rest.post "construction.start_build", 
			ty: building.type
			x: row
			y: col

		defer = new $.Deferred()

		result.done (data) =>
			constr = @buildConstruction(building.type, data)
			if constr?
				delete @available_constructions[building.type]
				@placeConstruction(constr)
				amjad.empire.resources.take(constr_xml.requirements.resources)
				amjad.trigger "CONSTRUCTIONS_UPDATED"
				defer.resolve(constr)
			else
				defer.reject("Construction couldn't be built")

		result.fail (err) =>
			defer.reject(err.responseText or err)

		return defer.promise()

	Constructions::placePOIEncampment = (poienc) ->
		try
			poienc.addToMap()
		catch
			console.error e

	Constructions::placePOIEncampments = () ->
		for poi in @pois
			@placePOIEncampment(poi)

	Constructions::placeConstructions = () ->
		for cn in @buildings
			@placeConstruction(cn)

	Constructions::placeConstruction = (cn) ->
		try 
			cn.addToMap()
			ID_MAP[cn.construction_id] = cn
			collisions = @fillSpanArea(cn.getSpan(cn.level), cn.construction_id)
		catch e
			console.error e
		
		if collisions?.length > 0
			console.error collisionReport(collisions)
	
	return Constructions