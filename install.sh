#!/bin/bash

function is_installed {
	local ret_code=1
	type $1 >/dev/null 2>&1 || { local ret_code=0; }
	echo "$ret_code"
}

function is_installed_g {
	npm list --global | grep $1 > /dev/null 2>&1
	echo "$?"
}

function echo_error {
  printf "\e[31m✘ ${1}"
  printf "\033[0m"
}
 
function echo_ok {
  printf "\e[32m✔ ${1}"
  printf "\033[0m"
}

function echo_if {
  if [ $1 == 0 ]; then
    echo_ok $2
  else
    echo_error $2
  fi
}

function install_npm_g {
	if [ $(is_installed_g $1) == 0 ]; then
		echo "0" && return
	fi
	npm install $1 -g > /dev/null 2>&1
	echo "$(is_installed_g $1)"
}

function install_npm_l {
	npm install > /dev/null 2>&1
	echo "$?"
}

function make_log_files {
	mkdir -p logs &&
	touch logs/mongod.log && 
	touch logs/express.log &&
	touch logs/grunt.log &&
	chown $1:$1 -R logs/
	echo "$?"
}

if [ "$(id -u)" != "0" ]; then echo "Run me as root!" 1>&2 && exit 1 
fi

# @todo
# download sources witnpmh curl/wget/whatever_available
# compile and then install
if [ $(is_installed node) == 0 ]; then
	echo "Couldn't find node.js" 1>&2 && exit 1
fi

if [ $(is_installed npm) == 0 ]; then
	echo "Couldn't find npm" 1>&2 && exit 1
fi

# @todo 
# check if mongod is installed...

# install dev tools
echo "grunt-cli $(echo_if $(install_npm_g grunt-cli))"
echo "nodemon $(echo_if $(install_npm_g nodemon))"
echo "bower $(echo_if $(install_npm_g bower))"

# install local modules
echo "node_modules $(echo_if $(install_npm_l))"

# setup as suid
u_name=`awk -v val=$SUDO_UID -F ":" '$3==val{print $1}' /etc/passwd`
echo "logs $(echo_if $(make_log_files $u_name))"
