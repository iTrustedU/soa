mongoose = require "mongoose"

schema = mongoose.Schema
	name:
		type: String
		required: true
		unique: true

	layer:
		type: Number
		required: true

	span:
		type: String
		required: true

	sprite:
		type: String
		required: true

	seedId:
		type: Number
		required: true

module.exports = TileMetaModel = mongoose.model("TileMeta", schema)

schema.pre "save", (next) ->
	TileMetaModel
	.findOne(seedId : @seedId)
	.exec (err, result) ->
		if result?
			next(new Error("Tile SeedID is a duplicate!!!"))
		else
			next()
