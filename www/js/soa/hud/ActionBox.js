(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"action-box\" style=\"z-index: 10000;\">\n    <img class=\"action-box-img\" />\n    <img src=\"assets/sprites/amjad/abox/move.png\" class=\"ac-move-to\" />\n    <img src=\"assets/sprites/amjad/abox/trade.png\" class=\"ac-spy\" />\n    <img src=\"assets/sprites/amjad/abox/camp.png\" class=\"ac-camp\" />\n    <img src=\"assets/sprites/amjad/abox/attack.png\" class=\"ac-attack\" />\n</div>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var ActionBox;
    ActionBox = (function(_super) {
      __extends(ActionBox, _super);

      function ActionBox() {
        ActionBox.__super__.constructor.call(this, html);
      }

      return ActionBox;

    })(BaseDialog);
    ActionBox.prototype.onPrologue = function() {
      var _this = this;
      this.hide();
      this.MoveBtn = this.html.find(".ac-move-to");
      this.AttackBtn = this.html.find(".ac-attack");
      this.CampBtn = this.html.find(".ac-camp");
      this.TradeBtn = this.html.find(".ac-spy");
      this.MoveBtn.click(function() {
        _this.ctx.doAction("move");
        return _this.hide();
      });
      this.AttackBtn.click(function() {
        _this.ctx.doAction("attack");
        return _this.hide();
      });
      this.TradeBtn.click(function() {
        _this.ctx.doAction("trade");
        return _this.hide();
      });
      this.CampBtn.click(function() {
        _this.ctx.doAction("camp");
        return _this.hide();
      });
      return this.html.click(function() {
        return _this.hide();
      });
    };
    ActionBox.prototype.setMoveAsToCity = function() {
      return this.MoveBtn.attr("src", "assets/sprites/amjad/abox/city.png");
    };
    ActionBox.prototype.setMoveAsDefault = function() {
      return this.MoveBtn.attr("src", "assets/sprites/amjad/abox/move.png");
    };
    return ActionBox;
  });

}).call(this);
