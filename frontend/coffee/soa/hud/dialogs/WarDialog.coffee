"use strict"

html =
"""
<div class="war-commander draggable">
    <div class="dialog-name">
        <span>W A R  &nbsp; T E N T</span>
        <div class="close-button"></div>
    </div>
    <div class="tavern-tabs">
        <div class="chat-tab" id="commander-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Commander</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="agent-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Agent</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab" id="research-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Research</span>
            <div class="chat-tab-r"></div>
        </div>
    </div>
    <div id="tab-content">
        {{{current_tab}}}
    </div>
    <div class="war-building-info">
        <img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img" />
        <div class="upgrade-price">
            <img src="img/top-resources/wood.png">
            <span>200000</span>
            <img src="img/top-resources/iron.png">
            <span>200000</span>
            <img src="img/top-resources/gold.png">
            <span>200000</span>
            <img src="img/top-resources/food.png">
            <span>200000</span>
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
        <div id="move-building" class="upgrade-building move-button">Move</div>
    </div>
</div>
"""

$war_commander_tab = 
"""
<div class="war-commander-info">
    <div class="war-bars"></div>
    <div id="commanders-list" class="war-info default-skin">
        {{{commanders_list}}}
    </div>
    <div class="trained-units">
        <div id="trained-units" class="war-troops-trained-units">
            {{#each units}}
                <div class="unit-box list-unit" id={{@key}}>
                    <img src={{concat_unit_avatar @key "borderless"}} class="list-troop"/>
                    <span class="units-amount">{{amount}}</span>
                </div>
            {{/each}}
        </div>
        <div id="selected-unit">
            {{{selected_unit_info}}}
        </div>
    </div>
</div>
"""

$commanders_list_template =
"""
<table style="width:426px">
    {{#each commanders}}
        <tr>
            <td>
                <div class="war-troops2" id="{{army_id}}" comm_id="{{commander_id}}">
                    {{#each getUnits}}
                        <div class="unit-box2">
                            <img class="top-left-img" src={{concat_unit_avatar @key "borderless"}} />
                            <span class="troops-amount2">{{this}}</span>
                        </div>
                    {{/each}}
                </div>
                <div class="war-troop-info">
                    <div class="name-img-level">
                        <span class="color-text war-unit-name">{{name}}</span>
                        <img src={{concat_hero_avatar "commander" "m"}} />
                    </div>
                    <div class="war-unit-info-name">
                        <span class="color-text">Readiness:</span>
                        <span class="color-text">Speed:</span>
                        <span class="color-text">Leadership:</span>
                        <!-- <span class="color-text">Agent:</span> -->
                    </div>
                    <div class="war-unit-info-value">
                        <span>{{readiness}}</span>
                        <span>{{speed}}</span>
                        <span>{{leadership}}</span>
                        <!-- <span>{{agent}}</span> -->
                    </div>
                    <div class="trader-bar-box war-exp-bar-box">
                        <div class="trader-bar war-bar">

                        </div>
                        <span class="level-unit-name">Level: {{level}}</span>
                    </div>
                </div>
            </td>
        </tr>
    {{/each}}
</table>
"""

$selected_unit_info = 
"""
<div class="wart-tent-unit-info-box">   
    <span class="war-available-unit-name">{{unit_name}}</span>
    <div class="unit-info-name war-name">
        <span class="color-text">Health:</span>
        <span class="color-text">Attack:</span>
        <span class="color-text">Defense:</span>
        <span class="color-text">Upkeep:</span>
    </div>
    <div class="unit-info-value">
        <span>{{health}}</span>
        <span>{{attack}}</span>
        <span>{{defense}}</span>
        <span>{{upkeep}}</span>
    </div>
    <div class="unit-info-name">
        <span class="color-text">Pillage:</span>
        <span class="color-text">Raze:</span>
        <span class="color-text">Steal:</span>
    </div>
    <div class="unit-info-value">
        <span>{{pillage}}</span>
        <span>{{raze}}</span>
        <span>{{steal}}</span>
    </div>
</div>  
"""

$war_agent_tab = 
"""
<div class="agent-commander-info">
    <div class="war-bars"></div>
    <table>
        <tr>
            <td>
                <div class="agent-missions">
                    <span>{{mission_1}}</span>
                    <span>{{mission_2}}</span>
                    <span>{{mission_3}}</span>
                </div>
                <div class="war-troop-info">
                    <div class="name-img-level">
                        <span class="color-text war-unit-name">{{unit_name}}</span>
                        <img src="{{concat_sprite avatar}}" />
                    </div>
                    <div class="war-unit-info-name">
                        <span class="color-text">Readiness:</span>
                        <span class="color-text">Speed:</span>
                        <span class="color-text">Leadership:</span>
                        <span class="color-text">Commander:</span>
                    </div>
                    <div class="war-unit-info-value">
                        <span>{{readiness}}</span>
                        <span>{{speed}}</span>
                        <span>{{leadership}}</span>
                        <span>{{unit_name}}</span>
                    </div>
                    <div class="trader-bar-box war-exp-bar-box">
                        <div class="trader-bar war-bar">
                            <div class="trader-bar-full"></div>
                        </div>
                        <span class="level-unit-name">Lewel: {{level}}</span>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class="agent-bottom">
        <div class="war-troops-trained-units">
            <span class="agent-mission-info">{{agent_mission_info}}</span>
        </div>
    </div>
</div>
"""

$war_research_tab = 
"""
<div class="war-research-info">
    <div class="war-content-tab">
         <span class="science-middle-deco">War</span>
    </div>
    <div class="war-tent-content-box  war-content-tab-sub-content">
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Offensive Tactics</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Protective Maneuvers</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Ranking System</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Honorary Service</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Food Quality</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Discipline</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
    </div>
    <div class="war-content-tab">
        <span class="science-middle-deco">Production</span>
    </div>
    <div class="war-tent-content-box  production-content-tab-sub-content">
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Siege Weaponry</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Equipment blueprints</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Giftmaker Tools</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Artisan Crafts</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Trainer Efficiency</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
    </div>
    <div class="war-content-tab">
        <span class="science-middle-deco">Espionage</span>
    </div>
    <div class="war-tent-content-box  espionage-content-tab-sub-content">
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Ambush</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Spy</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Sabotage</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Burn</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Blackmail</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
        <div class="science-tab-box">
            <div class="img-name">
                <img src="{{concat_sprite avatar}}" class="war-research-img" />
                <span class="science-name">Infiltrate</span>
            </div>
            <div class="st-info-box">
                <div class="st-level">
                    <span class="color-text">Level:</span>
                    <span>{{level}}</span>
                </div>
                <div class="st-info">
                    <span class="color-text">Info:</span>
                    <span>{{info}}</span>
                </div>
                <div class="st-time">
                    <span class="color-text">Time:</span>
                    <span>{{time}}</span>
                </div>
                <div class="st-price">
                    <span class="color-text">Price:</span>
                    <span>Wood:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Iron:</span><span>154856</span>
                </div>
                <div class="st-price st-sub-price">
                    <span>Gold:</span><span>154856</span>
                </div>
            </div>
        </div>
    </div>
</div>
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->

    class WarDialog extends BaseDialog
        constructor: () ->
            super(html)

    WarDialog::onPrologue = (hud) ->
        @setTemplates
            "research_tab": $war_research_tab
            "commanders_tab_template": $war_commander_tab
            "agent_tab": $war_agent_tab
            "selected_unit_info_template": $selected_unit_info
            "commanders_list_template": $commanders_list_template

        @upgradeable()
        @moveable()

        @closeable()
        @draggable()

    WarDialog::onEpilogue = (hud) ->
        @on "PRE_SHOW", _me_ = () =>
            @html
            .find(".dialog-building-img")
            .attr("src", Handlebars.helpers.concat_dialog_construction("war"))
            @remove "PRE_SHOW", _me_

        @clickTriggers
            ".list-unit/#id": "SELECTED_TROOP"

        @on "SELECTED_TROOP", (id) =>
            alert id
            @setData "SELECTED_UNIT", @unitList[id]
            @compile("selecetd_unit_info_template", @unitList[id]).then (data) =>
                @refresh("selected-unit", data)

        @on "SHOW", () =>
            if (Object.keys(@commandersList().commanders).length is 0)
                alert("You have no commanders at the moment")
                @disable()
                return
            @setData "all_units", @unitList
            @setData "commanders_list", @commandersList
            @recompile()
    
    WarDialog::recompile = () ->
        @compile("commanders_tab_template", "all_units").then (data) =>
            @refresh("tab-content", data)
            @html.find(".list-unit").draggable
                helper: "clone"
                appendTo: "body"
                containment: "window"
                start: () ->
                    $(@).data("origin", $(@).position())
                revert: "invalid"

            @html.find("tr").click () ->
                $('tr').not(@).removeClass('tr-click-bg')
                $(@).toggleClass("tr-click-bg")

            @html.find("#commanders-list").customScrollbar()

        @compile("commanders_list_template", "commanders_list").then (data) =>
            @refresh("commanders-list", data)
            @html.find(".war-troops2").droppable
                accept: ".list-unit"
                drop: (ev, ui) ->
                    
                    army_id = $(@).attr("id")
                    commander_id = $(@).attr("comm_id")
                    unit = ui.draggable.attr("id")

                    commanderUnitAmount = amjad.empire.heroes.commanders[commander_id].troops[unit]
                    war = SOA.Hud.getDialog("WAR")
                    trainedAmount = war.get("trained_map")[unit].amount

                    dlg = SOA.Hud.getDialog("EXCHANGE_DIALOG")
                    dlg.open(
                        {
                            minAmount: 0,
                            amount: trainedAmount,
                            picture: Handlebars.helpers.concat_unit_avatar(unit, "44x44")
                        }
                        ,{
                            amount: commanderUnitAmount || 0,
                            picture: Handlebars.helpers.concat_unit_avatar(unit, "44x44")
                        },
                        (numTransfered) ->
                            # war = SOA.Hud.getDialog("WAR")
                            # #dlg = war.Dialog
                            order = {}
                            order[unit] = numTransfered #war.get("trained_map")[unit].amount
                            ### @todo ovo prebacuje sve njih ###
                            res = Rest.post("army.reassign_from_settlement",
                                arid: army_id
                                rt: order
                            ).done((data) =>
                                ##console.debug "yay, you assigned some units #{unit}"
                                comm = amjad.empire.heroes.commanders[commander_id]
                                comm.addUnit(unit, order[unit])
                                war.ctx.removeUnit(unit, order[unit])
                                war.setData "all_units", war.unitList
                                war.recompile()
                            ).fail((data) =>
                                ui.draggable.animate(ui.draggable.data("origin"),"fast")
                            )
                    )


            @html.find("#commanders-list").customScrollbar()

    WarDialog::takeUnit = () ->
        @recompile()

    WarDialog::unitList = () ->
        trained_map = {}
        for k, v of @ctx.units
            continue if v.amount is 0
            p = XMLStore.units[v.type]
            trained_map[v.type] = v
            trained_map[v.type]["amount"] = v.amount
            trained_map[v.type]["stats"] = p.stats
        @set "trained_map", trained_map
        return "units": trained_map

    WarDialog::commandersList = () ->
        return "commanders": amjad.empire.heroes.commanders

    return WarDialog
