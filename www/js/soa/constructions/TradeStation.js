(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/constructions/Construction"], function(Construction) {
    var TradeStation, _ref;
    TradeStation = (function(_super) {
      __extends(TradeStation, _super);

      function TradeStation() {
        _ref = TradeStation.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return TradeStation;

    })(Construction);
    TradeStation.prototype.init = function() {
      var good, guri, k, spr, _ref1, _results;
      this.goods = {};
      this.traders = [];
      _ref1 = XMLStore.get("tradegoods");
      _results = [];
      for (k in _ref1) {
        good = _ref1[k];
        guri = ("assets/sprites/amjad/resources/" + k).trim();
        spr = PIXI.TextureCache[guri];
        if (spr != null) {
          _results.push(this.goods[k] = good);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    TradeStation.prototype.epilogue = function() {
      var trader, _i, _len, _ref1, _results;
      _ref1 = amjad.empire.heroes.traders;
      _results = [];
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        trader = _ref1[_i];
        _results.push(this.addTrader(trader));
      }
      return _results;
    };
    TradeStation.prototype.addTrader = function(trader) {
      if (trader.isInOwnCity()) {
        return this.traders.push(trader);
      }
    };
    return TradeStation;
  });

}).call(this);
