(function() {
  var mongoose, schema;

  mongoose = require("mongoose");

  schema = mongoose.Schema({
    x: {
      type: Number,
      required: true
    },
    y: {
      type: Number,
      required: true
    },
    name: {
      type: String,
      required: true
    }
  });

  module.exports = mongoose.model("Settlement", schema);

}).call(this);
