"use strict"

define [
    "jquery"
    "jquery-ui"
    "EventDispatcher"
    "soa/hud/HBHelpers"
    "soa/hud/dialogs/DialogsLoader"
],

(
    $
    $$
    EventDispatcher
    HBHelpers
    DialogsLoader
) ->

    class HUDManager extends EventDispatcher
        constructor: () ->
            super()
            @dialogs = {}
            @domElement = null
            @dm = null
            @queue = {}
            ##console.debug("HUDManager is starting")

    HUDManager::prologue = () ->
        $deferred = new $.Deferred()
        $(document).ready(() =>
            @onPrologue()
            $deferred.resolve()
        )
        return $deferred.promise()

    HUDManager::hideAllDialogs = () ->
        for nm, dlg of @dialogs
            dlg.hide()

    HUDManager::getDialog = (name) ->
        return @dialogs[name]

    HUDManager::onPrologue = () ->
        @start_time = window.performance.now()
        @domElement = $("#viewport")
        #@dm = new DialogManager(@domElement)
        @dialogs_loader = new DialogsLoader()
        for name, dialogCls of @queue
            #console.debug "Executing prologue for #{name}"
            # dlg = new dialogCls()
            # @todo ovo nije potrebno, al eto kad vlada zuri
            # if dlg.prologue?
            #   dlg.prologue(@domElement)
            # @dialogs[name] = dlg
            @addDialog(name, new dialogCls())
            #@domElement.append(dlg.html)
        #console.debug "HUD prologue executed"

    # Izvrsava se nakon uspesnog logina kada se podignu i isparsiraju sve strukture
    # Na taj nacin dijalozima dozvoljavam safe inicijalizaciju kada im je potreban
    # pristup SOA strukturi
    HUDManager::epilogue = () ->
        for name, dlg of @dialogs
            ##console.debug "Executing epilogue for #{name}"
            ### @todo ovo nije potrebno, al eto kad vlada zuri ###
            if dlg.epilogue?
                dlg.epilogue(@domElement)
        @end_time = window.performance.now() - @start_time
        ##console.debug "HUD epilogue executed"
        ##console.debug "From HUD prologue to epilogue, took #{@end_time.toFixed(2)}ms"

    HUDManager::showDialog = (name, ctx, animationSpeed = 300) ->
        dlg = @dialogs[name]
        throw new Error("No dialog with name: #{name}") if not dlg?
        if not ctx?
            ctx = {}
            console.warn("Context not passed to #{name}")
        dlg.show(ctx, animationSpeed)
        return dlg

    HUDManager::hideDialog = (name, animationSpeed = 300) ->
        dlg = @dialogs[name]
        if not dlg? throw new Error("No dialog with name: #{name}")
        dlg.hide(animationSpeed)
        return dlg

    HUDManager::insert = (name, Class) ->
        @queue[name] = Class

    HUDManager::fadeInViewport = () ->
        @domElement.css("opacity", 0)
        return @domElement.animate(
            {
                "opacity": 1
            }, 1000
        ).promise()

    HUDManager::addDialog = (name, dialog) ->
        if @dialogs[name]?
            throw new Error("Dialog with name:#{name} is already loaded")
        @dialogs[name] = dialog
        dialog.setName(name)
        @on "SHOW_#{name}_DIALOG", (ctx) =>
            wrapper.show(ctx)
        @on "HIDE_#{name}_DIALOG", (ctx) =>
            wrapper.hide(ctx)
        dialog.prologue(@domElement)
        dialog.hide(0)
        @domElement.append(dialog.html)
        return dialog

    return HUDManager