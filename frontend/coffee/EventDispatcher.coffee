"use strict"

define [], 

() ->

	class EventDispatcher
		constructor: () ->
			@listeners = {}

	EventDispatcher::on = (type, clb) ->
		if type instanceof Array
			for t in type
				if not @listeners[t]?
					@listeners[t] = []
				@listeners[t].push(clb)
		else
			if not @listeners[type]?
				@listeners[type] = []
			@listeners[type].push(clb)
		return clb

	EventDispatcher::removeTrigger = (type, clb) ->
		if @listeners[type]?
			ind = @listeners[type].indexOf(clb)
			if ind isnt -1
				@listeners[type].splice(ind, 1) if ind isnt -1
			clb = null

	EventDispatcher::removeAllTriggers = (type) ->
		if type
			delete @listeners[type]
		else
			keys = Object.keys(@listeners)
			for key in keys
				@removeTrigger(key, list) for list in @listeners[key]
		delete @listeners
		
	EventDispatcher::trigger = (type, args...) ->
		return if not @listeners[type]
		for clb in @listeners[type]
			clb.apply(args[0] || @, args[1...]) if clb?

	return EventDispatcher
	