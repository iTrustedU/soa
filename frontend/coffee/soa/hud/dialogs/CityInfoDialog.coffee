"use strict"

html =
"""
<div class="city-info">
    <div class="dialog-name">
        <span id="caravan-name">C I T Y &nbsp; I N F O</span>
        <div class="close-button"></div>
    </div>
    <div class="city-info-content">
    	<div class="city-info-box">
    		<img src="assets/sprites/amjad/dialogs/constructions/civil/war.png" />
    		<span>
    			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do 
    			eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
    		</span>
    	</div>
        <div class="city-info-box-2">
            <div class="text-info">
                <span class="city-text half"><img src="img/dialog/city-info/city-icon.png" />Citonija</span>
                <span class="city-text half"><span class="color-text-2">X</span> 154885 <span class="color-text-2">Y</span> 154885</span>
                <span class="city-text full"><img src="img/dialog/city-info/player-icon.png" />Player Name</span>
                <span class="city-text full"><img src="img/dialog/city-info/glory-icon.png" />15425</span>
                <span class="city-text full"><img src="img/dialog/city-info/alliance-icon.png" />Alianconia</span>
            </div>
            <div class="city-buttons">
                <div class="c-button">Save</div>
                <div class="c-button">Mail</div>
            </div>
        </div>
        <div class="active-gifts-box">
            <div class="active-gifts">Active Gifts</div>
            <img src="assets/sprites/amjad/resources/brass.png" />
            <img src="assets/sprites/amjad/resources/camel.png" />
            <img src="assets/sprites/amjad/resources/chick.png" />
            <img src="assets/sprites/amjad/resources/cloth.png" />
            <img src="assets/sprites/amjad/resources/copper.png" />
            <img src="assets/sprites/amjad/resources/dates.png" />
            <img src="assets/sprites/amjad/resources/grain.png" />
            <img src="assets/sprites/amjad/resources/milk.png" />
            <img src="assets/sprites/amjad/resources/olives.png" />
            <img src="assets/sprites/amjad/resources/paper.png" />
            <img src="assets/sprites/amjad/resources/salt.png" />
            <img src="assets/sprites/amjad/resources/silk.png" />
        </div>
    </div>
</div>
"""


define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->  

    class CityInfoDialog extends BaseDialog
        constructor: () ->
            super(html)
    
    CityInfoDialog::onPrologue = () ->

        @closeable()
        @draggable()

        # battle_status =
        #     b_stat: "Defeated"

        # template = Handlebars.compile($battle_status);
        # bs = template(battle_status)

        # template = Handlebars.compile(html)
        # $ExchangeDialog = $(template(
        #     battle_status: bs
        # ))

        # Hal.trigger "DOM_ADD", (domlayer) ->
        #     $(domlayer).append($ExchangeDialog)

        # Hal.on "OPEN_EXCHANGE_DIALOG", (exchange) ->
        #     $ExchangeDialog.show()

    return CityInfoDialog