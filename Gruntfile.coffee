module.exports = (grunt) ->
    fs      = require "fs"
    path    = require "path"
    wrench  = require "wrench"
    log     = console.log

    ###
        Meta information and settings
    ###
    config =
        cur_dir: process.cwd()
        pub_dir: "."
        js_dir: "www/js/"
        coffee_dir: "www/coffee/"
        sprite_dir: "www/assets/sprites/"
        partials_dir: "www/hbpartials/"
        sprite_list: "www/assets/sprites.list"
        partials_list: "www/hbpartials/partials.list"
        backend_dir: "backend/"
        frontend_dir: "frontend/coffee/"
        utils_dir: "utils/"
        config_dir: "config/"
        server_file: "server.coffee"

    is_win         = !!process.platform.match(/^win/)
    is_json        = /^.*\.[json]+$/
    is_sprite      = is_spritesheet = /^.*\.[png|jpg]+$/
    is_hbpartial   = /^.*\.handlebars$/
    all_sprites    = null
    all_partials   = null

    log "Is windows: #{is_win}".green
    log "Platform #{process.platform}".green

    grunt.loadNpmTasks("grunt-contrib-coffee")
    grunt.loadNpmTasks("grunt-contrib-watch")

    grunt.initConfig
        pkg: grunt.file.readJSON("package.json")

        coffee:
            glob_all:
                expand: true
                cwd: "#{config.frontend_dir}"
                src: ["**/*.coffee"]
                dest: "#{config.js_dir}"
                ext: ".js"

            all: 
                expand: true,
                flatten: false,
                cwd: "#{config.frontend_dir}",
                src: ['**/*.coffee'],
                dest: "#{config.js_dir}",
                ext: ".js"

            shared:
                expand: true,
                flatten: false,
                cwd: '.',
                src: ["#{config.utils_dir}**/*.coffee"],
                dest: '.',
                ext: '.js'

            server:
                expand: true,
                flatten: false,
                cwd: '.',
                src: ["./server.coffee"],
                dest: '.',
                ext: '.js'

            backend:
                expand: true,
                flatten: false,
                cwd: '.',
                src: ["#{config.backend_dir}**/*.coffee"],
                dest: '.',
                ext: '.js'

            configs:
                expand: true,
                flatten: false,
                cwd: '.',
                src: ["./config/**/*.coffee"],
                dest: '.',
                ext: '.js'

        watch:
            coffee_frontend:
                files: ["#{config.frontend_dir}**/*.coffee"]
                tasks: ["coffee:glob_all"]
                options:
                    nospawn: true
                    livereload: false

            coffee_server:                 
                files: ["#{config.server_file}"]
                tasks: ['coffee:server']
                options:
                    nospawn: true
                    livereload: true

            coffee_backend:
                files: ["#{config.backend_dir}**/*.coffee"]
                tasks: ['coffee:backend']
                options:
                    nospawn: true
                    livereload: true

            coffee_configs:
                files: ["#{config.config_dir}**/*.coffee"]
                tasks: ['coffee:configs']
                options:
                    nospawn: true
                    livereload: true

            coffee_shared: 
                files: ["#{config.utils_dir}**/*.coffee"]
                tasks: ['coffee:shared']
                options:
                    nospawn: true
                    livereload: true

    grunt.event.on "watch", (action, filepath) ->
        filepath = filepath.replace(grunt.config("coffee.glob_all.cwd"), "")
        log filepath.yellow
        grunt.config("coffee.glob_all.src", [filepath])
        
    grunt.registerTask "serve", ["watch"]

    saveSprites = (sprites) ->
        sprites = sprites.map(
            (x) ->
                return "assets/sprites/#{x}"
        )
        fs.writeFileSync(config.sprite_list, sprites.join().replace(/,/g,"\n"))

    savePartials = (partials) ->
        # partials = partials.map(
        #     (x) ->
        #         return "#{config.partials_dir}#{x}"
        # )
        fs.writeFileSync(config.partials_list, partials.join().replace(/,/g,"\n"))

    #load all sprites
    all_sprites = do getAllSprites = () ->
        out = wrench.readdirSyncRecursive(config.sprite_dir)
        out = out.filter((x) -> return is_sprite.test(x) or is_json.test(x))
        if is_win
            out = out.map (x) -> return x.replace(/\\/g,"\/")
        return out

    all_partials = do getAllPartials = () ->
        out = wrench.readdirSyncRecursive(config.partials_dir)
        out = out.filter((x) -> return is_hbpartial.test(x))
        if is_win
            out = out.map (x) -> return x.replace(/\\/g,"\/")
        return out
    
    saveSprites(all_sprites)
    savePartials(all_partials)