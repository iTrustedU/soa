(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/StructureLoader", "soa/hud/IntervalQueue"], function(StructureLoader, IntervalQueue) {
    var Resource, Resources, ShortMap, UPDATE_INTERVAL, _ref;
    ShortMap = {
      "*": "*"
    };
    Resource = (function(_super) {
      __extends(Resource, _super);

      function Resource() {
        _ref = Resource.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return Resource;

    })(StructureLoader);
    UPDATE_INTERVAL = 60 * 1000;
    Resources = (function(_super) {
      __extends(Resources, _super);

      function Resources() {
        Resources.__super__.constructor.call(this, {
          "*": [
            "*", function(meta) {
              return new Resource(ShortMap).load(meta);
            }
          ]
        });
      }

      Resources.prototype.load = function(meta) {
        return Resources.__super__.load.call(this, meta);
      };

      Resources.prototype.initListeners = function() {
        return this.on("DONE_LOADING", function() {
          return this.scheduleUpdate();
        });
      };

      return Resources;

    })(StructureLoader);
    Resources.prototype.scheduleUpdate = function() {
      var _this = this;
      return IntervalQueue.addToQueue(function() {
        return Rest.post("settlement.calculate_all_resources").then(function(data) {
          return _this.add(data);
        }).fail(function(data) {
          return console.error(data);
        });
      }, UPDATE_INTERVAL);
    };
    Resources.prototype.take = function(res) {
      var key, val;
      if (res == null) {
        console.error("You can't take null resources");
      }
      for (key in res) {
        val = res[key];
        this[key].amount -= +val;
      }
      amjad.trigger("RESOURCES_UPDATED");
      return SOA.Hud.getDialog("TOP_MENU").updateResources();
    };
    Resources.prototype.add = function(res) {
      var k, v;
      for (k in res) {
        v = res[k];
        if (this[k] != null) {
          this[k].amount += v;
        }
      }
      amjad.trigger("RESOURCES_UPDATED");
      return SOA.Hud.getDialog("TOP_MENU").updateResources();
    };
    Resources.prototype.hasEnough = function(res) {
      var amount, key, out;
      out = [];
      for (key in res) {
        amount = res[key];
        if (this[key] == null) {
          out.push("Resource doesnt exist: " + key);
        }
        if ((+this[key].amount) < (+amount)) {
          out.push("Not enough " + key + " resources, amount required: " + ((+amount).toFixed(0)) + ", amount available: " + ((+this[key].amount).toFixed(0)));
        }
      }
      return out;
    };
    Resources.prototype.getTradeResources = function() {
      var good, guri, k, out, spr, _ref1;
      out = {};
      _ref1 = XMLStore.get("tradegoods");
      for (k in _ref1) {
        good = _ref1[k];
        guri = "assets/sprites/amjad/resources/" + k + ".png";
        spr = PIXI.TextureCache[guri];
        if ((spr != null) && (this[k] != null)) {
          out[k] = this[k];
        }
      }
      return out;
    };
    Resources.prototype.getScienceResources = function(science, level) {
      var index, num, resourceAmounts, resourceTypes, resources, xmlSciences, _i, _len;
      xmlSciences = XMLStore.get("science");
      if (level === 0) {
        level = 1;
      }
      resourceAmounts = new String(xmlSciences[science].resources['level' + level].amount).split(',');
      resourceTypes = new String(xmlSciences[science].resources['level' + level].resource_type).split(',');
      resources = {};
      for (index = _i = 0, _len = resourceAmounts.length; _i < _len; index = ++_i) {
        num = resourceAmounts[index];
        resources[resourceTypes[index]] = num;
      }
      return resources;
    };
    Resources.prototype.getTechResources = function(tech, level) {
      var index, num, resourceAmounts, resourceTypes, resources, xmlTechnologies, _i, _len;
      xmlTechnologies = XMLStore.get("technologies");
      if (level === 0) {
        level = 1;
      }
      resourceAmounts = new String(xmlTechnologies[tech]['level' + level].resources.amount).split(',');
      resourceTypes = new String(xmlTechnologies[tech]['level' + level].resources.resource_type).split(',');
      resources = {};
      for (index = _i = 0, _len = resourceAmounts.length; _i < _len; index = ++_i) {
        num = resourceAmounts[index];
        resources[resourceTypes[index]] = num;
      }
      return resources;
    };
    Resources.prototype.getConstructionUpgradeResources = function(constr, level) {
      var xmlConstructions;
      xmlConstructions = XMLStore.get("constructions");
      return xmlConstructions.city[constr]['level' + level].requirements;
    };
    Resources.prototype.getEmpireResources = function() {
      var key, out, val, _ref1;
      out = {};
      _ref1 = amjad.empire.resources;
      for (key in _ref1) {
        val = _ref1[key];
        if (this[key].hasOwnProperty("resource_type")) {
          out[key] = this[key];
        }
      }
      return out;
    };
    return Resources;
  });

}).call(this);
