(function() {
  "use strict";
  define([], function() {
    var LoggingQueue;
    LoggingQueue = (function() {
      function LoggingQueue() {
        this.errors = [];
        this.warnings = [];
      }

      return LoggingQueue;

    })();
    LoggingQueue.prototype.error = function(msg) {
      this.errors.push(msg);
      return console.error(msg);
    };
    return window.LoggingQueue = new LoggingQueue();
  });

}).call(this);
