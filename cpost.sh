#!/bin/bash
# cpost.sh: Simple utility script to post JSON data via command line
# @author amilic
usage() {
	echo "Usage: $0 route/to/api {jsonobject: \"hi\"}" && exit 1
}
[[ $# -lt 2 ]] && usage

curl -X POST  -H "Accept: application/json" -H "Content-Type: application/json" \
http://localhost:3000/$1 -d $2 | grep '}' | python -mjson.tool
