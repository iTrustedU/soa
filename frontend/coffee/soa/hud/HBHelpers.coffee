"use strict"

define ["handlebars", "soa/Lang"], 

(Handlebars, lang) ->

    ##console.debug "Importing HB helpers"

    Handlebars.registerHelper "concat_sprite", (src) -> 
        return "assets/sprites/#{src}.png"

    Handlebars.registerHelper "concat_construction_icon", (construction, disabled) -> 
        icon = "#{construction}_icon.png"
        if disabled
            icon = "#{construction}_dis_icon.png"
        return "assets/sprites/amjad/constructions/#{amjad.user.architecture}/#{icon}"

    Handlebars.registerHelper "concat_science", (src) -> 
        return "assets/sprites/amjad/science/#{src}.png"

    Handlebars.registerHelper "concat_technology", (src) -> 
        return "assets/sprites/amjad/technologies/#{src}.png"

    Handlebars.registerHelper "lang", (word) -> 
        return lang[word]

    Handlebars.registerHelper "concat_hero_avatar", (src, type) ->
        avuri = "amjad/hero_avatars/#{src}#{type}"
        return Handlebars.helpers.concat_sprite avuri

    Handlebars.registerHelper "concat_construction", (type) ->
        constr = "#{amjad.user.CONSTRUCTION_SPRITES_URI}/#{type}"
        return Handlebars.helpers.concat_sprite constr

    Handlebars.registerHelper "concat_research", (type, typeOfResearch) ->
        constr = "#{amjad.user.TECH_RESEARCH_SPRITES_URI}/#{type}"
        constr = "#{amjad.user.SCIENCE_RESEARCH_SPRITES_URI}/#{type}" if typeOfResearch
        return Handlebars.helpers.concat_sprite constr

    Handlebars.registerHelper "concat_dialog_construction", (type) ->
        constr = "#{amjad.user.DIALOG_CONSTRUCTIONS_URI}/#{type}" 
        return Handlebars.helpers.concat_sprite constr
         
    Handlebars.registerHelper "concat_resource", (type) ->
        resuri = "amjad/resources/#{type}"
        return Handlebars.helpers.concat_sprite resuri

    Handlebars.registerHelper "concat_unit_avatar", (src, type) ->
        unuri = "amjad/units/#{amjad.user.architecture}/#{type}/#{src}"
        return Handlebars.helpers.concat_sprite unuri
        
    Handlebars.registerHelper "calc_experience_percentage", (hero) ->
        return "" if not hero.experience?
        return "
            style=width:#{((hero.experience / hero.nextLevelExperience()) * 100).toFixed()}%!important
        "
    Handlebars.registerHelper "concat_dialog_construction", (type) ->
        constr = "#{amjad.user.DIALOG_CONSTRUCTIONS_URI}/#{type}"
        return Handlebars.helpers.concat_sprite constr

    Handlebars.registerHelper "concat_poi", (poi_type) ->
        poi_uri = "amjad/poi/#{poi_type}"
        return Handlebars.helpers.concat_sprite poi_uri

    Handlebars.registerHelper "capitalize_word", (word) ->
        return word.charAt(0).toUpperCase() + word.slice(1)

    Handlebars.registerHelper "each_skip_null", (context, options) ->
        out = ""
        for key, it of context
            continue if not it?
            out = out + options.fn(it)
        return out