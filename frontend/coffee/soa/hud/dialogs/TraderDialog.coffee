"use strict"

html =
"""
<div class="trader-screen draggable">
    <div class="dialog-name">
        <span id="caravan-name">T R A D E R &nbsp; &nbsp; {{name}}</span>
        <div class="close-button"></div>
    </div>
    <div class="commander-info">
        <div id="trader-info-2">{{{trader_info}}}</div>
    </div>
    <div class="trader-img" style="left:-7px!important">
        <img src="{{concat_hero_avatar "trader" "l"}}" />
    </div>
    <div class="trader-inventory" id="own-goods">
        {{{own_goods}}}
    </div>
    <div class="trader-bar-box">
        <div class="trader-bar">
            <div class="trader-bar-full" {{calc_experience_percentage this}}></div>
        </div>
    </div>
    <div id="weight-money">{{{weight_money}}}</div>
    <div id="trader-item-info">{{{trader_item_info}}}</div>
</div>
"""

$trader_info = 
"""
<div class="commander-statistics"> 
    <span class="commander-level-name"><img src="img/dialog/training/level.png" /></span>
    <span class="commander-level-value">{{level}}</span>
    <span class="commander-expirience-name color-text">Experience:</span>
    <span class="commander-current-expirience">{{experience}} / {{nextLevelExperience}}</span>
    <span class="commander-level-name color-text">Capacity:</span>
    <span class="commander-level-value">{{capacity}}</span>
    <span class="commander-level-name color-text">Speed:</span>
    <span class="commander-level-value">{{speed}}</span>
    <span class="commander-level-name color-text">Tongue:</span>
    <span class="commander-level-value">{{tongue}}</span>
</div>
"""

$trader_item_info = 
"""
<div class="trader-inventory-info">
    <span>{{info}}</span>
</div>
"""

$weight_money = 
"""
<div class="weight-money">
    <span>Weight:</span>
    <span>{{getLoad}}</span>
    <span> / {{capacity}}</span></br>
    <span>Money:</span>
    <span>{{getBalance}}</span>
</div>
"""

$trader_goods = 
"""
{{#each goods}}
    <div class="goods-box4">
        <img class="poi-good trader-good" id="{{@key}}" src="{{concat_resource @key}}"/>
        <span class="goods-amount4">{{this}}</span>
    </div>
{{/each}}
"""

define ["soa/hud/dialogs/BaseDialog"],
(BaseDialog) ->

    class TraderDialog extends BaseDialog
        constructor: () ->
            super(html)

    TraderDialog::onPrologue = () ->   
        trader_item_info =
            info: "Lorems ipsum dolor sit amet, consectetur adipisicing elit, sed ko eiusmod tempor" 

        @setTemplates
            "weight_money": $weight_money
            "trader_info": $trader_info
            "trader_item_info": $trader_item_info
            "trader_goods": $trader_goods

        @on "SHOW", () ->
            @setData "trader_info", @ctx
            @setData "trader_goods", "goods": @ctx.getGoods()
            @setData "trader_item_info", trader_item_info

            @compileHtml(@ctx)

            @compile("trader_info", "trader_info").then (data) =>
                @refresh("trader-info-2", data)

            @compile("weight_money", "trader_info").then (data) =>
                @refresh("weight-money", data)

            @compile("trader_item_info", "trader_item_info").then (data) =>
                @refresh("trader-item-info", data)

            @compile("trader_goods", "trader_goods").then (data) =>
                @refresh("own-goods", data)

            @draggable()
            @closeable()

    return TraderDialog

    #TraderDialog.on "SHOW", () ->
    #    @recompile()

    # TraderDialog.recompile = () ->

    # trader =
    #     experience: 600
    #     capacity: 100
    #     speed: 25
    #     tongue: 56
 

    # weight_money =
    #     weight: 6070
    #     money: 1080

    # trader_image =
    #     avatar: "heroes/hero-icon-12"
        
    

    # template = Handlebars.compile($trader_info);
    # ti = template(trader)

    # template = Handlebars.compile($trader_item_info);
    # tii = template(trader_item_info)

    # template = Handlebars.compile($weight_money);
    # wm = template(weight_money)

    # template = Handlebars.compile($trader_image);
    # timg = template(trader_image)

    # template = Handlebars.compile(html)
    # $TraderDialog = $(template(
    #     trader_info: ti
    #     trader_item_info: tii
    #     weight_money: wm
    #     trader_image: timg
    # ))

    # Hal.trigger "DOM_ADD", (domlayer) ->
    #     $(domlayer).append($TraderDialog)

    # Hal.on "SHOW_TRADER_DIALOG", (trader) ->
    #     $TraderDialog.show()