"use strict"

define ["jquery", "EventDispatcher", "handlebars"], 

($, EventDispatcher) ->

	PARTIAL_DIR 	= "hbpartials/"
	PARTIALS_LIST 	= "#{PARTIAL_DIR}partials.list"
	instance 		= null

	class LoadPartials extends EventDispatcher
		constructor: () ->
			super()
			return instance if instance?
			instance = @
			window.HBPartials = instance
			@loaded = false
			return @load()

	LoadPartials::load = () ->
		$deferred = new $.Deferred()
		if @loaded
			$deferred.resolve()
		else 
			$.get(PARTIALS_LIST)
			.done (data) =>
				@loadAllPartials(data.split("\n"), $deferred)
		return $deferred.promise()

	LoadPartials::loadAllPartials = (list, $deferred) ->
		len = list.length
		loaded_so_far = 0
		for url in list
			if url is ""
				loaded_so_far++
			else 
				@loadPartial(url)
				.done () => 
					loaded_so_far++
					if loaded_so_far is len
						@loaded = true
						$deferred.resolve()
						@trigger "LOADED_ALL_PARTIALS", @

	LoadPartials::loadPartial = (url) ->
		return $.get("#{PARTIAL_DIR}#{url}")
		.done (html) =>
			@trigger "LOADED_PARTIAL", @, html
			name = url.substring(0, url.lastIndexOf(".handlebars"))
			Handlebars.registerPartial(name, html)

	return LoadPartials