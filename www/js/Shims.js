(function() {
  "use strict";
  define([], function() {
    /*
        A shim to support timer. 
        performance.now is an ultra-precise timer and is preferred over Date.now
    */

    if (window.performance == null) {
      window.performance = Date;
    }
    String.prototype.format = String.prototype.f = function() {
      var args, i, s;
      if (arguments[0] instanceof Array) {
        args = arguments[0];
      } else {
        args = arguments;
      }
      i = args.length;
      s = this;
      while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), args[i]);
      }
      return s;
    };
    String.prototype.endsWith = function(str) {
      return this.lastIndexOf(str) + str.length === this.length;
    };
    String.prototype.startsWith = function(str) {
      return this.indexOf(str) === 0;
    };
    return Array.prototype.chunk = function(chunkSize) {
      var i;
      return (function() {
        var _i, _ref, _results;
        _results = [];
        for (i = _i = 0, _ref = this.length; chunkSize > 0 ? _i < _ref : _i > _ref; i = _i += chunkSize) {
          _results.push(this.slice(i, i + chunkSize));
        }
        return _results;
      }).call(this);
    };
  });

}).call(this);
