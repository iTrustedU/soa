(function() {
  "use strict";
  var html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"loader-dialog-container\">\n	<div id=\"flipping-panel\" class=\"block panel\">\n		<div class=\"front loader-dialog gradient-background\">\n			<i id=\"new-flip-btn\" class=\"fa fa-windows flip-btn\">&nbsp;new map</i>\n			<br><br>\n			<div class=\"loader-combobox-container\">\n				<select class=\"loader-combobox\" id=\"map-choice\">\n		            {{#each maplist}}\n		            	<option value={{name}}>{{name}}</option>\n		            {{/each}}\n		        </select> \n		        <i id=\"map-delete\" class=\"fa fa-times delete-btn\"></i>\n		        <i id=\"map-load\" class=\"fa fa-refresh load-btn\"></i>\n	        </div>\n		</div>\n\n		<div class=\"back loader-dialog gradient-background\">\n			<i id=\"load-flip-btn\" class=\"fa fa-windows flip-btn\">&nbsp;load map</i>\n			<br><br>\n			<span>name:</span>\n			<input id=\"map-name\" class=\"stylish-input\"></input>\n			<br><br>\n			<span>rows:</span>\n			<input id=\"num-rows\" class=\"stylish-input\"></input>\n			<br><br>\n			<span>columns:</span>	 \n			<input id=\"num-columns\" class=\"stylish-input\"></input>\n			<br><br>\n			<input id=\"map-create\" class=\"loader-load-button\" type=\"button\" value=\"create\"/>\n		</div>\n	</div>\n</div>";

  define(["EventDispatcher", "MapConfig"], function(EventDispatcher, MapConfig) {
    var API, LoaderDialog;
    LoaderDialog = (function(_super) {
      __extends(LoaderDialog, _super);

      function LoaderDialog() {
        LoaderDialog.__super__.constructor.call(this);
        return this;
      }

      return LoaderDialog;

    })(EventDispatcher);
    API = {
      SAVE_MAP: "/maps/new",
      MAP_LIST: "/maps",
      DELETE_MAP: "/map/{0}/delete",
      DELETE_ALL: "/maps/delete",
      DELETE_MANY: "/maps/deletemany",
      LOAD_MAP: "/map/{0}/load"
    };
    LoaderDialog.prototype.prologue = function() {
      return this.loadMapList();
    };
    LoaderDialog.prototype.epilogue = function() {
      var $create_btn, $delete_btn, $load_btn, $load_flip_btn, $map_cbx, $map_columns, $map_name, $map_rows, $new_flip_btn, $panel,
        _this = this;
      $load_btn = this.html.find("#map-load");
      $create_btn = this.html.find("#map-create");
      $delete_btn = this.html.find("#map-delete");
      $map_name = this.html.find("#map-name");
      $map_rows = this.html.find("#num-rows");
      $map_columns = this.html.find("#num-columns");
      $map_cbx = this.html.find("#map-choice");
      $new_flip_btn = this.html.find("#new-flip-btn");
      $load_flip_btn = this.html.find("#load-flip-btn");
      $panel = this.html.find("#flipping-panel");
      this.container = this.html.find(".loader-dialog-container");
      $delete_btn.on("click", function() {
        var $selected_map;
        $selected_map = $map_cbx.find("option:selected");
        return $.post(API.DELETE_MAP.f($selected_map.text())).done(function(map) {
          $selected_map.remove();
          return _this.trigger("MAP_DELETED", _this, map);
        });
      });
      $create_btn.on("click", function() {
        var data;
        data = {
          name: $map_name.val(),
          rows: $map_rows.val(),
          columns: $map_columns.val()
        };
        return _this.createMap(data);
      });
      $load_btn.on("click", function() {
        var selected_map;
        selected_map = $map_cbx.find("option:selected").text();
        return _this.loadSelectedMap(selected_map);
      });
      $new_flip_btn.on("click", function() {
        return $panel.addClass("flip");
      });
      return $load_flip_btn.on("click", function() {
        return $panel.removeClass("flip");
      });
    };
    LoaderDialog.prototype.loadSelectedMap = function(selected_map) {
      var _this = this;
      return $.get(API.LOAD_MAP.f(selected_map)).done(function(map) {
        return _this.trigger("MAP_LOADED", _this, map);
      });
    };
    LoaderDialog.prototype.showSpinner = function() {
      this.spinner = $(Handlebars.partials["spinnerh"]);
      this.html.hide();
      return $(HAL.Dom.viewport).append(this.spinner);
    };
    LoaderDialog.prototype.hideSpinner = function() {
      return this.spinner.remove();
    };
    LoaderDialog.prototype.createNewBuffer = function() {
      return HAL.ImageUtils.createPixelBuffer(MapConfig.NN_COLUMNS, MapConfig.NN_ROWS, 0x00000000);
    };
    LoaderDialog.prototype.createMap = function(data) {
      var buff, cols, height, indX, indY, key, num_buffs, reg_height, reg_width, regions, regsOnX, regsOnY, rows, section_height, section_width, width, x, y,
        _this = this;
      cols = data.columns;
      rows = data.rows;
      width = cols * (MapConfig.TILE_WIDTH * 0.5);
      height = rows * MapConfig.TILE_HEIGHT;
      regsOnX = width / MapConfig.NN_COLUMNS;
      regsOnY = height / MapConfig.NN_ROWS;
      reg_width = MapConfig.NN_COLUMNS * (MapConfig.TILE_WIDTH * 0.5);
      reg_height = MapConfig.NN_ROWS * MapConfig.TILE_HEIGHT;
      section_width = reg_width / 3;
      section_height = reg_height / 3;
      num_buffs = Math.ceil(MapConfig.MAX_LAYERS / 2);
      regions = {};
      x = 0;
      y = 0;
      indY = 0;
      indX = 0;
      buff = (function() {
        var _i, _results;
        _results = [];
        for (buff = _i = 0; 0 <= num_buffs ? _i < num_buffs : _i > num_buffs; buff = 0 <= num_buffs ? ++_i : --_i) {
          _results.push(this.createNewBuffer());
        }
        return _results;
      }).call(this);
      while (x < width) {
        while (y < height) {
          key = indX + "_" + indY;
          regions[key] = {
            x: x,
            y: y,
            row: indY,
            col: indX,
            sections: [
              {
                x: 0,
                y: 0
              }, {
                x: section_width,
                y: 0
              }, {
                x: 2 * section_width,
                y: 0
              }, {
                x: 0,
                y: section_height
              }, {
                x: section_width,
                y: section_height
              }, {
                x: 2 * section_width,
                y: section_height
              }, {
                x: 0,
                y: 2 * section_height
              }, {
                x: section_width,
                y: 2 * section_height
              }, {
                x: 2 * section_width,
                y: 2 * section_height
              }
            ]
          };
          y += reg_height;
          indY++;
        }
        indY = 0;
        indX++;
        y = 0;
        x += reg_width;
      }
      data.region_width = reg_width;
      data.region_height = reg_height;
      data.width = width;
      data.height = height;
      data.regions = regions;
      return $.post(API.SAVE_MAP, data).done(function(data) {
        _this.trigger("MAP_CREATED", _this, data);
        console.debug(data);
        return _this.loadSelectedMap(data.name);
      });
    };
    LoaderDialog.prototype.loadMapList = function(defer) {
      var _this = this;
      return $.get(API.MAP_LIST).done(function(maplist) {
        var template;
        template = Handlebars.compile(html);
        _this.html = $(template({
          maplist: maplist
        }));
        return _this.epilogue();
      });
    };
    LoaderDialog.prototype.createMapRegions = function(rows, cols) {};
    return LoaderDialog;
  });

}).call(this);
