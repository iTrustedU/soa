"use strict"
html = 
"""
<div class="bottom">
<div class="left-section">
    <div class="map-box">
    </div>
    <div class="center-map-button"></div>
</div>
<div class="right-section">
    <img src="img/by-gold/premium-dinar.png" />
</div>
<div class="middle-section">
    <div class="bot-bar">   
        <div class="bot-bar-deco-middle">
            <div class="bot-bar-deco-name">
                <div class="friends-tab" id="friends-tab"></div>
                <div class="alliance-tab" id="aliance-tab"></div>
                <div class="bot-bar-deco-name-left"></div>
                <span class="bot-bar-deco-name-middle"></span>
                <div class="bot-bar-deco-name-right"></div>
                <div class="inventory-tab"></div>
                <div class="build-tab" id="build-tab"></div>
            </div>
        </div>
    </div>
    <div class="friends frame-bg">
        <img src="img/bottom-middle/friends-frame.png" class="img-frame" />
    </div>
    <div class="buildings frame-bg" id="buildings-list">
    </div>
    <div class="slider-bar">
        <div class="slider-bar-l"></div>
        <div class="slider-bar-r"></div>
        <div class="slider-bar-middle">
            <div class="slider"></div>
        </div>
    </div>
</div>
</div>
"""

$bldslist = 
"""
<img id="{{type}}" src="{{concat_construction_icon type disabled}}" class="img-frame bottom-menu-building">
"""

define ["jquery-ui", "jquery-perfect-scrollbar", "soa/hud/dialogs/BaseDialog"],

($, _, BaseDialog) ->

    class BottomMenu extends BaseDialog
        constructor: () ->
            super(html)

    BottomMenu::init = () ->
        @BuildTab = @html.find("#build-tab")
        @FriendsTab = @html.find("#friends-tab")
        @BuildingsList = @html.find("#buildings-list")
        @CityZoneViewButton = @html.find(".map-box")

        @BuildTab.click () =>
            @listBuildings()

        @FriendsTab.click () =>
            @html.find(".buildings").hide()
            @html.find(".friends").show()

        @CityZoneViewButton.click () ->
            SOA.toggleViews()

    BottomMenu::listBuildings = () ->
        @html.find(".friends").hide()
        @html.find(".buildings").show()
        amjad.empire.constructions.findAvailableConstructions()
        blds = amjad.empire.constructions.available_constructions
        ##console.debug blds
        @BuildingsList.empty()
        template = Handlebars.compile($bldslist);
        for k, construction of blds
            html = $(template(type: k, disabled: construction.type is "disabled"))
            html.data("construction", construction)
            html.unbind("click")
            if construction.type isnt "disabled"
                spr_uri = "#{construction.sprite}.png"
                do (spr_uri) ->
                    spr = PIXI.TextureCache[spr_uri]
                    grab_top =  if spr? then spr.frame.height else 0
                    grab_left = if spr? then spr.frame.width*0.5 else 0
                    html.draggable
                        revert: "invalid"
                        revertDuration: 0
                        helper: "clone"
                        appendTo: "body"
                        containment: "window"
                        cursorAt:
                            top: grab_top
                            left: grab_left
                        drag: (ev, ui) ->
                            pos = [
                                ev.pageX
                                ev.pageY
                            ]
                        start: (ev, ui) ->
                            ui.helper.css("width", "auto")
                            ui.helper.css("height", "auto")
                            ui.helper.css("opacity", 0.5)
                            ui.helper.attr("src", spr_uri)
            @BuildingsList.append(html)

    BottomMenu::onPrologue = (hud) ->
        $(hud).droppable
            accept: ".bottom-menu-building"
            drop: (ev, ui) ->
                if SOA.isInCityView()
                    constr = ui.draggable.data("construction")
                    amjad.empire.constructions.construct(constr)
                else
                    console.error "You can't construct building while in zone view"
        @html.find(".center-map-button").click () ->
            if SOA.isInZoneView()
                SOA.centerOnZone()

    return BottomMenu
        # $(hud).find(".bottom").append(@html)

            # Hud.on "SHOW_BOTTOM_MENU", () ->
            #     @show()
            #     Hud.trigger "LIST_CITY_BUILDINGS"
                
            # Hud.on "LIST_CITY_BUILDINGS", () ->
            #     $(".friends").hide()
            #     $(".buildings").show()
            # amjad.empire.constructions.findAvailableConstructions()
            # blds = amjad.empire.constructions.available_constructions
            # $BuildingsList.empty()
            # template = Handlebars.compile($bldslist);
            # for k, construction of blds
            #     html = $(template(construction))
            #     html.data("construction", construction)
            #     html.unbind("click")
            #     spr = Hal.asm.getSprite(construction.sprite)
            #     grab_top =  if spr? then spr.h else 0
            #     grab_left = if spr? then spr.w2 else 0
            #     if construction.type isnt "disabled"
            #         html.draggable
            #             revert: "invalid"
            #             revertDuration: 0
            #             helper: "clone"
            #             appendTo: "body"
            #             containment: "window"
            #             cursorAt:
            #                 top: grab_top
            #                 left: grab_left
            #             drag: (ev, ui) ->
            #                 pos = [
            #                     ev.pageX - Hal.dom.area.left,
            #                     ev.pageY - Hal.dom.area.top
            #                 ]
            #                 Hal.trigger "MOUSE_MOVE", pos
            #             start: (ev, ui) ->
            #                 ui.helper.css("width", "auto")
            #                 ui.helper.css("height", "auto")
            #                 ui.helper.css("opacity", 0.5)
            #     $BuildingsList.append(html)