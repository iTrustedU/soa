"use strict"

html = 
"""
    <div class="editing-bar">
        <i id="mode-place" class="fa fa-edit"></i>
        <i id="mode-erase" class="fa fa-times"></i>
        <i id="mode-default" class="fa fa-ban"></i>
        <i id="map-save" class="fa fa-save"></i>
        <i id="map-load" class="fa fa-refresh"></i>
        <i id="map-list" class="fa fa-list-alt"></i>
    </div>
"""

define ["EventDispatcher", "MapConfig"],

(EventDispatcher, MapConfig) ->

    class MainMenu extends EventDispatcher
        constructor: () ->
            super()
            @init()

    MainMenu::init = () ->
        @html = $(html)
        console.debug "ohdsadsa"

    return MainMenu