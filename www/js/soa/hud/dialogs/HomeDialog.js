(function() {
  "use strict";
  var $home_template, $upgrade_price, html,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  html = "<div class=\"home draggable\">\n    <div class=\"dialog-name\">\n        <span id=\"caravan-name\">H O M E</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div id=\"home-content\" class=\"home-content\">\n        {{{home_template}}}\n    </div>\n     <div class=\"home-building-info\">\n        <img src=\"img/bottom-middle/buildings/trade.png\" class=\"dialog-building-img\" />\n        <div id=\"upgrade-price\" class=\"upgrade-price\">\n            {{{upgrade_price}}}\n        </div>\n        <div id=\"upgrade-building\" class=\"upgrade-building\">Upgrade</div>\n        <div id=\"move-building\" class=\"upgrade-building move-button\">Move</div>\n    </div>\n</div>";

  $upgrade_price = "\n{{#each data.resources}}\n    <img src=\"img/top-resources/{{@key}}.png\">\n    <span>{{this}}</span>\n{{/each}}\n";

  $home_template = "<span><img src=\"img/dialog/training/level.png\" /></span>\n<span>{{data.level}}</span><br />\n<span class=\"color-text\">Population</span>\n<span>{{data.population}}</span>\n<span class=\"home-info-text\">\n    {{lang data.info_text}}\n</span>";

  define(["soa/hud/dialogs/BaseDialog"], function(BaseDialog) {
    var HomeDialog;
    HomeDialog = (function(_super) {
      __extends(HomeDialog, _super);

      function HomeDialog() {
        HomeDialog.__super__.constructor.call(this, html);
      }

      return HomeDialog;

    })(BaseDialog);
    HomeDialog.prototype.onPrologue = function() {
      this.setTemplates({
        "home_template": $home_template,
        "upgrade_template": $upgrade_price
      });
      this.upgradeable();
      this.moveable();
      this.closeable();
      return this.draggable();
    };
    HomeDialog.prototype.onEpilogue = function() {
      var _me_;
      this.on("PRE_SHOW", _me_ = function() {
        this.html.find(".dialog-building-img").attr("src", Handlebars.helpers.concat_dialog_construction("home"));
        return this.remove("PRE_SHOW", _me_);
      });
      return this.on("SHOW", function() {
        var datakey, homeData, upgradeResources,
          _this = this;
        homeData = {
          level: this.ctx.level,
          population: 120,
          info_text: 'home_info_text'
        };
        this.setData("home_template", {
          data: homeData
        });
        upgradeResources = amjad.empire.resources.getConstructionUpgradeResources('home', HAL.MathUtils.clamp(this.ctx.level + 1, 1, 10));
        this.setData("upgrade_price", {
          data: upgradeResources
        });
        datakey = "upgrade_price";
        this.compile("upgrade_template", datakey).then(function(data) {
          return _this.refresh("upgrade-price", data);
        });
        datakey = "home_template";
        this.compile("home_template", datakey).then(function(data) {
          return _this.refresh("home-content", data);
        });
        return this.ctx.on("UPGRADED", function() {
          return _this.trigger("SHOW");
        });
      });
    };
    return HomeDialog;
  });

}).call(this);
