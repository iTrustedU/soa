(function() {
  "use strict";
  define(["MinHeap"], function(MinHeap) {
    var PathFinder, compareFs;
    compareFs = function(a, b) {
      return a.f > b.f;
    };
    PathFinder = (function() {
      function PathFinder(map) {
        this.map = map;
        this.diagonal_cost = 1.4;
        this.straight_cost = 1.0;
      }

      PathFinder.prototype.tracePath = function(dest) {
        var out;
        out = [];
        while (dest.parent != null) {
          out.push(dest);
          dest = dest.parent;
        }
        return out;
      };

      /*
          @throws
      */


      PathFinder.prototype.find = function(from, to) {
        var closed, cost, cur, from_, g, h, neighs, open, open_map, out, t, tt, xDiff, yDiff, _i, _len;
        open = new MinHeap(null, compareFs);
        closed = {};
        open_map = {};
        if ((from == null) || (to == null)) {
          throw new Error("no start or end node given");
        }
        from_ = {
          row: from.row,
          col: from.col,
          id: from.id,
          f: 0,
          g: 0,
          h: 0,
          parent: null
        };
        open.push(from_);
        open_map[from.id] = from;
        while (open.size() > 0) {
          cur = open.pop();
          open_map[cur.id] = null;
          if (cur.row === to.row && cur.col === to.col) {
            out = this.tracePath(cur);
            out.push(from_);
            return out;
          }
          closed[cur.id] = cur;
          neighs = this.map.getNeighbours(this.map.getTileAt(cur.row, cur.col));
          for (_i = 0, _len = neighs.length; _i < _len; _i++) {
            t = neighs[_i];
            if ((t.row === cur.row) || (t.col === cur.col)) {
              g = this.straight_cost;
            } else {
              g = this.diagonal_cost;
            }
            if (closed[t.id] != null) {
              continue;
            }
            if (open_map[t.id] == null) {
              xDiff = Math.abs(to.position.x - t.position.x);
              yDiff = Math.abs(to.position.y - t.position.y);
              if (t.col % 2) {
                cost = this.straight_cost;
              } else {
                cost = this.diagonal_cost;
              }
              h = Math.sqrt((yDiff * yDiff) + (xDiff * xDiff)) * cost;
              open.push({
                h: h,
                row: t.row,
                id: t.id,
                col: t.col,
                g: cur.g + g,
                f: h + g,
                parent: cur
              });
              open_map[t.id] = t;
            } else {
              tt = open_map[t.id];
              if (cur.g + g < tt.g) {
                tt.g = cur.g + g;
                tt.f = tt.h + tt.g;
                tt.parent = cur;
              }
            }
          }
        }
      };

      return PathFinder;

    })();
    return PathFinder;
  });

}).call(this);
