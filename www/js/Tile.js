(function() {
  "use strict";
  define(["PIXI", "TileLayer", "AnimatedLayer", "MapConfig"], function(PIXI, TileLayer, AnimatedLayer, MapConfig) {
    var Tile, _id_;
    _id_ = 0;
    Tile = (function() {
      function Tile(row, col) {
        this.row = row;
        this.col = col;
        this.init();
        this.layers = {};
      }

      return Tile;

    })();
    Tile.prototype.init = function() {
      this.position = HAL.MathUtils.Vec2.from(0, 0);
      return this.id = _id_++;
    };
    Tile.prototype.addLayer = function(layer_container, meta) {
      var spr_layer;
      spr_layer = new TileLayer(this, meta);
      this.layers[meta.layer] = spr_layer;
      layer_container.addChild(spr_layer);
      return spr_layer;
    };
    Tile.prototype.removeLayer = function(layerZ) {
      var layer, parent;
      layer = this.layers[layerZ];
      if (layer == null) {
        throw new Error("No layer at " + layerZ);
      }
      parent = layer.parent;
      parent.removeChild(layer);
      delete this.layers[layerZ];
      return layer.id;
    };
    Tile.prototype.addAnimatedLayer = function(layer_container, meta) {
      var anim_layer;
      if (meta == null) {
        throw new Error("AnimatedLayer meta not provided");
      }
      anim_layer = new AnimatedLayer(this, meta);
      this.layers[meta.layer] = anim_layer;
      layer_container.addChild(anim_layer);
      return anim_layer;
    };
    Tile.prototype.copyLayer = function(layer, force) {
      var parent;
      layer.changeParentTile(this);
      this.layers[layer.z] = layer;
      parent = layer.parent;
      parent.removeChild(layer);
      parent.addChild(layer);
      return layer;
    };
    Tile.prototype.destroy = function() {
      return HAL.MathUtils.Vec2.release(this.position);
    };
    Tile.prototype.toString = function() {
      return "row: " + this.row + ", col: " + this.col;
    };
    return Tile;
  });

}).call(this);
