"use strict"

html = 
"""
<div class="live-stock">
    <div class="live-stock-content">
        <div class="live-stock-box">
            <div class="live-stock-tabs">
                <div class="chat-tab">
                    <div class="chat-tab-l"></div>
                    <span class="chat-tab-middle">Livestock</span>
                    <div class="chat-tab-r"></div>
                </div>
                <div class="chat-tab">
                    <div class="chat-tab-l"></div>
                    <span class="chat-tab-middle">Horses</span>
                    <div class="chat-tab-r"></div>
                </div>
                <div class="chat-tab">
                    <div class="chat-tab-l"></div>
                    <span class="chat-tab-middle">Camel</span>
                    <div class="chat-tab-r"></div>
                </div>
            </div>
            {{{livestock}}}
            {{{camels}}}
        </div>
        <div class="live-stock-tab">
            <div class="chat-tab-t"></div>
            <span class="chat-tab-middle-v">L<br />i<br />v<br />e<br /><br />s<br />t<br />o<br />c<br />k</span>
            <div class="chat-tab-b"></div>
        </div>
    </div>
</div>
"""

$camels = 
"""
<div class="camels">
    <div class="camels-button">Stats</div>
    <table>
        <tr>
            <th>Weekly Growth</th>
            <th>Food Rate</th>
            <th>Number</th>
        </tr>
        <tr>
            <td>{{weekly_growth}}<br /> Per Week</td>
            <td>{{food_rate}}<br /> Per Hour</td>
            <td>+ {{animal_type}} {{animals}}/1000</td>
        </tr>
    </table>
</div>
"""

$livestock = 
"""
<div class="livestock">
    <table>
        <tr>
            <th>Goods</th>
            <th>Weekly Growth</th>
            <th>Food Rate</th>
            <th>Animals</th>
        </tr>
        <tr>
            <td>
                {{goods_1}}<br />
                {{goods_2}}<br />
                {{goods_3}}<br />
                {{goods_4}}<br />
            </td>
            <td>{{weekly_growth}}<br /> Per Week</td>
            <td>{{food_rate}}<br /> Per Hour</td>
            <td>+ {{animal_type}} {{animals}}/1000</td>
        </tr>
    </table>
</div>
"""

define ["jquery"],

($) ->

    class LiveStock
    LiveStock::constructor = () ->
        camels =
            weekly_growth: 9
            food_rate: 199
            animals: 499
            animal_type: "Majaheem"

        livestock =
            weekly_growth: 9
            food_rate: 199
            animals: 499
            animal_type: "Sheep"
            goods_1: "Milk"
            goods_2: "Skin"
            goods_3: "Wool"
            goods_4: "Meet"


        template = Handlebars.compile($camels);
        cm = template(camels)

        template = Handlebars.compile($livestock);
        ls = template(livestock)


        template = Handlebars.compile(html)
        $LiveStock = $(template(
            camels: cm
            livestock: ls
        ))

        Hal.trigger "DOM_ADD", (domlayer) ->
            $(domlayer).append($LiveStock)
            $LiveStock.find(".live-stock-tab").click () ->
                $LiveStock.find(".live-stock-box").toggle "fast", () ->
                    ##console.debug "Animation completed"
                    # $LiveStock.find("").toggle()
                    content = $LiveStock.find(".live-stock-content")
                    if content.css("width") is "24px"
                        content.css("width", "375px")
                    else
                        content.css("width", "24px")
                    
        Hal.on "OPEN_LIVE_STOCK", (livestock) ->
            $LiveStock.show()