"use strict"

define [],

() ->

    class EventDispatcher
        constructor: () ->
            @__listeners__ = {}

    EventDispatcher::on = (type, clb) ->
        if type instanceof Array
            for t in type
                if not @__listeners__[t]?
                    @__listeners__[t] = []
                @__listeners__[t].push(clb)
        else
            if not @__listeners__[type]?
                @__listeners__[type] = []
            @__listeners__[type].push(clb)
            ind = @__listeners__[type].indexOf(clb)
        #console.debug "Added listener on #{@constructor.name}: TYPE = #{type}"
        return clb

    EventDispatcher::remove = (type, clb) ->
        if @__listeners__[type]?
            ind = @__listeners__[type].indexOf(clb)
            @__listeners__[type].splice(ind, 1) if ind isnt -1
            clb = null

    EventDispatcher::removeAll = (type) ->
        if type
            delete @__listeners__[type]
        else
            keys = Object.keys(@__listeners__)
            for key in keys
                @remove(key, list) for list in @__listeners__[key]

    EventDispatcher::trigger = (type, msg) ->
        if not @__listeners__[type]
            return
        for clb in @__listeners__[type]
            #console.debug "Triggerring listener on #{@constructor.name}: TYPE = #{type}"
            clb.call(@, msg, clb) if clb?

    return EventDispatcher
    