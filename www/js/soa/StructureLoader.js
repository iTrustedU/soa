(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/SOAEventDispatcher"], function(SOAEventDispatcher) {
    var ResourceLoader;
    ResourceLoader = (function(_super) {
      __extends(ResourceLoader, _super);

      function ResourceLoader(ShortMap) {
        this.ShortMap = ShortMap;
        ResourceLoader.__super__.constructor.call(this);
        this.initListeners();
        this.init();
        return;
      }

      return ResourceLoader;

    })(SOAEventDispatcher);
    ResourceLoader.prototype.init = function() {};
    ResourceLoader.prototype.initListeners = function() {};
    ResourceLoader.prototype.load = function(meta) {
      var entry_tr, fn, is_arr, key, keys, prop, res, val, _ref;
      keys = Object.keys(meta);
      _ref = this.ShortMap;
      for (key in _ref) {
        val = _ref[key];
        prop = meta[key];
        entry_tr = val;
        is_arr = val instanceof Array && val.length === 2;
        if (is_arr && (prop != null)) {
          entry_tr = val[0];
          fn = val[1];
          res = fn.call(this, prop, entry_tr, key);
          if (res != null) {
            this[entry_tr] = res;
          }
        } else if (typeof entry_tr === "function") {
          res = entry_tr.call(this, meta[key]);
          if (res != null) {
            this[key] = res;
          }
        } else if (key === "*" && is_arr) {
          if (typeof val[1] === "function") {
            fn = val[1];
            for (key in meta) {
              val = meta[key];
              res = fn.call(this, val, key);
              if (res != null) {
                this[key] = res;
              }
            }
          }
        } else if (key === "*" && val === "*") {
          for (key in meta) {
            val = meta[key];
            if (val != null) {
              this[key] = val;
            }
          }
        } else if (prop != null) {
          if (prop != null) {
            this[entry_tr] = prop;
          }
        }
      }
      this.trigger("DONE_LOADING", this);
      return this;
    };
    return ResourceLoader;
  });

}).call(this);
