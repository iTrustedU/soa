(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher", "MapConfig", "Geometry", "Vec2", "MathUtils"], function(EventDispatcher, MapConfig, Geometry, Vec2, MathUtils) {
    var API, INCIDENCE_MATRIX, RegionManager;
    API = {
      LOAD_REGION_BUFFERS: "/map/{0}/region/{1}/{2}/load"
    };
    INCIDENCE_MATRIX = [[[-1, 1], [0, 1], [1, 1]], [[-1, 0], [0, 0], [1, 0]], [[-1, -1], [0, -1], [1, -1]]];
    RegionManager = (function(_super) {
      __extends(RegionManager, _super);

      function RegionManager(map_manager) {
        this.map_manager = map_manager;
        RegionManager.__super__.constructor.call(this);
        this.init();
      }

      return RegionManager;

    })(EventDispatcher);
    RegionManager.prototype.findEnvelopeSections = function() {
      var ccol, center, crow, maxX, maxY, minX, minY, node, out, rows, sect, _i, _j, _len, _len1, _ref;
      center = this.getGlobalCenterSection();
      _ref = this.getSectionGlobalCoordinates(center.x + center.parent_region.x, center.parent_region.y + center.y), crow = _ref[0], ccol = _ref[1];
      out = [];
      console.debug("Center: ", crow, ccol);
      minX = minY = Number.MAX_VALUE;
      maxX = maxY = Number.MIN_VALUE;
      for (_i = 0, _len = INCIDENCE_MATRIX.length; _i < _len; _i++) {
        rows = INCIDENCE_MATRIX[_i];
        for (_j = 0, _len1 = rows.length; _j < _len1; _j++) {
          node = rows[_j];
          sect = this.getSectionAt(node[0] + crow, node[1] + ccol);
          if (sect == null) {
            continue;
          }
          minX = Math.min(minX, sect.parent_region.x + sect.x);
          minY = Math.min(minY, sect.parent_region.y + sect.y);
          maxX = Math.max(maxX, sect.parent_region.x + sect.x);
          maxY = Math.max(maxY, sect.parent_region.y + sect.y);
          out.push(sect);
        }
      }
      this.envelope.sections = out;
      this.envelope.bbox.x = minX;
      this.envelope.bbox.y = minY;
      this.envelope.bbox.width = (maxX - minX) + this.swidth;
      this.envelope.bbox.height = (maxY - minY) + this.sheight;
      this.loadEnvelopeBuffers();
      return out;
    };
    RegionManager.prototype.getGlobalCenterSection = function() {
      var cx, cy;
      cx = this.map_manager.hit_area.x + this.map_manager.hit_area.width * 0.5;
      cy = this.map_manager.hit_area.y + this.map_manager.hit_area.height * 0.5;
      return this.getSectionAtGlobalPosition(cx, cy);
    };
    RegionManager.prototype.getRegionAtGlobalPosition = function(x, y) {
      var rx, ry;
      rx = Math.floor(x / this.rwidth);
      ry = Math.floor(y / this.rheight);
      if (rx < 0 || ry < 0) {
        return null;
      }
      return this.regions[ry][rx];
    };
    RegionManager.prototype.getSectionAtGlobalPosition = function(x, y) {
      var region, sx, sy, _ref;
      region = this.getRegionAtGlobalPosition(x, y);
      if (region == null) {
        return null;
      }
      _ref = this.getSectionLocalCoordinates(x, y), sx = _ref[0], sy = _ref[1];
      return region.sections[sx + sy * 3];
    };
    RegionManager.prototype.getSectionLocalCoordinates = function(x, y) {
      var region, sx, sy;
      region = this.getRegionAtGlobalPosition(x, y);
      sx = Math.floor((x - region.x) / this.swidth);
      sy = Math.floor((y - region.y) / this.sheight);
      return [sx, sy, region];
    };
    RegionManager.prototype.getSectionGlobalCoordinates = function(x, y) {
      var col, rcol, row, rrow, section, _ref;
      section = this.getSectionAtGlobalPosition(x, y);
      rrow = section.parent_region.row;
      rcol = section.parent_region.col;
      _ref = this.getSectionLocalCoordinates(x, y), row = _ref[0], col = _ref[1];
      console.debug("region", rrow, rcol);
      console.debug("section", col, row);
      return [rcol * 3 + row, rrow * 3 + col];
    };
    RegionManager.prototype.getSectionAt = function(row, col) {
      var px, py, _ref;
      _ref = [row * this.sheight, col * this.swidth], px = _ref[0], py = _ref[1];
      return this.getSectionAtGlobalPosition(px, py);
    };
    RegionManager.prototype.getTileAtSectionPosition = function(x, y) {
      var region, sect;
      region = this.getRegionAtGlobalPosition(x, y);
      sect = this.getSectionAtGlobalPosition(x, y);
      return this.map_manager.getTileAtPosition(x - (region.x + sect.x), y - (region.y + sect.y));
    };
    RegionManager.prototype.init = function() {
      var _this = this;
      this.map_manager.on("TILES_INITIALIZED", function(map_meta) {
        return _this.load(map_meta);
      });
      this.map_manager.scene.on("MOUSE_WORLD_CHANGED", function(x, y) {
        var region, rx, ry, sect_pos, sx, sy;
        rx = Math.floor(x / _this.rwidth);
        ry = Math.floor(y / _this.rheight);
        region = _this.regions[rx][ry];
        sx = Math.floor((x - region.x) / _this.swidth);
        sy = Math.floor((y - region.y) / _this.sheight);
        _this.trigger("MOUSE_OVER_REGION", _this, rx, ry, sx, sy);
        return sect_pos = _this.getTileAtSectionPosition(x, y);
      });
      return this.map_manager.on("HIT_AREA_CHANGED", function(area) {
        var new_region, rx, ry, x, y;
        x = area.x + area.width;
        y = area.y + area.height;
        rx = Math.floor(x / _this.rwidth);
        ry = Math.floor(y / _this.rheight);
        new_region = _this.regions[rx][ry];
        if (!Geometry.rectangleContainsRectangle(area, _this.envelope.bbox)) {
          console.debug("Hahah");
          _this.envelope.row = rx;
          _this.envelope.col = ry;
          _this.envelope.ref = new_region;
          _this.envelope.sections = _this.findEnvelopeSections();
          return _this.trigger("REGION_CHANGED", _this, rx, ry);
        }
      });
    };
    RegionManager.prototype.drawRegionBorders = function() {
      var col, color, colors, g, i, reg, row, rows, sh, sw, _i, _j, _k, _l, _len, _len1, _ref;
      g = new PIXI.Graphics();
      colors = [0x00CC0F, 0x0F0C0D, 0xFEFA00, 0x124C0F, 0xC0FC33, 0xF0CA94, 0xCC33FF, 0x22029F, 0x90CF03];
      i = 0;
      _ref = this.regions;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        rows = _ref[_i];
        for (_j = 0, _len1 = rows.length; _j < _len1; _j++) {
          reg = rows[_j];
          g.lineStyle(4, 0x000000, 1);
          sw = this.rwidth / 3;
          sh = this.rheight / 3;
          for (row = _k = 0; _k < 3; row = ++_k) {
            for (col = _l = 0; _l <= 3; col = ++_l) {
              color = colors[i % 9];
              g.lineStyle(1, color, 1);
              g.drawRect(reg.x + (col * sw), reg.y + (row * sh), sw, sh);
              ++i;
            }
          }
          i = 0;
        }
      }
      this.map_manager.scene.world.addChild(g);
    };
    RegionManager.prototype.writeTileLayer = function(buffer, x, y, layer, value) {
      var colors, colors_en;
      if ((!0 < layer && layer < 2)) {
        throw new Error("Layer out of bounds");
      }
      if (value > ((2 << 11) - 1)) {
        throw new Error("Value " + val + " exceeds the maximum allowed of " + (2 << 11 - 1));
      }
      colors_en = MathUtils.decode12.apply(MathUtils, MathUtils.encode8(HAL.ImageUtils.getPixelAtBuffer(buffer, x, y)));
      colors_en[layer] = value;
      colors = MathUtils.decode8(MathUtils.encode12.apply(MathUtils, colors_en));
      return buffer = HAL.ImageUtils.putPixelToBuffer(buffer, x, y, colors);
    };
    RegionManager.prototype.readTileLayer = function(buffer, x, y, layer) {
      var out, vals;
      if ((!0 < layer && layer < 2)) {
        throw new Error("Layer out of bounds");
      }
      out = HAL.ImageUtils.getPixelAtBuffer(buffer, x, y);
      vals = MathUtils.decode12(MathUtils.encode8.apply(MathUtils, out));
      return vals[layer];
    };
    RegionManager.prototype.loadEnvelopeBuffers = function() {
      var buffer, section, _i, _len, _ref, _results;
      _ref = this.envelope.sections;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        section = _ref[_i];
        if (section.loaded) {
          continue;
        }
        section.loaded = true;
        _results.push(section.buffers = (function() {
          var _j, _len1, _ref1, _results1;
          _ref1 = section.buffers;
          _results1 = [];
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            buffer = _ref1[_j];
            _results1.push(HAL.ImageUtils.base64ToPixelBuffer(buffer));
          }
          return _results1;
        })());
      }
      return _results;
    };
    RegionManager.prototype.load = function(map) {
      var key, region, rows, section, _base, _i, _j, _k, _len, _len1, _len2, _name, _ref, _ref1, _ref2;
      this.regions = [];
      this.rwidth = map.region_width;
      this.rheight = map.region_height;
      this.swidth = this.rwidth / 3;
      this.sheight = this.rheight / 3;
      this.numbuffs = Math.ceil(MapConfig.MAX_LAYERS / 2);
      _ref = this.map_manager.map_meta.regions;
      for (key in _ref) {
        region = _ref[key];
        rows = (_base = this.regions)[_name = region.row] != null ? (_base = this.regions)[_name = region.row] : _base[_name] = [];
        if (rows[region.col] == null) {
          rows[region.col] = region;
        }
      }
      this.columns = map.width / this.rwidth;
      this.rows = map.height / this.rheight;
      this.envelope = {
        row: 0,
        col: 0,
        ref: this.regions[0][0],
        tile_over: null,
        sections: [],
        pivot: new PIXI.Point(0, 0),
        bbox: new PIXI.Rectangle(0, 0, 0, 0)
      };
      _ref1 = this.regions;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        rows = _ref1[_i];
        for (_j = 0, _len1 = rows.length; _j < _len1; _j++) {
          region = rows[_j];
          _ref2 = region.sections;
          for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
            section = _ref2[_k];
            section.parent_region = region;
          }
        }
      }
      this.findEnvelopeSections();
      this.drawRegionBorders();
      return this.trigger("LOADED");
    };
    return RegionManager;
  });

}).call(this);
