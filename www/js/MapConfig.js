(function() {
  "use strict";
  define([], function() {
    var MAP_CONFIG;
    MAP_CONFIG = {
      TILE_HEIGHT: 64,
      TILE_WIDTH: 128,
      DEFAULT_ANIMATED_LAYER: 6,
      OVER_NEW_TILE_LAYER: 1,
      GRID_LINE_COLOR: 0xAAAAAA,
      GRID_LINE_THICKNESS: 1,
      GRID_LINE_ALPHA: 1,
      TILE_OVER_COLOR: 0xCC0FF02,
      MAX_LAYERS: 7,
      SELECTION_RECT_COLOR: 0xE32F17,
      NN_COLUMNS: 150,
      NN_ROWS: 150
    };
    return MAP_CONFIG;
  });

}).call(this);
