UserModel = require "../../models/soa/User"

module.exports = (app) ->
	app.post('/login', (req, res, next) ->
		if req.body.rememberme?
			req.session.cookie.maxAge = 30*24*60*60*1000 #30 dana
		else
			req.session.cookie.expires = false
	
		passport.authenticate("local",
			(err, user, info) ->
				if err
					return next(err)
				if not user
					req.session.messages = [info.message]
					return res.send(JSON.stringify(info))
				req.logIn(user, (err) ->
					if err
						return next(err)
					return res.send(JSON.stringify(info))
				)
		)(req, res, next)
	)

	app.post('/register', (req, res, next) ->
		body = req.body
		user = new UserModel(
			username: body.username
			firstName: body.firstName
			lastName: body.lastName
			email: body.email
			password: body.password
			faction: body.faction
			architecture: body.architecture
		)
		
		user.save (err) ->
			console.dir err
			if err?
				res
				.status(422)
				.send(err.message)
			else
				res
				.status(200)
				.send("Successfuly registered")
	)




# Logic pipes

# has certain level
# ->
# 	has certain resources
# 	->
# 		has certain constructions
