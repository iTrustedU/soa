"use strict"

define ["soa/constructions/Construction"],

(Construction) ->

    class EncampedPOI extends Construction
    
    EncampedPOI::epilogue = () ->
        console.debug "POI: #{@c_type} loaded"
        super()

    EncampedPOI::addToMap = () ->
        spr = "assets/sprites/amjad/poi/encampment_established"
        @layer = SOA.Zone.map.addLayerSpriteToTile(@x, @y, 5, spr)

    return EncampedPOI