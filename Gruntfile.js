(function() {
  module.exports = function(grunt) {
    var all_sprites, config, fs, getAllSprites, is_json, is_sprite, is_spritesheet, is_win, log, path, saveSprites, wrench;
    fs = require("fs");
    path = require("path");
    wrench = require("wrench");
    log = console.log;
    /*
        Meta information and settings
    */

    config = {
      pub_dir: ".",
      js_dir: "public/js" + path.sep,
      coffee_dir: "public/coffee" + path.sep,
      cur_dir: process.cwd(),
      sprite_dir: "public/assets/sprites/",
      sprite_list: "public/assets/sprites.list",
      backend_dir: "./backend/"
    };
    is_win = !!process.platform.match(/^win/);
    is_json = /^.*\.[json]+$/;
    is_sprite = is_spritesheet = /^.*\.[png|jpg]+$/;
    all_sprites = null;
    log("Is windows: " + is_win);
    log("Platform " + process.platform);
    grunt.loadNpmTasks("grunt-contrib-coffee");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-connect");
    grunt.initConfig({
      pkg: grunt.file.readJSON("package.json"),
      coffee: {
        glob_all: {
          expand: true,
          cwd: "" + config.coffee_dir,
          src: ["**/*.coffee"],
          dest: "" + config.js_dir,
          ext: ".js"
        },
        all: {
          expand: true,
          flatten: false,
          cwd: "" + config.coffee_dir,
          src: ['**/*.coffee'],
          dest: "" + config.js_dir,
          ext: ".js"
        },
        server: {
          expand: true,
          flatten: false,
          cwd: '.',
          src: ["./server.coffee"],
          dest: '.',
          ext: '.js'
        },
        backend: {
          expand: true,
          flatten: false,
          cwd: '.',
          src: '*.coffee',
          dest: '.',
          ext: '.js'
        },
        configs: {
          expand: true,
          flatten: false,
          cwd: '.',
          src: ["./config/**/*.coffee"],
          dest: '.',
          ext: '.js'
        }
      },
      watch: {
        coffee: {
          files: ["" + config.coffee_dir + "/**/*.coffee"],
          tasks: ["coffee:glob_all"],
          options: {
            nospawn: true,
            livereload: false
          }
        },
        coffee_server: {
          files: ["./server.coffee"],
          tasks: ['coffee:server'],
          options: {
            nospawn: true,
            livereload: true
          }
        },
        coffee_backend: {
          files: ["" + config.backend_dir + "**/*.coffee"],
          tasks: ['coffee:backend'],
          options: {
            nospawn: true,
            livereload: true
          }
        },
        coffee_configs: {
          files: ["./config/**/*.coffee"],
          tasks: ['coffee:configs'],
          options: {
            nospawn: true,
            livereload: true
          }
        }
      },
      connect: {
        server: {
          options: {
            keepalive: false,
            port: 9000,
            base: config.pub_dir,
            debug: false
          }
        }
      }
    });
    grunt.event.on("watch", function(action, filepath) {
      log(filepath.red);
      filepath = filepath.replace(grunt.config("coffee.glob_all.cwd"), "");
      log(filepath.yellow);
      return grunt.config("coffee.glob_all.src", [filepath]);
    });
    grunt.registerTask("serve", ["watch"]);
    grunt.registerTask("compile", "Compiling Halal", function() {
      var proc, spawn;
      spawn = require("child_process").spawn;
      proc = spawn("r.js", ["-o", "build.js"]);
      console.log("Compiling Halal".yellow);
      proc.stdout.setEncoding("utf8");
      proc.stderr.setEncoding("utf8");
      proc.stdout.on("data", function(data) {
        return console.log(data.yellow);
      });
      proc.stderr.on("data", function(data) {
        return console.log(data.red);
      });
      proc.on("exit", function(retcode) {
        console.log(retcode);
        return console.log("Done compiling".green);
      });
      return proc.on("close", function(retcode) {
        console.log(retcode);
        return console.log("Done compiling".green);
      });
    });
    saveSprites = function(sprites) {
      sprites = sprites.map(function(x) {
        return "assets/sprites/" + x;
      });
      return fs.writeFileSync(config.sprite_list, sprites.join().replace(/,/g, "\n"));
    };
    all_sprites = (getAllSprites = function() {
      var out;
      out = wrench.readdirSyncRecursive(config.sprite_dir);
      out = out.filter(function(x) {
        return is_sprite.test(x) || is_json.test(x);
      });
      if (is_win) {
        out = out.map(function(x) {
          return x.replace(/\\/g, "\/");
        });
      }
      return out;
    })();
    return saveSprites(all_sprites);
  };

}).call(this);
