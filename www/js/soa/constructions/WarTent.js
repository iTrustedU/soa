(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/constructions/Construction"], function(Construction) {
    var WarTent, _ref;
    WarTent = (function(_super) {
      __extends(WarTent, _super);

      function WarTent() {
        _ref = WarTent.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      return WarTent;

    })(Construction);
    WarTent.prototype.init = function() {
      this.units = [];
      return this.commanders = [];
    };
    WarTent.prototype.epilogue = function() {
      var cmndr, un, _i, _j, _len, _len1, _ref1, _ref2, _results;
      _ref1 = amjad.empire.units;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        un = _ref1[_i];
        this.addUnit(un);
      }
      _ref2 = amjad.empire.heroes.commanders;
      _results = [];
      for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
        cmndr = _ref2[_j];
        _results.push(this.addCommander(cmndr));
      }
      return _results;
    };
    WarTent.prototype.addUnit = function(un) {
      var f;
      f = (this.units.filter(function(unit) {
        return un.type === unit.type;
      }))[0];
      if (f != null) {
        f.amount += un.amount;
      } else {
        this.units.push(un);
      }
      return this.trigger("REFRESH_UNITS");
    };
    WarTent.prototype.removeUnit = function(type, amnt) {
      var f, ind;
      f = (this.units.filter(function(unit) {
        return unit.type === type;
      }))[0];
      if (f == null) {
        throw new Error("You can't remove the unit which you don't even have");
      }
      f["amount"] -= amnt;
      if (f["amount"] <= 0) {
        ind = this.units.indexOf(f);
        this.units.splice(ind, 1);
      }
      return this.trigger("REFRESH_UNITS");
    };
    WarTent.prototype.addCommander = function(comm) {
      if (cmndr.isInOwnCity()) {
        return this.commanders.push(comm);
      }
    };
    return WarTent;
  });

}).call(this);
