"use strict"

html =
"""
<div class="home draggable">
    <div class="dialog-name">
        <span id="caravan-name">H O M E</span>
        <div class="close-button"></div>
    </div>
    <div id="home-content" class="home-content">
        {{{home_template}}}
    </div>
     <div class="home-building-info">
        <img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img" />
        <div id="upgrade-price" class="upgrade-price">
            {{{upgrade_price}}}
        </div>
        <div id="upgrade-building" class="upgrade-building">Upgrade</div>
        <div id="move-building" class="upgrade-building move-button">Move</div>
    </div>
</div>
"""

$upgrade_price = """

        {{#each data.resources}}
            <img src="img/top-resources/{{@key}}.png">
            <span>{{this}}</span>
        {{/each}}
        
        """

$home_template = """
        <span><img src="img/dialog/training/level.png" /></span>
        <span>{{data.level}}</span><br />
        <span class="color-text">Population</span>
        <span>{{data.population}}</span>
        <span class="home-info-text">
            {{lang data.info_text}}
        </span>
"""

define ["soa/hud/dialogs/BaseDialog"],

(BaseDialog) ->  

    class HomeDialog extends BaseDialog
        constructor: () ->
            super(html)
    
    HomeDialog::onPrologue = () ->
        @setTemplates
            "home_template" : $home_template
            "upgrade_template" : $upgrade_price

        @upgradeable()
        @moveable()
        @closeable()
        @draggable()

    HomeDialog::onEpilogue = () ->
        @on "PRE_SHOW", _me_ = () ->
            @html
            .find(".dialog-building-img")
            .attr("src", Handlebars.helpers.concat_dialog_construction("home"))
            @remove "PRE_SHOW", _me_

        @on "SHOW", () ->
            #load home data
            homeData = 
                level : @ctx.level
                population :  120
                info_text : 'home_info_text'
            @setData "home_template", {data : homeData}

            #load needed resources for upgrade
            upgradeResources = amjad.empire.resources.getConstructionUpgradeResources('home', HAL.MathUtils.clamp(@ctx.level+1, 1, 10))
            @setData "upgrade_price", {data : upgradeResources}
            
            #compile templates
            datakey = "upgrade_price"
            @compile("upgrade_template", datakey).then (data) =>  
                @refresh "upgrade-price",data         

            datakey = "home_template"
            @compile("home_template", datakey).then (data) =>  
                @refresh "home-content",data         

            #refresh data on upgrade finish
            @ctx.on "UPGRADED", () =>
                @trigger "SHOW"   
    return HomeDialog