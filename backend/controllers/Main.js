(function() {
  var CommanderModel;

  CommanderModel = require("../models/soa/heroes/Commander");

  module.exports = function(app) {
    app.get("/", function(req, res) {
      return res.render("./layouts/main");
    });
    app.get("/editor", function(req, res) {
      var seeder;
      seeder = require("../models/editor/TileSeed");
      seeder(app);
      return res.render("main", {
        editor: true
      });
    });
    return app.get("/empire/new", function(req, res) {});
  };

}).call(this);
