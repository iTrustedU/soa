(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["soa/Rest", "soa/StructureLoader"], function(Rest, StructureLoader) {
    var Settlement, ShortMap;
    ShortMap = {
      "*": "*"
    };
    Settlement = (function(_super) {
      __extends(Settlement, _super);

      function Settlement() {
        Settlement.__super__.constructor.call(this, ShortMap);
        this.neighs = [];
        return this;
      }

      return Settlement;

    })(StructureLoader);
    Settlement.prototype.load = function(meta) {
      Settlement.__super__.load.call(this, meta);
      return this;
    };
    Settlement.prototype.initListeners = function() {
      var _this = this;
      this.on("DONE_LOADING", function() {});
      return HAL.on("ZONE_MAP_LOADED", function() {
        _this.layer = amjad.putCityOnZone(_this.x, _this.y, "city", "amjad/constructions/" + amjad.user.architecture + "/");
        _this.neighs = _this.findNeighbours();
        _this.layer.onLeftClick = function() {
          return SOA.goToCityView();
        };
        return HAL.trigger("MY_SETTLEMENT_LOADED", _this);
      });
    };
    Settlement.prototype.putHeroNearby = function(hero_texture) {
      var tile;
      tile = this.neighs.pop();
      return SOA.Zone.map.addAnimatedLayer(tile.row, tile.col, hero_texture);
    };
    Settlement.prototype.isInNeighbourhood = function(row, col) {
      var neigh, _i, _len, _ref;
      _ref = this.findNeighbours();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        neigh = _ref[_i];
        if (neigh.row === row && neigh.col === col) {
          return true;
        }
      }
      return false;
    };
    Settlement.prototype.findNeighbours = function() {
      var dupes, le, lw, out, pivot, se, sew, size, sw, swe, t, _i, _len;
      size = ~~((this.layer.width / 128) + 1);
      out = [];
      pivot = SOA.Zone.map.getTile(this.x + 1, this.y);
      sw = SOA.Zone.map.findInDirectionOf(pivot, "northwest", size);
      se = SOA.Zone.map.findInDirectionOf(pivot, "northeast", size);
      lw = sw[sw.length - 1];
      le = se[se.length - 1];
      swe = SOA.Zone.map.findInDirectionOf(lw, "northeast", size);
      sew = SOA.Zone.map.findInDirectionOf(le, "northwest", size);
      out = out.concat(sw).concat(swe).concat(sew).concat(se);
      dupes = [];
      for (_i = 0, _len = out.length; _i < _len; _i++) {
        t = out[_i];
        if (dupes[t.id] == null) {
          dupes[t.id] = t;
          out.push(t);
        }
      }
      return out;
    };
    return Settlement;
  });

}).call(this);
